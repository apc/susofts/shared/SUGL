#include "Translator.hpp"

#include <algorithm>
#include <vector>

#include <QActionGroup>
#include <QCoreApplication>
#include <QDir>
#include <QTranslator>

class Translator::_Translator_pimpl
{
public:
	_Translator_pimpl() : translator(std::make_unique<QTranslator>()), translatorQt(std::make_unique<QTranslator>()) {}

	/** Prefix for the file names */
	std::string prefix;
	/** List of loaded languages */
	std::unordered_set<std::string> languages;
	/** Current language */
	std::string currentLanguage;
	/** Translator for the application */
	std::unique_ptr<QTranslator> translator;
	/** Translator for Qt */
	std::unique_ptr<QTranslator> translatorQt;
};

Translator &Translator::getTranslator()
{
	static Translator _instance;
	return _instance;
}

Translator::~Translator() = default;

void Translator::setPrefix(const std::string &prefix)
{
	_pimpl->prefix = prefix;
}

const std::string &Translator::getPrefix() const noexcept
{
	return _pimpl->prefix;
}

const std::string &Translator::getcurrentLanguage() const noexcept
{
	return _pimpl->currentLanguage;
}

void Translator::loadLanguages()
{
	clear();
	QStringList filenames = QDir(":/translations/").entryList(QStringList(QString::fromStdString(_pimpl->prefix) + '*'));
	for (auto &f : filenames)
		_pimpl->languages.insert(f.remove(QString::fromStdString(_pimpl->prefix)).toStdString());
}

const std::unordered_set<std::string> &Translator::getLanguages() const noexcept
{
	return _pimpl->languages;
}

bool Translator::has(const std::string &key) const
{
	return _pimpl->languages.find(key) != std::end(_pimpl->languages);
}

void Translator::clear()
{
	QCoreApplication::removeTranslator(_pimpl->translator.get());
	QCoreApplication::removeTranslator(_pimpl->translatorQt.get());
	_pimpl->currentLanguage.clear();
	_pimpl->languages.clear();
}

size_t Translator::size() const noexcept
{
	return _pimpl->languages.size();
}

bool Translator::empty() const noexcept
{
	return _pimpl->languages.empty();
}

QActionGroup *Translator::buildActions(QObject *parent)
{
	std::vector<std::string> languages(std::cbegin(_pimpl->languages), std::cend(_pimpl->languages));
	std::sort(std::begin(languages), std::end(languages));
	auto group = new QActionGroup(parent);

	for (const auto &lang : languages)
	{
		auto *action = new QAction(QString::fromStdString(lang), parent);
		action->setCheckable(true);
		action->setChecked(_pimpl->currentLanguage == lang);
		group->addAction(action);
	}
	return group;
}

Translator::Translator(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_Translator_pimpl>())
{
}

void Translator::switchTranslator(QTranslator *translator, const std::string &filename)
{
	// remove the old translator
	QCoreApplication::removeTranslator(translator);

	// load the new translator
	if (translator->load(QString::fromStdString(filename)))
		QCoreApplication::installTranslator(translator);
}

void Translator::changeLanguage(const std::string &code)
{
	if (code == _pimpl->currentLanguage || !has(code))
		return;
	_pimpl->currentLanguage = code;
	switchTranslator(_pimpl->translator.get(), ":/translations/" + _pimpl->prefix + code);
	switchTranslator(_pimpl->translatorQt.get(), "translations/qt_" + code + ".qm");
	emit languageChanged(_pimpl->currentLanguage);
}
