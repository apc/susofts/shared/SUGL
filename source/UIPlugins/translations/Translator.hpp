/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TRANSLATOR_HPP
#define TRANSLATOR_HPP

#include <memory>
#include <unordered_set>

#include <QObject>

#include "PluginsGlobal.hpp"

class QActionGroup;
class QTranslator;

/**
 * Class that manages the translations of the program.
 *
 * This class is a singleton, you can access it from its static method: `Translator::getTranslator()`.
 *
 * It is assumed that the software uses the function `create_translations` from the CMake function of the file `resources.cmake` of the SUGL.
 * Thus, the translations should be part of a loaded Qt resource file, under the path `:/translations/`, and in the folder "translations/" for Qt files.
 *
 * This class can automatically load all the languages from the loaded Qt resources. To do so, you need to give the prefix used in the translation file names.
 * I.E. if you have `:/translations/surveypad_en[.qm]`, the prefix is `surveypad_`. See the methods getPrefix() and setPrefix().
 *
 * Once the languages are loaded, you can generate QActions automatically, to integrate them in a QMenu, thanks to the method buildActions().
 *
 * Finally, it is possible to change the language with changeLanguage(). Note that this will immediately emit the signal languageChanged(). Then, Qt mechanism
 * will call the function [QWidget::changeEvent()](https://doc.qt.io/qt-5/qwidget.html#changeEvent) on all the widget, with an event of type
 * [QEvent::LanguageChange](https://doc.qt.io/qt-5/qevent.html#Type-enum). It is your responsibility to override
 * this method and handle it.
 */
class SUGL_SHARED_EXPORT Translator : public QObject
{
	Q_OBJECT

public:
	/** @return the instance of the translator (singleton). */
	static Translator &getTranslator();

	~Translator() override;
	Translator(const Translator &) = delete; // can't copy
	Translator(const Translator &&) = delete;
	Translator &operator=(const Translator &) = delete;
	Translator &operator=(const Translator &&) = delete;

	/**
	 * Change the prefix of the QM translation files.
	 *
	 * The prefix is the part of the file name that preceed the language code. I.E. in ":/translations/surveypad_en.qm", the prefix is "surveypad_".
	 * @see getPrefix()
	 */
	void setPrefix(const std::string &prefix);
	/**
	 * @return the prefix of the QM translation files.
	 * @see setPrefix()
	 */
	const std::string &getPrefix() const noexcept;
	/**
	 * @return the code of the currently used language.
	 * @see changeLanguage()
	 */
	const std::string &getcurrentLanguage() const noexcept;
	/**
	 * Load all the languages stored in the loaded resources.
	 *
	 * It first clear the loaded languages. Then, it tries to find any translation files from Qt resources.
	 * The search is done in ":/translations/", and it looks for any files that start with the given prefix.
	 * @see setPrefix(), clear()
	 */
	void loadLanguages();
	/**
	 * @return all the loaded languages.
	 * @warning The object returned is a set, it is not sorted.
	 * @see loadLanguages(), has()
	 */
	const std::unordered_set<std::string> &getLanguages() const noexcept;
	/** @return true if the language is loaded. */
	bool has(const std::string &key) const;
	/** Remove all values and remove the current language from Qt. */
	void clear();
	/** Return the number of currently loaded languages. */
	size_t size() const noexcept;
	/** Tells if the container is empty (size == 0). */
	bool empty() const noexcept;
	/**
	 * Builds a QActionGroup containing all the loaded languages.
	 * The built QActionGroup contains all languages as actions. It is sorted. You can add them directly in a menu.
	 * @param parent the parent of the returned value. `nullptr` by default (you have to manually change its parent afterwards or you may have a memory leak).
	 * @return a list of QActions, sorted, in a QActionGroup.
	 */
	QActionGroup *buildActions(QObject *parent = nullptr);

public slots:
	/**
	 * Changes the currently used language.
	 * If the given language is not loaded, does nothing.
	 * @see languageChanged(), loadLanguages(), getcurrentLanguage()
	 */
	void changeLanguage(const std::string &code);

signals:
	/**
	 * Signal emitted each time the language changes.
	 * @see changeLanguage()
	 */
	void languageChanged(const std::string &code);

private:
	/** Private constructor: singleton. */
	Translator(QObject *parent = nullptr);
	/** Remove translator and add a new one from the given filename. */
	void switchTranslator(QTranslator *translator, const std::string &filename);

private:
	/** pimpl */
	class _Translator_pimpl;
	std::unique_ptr<_Translator_pimpl> _pimpl;
};

#endif // TRANSLATOR_HPP
