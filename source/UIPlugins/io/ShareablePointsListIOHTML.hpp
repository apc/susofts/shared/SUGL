/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef ISHAREABLEPOINTSLISTIOHTML_HPP
#define ISHAREABLEPOINTSLISTIOHTML_HPP

#include <memory>
#include <utility>

#include <QString>

#include <ShareablePoints/IShareablePointsListIO.hpp>

#include "PluginsGlobal.hpp"

class QXmlStreamReader;
class QXmlStreamWriter;

class ShareableExtraInfos;
class ShareableFrame;
struct ShareableParams;
struct ShareablePoint;
class ShareablePointsList;
struct ShareablePosition;

/**
 * HTML serialization interface for Shareable Points.
 *
 * To be able to serialize a @ref shpoints object into HTML, you need to create a ShareablePointsListIOHTML object
 * and call the write() method with the object you want to serialize.
 *
 * Note that the HTML export is a lossy export: not all information can be exported to HTML table. The main example is when you
 * serialize a ShareablePointsList, only the points are serialized. As the export is done in one HTML table,
 * the parameters can't be exported. Same for all the frames (hierarchie, names, positions...). Only the points
 * are extracted, all at the same level.
 *
 * This is done without any transformations or convertion for the points that are inside frames with a specific position.
 * Thus, the positions may be false.
 *
 * Note also that ShareableExtraInfos can be exported, but they can't be loaded from HTML (because we may not know what do they represent).
 *
 * Finally, you can change the way the HTML file is written, by defining if the headers should be written or not.
 *
 * @note The output is similar to what creates ShareablePointsListIOCSV.
 */
class SUGL_SHARED_EXPORT ShareablePointsListIOHTML : public IShareablePointsListIO
{
public:
	virtual ~ShareablePointsListIOHTML() override = default;

	/**
	 * @return true if the serializer is configured to write the headers on the first line
	 * @see writeHeaders(bool)
	 */
	bool writeHeaders() const noexcept { return _writeHeaders; }
	/**
	 * Tells if we should write the headers in the first line or not (default is true).
	 * Note that headers are only written with write(const ShareablePointsList&).
	 * @params write tell if the headers should be written
	 * @see writeHeaders()
	 */
	void writeHeaders(bool write) noexcept { _writeHeaders = write; }
	/** @return true if the output should be automatically indented */
	bool isIndented() const noexcept { return _isindented; }
	/** Change the indent mode */
	void isIndented(bool indent) noexcept { _isindented = indent; }

	virtual const std::string &getMIMEType() const override
	{
		static const std::string mime = "text/html";
		return mime;
	}

	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;

	virtual std::string write(const ShareablePointsList &list) override;
	virtual std::string write(const ShareableExtraInfos &infos) override;
	virtual std::string write(const ShareableFrame &frame) override;
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &point) override;
	virtual std::string write(const ShareablePosition &position) override;

protected:
	/** Initialize the XML object with the syntaxic sugar */
	void startWriting(QXmlStreamWriter &writer) const;
	/** write the header in the XML object */
	void writePointHeaders(QXmlStreamWriter &writer, const ShareableParams &params) const;
	/** Starts reading and returns eventually the headers. It will also initialize correctly the reader and the data. */
	std::pair<Fields, Fields> startReading(QXmlStreamReader &reader, QString &data, bool tryHeaders = true) const;
	/** Try to read the given <tr> as a header and fill the Fields with the given fields, otherwise returns empty sets. First is position, second is point. */
	std::pair<Fields, Fields> tryToReadHeader(QXmlStreamReader &reader) const;
	/** @return the text of the next element if it has the right name. @throw SPIOException if needed */
	QString readNextTextElement(QXmlStreamReader &reader, const QString &name = "td") const;

	/** @return a ShareablePointsList constructed from its XML representation. */
	ShareablePointsList fromxmlPointList(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields, const IShareablePointsListIO::Fields &pointFields) const;
	/** @return a ShareableExtraInfos constructed from its XML representation. */
	ShareableExtraInfos fromxmlInfo(QXmlStreamReader &reader) const;
	/** @return a ShareableFrame constructed from its XML representation. */
	ShareableFrame fromxmlFrame(QXmlStreamReader &reader,
		const IShareablePointsListIO::Fields &positionFields,
		const IShareablePointsListIO::Fields &pointFields,
		std::shared_ptr<ShareableParams> params = nullptr) const;
	/** @return a ShareablePoint constructed from its XML representation. */
	ShareablePoint fromxmlPoint(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields, const IShareablePointsListIO::Fields &pointFields) const;
	/** @return a ShareablePosition constructed from its XML representation. */
	ShareablePosition fromxmlPosition(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields) const;

	/** write in the XML object a representation of a ShareablePointsList. */
	void toxml(QXmlStreamWriter &writer, const ShareablePointsList &list) const;
	/** write in the XML object a representation of a ShareableExtraInfos. */
	void toxml(QXmlStreamWriter &writer, const ShareableExtraInfos &infos) const;
	/** write in the XML object a representation of a ShareableFrame. */
	void toxml(QXmlStreamWriter &writer, const ShareableFrame &frame) const;
	/** write in the XML object a representation of a ShareableParams. */
	void toxml(QXmlStreamWriter &, const ShareableParams &) const {}
	/** write in the XML object a representation of a ShareablePoint. */
	void toxml(QXmlStreamWriter &writer, const ShareablePoint &point) const;
	/** write in the XML object a representation of a ShareablePosition. */
	void toxml(QXmlStreamWriter &writer, const ShareablePosition &position) const;

protected:
	/** Tells if the headers should be written */
	bool _writeHeaders = true;
	/** Tells if the output should be indented */
	bool _isindented = true;
};

#endif // ISHAREABLEPOINTSLISTIOHTML_HPP
