/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef ISHAREABLEPOINTSLISTIOJSON_HPP
#define ISHAREABLEPOINTSLISTIOJSON_HPP

#include <memory>

#include <ShareablePoints/IShareablePointsListIO.hpp>

#include "PluginsGlobal.hpp"

class QJsonObject;

class ShareableExtraInfos;
class ShareableFrame;
struct ShareableParams;
struct ShareablePoint;
class ShareablePointsList;
struct ShareablePosition;

/**
 * JSON serialization interface for Shareable Points.
 *
 * To be able to serialize a @ref shpoints object into JSON, you need to create a ShareablePointsListIOJSon object
 * and give it the path to the file you want to write. Then, you just need to call the write() method with the object
 * you want to serialize.
 *
 * To load a @ref shpoints object from a JSON file, you need to create a ShareablePointsListIOJSon object
 * and give it the path to the file you want to read. Then, call the correct read() method.
 *
 * By default, it writes a text JSON file, with beautiful human reading indentation. You can write a smaller file
 * if you desactivate the indentation with isIndented().
 *
 * You can also write Qt style binary JSON files by activating the isBinary() option. This will give even smaller files
 * but impossible to read for humans. Note that you will have to enable the isBinary() switch when you'll want to read
 * the file.
 */
class SUGL_SHARED_EXPORT ShareablePointsListIOJSon : public IShareablePointsListIO
{
public:
	virtual ~ShareablePointsListIOJSon() override = default;

	/**
	 * @return the state of the indented switch.
	 * @see isIndented(bool)
	 */
	bool isIndented() const noexcept { return _indented; }
	/**
	 * Sets the indented switch. If the switch is set to true, the files written will have no
	 * indentation, which makes it smaller but also harder to read for a human.
	 *
	 * @note This switch has no effect on reading.
	 * @params indent the new value for the indentation
	 * @see isIndented()
	 */
	void isIndented(bool indent) noexcept { _indented = indent; }
	/**
	 * @return the state of the binary switch.
	 * @see isBinary(bool)
	 */
	bool isBinary() const noexcept { return _binary; }
	/**
	 * Sets the binary switch. If the switch is set to true, it will read and write files in binary mode.
	 * When you write a file in binary mode, the file will be smaller but totally impossible to read for a
	 * human.
	 *
	 * @note If you want to read a file that has been written with the binary swich on, you need the switch on
	 * to read it too.
	 * @params bin the new value for the binary switch
	 * @see isBinary()
	 */
	void isBinary(bool bin) noexcept { _binary = bin; }

	virtual const std::string &getMIMEType() const override
	{
		static const std::string mimejson = "application/json", mimedat = "application/vnd.cern.susoft.binary.json";
		return _binary ? mimedat : mimejson;
	}

	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &contents) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;

	virtual std::string write(const ShareablePointsList &list) override;
	virtual std::string write(const ShareableExtraInfos &info) override;
	virtual std::string write(const ShareableFrame &frame) override;
	virtual std::string write(const ShareableParams &params) override;
	virtual std::string write(const ShareablePoint &point) override;
	virtual std::string write(const ShareablePosition &position) override;

protected:
	/** Checks that obj has the given key and that it is linked to a JSON object. Otherwise, it throws a SPIOException exception. */
	void checkObject(const QJsonObject &obj, const std::string &key, const std::string &type, const std::string &objectName = "") const;
	/** Checks that obj has the given key and that it is linked to a JSON array. Otherwise, it throws a SPIOException exception. */
	void checkArray(const QJsonObject &obj, const std::string &key, const std::string &type, const std::string &objectName = "") const;

	/** @return a ShareablePointsList constructed from its JSON representation. */
	ShareablePointsList fromjsonPointList(const QJsonObject &jobj) const;
	/** @return a ShareableExtraInfos constructed from its JSON representation. */
	ShareableExtraInfos fromjsonInfo(const QJsonObject &jobj) const;
	/** @return a ShareableFrame constructed from its JSON representation. */
	ShareableFrame fromjsonFrame(const QJsonObject &jobj, std::shared_ptr<ShareableParams> params = nullptr) const;
	/** @return a ShareableParams constructed from its JSON representation. */
	ShareableParams fromjsonParams(const QJsonObject &jobj) const;
	/** @return a ShareablePoint constructed from its JSON representation. */
	ShareablePoint fromjsonPoint(const QJsonObject &jobj) const;
	/** @return a ShareablePosition constructed from its JSON representation. */
	ShareablePosition fromjsonPosition(const QJsonObject &jobj) const;

	/** @return a JSON representation of a ShareablePointsList. */
	QJsonObject tojson(const ShareablePointsList &list) const;
	/** @return a JSON representation of a ShareableExtraInfos. */
	QJsonObject tojson(const ShareableExtraInfos &info) const;
	/** @return a JSON representation of a ShareableFrame. */
	QJsonObject tojson(const ShareableFrame &frame) const;
	/** @return a JSON representation of a ShareableParams. */
	QJsonObject tojson(const ShareableParams &params) const;
	/** @return a JSON representation of a ShareablePoint. */
	QJsonObject tojson(const ShareablePoint &point) const;
	/** @return a JSON representation of a ShareablePosition. */
	QJsonObject tojson(const ShareablePosition &position) const;

protected:
	/** Indented switch. */
	bool _indented = true;
	/** Binary switch. */
	bool _binary = false;
};

#endif // ISHAREABLEPOINTSLISTIOJSON_HPP
