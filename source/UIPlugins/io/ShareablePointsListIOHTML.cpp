#include "ShareablePointsListIOHTML.hpp"

#include <QRegularExpression>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

namespace
{
inline std::string toString(const QString &str, bool utf8)
{
	return utf8 ? str.toUtf8().constData() : str.toLatin1().constData();
}

inline QString toQString(const std::string &str, bool utf8)
{
	return utf8 ? QString::fromUtf8(str.c_str()) : QString::fromLatin1(str.c_str());
}

inline bool readBool(const QString &b)
{
	return b.toLower() == "true" ? true : false;
}

inline SPIOException createError(const QXmlStreamReader &reader, const std::string &error = "")
{
	return SPIOException(error, "", reader.tokenString().toStdString(), reader.characterOffset());
}
} // namespace

ShareablePointsList ShareablePointsListIOHTML::read(const std::string &contents)
{
	QString xml = contents.c_str();
	QXmlStreamReader reader;

	auto [positionFields, pointFields] = startReading(reader, xml);

	return fromxmlPointList(reader, positionFields, pointFields);
}

ShareableExtraInfos ShareablePointsListIOHTML::readExtraInfos(const std::string &contents)
{
	QString xml = contents.c_str();
	QXmlStreamReader reader(xml);

	return fromxmlInfo(reader);
}

ShareableFrame ShareablePointsListIOHTML::readFrame(const std::string &contents)
{
	QString xml = contents.c_str();
	QXmlStreamReader reader;

	auto [positionFields, pointFields] = startReading(reader, xml);

	return fromxmlFrame(reader, positionFields, pointFields);
}

ShareableParams ShareablePointsListIOHTML::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint ShareablePointsListIOHTML::readPoint(const std::string &contents)
{
	QString xml = contents.c_str();
	QXmlStreamReader reader;

	auto [positionFields, pointFields] = startReading(reader, xml);

	while (reader.readNextStartElement() && reader.name() != "tr")
		;
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading the beginning of the table.");

	return fromxmlPoint(reader, positionFields, pointFields);
}

ShareablePosition ShareablePointsListIOHTML::readPosition(const std::string &contents)
{
	QString xml = contents.c_str();
	QXmlStreamReader reader;

	auto [positionFields, pointFields] = startReading(reader, xml);

	while (reader.readNextStartElement() && reader.name() != "tr")
		;
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading the beginning of the table.");

	return fromxmlPosition(reader, positionFields);
}

std::string ShareablePointsListIOHTML::write(const ShareablePointsList &list)
{
	QString result;
	QXmlStreamWriter writer(&result);
	startWriting(writer);

	if (_writeHeaders)
		writePointHeaders(writer, list.getParams());

	toxml(writer, list);
	writer.writeEndDocument();

	return toString(result, utf8());
}

std::string ShareablePointsListIOHTML::write(const ShareableExtraInfos &infos)
{
	QString result;
	QXmlStreamWriter writer(&result);
	startWriting(writer);

	writer.writeStartElement("tr");
	toxml(writer, infos);
	writer.writeEndDocument();

	return toString(result, utf8());
}

std::string ShareablePointsListIOHTML::write(const ShareableFrame &frame)
{
	QString result;
	QXmlStreamWriter writer(&result);
	startWriting(writer);

	if (_writeHeaders)
		writePointHeaders(writer, frame.getParams());

	toxml(writer, frame);
	writer.writeEndDocument();

	return toString(result, utf8());
}

std::string ShareablePointsListIOHTML::write(const ShareablePoint &point)
{
	QString result;
	QXmlStreamWriter writer(&result);
	startWriting(writer);

	if (_writeHeaders)
		writePointHeaders(writer, ShareableParams());

	writer.writeStartElement("tr");
	toxml(writer, point);
	writer.writeEndDocument();

	return toString(result, utf8());
}

std::string ShareablePointsListIOHTML::write(const ShareablePosition &position)
{
	QString result;
	QXmlStreamWriter writer(&result);
	startWriting(writer);

	writer.writeStartElement("tr");
	toxml(writer, position);
	writer.writeEndDocument();

	return toString(result, utf8());
}

void ShareablePointsListIOHTML::startWriting(QXmlStreamWriter &writer) const
{
	writer.setAutoFormatting(_isindented);
	writer.writeStartDocument(); // <?xml ...>
	writer.writeStartElement("html");
	writer.writeStartElement("head");
	writer.writeStartElement("meta");
	writer.writeAttribute("content", "text/html; charset=utf-8");
	writer.writeEndElement(); // meta
	writer.writeEndElement(); // head

	writer.writeStartElement("body");
	writer.writeStartElement("table"); // these will automatically be closed with `writeEndDocument()`
}

void ShareablePointsListIOHTML::writePointHeaders(QXmlStreamWriter &writer, const ShareableParams &params) const
{
	if (!exportFieldsFrame.hasField("points"))
		return;
	writer.writeStartElement("tr");
	if (exportFieldsPoint.hasField("name"))
		writer.writeTextElement("th", "name");
	// position
	if (exportFieldsPoint.hasField("position"))
	{
		// x, y, z
		if (exportFieldsPosition.hasField("x"))
			writer.writeTextElement("th", "X [m]");
		if (exportFieldsPosition.hasField("y"))
			writer.writeTextElement("th", "Y [m]");
		if (exportFieldsPosition.hasField("z"))
			writer.writeTextElement("th", params.coordsys == ShareableParams::k3DCartesian ? "Z [m]" : "H [m]");
		// sigma
		if (exportFieldsPosition.hasField("sigmax"))
			writer.writeTextElement("th", "sigmax");
		if (exportFieldsPosition.hasField("sigmay"))
			writer.writeTextElement("th", "sigmay");
		if (exportFieldsPosition.hasField("sigmaz"))
			writer.writeTextElement("th", "sigmaz");
		// free
		if (exportFieldsPosition.hasField("isfreex"))
			writer.writeTextElement("th", "isfreex");
		if (exportFieldsPosition.hasField("isfreey"))
			writer.writeTextElement("th", "isfreey");
		if (exportFieldsPosition.hasField("isfreez"))
			writer.writeTextElement("th", "isfreez");
	}
	// point
	if (exportFieldsPoint.hasField("inlineComment"))
		writer.writeTextElement("th", "inlineComment");
	if (exportFieldsPoint.hasField("headerComment"))
		writer.writeTextElement("th", "headerComment");
	if (exportFieldsPoint.hasField("active"))
		writer.writeTextElement("th", "active");
	// extra infos
	if (exportFieldsPoint.hasField("extraInfos"))
		writer.writeTextElement("th", "extraInfos");
	writer.writeEndElement(); // tr
}

std::pair<ShareablePointsListIOHTML::Fields, ShareablePointsListIOHTML::Fields> ShareablePointsListIOHTML::startReading(QXmlStreamReader &reader, QString &data, bool tryHeaders) const
{
	// first, we remove the <head></head> section
	data.remove(QRegularExpression(R"""(<head>.*<\/head>)""", QRegularExpression::CaseInsensitiveOption | QRegularExpression::DotMatchesEverythingOption));
	// then, we remove any <col> or <colgroup></colgroup> or <br>
	data.remove(QRegularExpression("<col.*>"));
	data.remove(QRegularExpression("<colgroup.*>"));
	data.remove("<br>");
	// we remove all newlines
	data.replace('\n', ' ');
	// we remove all style stuff
	data.replace(QRegularExpression(R"""(<(\w+)\s.*?>)"""), R"""(<\1>)""");
	// we initialize the reader
	reader.clear();
	reader.addData(data);

	while (!reader.atEnd() && !reader.hasError() && reader.name() != "table")
		reader.readNext();
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading the beginning of the file.");

	if (tryHeaders)
	{
		auto [positionFields, pointFields] = tryToReadHeader(reader);
		if (positionFields.getFields().empty() && pointFields.getFields().empty())
		{
			reader.clear();
			reader.addData(data);
			return startReading(reader, data, false);
		}
		return std::make_pair(positionFields, pointFields);
	}
	return std::make_pair(exportFieldsPosition, exportFieldsPoint);
}

std::pair<IShareablePointsListIO::Fields, IShareablePointsListIO::Fields> ShareablePointsListIOHTML::tryToReadHeader(QXmlStreamReader &reader) const
{
	Fields position(exportFieldsPosition.getAllFields());
	position.clear();
	Fields point(exportFieldsPoint.getAllFields());
	point.clear();

	while (reader.readNextStartElement() && reader.name() != "td" && reader.name() != "th")
		;
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading the table.");
	do
	{
		if (reader.isComment())
			continue;
		std::string currentHeader = reader.readElementText(QXmlStreamReader::IncludeChildElements).toStdString();
		// Interpret the value
		size_t pos = currentHeader.find(" [m]");
		if (pos == 1)
			currentHeader = std::tolower(currentHeader[0]);
		if (currentHeader == "h")
			currentHeader = "z";

		if (position.getAllFields().find(currentHeader) != std::end(position.getAllFields()))
			position.addField(currentHeader);
		else if (point.getAllFields().find(currentHeader) != std::end(point.getAllFields()))
			point.addField(currentHeader);
	} while (reader.readNextStartElement() && (reader.name() == "td" || reader.name() == "th" || reader.isComment()));
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading headers");

	if (!position.getFields().empty())
		point.addField("position");

	return std::make_pair(position, point);
}

QString ShareablePointsListIOHTML::readNextTextElement(QXmlStreamReader &reader, const QString &name) const
{
	if (!reader.readNextStartElement() || reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading '" + name.toStdString() + "'");
	if (reader.isComment())
		return readNextTextElement(reader, name);
	if (reader.name() == name)
		return reader.readElementText(QXmlStreamReader::IncludeChildElements);
	return QString();
}

ShareablePointsList
	ShareablePointsListIOHTML::fromxmlPointList(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields, const IShareablePointsListIO::Fields &pointFields) const
{
	Q_ASSERT(reader.name() == "table" || reader.name() == "tr");

	ShareablePointsList spl;

	auto *frame = new ShareableFrame(fromxmlFrame(reader, positionFields, pointFields, spl.getParamsPointer()));
	spl.setRootFrame(frame);

	return spl;
}

ShareableExtraInfos ShareablePointsListIOHTML::fromxmlInfo(QXmlStreamReader &reader) const
{
	readNextTextElement(reader); // we read it to remove the value from the reader, but we don't use it
	return ShareableExtraInfos();
}

ShareableFrame ShareablePointsListIOHTML::fromxmlFrame(QXmlStreamReader &reader,
	const IShareablePointsListIO::Fields &positionFields,
	const IShareablePointsListIO::Fields &pointFields,
	std::shared_ptr<ShareableParams> params) const
{
	Q_ASSERT(reader.name() == "table" || reader.name() == "tr");

	if (!params)
		params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	while (!reader.hasError() && !reader.atEnd() && (reader.isComment() || reader.name() == "tr" || reader.name() == "table"))
	{
		reader.readNextStartElement(); // <tr>
		if (reader.isComment())
			continue;
		if (reader.name() == "table")
			break;
		frame.add(new ShareablePoint(fromxmlPoint(reader, positionFields, pointFields)));
		while (reader.name() != "tr") // </td></tr>
			reader.readNext();
	}
	if (reader.hasError() || reader.atEnd())
		throw createError(reader, "Error while reading Frame");

	return frame;
}

ShareablePoint ShareablePointsListIOHTML::fromxmlPoint(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields, const IShareablePointsListIO::Fields &pointFields) const
{
	Q_ASSERT(reader.name() == "tr");

	/* clang-format off */
	return ShareablePoint{
		pointFields.hasField("name") ? toString(readNextTextElement(reader), utf8()) : "",
		pointFields.hasField("position") ? fromxmlPosition(reader, positionFields) : ShareablePosition{},
		pointFields.hasField("inlineComment") ? toString(readNextTextElement(reader), utf8()) : "",
		pointFields.hasField("headerComment") ? toString(readNextTextElement(reader), utf8()) : "",
		pointFields.hasField("active") ? readBool(readNextTextElement(reader)) : true,
		pointFields.hasField("extraInfos") ? fromxmlInfo(reader) : ShareableExtraInfos{}
	};
	/* clang-format on */
}

ShareablePosition ShareablePointsListIOHTML::fromxmlPosition(QXmlStreamReader &reader, const IShareablePointsListIO::Fields &positionFields) const
{
	/* clang-format off */
	return ShareablePosition{
		// x, y, z
		positionFields.hasField("x") ? readNextTextElement(reader).toDouble() : 0,
		positionFields.hasField("y") ? readNextTextElement(reader).toDouble() : 0,
		positionFields.hasField("z") ? readNextTextElement(reader).toDouble() : 0,
		// sigma
		positionFields.hasField("sigmax") ? readNextTextElement(reader).toDouble() : 0,
		positionFields.hasField("sigmay") ? readNextTextElement(reader).toDouble() : 0,
		positionFields.hasField("sigmaz") ? readNextTextElement(reader).toDouble() : 0,
		// free
		positionFields.hasField("isfreex") ? readBool(readNextTextElement(reader)) : false,
		positionFields.hasField("isfreey") ? readBool(readNextTextElement(reader)) : false,
		positionFields.hasField("isfreez") ? readBool(readNextTextElement(reader)) : false
	};
	/* clang-format on */
}

void ShareablePointsListIOHTML::toxml(QXmlStreamWriter &writer, const ShareablePointsList &list) const
{
	if (exportFieldsPointsList.hasField("rootFrame"))
		toxml(writer, list.getRootFrame());
}

void ShareablePointsListIOHTML::toxml(QXmlStreamWriter &writer, const ShareableExtraInfos &infos) const
{
	bool first = true;
	QString tmp;
	for (const auto &[key, info] : infos.getMap())
	{
		if (!first)
			tmp += ';';
		else
			first = false;
		tmp += toQString(info, utf8());
	}
	writer.writeTextElement("td", tmp);
}

void ShareablePointsListIOHTML::toxml(QXmlStreamWriter &writer, const ShareableFrame &frame) const
{
	if (!exportFieldsFrame.hasField("points"))
		return;

	// template lambda bitch!
	auto writepoints = [this, &writer](const auto &points) -> void {
		for (const auto &p : points)
		{
			writer.writeStartElement("tr");
			toxml(writer, *p);
			writer.writeEndElement(); // tr
		}
	};

	if (exportFieldsFrame.hasField("innerFrames"))
		writepoints(frame.getAllPoints()); // vector of naked pointers
	else
		writepoints(frame.getPoints()); // vector of unique_ptr (to go faster)
}

void ShareablePointsListIOHTML::toxml(QXmlStreamWriter &writer, const ShareablePoint &point) const
{
	if (!point.headerComment.empty())
		writer.writeComment(toQString(point.headerComment, utf8()).replace('-', '_'));

	if (exportFieldsPoint.hasField("name"))
		writer.writeTextElement("td", toQString(point.name, utf8()));
	if (exportFieldsPoint.hasField("position"))
		toxml(writer, point.position);
	if (exportFieldsPoint.hasField("inlineComment"))
		writer.writeTextElement("td", toQString(point.inlineComment, utf8()));
	if (exportFieldsPoint.hasField("headerComment"))
		writer.writeTextElement("td", toQString(point.headerComment, utf8()));
	if (exportFieldsPoint.hasField("active"))
		writer.writeTextElement("td", point.active ? "true" : "false");
	if (exportFieldsPoint.hasField("extraInfos"))
		toxml(writer, point.extraInfos);
}

void ShareablePointsListIOHTML::toxml(QXmlStreamWriter &writer, const ShareablePosition &position) const
{
	// x, y, z
	if (exportFieldsPosition.hasField("x"))
		writer.writeTextElement("td", QString::number(position.x, 'g', std::numeric_limits<double>::digits10));
	if (exportFieldsPosition.hasField("y"))
		writer.writeTextElement("td", QString::number(position.y, 'g', std::numeric_limits<double>::digits10));
	if (exportFieldsPosition.hasField("z"))
		writer.writeTextElement("td", QString::number(position.z, 'g', std::numeric_limits<double>::digits10));
	// sigma
	if (exportFieldsPosition.hasField("sigmax"))
		writer.writeTextElement("td", QString::number(position.sigmax, 'g', std::numeric_limits<double>::digits10));
	if (exportFieldsPosition.hasField("sigmay"))
		writer.writeTextElement("td", QString::number(position.sigmay, 'g', std::numeric_limits<double>::digits10));
	if (exportFieldsPosition.hasField("sigmaz"))
		writer.writeTextElement("td", QString::number(position.sigmaz, 'g', std::numeric_limits<double>::digits10));
	// free
	if (exportFieldsPosition.hasField("isfreex"))
		writer.writeTextElement("td", position.isfreex ? "true" : "false");
	if (exportFieldsPosition.hasField("isfreey"))
		writer.writeTextElement("td", position.isfreey ? "true" : "false");
	if (exportFieldsPosition.hasField("isfreez"))
		writer.writeTextElement("td", position.isfreez ? "true" : "false");
}
