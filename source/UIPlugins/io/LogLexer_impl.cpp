#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "LogLexer.hpp"

LogLexer::LogLexer(QObject *parent) : LexerParser(parent)
{
}

const char *LogLexer::keywords(int) const
{
	return nullptr;
}

QString LogLexer::description(int style) const
{
	style &= ~Styles::Debug;
	style &= ~Styles::Info;
	style &= ~Styles::Warning;
	style &= ~Styles::Critical;
	style &= ~Styles::Fatal;
	style &= ~Styles::Error;

	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Date:
		return "Date";
	case Styles::Type:
		return "Type";
	case Styles::Content:
		return "Content";
	case Styles::FileInfo:
		return "FileInfo";
	}

	return QString();
}

const char *LogLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*:/![]()";
}

bool LogLexer::defaultEolFill(int) const
{
	return false;
}

QColor LogLexer::defaultColor(int style) const
{
	int cleanStyle = style;
	cleanStyle &= ~Styles::Debug;
	cleanStyle &= ~Styles::Info;
	cleanStyle &= ~Styles::Warning;
	cleanStyle &= ~Styles::Critical;
	cleanStyle &= ~Styles::Fatal;
	cleanStyle &= ~Styles::Error;

	if (cleanStyle == Styles::Default || cleanStyle == Styles::FileInfo)
		return Qt::GlobalColor::darkGray;

	if (cleanStyle == Styles::Type || cleanStyle == Styles::Date || cleanStyle == Styles::Content)
	{
		if (style & (Styles::Fatal | Styles::Critical | Styles::Error))
			return QColor(218, 68, 83);
		if (style & Styles::Warning)
			return QColor(246, 116, 0);
		if (style & Styles::Info)
			return QColor(61, 174, 233);
		if (style & Styles::Debug)
			return QColor(35, 38, 41);
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont LogLexer::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	style &= ~Styles::Debug;
	style &= ~Styles::Info;
	style &= ~Styles::Warning;
	style &= ~Styles::Critical;
	style &= ~Styles::Fatal;
	style &= ~Styles::Error;

	switch (style)
	{
	case Styles::Default:
	case Styles::Date:
		return baseFont;
	case Styles::Type:
	case Styles::Content:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	case Styles::FileInfo:
		baseFont.setItalic(true);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor LogLexer::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList LogLexer::read(const std::string &)
{
	return ShareablePointsList();
}

ShareableExtraInfos LogLexer::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame LogLexer::readFrame(const std::string &)
{
	return ShareableFrame(std::make_shared<ShareableParams>());
}

ShareableParams LogLexer::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint LogLexer::readPoint(const std::string &)
{
	return ShareablePoint();
}

ShareablePosition LogLexer::readPosition(const std::string &)
{
	return ShareablePosition();
}

bool LogLexer::startLexer(int start, int end)
{
	_currentTypeFlag = Styles::Unknown;
	return LexerParser::startLexer(start, end);
}

void LogLexer::endLexer()
{
	LexerParser::endLexer();
	_currentTypeFlag = Styles::Unknown;
}
