// This file was generated by qlalr - DO NOT EDIT!
#include "table_loglexer_p.h"

const char *const Table_LogLexer::spell [] = {
    "end of file", "New line '\\n'", "DEBUG:", "INFO:", "WARNING:", "CRITICAL:", "FATAL:", "ERROR:", "\tat '", "Date",
    "Time", "Word"
};

const short Table_LogLexer::lhs [] = {
    13, 13, 12, 12, 14, 14, 15, 18, 18, 18,
    18, 18, 18, 16, 16, 19, 19, 17, 20, 21,
    21, 22, 22, 23
};

const short Table_LogLexer::rhs [] = {
    1, 2, 1, 2, 2, 3, 3, 1, 1, 1,
    1, 1, 1, 2, 3, 1, 2, 2, 1, 2,
    3, 1, 2, 2
};

const short Table_LogLexer::action_default [] = {
    0, 0, 0, 3, 0, 0, 11, 8, 13, 12,
    9, 7, 10, 24, 4, 5, 0, 16, 19, 0,
    6, 0, 15, 1, 17, 2, 18, 0, 22, 0,
    21, 23, 20, 14
};

const short Table_LogLexer::goto_default [] = {
    2, 33, 3, 4, 15, 20, 11, 16, 21, 26,
    27, 0
};

const short Table_LogLexer::action_index [] = {
    -6, -8, 9, -12, -7, 21, -12, -12, -12, -12,
    -12, -12, -12, -12, -12, 2, 10, -12, -12, 5,
    -12, -11, 0, -12, -12, -12, -11, 4, -12, 6,
    11, -12, 11, 11,

    -12, -12, -2, -12, -12, -12, -12, -12, -12, -12,
    -12, -12, -12, -12, -12, -6, -12, -12, -12, 2,
    -12, -12, -12, -12, -12, -12, -8, 9, -12, 3,
    -12, -12, -12, -12
};

const short Table_LogLexer::action_info [] = {
    28, 25, 5, 1, 17, 23, 23, 23, 0, 13,
    18, 23, 25, 17, 0, 31, 24, 31, 1, 0,
    0, 24, 0, 7, 10, 12, 6, 9, 8, 0,
    0, 0, 0,

    14, 19, 29, 22, 30, 0, 0, 0, 0, 0,
    32, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0
};

const short Table_LogLexer::action_check [] = {
    11, 1, 10, 9, 11, 1, 1, 1, -1, 0,
    8, 1, 1, 11, -1, 11, 11, 11, 9, -1,
    -1, 11, -1, 2, 3, 4, 5, 6, 7, -1,
    -1, -1, -1,

    2, 7, 10, 1, 1, -1, -1, -1, -1, -1,
    1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1
};

