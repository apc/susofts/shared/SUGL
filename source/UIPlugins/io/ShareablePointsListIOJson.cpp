#include "ShareablePointsListIOJson.hpp"

#include <algorithm>
#include <fstream>
#include <memory>
#include <sstream>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

namespace
{
inline std::string toString(const QString &str, bool utf8)
{
	return utf8 ? str.toUtf8().constData() : str.toLatin1().constData();
}

inline QString toQString(const std::string &str, bool)
{
	return str.c_str();
}

class _iojson : public ShareablePointsListIOJSon
{
public:
	_iojson(ShareablePointsListIOJSon &base) : ShareablePointsListIOJSon(base) {}

	QJsonObject _read(const std::string &contents)
	{
		QByteArray bytes(contents.c_str(), (int)contents.size());
		QJsonParseError error{};
		auto jdoc = _binary ? QJsonDocument::fromBinaryData(bytes) : QJsonDocument::fromJson(bytes, &error);

		if (!_binary && error.error != QJsonParseError::ParseError::NoError)
			throw SPIOException(error.errorString().toStdString(), "", contents, error.offset);
		if (jdoc.isNull() || jdoc.isEmpty())
			throw SPIOException("Error bad contents: can't be read or is empty.", "", contents, 0);
		if (jdoc.isArray())
			throw SPIOException("Error contents contains an array, not an object.", "", contents, 0);

		return jdoc.object();
	}

	template<class T>
	std::string _write(const T &towrite)
	{
		QJsonDocument::JsonFormat format = _indented ? QJsonDocument::JsonFormat::Indented : QJsonDocument::JsonFormat::Compact;
		QJsonDocument jdoc(tojson(towrite));
		QByteArray output = _binary ? jdoc.toBinaryData() : jdoc.toJson(format);
		return std::string(output.data(), output.size());
	}
};
} // namespace

ShareablePointsList ShareablePointsListIOJSon::read(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonPointList(io._read(contents));
}

ShareableExtraInfos ShareablePointsListIOJSon::readExtraInfos(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonInfo(io._read(contents));
}

ShareableFrame ShareablePointsListIOJSon::readFrame(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonFrame(io._read(contents));
}

ShareableParams ShareablePointsListIOJSon::readParams(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonParams(io._read(contents));
}

ShareablePoint ShareablePointsListIOJSon::readPoint(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonPoint(io._read(contents));
}

ShareablePosition ShareablePointsListIOJSon::readPosition(const std::string &contents)
{
	_iojson io(*this);
	return fromjsonPosition(io._read(contents));
}

std::string ShareablePointsListIOJSon::write(const ShareablePointsList &list)
{
	_iojson io(*this);
	return io._write(list);
}

std::string ShareablePointsListIOJSon::write(const ShareableExtraInfos &info)
{
	_iojson io(*this);
	return io._write(info);
}

std::string ShareablePointsListIOJSon::write(const ShareableFrame &frame)
{
	_iojson io(*this);
	return io._write(frame);
}

std::string ShareablePointsListIOJSon::write(const ShareableParams &params)
{
	_iojson io(*this);
	return io._write(params);
}

std::string ShareablePointsListIOJSon::write(const ShareablePoint &point)
{
	_iojson io(*this);
	return io._write(point);
}

std::string ShareablePointsListIOJSon::write(const ShareablePosition &position)
{
	_iojson io(*this);
	return io._write(position);
}

void ShareablePointsListIOJSon::checkObject(const QJsonObject &obj, const std::string &key, const std::string &type, const std::string &objectName) const
{
	if (!obj.contains(key.c_str()))
		throw SPIOException("Error in " + type + ' ' + objectName + " does not have required key '" + key + "'.");
	if (!obj[key.c_str()].isObject())
		throw SPIOException("Error in " + type + ' ' + objectName + " key '" + key + "' is not an object.");
}

void ShareablePointsListIOJSon::checkArray(const QJsonObject &obj, const std::string &key, const std::string &type, const std::string &objectName) const
{
	if (!obj.contains(key.c_str()))
		throw SPIOException("Error in " + type + ' ' + objectName + " does not have required key '" + key + "'.");
	if (!obj[key.c_str()].isArray())
		throw SPIOException("Error in " + type + ' ' + objectName + " key '" + key + "' is not an array.");
}

ShareablePointsList ShareablePointsListIOJSon::fromjsonPointList(const QJsonObject &jobj) const
{
	ShareablePointsList list(jobj.contains("title") ? toString(jobj["title"].toString(), utf8()) : "");
	checkObject(jobj, "params", "PointList", list.getTitle());
	list.getParams() = std::move(fromjsonParams(jobj["params"].toObject()));
	checkObject(jobj, "rootFrame", "PointList", list.getTitle());
	auto *frame = new ShareableFrame(fromjsonFrame(jobj["rootFrame"].toObject(), list.getParamsPointer())); // we use the direct access to the params pointer for optimization
	list.setRootFrame(frame);
	return list;
}

ShareableExtraInfos ShareablePointsListIOJSon::fromjsonInfo(const QJsonObject &jobj) const
{
	ShareableExtraInfos infos;
	for (auto it = jobj.begin(); it != jobj.end(); it++)
		infos.addExtraInfo(toString(it.key(), utf8()), toString(it.value().toString(), utf8()));
	return infos;
}

ShareableFrame ShareablePointsListIOJSon::fromjsonFrame(const QJsonObject &jobj, std::shared_ptr<ShareableParams> params) const
{
	if (!params)
		params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params, jobj.contains("name") ? toString(jobj["name"].toString(), utf8()) : "");
	checkObject(jobj, "translation", "Frame", frame.getName());
	frame.setTranslation(fromjsonPosition(jobj["translation"].toObject()));
	checkObject(jobj, "rotation", "Frame", frame.getName());
	frame.setRotation(fromjsonPosition(jobj["rotation"].toObject()));
	frame.setScale(jobj.contains("scale") ? jobj["scale"].toDouble() : 1.);
	frame.isFreeScale(jobj.contains("isfreescale") ? jobj["isfreescale"].toBool() : false);
	// points (leaves)
	checkArray(jobj, "points", "Frame", frame.getName());
	QJsonArray points = jobj["points"].toArray();
	for (const auto &p : points)
	{
		if (!p.isObject())
			throw SPIOException("Error in Frame " + frame.getName() + " key 'points' is not an array of objects.");
		frame.add(new ShareablePoint(fromjsonPoint(p.toObject())));
	}
	// inner frames (nodes)
	checkArray(jobj, "innerFrames", "Frame", frame.getName());
	QJsonArray frames = jobj["innerFrames"].toArray();
	for (const auto &f : frames)
	{
		if (!f.isObject())
			throw SPIOException("Error in Frame " + frame.getName() + " key 'innerFrames' is not an array of objects.");
		frame.add(new ShareableFrame(fromjsonFrame(f.toObject())));
	}
	return frame;
}

ShareableParams ShareablePointsListIOJSon::fromjsonParams(const QJsonObject &jobj) const
{
	checkObject(jobj, "extraInfos", "Params");
	/* clang-format off */
	return ShareableParams{
		jobj.contains("precision") ? jobj["precision"].toInt() : 6,
		jobj.contains("coordsys") ? ShareableParams::coordsysFromString(jobj["coordsys"].toString().toUtf8().constData()) : ShareableParams::ECoordSys::unknown,
		jobj.contains("extraInfos") ? fromjsonInfo(jobj["extraInfos"].toObject()) : fromjsonInfo(QJsonObject())
	};
	/* clang-format on */
}

ShareablePoint ShareablePointsListIOJSon::fromjsonPoint(const QJsonObject &jobj) const
{
	if (jobj.contains("extraInfos"))
		checkObject(jobj, "extraInfos", "Point", toString(jobj["name"].toString(), utf8()));
	/* clang-format off */
	return ShareablePoint{
		jobj.contains("name") ? toString(jobj["name"].toString(), utf8()) : "",
		jobj.contains("position") ? fromjsonPosition(jobj["position"].toObject()) : fromjsonPosition(QJsonObject()),
		jobj.contains("inlineComment") ? toString(jobj["inlineComment"].toString(), utf8()) : "",
		jobj.contains("headerComment") ? toString(jobj["headerComment"].toString(), utf8()) : "",
		jobj.contains("active") ? jobj["active"].toBool() : true,
		jobj.contains("extraInfos") ? fromjsonInfo(jobj["extraInfos"].toObject()) : fromjsonInfo(QJsonObject()), nullptr
	};
	/* clang-format on */
}

ShareablePosition ShareablePointsListIOJSon::fromjsonPosition(const QJsonObject &jobj) const
{
	/* clang-format off */
	return ShareablePosition{
		jobj.contains("x") && !jobj["x"].isNull() ? jobj["x"].toDouble() : 0,
		jobj.contains("y") && !jobj["y"].isNull() ? jobj["y"].toDouble() : 0,
		jobj.contains("z") && !jobj["z"].isNull() ? jobj["z"].toDouble() : 0,
		jobj.contains("sigmax") && !jobj["sigmax"].isNull() ? jobj["sigmax"].toDouble() : 0,
		jobj.contains("sigmay") && !jobj["sigmay"].isNull() ? jobj["sigmay"].toDouble() : 0,
		jobj.contains("sigmaz") && !jobj["sigmaz"].isNull() ? jobj["sigmaz"].toDouble() : 0,
		jobj.contains("isfreex") ? jobj["isfreex"].toBool() : false,
		jobj.contains("isfreey") ? jobj["isfreey"].toBool() : false,
		jobj.contains("isfreez") ? jobj["isfreez"].toBool() : false
	};
	/* clang-format on */
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareablePointsList &list) const
{
	QJsonObject jobj;
	if (exportFieldsPointsList.hasField("title"))
		jobj["title"] = toQString(list.getTitle(), utf8());
	if (exportFieldsPointsList.hasField("params"))
		jobj["params"] = tojson(list.getParams());
	if (exportFieldsPointsList.hasField("rootFrame"))
		jobj["rootFrame"] = tojson(list.getRootFrame());
	return jobj;
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareableExtraInfos &info) const
{
	QJsonObject jobj;
	for (const auto &[key, value] : info.getMap())
		jobj[toQString(key, utf8())] = toQString(value, utf8());
	return jobj;
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareableFrame &frame) const
{
	QJsonObject jobj;
	if (exportFieldsFrame.hasField("name"))
		jobj["name"] = toQString(frame.getName(), utf8());
	if (exportFieldsFrame.hasField("translation"))
		jobj["translation"] = tojson(frame.getTranslation());
	if (exportFieldsFrame.hasField("rotation"))
		jobj["rotation"] = tojson(frame.getRotation());
	if (exportFieldsFrame.hasField("scale"))
		jobj["scale"] = frame.getScale();
	if (exportFieldsFrame.hasField("isfreescale"))
		jobj["isfreescale"] = frame.isFreeScale();
	// we serialize the lists with std::transform so in the future, we can use C++17 execution policy improvement to parallalize the loops for free!
	// points (leaves)
	if (exportFieldsFrame.hasField("points"))
	{
		QJsonArray points;
		std::transform(std::cbegin(frame.getPoints()), std::cend(frame.getPoints()), std::inserter(points, std::begin(points)),
			[this](const std::unique_ptr<ShareablePoint> &p) -> QJsonObject { return tojson(*p); });
		jobj["points"] = points;
	}
	// inner frames (nodes)
	if (exportFieldsFrame.hasField("innerFrames"))
	{
		QJsonArray frames;
		std::transform(std::cbegin(frame.getFrames()), std::cend(frame.getFrames()), std::inserter(frames, std::begin(frames)),
			[this](const std::unique_ptr<ShareableFrame> &f) -> QJsonObject { return tojson(*f); });
		jobj["innerFrames"] = frames;
	}
	return jobj;
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareableParams &params) const
{
	QJsonObject jobj;
	if (exportFieldsParams.hasField("precision"))
		jobj["precision"] = params.precision;
	if (exportFieldsParams.hasField("coordsys"))
		jobj["coordsys"] = toQString(ShareableParams::coordsysToString(params.coordsys), utf8());
	if (exportFieldsParams.hasField("extraInfos"))
		jobj["extraInfos"] = tojson(params.extraInfos);
	return jobj;
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareablePoint &point) const
{
	QJsonObject jobj;
	if (exportFieldsPoint.hasField("name"))
		jobj["name"] = toQString(point.name, utf8());
	if (exportFieldsPoint.hasField("position"))
		jobj["position"] = tojson(point.position);
	if (exportFieldsPoint.hasField("inlineComment"))
		jobj["inlineComment"] = toQString(point.inlineComment, utf8());
	if (exportFieldsPoint.hasField("headerComment"))
		jobj["headerComment"] = toQString(point.headerComment, utf8());
	if (exportFieldsPoint.hasField("active"))
		jobj["active"] = point.active;
	if (exportFieldsPoint.hasField("extraInfos"))
		jobj["extraInfos"] = tojson(point.extraInfos);
	return jobj;
}

QJsonObject ShareablePointsListIOJSon::tojson(const ShareablePosition &position) const
{
	QJsonObject jobj;
	if (exportFieldsPosition.hasField("x"))
		jobj["x"] = position.x;
	if (exportFieldsPosition.hasField("y"))
		jobj["y"] = position.y;
	if (exportFieldsPosition.hasField("z"))
		jobj["z"] = position.z;
	if (exportFieldsPosition.hasField("sigmax"))
		jobj["sigmax"] = position.sigmax;
	if (exportFieldsPosition.hasField("sigmay"))
		jobj["sigmay"] = position.sigmay;
	if (exportFieldsPosition.hasField("sigmaz"))
		jobj["sigmaz"] = position.sigmaz;
	if (exportFieldsPosition.hasField("isfreex"))
		jobj["isfreex"] = position.isfreex;
	if (exportFieldsPosition.hasField("isfreey"))
		jobj["isfreey"] = position.isfreey;
	if (exportFieldsPosition.hasField("isfreez"))
		jobj["isfreez"] = position.isfreez;
	return jobj;
}
