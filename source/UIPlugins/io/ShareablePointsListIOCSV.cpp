#include "ShareablePointsListIOCSV.hpp"

#include <algorithm>
#include <iomanip>
#include <limits>
#include <locale>
#include <memory>
#include <sstream>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>

namespace
{
inline std::string writeBool(bool b)
{
	return b ? "true" : "false";
}

inline bool readBool(std::string b)
{
	std::transform(
		b.begin(), b.end(), b.begin(), [](auto c) -> auto { return std::tolower(c, std::locale()); });
	return b == "true" ? true : false;
}

template<class T>
inline void addtostring(std::string &base, const char separator, const T &value)
{
	std::ostringstream sstr;
	if (!base.empty())
		sstr << separator;
	sstr << value;
	base += sstr.str();
}

template<>
inline void addtostring(std::string &base, const char separator, const double &value)
{
	std::ostringstream sstr;
	if (!base.empty())
		sstr << separator;
	sstr << std::setprecision(std::numeric_limits<double>::digits10) << value;
	base += sstr.str();
}
} // namespace

ShareablePointsList ShareablePointsListIOCSV::read(const std::string &contents)
{
	ShareablePointsList spl;

	if (!exportFieldsPointsList.hasField("rootFrame"))
		return spl;

	std::string text = contents;
	tryToReadHeader(text);
	if (text.empty())
		throw SPIOException("Error: empty contents", "", contents);
	ShareableFrame &frame = spl.getRootFrame();

	std::istringstream stream(text);
	std::string line;
	while (std::getline(stream, line))
	{
		line = trim(std::move(line));
		if (isEmpty(line))
			continue;
		frame.add(new ShareablePoint(consumePoint(line)));
	}

	return spl;
}

ShareableExtraInfos ShareablePointsListIOCSV::readExtraInfos(const std::string &contents)
{
	std::string copy = trim(contents);
	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;
	return consumeExtraInfos(copy);
}

ShareableFrame ShareablePointsListIOCSV::readFrame(const std::string &contents)
{
	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;
	ShareableFrame frame(std::make_shared<ShareableParams>());

	std::istringstream stream(contents);
	std::string line;
	while (std::getline(stream, line))
	{
		line = trim(std::move(line));
		if (isEmpty(line))
			continue;
		frame.add(new ShareablePoint(consumePoint(line)));
	}
	return frame;
}

ShareableParams ShareablePointsListIOCSV::readParams(const std::string &)
{
	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;
	return ShareableParams();
}

ShareablePoint ShareablePointsListIOCSV::readPoint(const std::string &contents)
{
	std::string copy = trim(contents);
	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;
	return consumePoint(copy);
}

ShareablePosition ShareablePointsListIOCSV::readPosition(const std::string &contents)
{
	std::string copy = trim(contents);
	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;
	return (consumePosition(copy));
}

std::string ShareablePointsListIOCSV::write(const ShareablePointsList &list)
{
	std::string result = writeHeaders() ? writePointHeaders(list.getParams()) + '\n' : "";
	return exportFieldsPointsList.hasField("rootFrame") ? result + write(list.getRootFrame()) : std::string();
}

std::string ShareablePointsListIOCSV::write(const ShareableExtraInfos &infos)
{
	std::string result;
	for (const auto &[key, info] : infos.getMap())
		addtostring(result, _separator, info);
	return writeText(result);
}

std::string ShareablePointsListIOCSV::write(const ShareableFrame &frame)
{
	if (!exportFieldsFrame.hasField("points"))
		return std::string();

	// template lambda bitch!
	auto writepoints = [this](const auto &points) -> std::string {
		std::string result;
		for (const auto &p : points)
			result += write(*p) + '\n';
		return result;
	};

	if (exportFieldsFrame.hasField("innerFrames"))
		return writepoints(frame.getAllPoints()); // vector of naked pointers
	return writepoints(frame.getPoints()); // vector of unique_ptr (to go faster)
}

std::string ShareablePointsListIOCSV::write(const ShareablePoint &point)
{
	std::string result;
	if (exportFieldsPoint.hasField("name"))
		addtostring(result, _separator, point.name);
	if (exportFieldsPoint.hasField("position"))
		addtostring(result, _separator, write(point.position));
	if (exportFieldsPoint.hasField("inlineComment"))
		addtostring(result, _separator, writeText(point.inlineComment));
	if (exportFieldsPoint.hasField("headerComment"))
		addtostring(result, _separator, writeText(point.headerComment));
	if (exportFieldsPoint.hasField("active"))
		addtostring(result, _separator, writeBool(point.active));
	if (exportFieldsPoint.hasField("extraInfos"))
		addtostring(result, _separator, write(point.extraInfos));
	return result;
}

std::string ShareablePointsListIOCSV::write(const ShareablePosition &position)
{
	std::string result;
	// x, y, z
	if (exportFieldsPosition.hasField("x"))
		addtostring(result, _separator, position.x);
	if (exportFieldsPosition.hasField("y"))
		addtostring(result, _separator, position.y);
	if (exportFieldsPosition.hasField("z"))
		addtostring(result, _separator, position.z);
	// sigma
	if (exportFieldsPosition.hasField("sigmax"))
		addtostring(result, _separator, position.sigmax);
	if (exportFieldsPosition.hasField("sigmay"))
		addtostring(result, _separator, position.sigmay);
	if (exportFieldsPosition.hasField("sigmaz"))
		addtostring(result, _separator, position.sigmaz);
	// free
	if (exportFieldsPosition.hasField("isfreex"))
		addtostring(result, _separator, writeBool(position.isfreex));
	if (exportFieldsPosition.hasField("isfreey"))
		addtostring(result, _separator, writeBool(position.isfreey));
	if (exportFieldsPosition.hasField("isfreez"))
		addtostring(result, _separator, writeBool(position.isfreez));
	return result;
}

void ShareablePointsListIOCSV::tryToReadHeader(std::string &contents)
{
	std::istringstream stream(contents);
	std::string line;
	size_t size = 0;
	Fields position(exportFieldsPosition.getAllFields());
	position.clear();
	Fields point(exportFieldsPoint.getAllFields());
	point.clear();

	_currentPosition = exportFieldsPosition;
	_currentPoint = exportFieldsPoint;

	while (std::getline(stream, line) && isEmpty(line))
		;
	size = stream.tellg();

	line = rtrim(std::move(line));
	std::string field;
	while (!isEmpty(line))
	{
		field = readValue(line);
		// Interpret the value
		size_t pos = field.find(" [m]");
		if (pos == 1)
			field = std::tolower(field[0]);
		if (field == "h")
			field = "z";

		if (position.getAllFields().find(field) != std::end(position.getAllFields()))
			position.addField(field);
		else if (point.getAllFields().find(field) != std::end(point.getAllFields()))
			point.addField(field);
	}

	if (!position.getFields().empty())
		point.addField("position");

	if (!position.getFields().empty() || !point.getFields().empty())
	{
		contents.erase(0, size);
		_currentPosition = std::move(position);
		_currentPoint = std::move(point);
	}
}

ShareableExtraInfos ShareablePointsListIOCSV::consumeExtraInfos(std::string &contents) const
{
	readValue(contents); // we read it to remove the value from the string, but we don't use it
	return ShareableExtraInfos();
}

ShareablePoint ShareablePointsListIOCSV::consumePoint(std::string &contents) const
{
	std::string copy = contents;

	try
	{
		/* clang-format off */
		return ShareablePoint{
			_currentPoint.hasField("name") ? readValue(contents) : "",
			_currentPoint.hasField("position") ? consumePosition(contents) : ShareablePosition{},
			_currentPoint.hasField("inlineComment") ? readValue(contents) : "",
			_currentPoint.hasField("headerComment") ? readValue(contents) : "",
			_currentPoint.hasField("active") ? readBool(readValue(contents)) : true,
			_currentPoint.hasField("extraInfos") ? consumeExtraInfos(contents) : ShareableExtraInfos{}
		};
		/* clang-format on */
	}
	catch (const std::exception &e)
	{
		throw SPIOException(std::string("Can't convert to a ShareablePoint: ") + e.what(), "", copy);
	}
}

ShareablePosition ShareablePointsListIOCSV::consumePosition(std::string &contents) const
{
	const std::string copy = contents;

	try
	{
		/* clang-format off */
		return ShareablePosition{
			// x, y, z
			_currentPosition.hasField("x") ? std::stod(readValue(contents)) : 0,
			_currentPosition.hasField("y") ? std::stod(readValue(contents)) : 0,
			_currentPosition.hasField("z") ? std::stod(readValue(contents)) : 0,
			// sigma
			_currentPosition.hasField("sigmax") ? std::stod(readValue(contents)) : 0,
			_currentPosition.hasField("sigmay") ? std::stod(readValue(contents)) : 0,
			_currentPosition.hasField("sigmaz") ? std::stod(readValue(contents)) : 0,
			// free
			_currentPosition.hasField("isfreex") ? readBool(readValue(contents)) : false,
			_currentPosition.hasField("isfreey") ? readBool(readValue(contents)) : false,
			_currentPosition.hasField("isfreez") ? readBool(readValue(contents)) : false
		};
		/* clang-format on */
	}
	catch (const std::invalid_argument &e)
	{
		throw SPIOException(std::string("Can't convert to a ShareablePosition: ") + e.what(), "", copy);
	}
}

std::string ShareablePointsListIOCSV::writePointHeaders(const ShareableParams &params)
{
	std::string headers;
	if (!exportFieldsFrame.hasField("points"))
		return headers;
	if (exportFieldsPoint.hasField("name"))
		addtostring(headers, _separator, "name");
	// position
	if (exportFieldsPoint.hasField("position"))
	{
		// x, y, z
		if (exportFieldsPosition.hasField("x"))
			addtostring(headers, _separator, "X [m]");
		if (exportFieldsPosition.hasField("y"))
			addtostring(headers, _separator, "Y [m]");
		if (exportFieldsPosition.hasField("z"))
			addtostring(headers, _separator, params.coordsys == ShareableParams::k3DCartesian ? "Z [m]" : "H [m]");
		// sigma
		if (exportFieldsPosition.hasField("sigmax"))
			addtostring(headers, _separator, "sigmax");
		if (exportFieldsPosition.hasField("sigmay"))
			addtostring(headers, _separator, "sigmay");
		if (exportFieldsPosition.hasField("sigmaz"))
			addtostring(headers, _separator, "sigmaz");
		// free
		if (exportFieldsPosition.hasField("isfreex"))
			addtostring(headers, _separator, "isfreex");
		if (exportFieldsPosition.hasField("isfreey"))
			addtostring(headers, _separator, "isfreey");
		if (exportFieldsPosition.hasField("isfreez"))
			addtostring(headers, _separator, "isfreez");
	}
	// point
	if (exportFieldsPoint.hasField("inlineComment"))
		addtostring(headers, _separator, "inlineComment");
	if (exportFieldsPoint.hasField("headerComment"))
		addtostring(headers, _separator, "headerComment");
	if (exportFieldsPoint.hasField("active"))
		addtostring(headers, _separator, "active");
	// extra infos
	if (exportFieldsPoint.hasField("extraInfos"))
		addtostring(headers, _separator, "extraInfos");
	return headers;
}

std::string ShareablePointsListIOCSV::writeText(const std::string &text) const
{
	std::string result;
	result.reserve(text.size() + 4); // we need at least 2 characters to add the delimiter. We assume we'll have to escape 2 characters

	result = _textdelimiter;
	for (const auto &c : text)
	{
		if (c == _textdelimiter)
			result += _textdelimiter;
		result += c;
	}
	result += _textdelimiter;

	return result;
}

std::string ShareablePointsListIOCSV::readValue(std::string &text) const
{
	char c;
	std::string result;
	size_t i = 0;

	// we ltrim text
	text = ltrim(std::move(text));
	result.reserve(text.size());
	bool intext = (text[0] == _textdelimiter);
	if (intext)
		i = 1;

	for (; i < text.size(); i++)
	{
		c = text[i];
		if (intext && c == _textdelimiter)
		{
			if (i == text.size() - 1 || text[i + 1] != _textdelimiter) // end of string
				break;
			result += c;
			i++;
			continue;
		}
		if (!intext && c == _separator)
			break;
		result += c;
	}
	if (intext)
	{
		if (text[i] != '"')
			throw SPIOException("Error: unbalanced quotes for text field", "", text);
		text.erase(std::begin(text));
	}
	text.erase(0, i);
	text = ltrim(std::move(text));
	if (!text.empty() && text[0] != _separator)
		throw SPIOException("Error: garbage at the end of a field", "", text);
	if (!text.empty())
		text.erase(std::begin(text));

	return result;
}
