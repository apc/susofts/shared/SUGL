/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef ISHAREABLEPOINTSLISTIOCSV_HPP
#define ISHAREABLEPOINTSLISTIOCSV_HPP

#include <memory>

#include <ShareablePoints/IShareablePointsListIO.hpp>

#include "PluginsGlobal.hpp"

class ShareableExtraInfos;
class ShareableFrame;
struct ShareableParams;
struct ShareablePoint;
class ShareablePointsList;
struct ShareablePosition;

/**
 * CSV serialization interface for Shareable Points.
 *
 * To be able to serialize a @ref shpoints object into CSV, you need to create a ShareablePointsListIOCSV object
 * and call the write() method with the object you want to serialize.
 *
 * Note that the CSV export is a lossy export: not all information can be exported to CSV. The main example is when you
 * serialize a ShareablePointsList, only the points are serialized. As the CSV format only allow for one table,
 * the parameters can't be exported. Same for all the frames (hierarchie, names, positions...). Only the points
 * are extracted, all at the same level.
 *
 * This is done without any transformations or convertion for the points that are inside frames with a specific position.
 * Thus, the positions may be false.
 *
 * Note also that ShareableExtraInfos can be exported, but they can't be loaded from CSV (because we may not know what do they represent).
 *
 * Finally, you can change the way the CSV file is written, by defining the separator character (default is the comma `,`),
 * the text delimiter character (default is the double quotes `"`) and if the headers should be written or not.
 */
class SUGL_SHARED_EXPORT ShareablePointsListIOCSV : public IShareablePointsListIO
{
public:
	virtual ~ShareablePointsListIOCSV() override = default;

	/**
	 * @return true if the serializer is configured to write the headers on the first line
	 * @see writeHeaders(bool)
	 */
	bool writeHeaders() const noexcept { return _writeHeaders; }
	/**
	 * Tells if we should write the headers in the first line or not (default is true).
	 * Note that headers are only written with write(const ShareablePointsList&).
	 * @params write tell if the headers should be written
	 * @see writeHeaders()
	 */
	void writeHeaders(bool write) noexcept { _writeHeaders = write; }
	/**
	 * @return the character used to separate fields in the file.
	 * @see setSeparator()
	 */
	char getSeparator() const noexcept { return _separator; }
	/**
	 * Sets the character used to separate fields in the file.
	 * @params separator the separator character
	 * @see getSeparator()
	 */
	void setSeparator(char separator) noexcept { _separator = separator; }
	/**
	 * @return the character used to delimit text fields.
	 * @note text fields can have the separator character inside
	 * @see setTextDelimiter()
	 */
	char getTextDelimiter() const noexcept { return _textdelimiter; }
	/**
	 * Sets the character used to delimit text fields.
	 * @params textdelimiter the delimiter character
	 * @see getTextDelimiter()
	 */
	void setTextDelimiter(char textdelimiter) noexcept { _textdelimiter = textdelimiter; }

	virtual const std::string &getMIMEType() const override { return _mimeType; }
	/** Because some software export CSV with the wrong MIME type, it is possible to change the MIME type of this class. */
	void changeMimeType(std::string mimeType) { _mimeType = std::move(mimeType); }

	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;

	virtual std::string write(const ShareablePointsList &list) override;
	virtual std::string write(const ShareableExtraInfos &infos) override;
	virtual std::string write(const ShareableFrame &frame) override;
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &point) override;
	virtual std::string write(const ShareablePosition &position) override;

protected:
	/** Try to read the line as a header and fill the Fields with the given fields, otherwise returns empty sets. First is position, second is point. */
	void tryToReadHeader(std::string &contents);
	/** Create a ShareableExtraInfos from the contents, and removes the description in contents. */
	ShareableExtraInfos consumeExtraInfos(std::string &contents) const;
	/** Create a ShareablePoint from the contents, and removes the description in contents. */
	ShareablePoint consumePoint(std::string &contents) const;
	/** Create a ShareablePosition from the contents, and removes the description in contents. */
	ShareablePosition consumePosition(std::string &contents) const;

	/** @return a string containing the headers for a ShareablePoint */
	std::string writePointHeaders(const ShareableParams &params);
	/** Surrounds the text with the _textdelimiter character, and escape all occurences of this character in the text */
	std::string writeText(const std::string &text) const;
	/** @return the first value from the text (up to the first comma), dealing with text delimiters... The value is removed from the input parameter. */
	std::string readValue(std::string &text) const;

protected:
	/** Tells if the headers should be written at the firt line */
	bool _writeHeaders = true;
	/** the character used to separate fields */
	char _separator = ',';
	/** the character used to delimit text fields */
	char _textdelimiter = '"';
	/** mime type */
	std::string _mimeType = "text/csv";
	/** current position field */
	Fields _currentPosition = exportFieldsPosition;
	/** current point field */
	Fields _currentPoint = exportFieldsPoint;
};

#endif // ISHAREABLEPOINTSLISTIOCSV_HPP
