#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>

#include "CooLexerIO.hpp"

namespace
{
constexpr char *BASEHEADER = R"""(#
# Coo Lexer Serializer from SUGL
#
#
# Copyright 2020, CERN EN-SMM-APC.
#
#)""";

constexpr char *OUT1HEADERZ = "%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%\n";

constexpr char *OUT1HEADERH = "%NOM X Y H ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%\n";
} // namespace

CooLexerIO::CooLexerIO(QObject *parent) : LexerParser(parent)
{
	commentChars("#");
}

const char *CooLexerIO::keywords(int) const
{
	return nullptr;
}

QString CooLexerIO::description(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::Normal:
		return "Normal";
	case Styles::Referential:
		return "Referential";
	case Styles::PointName:
		return "Point Name";
	case Styles::PointCoordinate:
		return "Point Coordinates";
	case Styles::PointSigma:
		return "Point Sigma";
	case Styles::PointSigmaKeyword:
		return "Point SigmaKeyword";
	case Styles::PointDisplacement:
		return "Point Displacement";
	case Styles::PointInfo:
		return "Point Informations";
	case Styles::PointType:
		return "Point Type";
	// Frames
	case Styles::Frame:
		return "Frame name";
	case Styles::FrameID:
		return "Frame ID";
	case Styles::FrameTranslation:
		return "Frame translation";
	case Styles::FrameRotation:
		return "Frame Rotation";
	}

	return QString();
}

const char *CooLexerIO::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*:!?[]()%";
}

bool CooLexerIO::defaultEolFill(int) const
{
	return false;
}

QColor CooLexerIO::defaultColor(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::Normal:
		return QColor(0, 43, 54, 255); // almost black
	case Styles::Referential:
		return Qt::GlobalColor::darkBlue;
	case Styles::PointName:
		return Qt::GlobalColor::darkCyan;
	case Styles::PointCoordinate:
	case Styles::FrameTranslation:
		return Qt::GlobalColor::magenta;
	case Styles::PointSigma:
	case Styles::PointSigmaKeyword:
	case Styles::PointDisplacement:
	case Styles::FrameRotation:
		return Qt::GlobalColor::darkMagenta;
	case Styles::PointInfo:
		return Qt::GlobalColor::darkBlue;
	case Styles::PointType:
		return QColor(181, 137, 0, 255); // brown
	case Styles::Frame:
		return Qt::GlobalColor::darkRed;
	case Styles::FrameID:
		return Qt::GlobalColor::blue;
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont CooLexerIO::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	switch (style)
	{
	case Styles::Default:
	case Styles::Normal:
	case Styles::PointCoordinate:
	case Styles::PointSigma:
	case Styles::PointDisplacement:
	case Styles::PointInfo:
	case Styles::FrameTranslation:
	case Styles::FrameRotation:
		return baseFont;
	case Styles::Comment:
		baseFont.setItalic(true);
		return baseFont;
	case Styles::Referential:
		baseFont.setItalic(true);
		[[fallthrough]];
	case Styles::PointName:
	case Styles::PointType:
	case Styles::PointSigmaKeyword:
	case Styles::Frame:
	case Styles::FrameID:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor CooLexerIO::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

const std::string &CooLexerIO::getMIMEType() const
{
	static const std::string mime = "application/vnd.cern.susoft.surveypad.cooPoint";
	return mime;
}

ShareablePointsList CooLexerIO::read(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents) + '\n';
	if (toparse[0] != '#')
		toparse = "# \n%\n" + toparse;

	parseString(toparse);
	return std::move(_curList);
}

ShareableExtraInfos CooLexerIO::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame CooLexerIO::readFrame(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents) + '\n';
	if (toparse.rfind("%NOM", 0) == std::string::npos)
	{
		if (_columnsHeader.isEmpty())
			toparse = "# \n" + std::string(OUT1HEADERZ) + toparse;
		else
			toparse = "# \n%\n" + _columnsHeader.toStdString() + "\n" + toparse;
	}

	parseString(toparse);
	return std::move(_curList.getRootFrame());
}

ShareableParams CooLexerIO::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint CooLexerIO::readPoint(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents) + '\n';
	if (_columnsHeader.isEmpty())
		toparse = "# \n" + std::string(OUT1HEADERZ) + toparse;
	else
		toparse = "# \n%\n" + _columnsHeader.toStdString() + "\n" + toparse;

	parseString(toparse);
	if (_curList.getRootFrame().getPoints().empty())
		throw SPIOException("No point read", "", std::move(toparse));
	auto &p = _curList.getRootFrame().getPoint(0);
	p.parent = nullptr;
	return std::move(p);
}

ShareablePosition CooLexerIO::readPosition(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents) + " 0 0 0 0 0 CALA\n";
	if (_columnsHeader.isEmpty())
		toparse = "# \n" + std::string(OUT1HEADERZ) + "dummy " + toparse;
	else
		toparse = "# \n%\n" + _columnsHeader.toStdString() + "\ndummy " + toparse;

	parseString(toparse);
	if (_curList.getRootFrame().getPoints().empty())
		throw SPIOException("No point read", "", std::move(toparse));
	return std::move(_curList.getRootFrame().getPoint(0).position);
}

std::string CooLexerIO::write(const ShareablePointsList &list)
{
	std::string result = BASEHEADER;

	if (list.getParams().extraInfos.has("coordinateSystem"))
		result += list.getParams().extraInfos.at("coordinateSystem");
	result += "\n%\n";

	if (list.getParams().coordsys == ShareableParams::ECoordSys::k2DCartesian || list.getParams().coordsys == ShareableParams::ECoordSys::k3DCartesian)
		result += OUT1HEADERZ;
	else
		result += OUT1HEADERH;

	result += write(list.getRootFrame());
	return result;
}

std::string CooLexerIO::write(const ShareableFrame &frame)
{
	std::string result;

	for (const auto &p : frame.getAllPoints())
		result += write(*p) + '\n';

	return result;
}

std::string CooLexerIO::write(const ShareablePoint &point)
{
	std::string result = point.name + ' ' + write(point.position) + ' ';

	// id
	if (point.extraInfos.has("id"))
		result += point.extraInfos.at("id") + ' ';
	else
		result += "-1 ";
	// dx dy dz
	result += "0 0 0 ";
	// dcum
	if (point.extraInfos.has("DCUM"))
		result += point.extraInfos.at("DCUM") + ' ';
	else
		result += "-1 ";
	// option
	if (!point.position.isfreex && !point.position.isfreey && !point.position.isfreez)
		result += "CALA";
	else if (point.position.isfreex && point.position.isfreey && point.position.isfreez)
		result += "VXYZ";
	else if (point.position.isfreex && point.position.isfreey && !point.position.isfreez)
		result += "VXY";
	else if (point.position.isfreex && !point.position.isfreey && point.position.isfreez)
		result += "VXZ";
	else if (!point.position.isfreex && point.position.isfreey && point.position.isfreez)
		result += "VYZ";
	else if (!point.position.isfreex && !point.position.isfreey && point.position.isfreez)
		result += "VZ";
	else
		result += "UNKNOWN";

	return result;
}

std::string CooLexerIO::write(const ShareablePosition &position)
{
	return std::to_string(position.x) + ' ' + std::to_string(position.y) + ' ' + std::to_string(position.z);
}
