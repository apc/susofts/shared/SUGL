#include "TextEditor.hpp"

#include <algorithm>
#include <bitset>

#include <QAction>
#include <QContextMenuEvent>
#include <QFont>
#include <QGuiApplication>
#include <QMenu>
#include <QMimeData>
#include <QShortcut>

#include <Qsci/qscicommand.h>
#include <Qsci/qscicommandset.h>

#include <Logger.hpp>
#include <utils/ShortcutsConfig.hpp>

#include "TextEditorConfig.hpp"
#include "TextLexer.hpp"
#include "TextUtils.hpp"
#include "utils/PointsExportConfig.hpp"
#include "utils/SettingsManager.hpp"
#include "utils/StylesHelper.hpp"

namespace
{
void initMarkers(QsciScintilla *editor)
{
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::ERROR).iconpath).scaled(14, 14), TextMarker::ERROR);
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::WARNING).iconpath).scaled(14, 14), TextMarker::WARNING);
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::INFO).iconpath).scaled(14, 14), TextMarker::INFO);
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::SEARCH).iconpath).scaled(14, 14), TextMarker::SEARCH);
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::USER1).iconpath).scaled(20, 20), TextMarker::USER1);
	editor->markerDefine(QPixmap(markerDefinitions.at(TextMarker::USER2).iconpath).scaled(20, 20), TextMarker::USER2);
}

void initIndicators(QsciScintilla *editor)
{
	// hover only
	editor->indicatorDefine(QsciScintilla::IndicatorStyle::HiddenIndicator, TextIndicator::HOVERONLY);
	editor->setIndicatorHoverStyle(QsciScintilla::IndicatorStyle::DotsIndicator, TextIndicator::HOVERONLY);
	// ctrl+click
	editor->indicatorDefine(QsciScintilla::IndicatorStyle::HiddenIndicator, TextIndicator::INTERNAL);
	editor->setIndicatorHoverStyle(QsciScintilla::IndicatorStyle::PlainIndicator, TextIndicator::INTERNAL);
	editor->setIndicatorHoverForegroundColor(Qt::GlobalColor::blue, TextIndicator::INTERNAL);
	editor->indicatorDefine(QsciScintilla::IndicatorStyle::HiddenIndicator, TextIndicator::EXTERNAL);
	editor->setIndicatorHoverStyle(QsciScintilla::IndicatorStyle::PlainIndicator, TextIndicator::EXTERNAL);
	editor->setIndicatorHoverForegroundColor(Qt::GlobalColor::blue, TextIndicator::EXTERNAL);
	// search
	editor->indicatorDefine(QsciScintilla::RoundBoxIndicator, TextIndicator::SEARCHRESULT); // Indicator type
	editor->setIndicatorForegroundColor(getThemeColor(QColor(173, 216, 230, 200)), TextIndicator::SEARCHRESULT); // Light blue
	editor->setIndicatorDrawUnder(true, TextIndicator::SEARCHRESULT);
	// selected
	editor->indicatorDefine(QsciScintilla::RoundBoxIndicator, TextIndicator::SELECTEDRESULT);
	editor->setIndicatorForegroundColor(getThemeColor(QColor(200, 240, 200, 200)), TextIndicator::SELECTEDRESULT); // Light green
	editor->setIndicatorDrawUnder(true, TextIndicator::SELECTEDRESULT);
}

void initMargins(QsciScintilla *editor)
{
	auto marginWidth = [](const QFont &font, const QString &text) -> int {
#if (QT_VERSION < QT_VERSION_CHECK(5, 11, 0))
		return QFontMetrics(font).width(text);
#else
		return QFontMetrics(font).horizontalAdvance(text);
#endif
	};

	constexpr int marginsize = 14;
	std::bitset<32> marginmask;
	// line number
	QFont fmargin("monospace");
	fmargin.setStyleHint(QFont::Monospace);
	fmargin.setPointSize(8);
	editor->setMarginsFont(fmargin);
	// Change the width of the line number margin when the text changes when the text changes
	QObject::connect(editor, &QsciScintilla::textChanged, [editor, fmargin, marginWidth]() {
		auto numberOfLines = QString::number(editor->lines());
		// It's append another digit to set the proper margin for visualize
		editor->setMarginWidth(TextMargin::LINENUMBER, marginWidth(fmargin, numberOfLines.append("0")));
	});
	// report
	const QColor lightGray = getThemeColor({224, 224, 224});
	const QColor darkGray = getThemeColor({200, 200, 200});
	editor->setMarginType(TextMargin::MARKERREPORT, QsciScintilla::MarginType::SymbolMarginColor);
	editor->setMarginBackgroundColor(TextMargin::MARKERREPORT, lightGray);
	editor->setMarginWidth(TextMargin::MARKERREPORT, marginsize);
	marginmask.reset();
	marginmask[TextMarker::ERROR] = true;
	marginmask[TextMarker::WARNING] = true;
	marginmask[TextMarker::INFO] = true;
	editor->setMarginMarkerMask(TextMargin::MARKERREPORT, (int)marginmask.to_ulong());
	// search
	editor->setMarginType(TextMargin::MARKERSEARCH, QsciScintilla::MarginType::SymbolMarginColor);
	editor->setMarginBackgroundColor(TextMargin::MARKERSEARCH, lightGray);
	editor->setMarginWidth(TextMargin::MARKERSEARCH, marginsize);
	marginmask.reset();
	marginmask[TextMarker::SEARCH] = true;
	editor->setMarginMarkerMask(TextMargin::MARKERSEARCH, (int)marginmask.to_ulong());
	// user
	editor->setMarginType(TextMargin::MARKERUSER, QsciScintilla::MarginType::SymbolMarginColor);
	editor->setMarginBackgroundColor(TextMargin::MARKERUSER, getThemeColor(Qt::GlobalColor::lightGray));
	editor->setMarginWidth(TextMargin::MARKERUSER, 20);
	editor->setMarginSensitivity(TextMargin::MARKERUSER, true);
	QObject::connect(editor, &QsciScintilla::marginClicked, [editor](int margin, int line, auto state) -> void {
		if (margin != TextMargin::MARKERUSER || (state != Qt::KeyboardModifier::NoModifier && state != Qt::KeyboardModifier::AltModifier))
			return;
		int marker1 = state == Qt::KeyboardModifier::AltModifier ? TextMarker::USER2 : TextMarker::USER1;
		int marker2 = state == Qt::KeyboardModifier::AltModifier ? TextMarker::USER1 : TextMarker::USER2;
		std::bitset<32> markers = editor->markersAtLine(line);
		if (markers[marker1])
			editor->markerDelete(line, marker1);
		else
		{
			editor->markerAdd(line, marker1);
			editor->markerDelete(line, marker2);
		}
	});
	marginmask.reset();
	marginmask[TextMarker::USER1] = true;
	marginmask[TextMarker::USER2] = true;
	editor->setMarginMarkerMask(TextMargin::MARKERUSER, (int)marginmask.to_ulong());
	// folding
	// already set in resetEditor()
	// margin colors
	editor->setMarginsForegroundColor(getThemeColor(Qt::GlobalColor::black)); // text
	editor->setMarginsBackgroundColor(lightGray); // text
	editor->setFoldMarginColors(darkGray, lightGray);
}

void initAutoCompletion(QsciScintilla *editor)
{
	editor->setAutoCompletionSource(QsciScintilla::AutoCompletionSource::AcsAPIs);
	editor->setAutoCompletionUseSingle(QsciScintilla::AutoCompletionUseSingle::AcusExplicit);
	QObject::connect(new QShortcut(QKeySequence(ShortcutsConfig::getShortcutConfig().ShortcutMap[ShortcutsConfigObject::AUTOCOMPLETE].second), editor),
		&QShortcut::activated, editor, &QsciScintilla::autoCompleteFromAPIs);
}

void initCallTips(QsciScintilla *editor)
{
	editor->setCallTipsForegroundColor(Qt::GlobalColor::black);
	editor->setCallTipsBackgroundColor(Qt::GlobalColor::lightGray);
	editor->SendScintilla(QsciScintilla::SCI_SETMOUSEDWELLTIME, 500);
	QObject::connect(editor, &QsciScintillaBase::SCN_DWELLEND, [editor](int, int, int) -> void { editor->SendScintilla(QsciScintilla::SCI_CALLTIPCANCEL); });
}

void initIndentation(QsciScintilla *editor)
{
	editor->setIndentationGuides(true);
	editor->setAutoIndent(true);
}

void initZoom(QsciScintilla *editor)
{
	// Enable horizontal scroll width tracking
	editor->SendScintilla(QsciScintillaBase::SCI_SETSCROLLWIDTHTRACKING, true);

	QObject::connect(editor, &QsciScintillaBase::SCN_ZOOM, [editor]() -> void {
		auto &settings = SettingsManager::settings();
		auto *conf = new TextEditorConfigObject(settings.settings<TextEditorConfigObject>(TextEditorConfig::configName()));

		conf->zoom = editor->SendScintilla(QsciScintilla::SCI_GETZOOM);
		conf->write();
		settings.settings(TextEditorConfig::configName(), conf);
	});
}

void initLineMove(QsciScintilla *editor)
{
	auto *commands = editor->standardCommands();
	auto *commandUp = commands->find(QsciCommand::MoveSelectedLinesUp);
	auto *commandDown = commands->find(QsciCommand::MoveSelectedLinesDown);

	if (commandUp && commandDown)
	{
		commandUp->setKey(Qt::Key_Up | Qt::ShiftModifier | Qt::ControlModifier);
		commandDown->setKey(Qt::Key_Down | Qt::ShiftModifier | Qt::ControlModifier);
	}
}

void resetEditor(QsciScintilla *editor)
{
	if (!editor)
		return;

	if (!editor->lexer())
	{
		// annotations
		editor->clearAnnotations();

		// markers
		editor->markerDeleteAll(TextMarker::ERROR);
		editor->markerDeleteAll(TextMarker::WARNING);
		editor->markerDeleteAll(TextMarker::INFO);

		// indicators
		clearIndications(editor, TextIndicator::HOVERONLY);
		clearIndications(editor, TextIndicator::INTERNAL);
		clearIndications(editor, TextIndicator::EXTERNAL);

		// font
		QFont font("Courier New", 10);
		font.setStyleHint(QFont::StyleHint::Monospace);
		font.setWeight(QFont::Weight::Medium);
		editor->setFont(font); // set font for DEFAULT_STYLE
		// SCI_STYLECLEARALL does NOT clear styles... It reset all the styles to look like DEFAULT_STYLE
		// if there was a lexer before, we keep the style numbers, so we have to put all styles to DEFAULT_STYLE
		editor->SendScintilla(QsciScintilla::SCI_STYLECLEARALL);
		// folding
		editor->setFolding(QsciScintilla::FoldStyle::NoFoldStyle, TextMargin::FOLDING);
	}
	else if (editor->folding() == QsciScintilla::FoldStyle::NoFoldStyle)
	{
		// folding
		auto config = SettingsManager::settings().settings<TextEditorConfigObject>(TextEditorConfig::configName());
		editor->setFolding((QsciScintilla::FoldStyle)config.foldStyle, TextMargin::FOLDING);
	}
}
} // namespace

class TextEditor::_TextEditor_pimpl
{
public:
	std::function<QByteArray(const QMimeData *)> fromMime;
	std::function<QMimeData *(const QByteArray &, QMimeData *)> toMime;
	std::set<std::string> supportedMime;
	std::vector<QAction *> contextualActions;
};

TextEditor::TextEditor(QWidget *parent) : QsciScintilla(parent), _pimpl(std::make_unique<_TextEditor_pimpl>())
{
	// default font
	resetEditor(this);
	// other initialisation
	initMarkers(this);
	initIndicators(this);
	initMargins(this);
	initAutoCompletion(this);
	initCallTips(this);
	initIndentation(this);
	initZoom(this);
	initLineMove(this);

	// charset
	setEolMode(EolMode::EolUnix);
#ifdef _WIN32
	setUtf8(false);
#else
	setUtf8(true);
#endif
	// cursor colour
	setCaretForegroundColor(getThemeColor({0, 0, 0}));
	// selection highlighting
	connect(this, &QsciScintilla::selectionChanged, this, &TextEditor::highlightSelected);
	// prevent focus on wheel events
	setFocusPolicy(Qt::StrongFocus);
	// column edition
	SendScintilla(QsciScintilla::SCI_SETMULTIPLESELECTION, false);
	SendScintilla(QsciScintilla::SCI_SETADDITIONALSELECTIONTYPING, true);
	SendScintilla(QsciScintilla::SCI_SETMULTIPASTE, true);
	// jump to caret centering - the proposed settings places the caret somewhere in the middle of the window
	SendScintilla(QsciScintilla::SCI_SETYCARETPOLICY, QsciScintilla::CARET_SLOP | QsciScintilla::CARET_EVEN | QsciScintilla::CARET_STRICT, 18);
	SendScintilla(QsciScintilla::SCI_SETVISIBLEPOLICY, QsciScintilla::CARET_SLOP | QsciScintilla::CARET_EVEN | QsciScintilla::CARET_STRICT, 18);
	// config
	connect(&SettingsManager::settings(), &SettingsManager::settingsChanged, this, &TextEditor::updateUi);
	try
	{
		updateUi(TextEditorConfig::configName());
	}
	catch (...)
	{
		logDebug() << "No config for text editor!!!";
	}
}

void TextEditor::setLexer(QsciLexer *newlex)
{
	const auto *oldlex = lexer();
	QsciScintilla::setLexer(newlex);
	if (newlex != oldlex)
		resetEditor(this);
	// signals
	const auto *splex = qobject_cast<const TextLexer *>(oldlex);
	if (splex)
		disconnect(splex, &TextLexer::globalIndicatorClicked, this, &TextEditor::globalIndicatorClicked);
	splex = qobject_cast<TextLexer *>(newlex);
	if (splex)
		connect(splex, &TextLexer::globalIndicatorClicked, this, &TextEditor::globalIndicatorClicked);
}

void TextEditor::updateUi(const QString &config)
{
	if (config != TextEditorConfig::configName())
		return;
	auto conf = SettingsManager::settings().settings<TextEditorConfigObject>(config);

	// caret
	setCaretLineVisible(conf.caretLineVisible);
	setCaretLineBackgroundColor(conf.caretLineBg);
	// Selection Result
	setIndicatorForegroundColor(conf.SelectedResultBg, TextIndicator::SELECTEDRESULT);
	// whitespace
	setWhitespaceVisibility((QsciScintilla::WhitespaceVisibility)conf.whitespaceVisible);
	setWhitespaceSize(conf.whitespaceSize);
	if (whitespaceVisibility() > QsciScintilla::WhitespaceVisibility::WsInvisible)
		setWhitespaceForegroundColor(conf.whitespaceFg);
	// autocompletion
	setAutoCompletionCaseSensitivity(conf.acCaseSensitive);
	setAutoCompletionThreshold(conf.acThreshold);
	// misc
	if (conf.foldStyle == QsciScintilla::FoldStyle::NoFoldStyle)
		SendScintilla(QsciScintilla::SCI_FOLDALL, QsciScintilla::SC_FOLDACTION_EXPAND);
	setFolding((QsciScintilla::FoldStyle)conf.foldStyle, TextMargin::FOLDING);
	setBraceMatching((QsciScintilla::BraceMatch)conf.braceMatching);
	setAnnotationDisplay((QsciScintilla::AnnotationDisplay)conf.annotationDisplay);
	setTabWidth(conf.tabSize);

	blockSignals(true);
	zoomTo(conf.zoom);
	blockSignals(false);
}

TextEditor::~TextEditor() = default;

void TextEditor::contextMenuEvent(QContextMenuEvent *event)
{
	if (!contextMenuNeeded(event->x(), event->y()))
		return;

	auto *menu(createContextMenu());
	menu->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
	menu->popup(event->globalPos());
}

QMenu *TextEditor::createContextMenu()
{
	std::unique_ptr<QMenu> menu(createStandardContextMenu());
	// special copy
	auto *actCpy = new QAction(QIcon(":/actions/special-copy"), "Special Copy", menu.get());
	actCpy->setEnabled(hasSelectedText());
	connect(actCpy, &QAction::triggered, this, &TextEditor::specialCopy);
	menu->addSeparator();
	menu->addAction(actCpy);
	// additional actions
	menu->addSeparator();
	for (auto *a : _pimpl->contextualActions)
		menu->addAction(a);

	return menu.release();
}

void TextEditor::additionalContextualActions(std::vector<QAction *> actions) noexcept
{
	_pimpl->contextualActions = std::move(actions);
}

const std::vector<QAction *> &TextEditor::additionalContextualActions() const noexcept
{
	return _pimpl->contextualActions;
}

void TextEditor::jumpTo(int position, bool isLine)
{
	int line = -1;
	if (isLine)
	{
		SendScintilla(QsciScintillaBase::SCI_GOTOLINE, position - 1);
		line = position - 1;
	}
	else
	{
		SendScintilla(QsciScintillaBase::SCI_GOTOPOS, position);
		int index = -1;
		lineIndexFromPosition(position, &line, &index);
	}
	ensureLineVisible(line);
	setFocus();
}

void TextEditor::specialCopy()
{
	auto copyConfig = PointsExportConfig::showCopyDialog();
	if (!copyConfig)
		return;
	auto appConfig = PointsExportConfig::getAppPointsConfig();
	blockSignals(true);
	SettingsManager::settings().settings(PointsExportConfig::configName(), copyConfig);
	copy();
	SettingsManager::settings().settings(PointsExportConfig::configName(), appConfig);
	blockSignals(false);
}

void TextEditor::setMimeTypesSupport(std::set<std::string> mimeTypes,
	std::function<QByteArray(const QMimeData *)> fromMime,
	std::function<QMimeData *(const QByteArray &, QMimeData *)> toMime) noexcept
{
	_pimpl->supportedMime = std::move(mimeTypes);
	_pimpl->fromMime = std::move(fromMime);
	_pimpl->toMime = std::move(toMime);
}

QString TextEditor::annotation(int line) const
{
	// Fixing QScintilla regression
	int size = SendScintilla(SCI_ANNOTATIONGETTEXT, line, (const char *)0);
	char *buf = new char[size + 1];
	buf[SendScintilla(SCI_ANNOTATIONGETTEXT, line, buf)] = '\0';

	QString qs = bytesAsText(buf, size);
	delete[] buf;

	return qs;
}

void TextEditor::mouseMoveEvent(QMouseEvent *event)
{
	QsciScintilla::mouseMoveEvent(event);
	SendScintilla(SCI_CALLTIPCANCEL);
}

void TextEditor::focusOutEvent(QFocusEvent *event)
{
	QsciScintilla::focusOutEvent(event);
	SendScintilla(SCI_CALLTIPCANCEL);
}

void TextEditor::dragEnterEvent(QDragEnterEvent *event)
{
	if (!canInsertFromMimeData(event->mimeData()))
	{
		QWidget::dragEnterEvent(event);
		return;
	}
	QsciScintilla::dragEnterEvent(event);
}

void TextEditor::dropEvent(QDropEvent *event)
{
	if (!canInsertFromMimeData(event->mimeData()))
	{
		QWidget::dropEvent(event);
		return;
	}
	QsciScintilla::dropEvent(event);
}

bool TextEditor::canInsertFromMimeData(const QMimeData *source) const
{
	bool can = false;
	for (const auto &mimetype : _pimpl->supportedMime)
	{
		if (source->hasFormat(QString::fromStdString(mimetype)))
		{
			can = true;
			break;
		}
	}
	return can || QsciScintilla::canInsertFromMimeData(source);
}

QByteArray TextEditor::fromMimeData(const QMimeData *source, bool &rectangular) const
{
	QByteArray str;
	if (_pimpl->fromMime)
		str = _pimpl->fromMime(source);
	// It is required for proper behavior of paste - it ensures that caret is moved properly and that pasting multi-line selections will in fact paste and not append the
	// contents, see  the issue: https://its.cern.ch/jira/browse/SUS-1358
	QByteArray qContents = QsciScintilla::fromMimeData(source, rectangular);
	if (!str.isEmpty())
		return str;
	return qContents;
}

QMimeData *TextEditor::toMimeData(const QByteArray &text, bool rectangular) const
{
	auto *mimeType = QsciScintilla::toMimeData(text, rectangular);
	return _pimpl->toMime ? _pimpl->toMime(text, mimeType) : mimeType;
}

void TextEditor::highlightSelected()
{
	clearIndications(this, TextIndicator::SELECTEDRESULT);
	// Check for Alt key modifier - otherwise while using alt it may cause memory corruption on calling `selectedText()`
	if (!hasSelectedText() || QGuiApplication::keyboardModifiers() & Qt::KeyboardModifier::AltModifier)
		return;
	const QString searched = selectedText();
	if (std::any_of(std::cbegin(searched), std::cend(searched), [](const QChar &c) { return c.isSpace(); }))
		return;
	const int lenSearched = searched.length();
	const QString contents = text();
	int currStart = contents.indexOf(searched);
	while (currStart != -1)
	{
		addIndication(this, TextIndicator::SELECTEDRESULT, currStart, lenSearched);
		currStart = contents.indexOf(searched, currStart + 1);
	}
}
