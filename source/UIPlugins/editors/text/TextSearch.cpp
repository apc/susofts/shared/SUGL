#include "TextSearch.hpp"
#include "ui_TextSearch.h"

#include <QCompleter>
#include <QKeyEvent>
#include <QLineEdit>
#include <QRegularExpression>
#include <QSettings>
#include <QStringListModel>

#include <Qsci/qsciscintilla.h>

#include <Logger.hpp>

#include "QtStreams.hpp"
#include "TextUtils.hpp"
#include "interface/SConfigInterface.hpp"
#include "utils/SettingsManager.hpp"

namespace
{
struct TextSearchConfigObject : SConfigObject
{
	TextSearchConfigObject() = default;
	TextSearchConfigObject(const TextSearchConfigObject &o) = default;
	TextSearchConfigObject(TextSearchConfigObject &&o) = default;
	virtual ~TextSearchConfigObject() override = default;

	TextSearchConfigObject &operator=(const TextSearchConfigObject &o) = default;
	TextSearchConfigObject &operator=(TextSearchConfigObject &&o) = default;
	bool operator==(const SConfigObject &o) const noexcept override
	{
		if (!SConfigObject::operator==(o))
			return false;
		const TextSearchConfigObject &oc = static_cast<const TextSearchConfigObject &>(o);
		return findText == oc.findText && replaceText == oc.replaceText && searchHistory == oc.searchHistory && replaceHistory == oc.replaceHistory && replace == oc.replace
			&& inSelection == oc.inSelection && wholeWord == oc.wholeWord && matchCase == oc.matchCase && showAdvanced == oc.showAdvanced && mode == oc.mode;
	}
	bool operator!=(const TextSearchConfigObject &o) { return !(*this == o); }

	virtual void write() const override
	{
		QSettings settings;
		settings.beginGroup(name);

		// Settings
		settings.setValue("findText", findText);
		settings.setValue("replaceText", replaceText);
		settings.setValue("searchHistory", searchHistory);
		settings.setValue("replaceHistory", replaceHistory);
		settings.setValue("replace", replace);
		settings.setValue("inSelection", inSelection);
		settings.setValue("wholeWord", wholeWord);
		settings.setValue("matchCase", matchCase);
		settings.setValue("showAdvanced", showAdvanced);
		settings.setValue("mode", (int)mode);

		settings.endGroup();
	}
	virtual void read() override
	{
		QSettings settings;
		settings.beginGroup(name);

		// Settings
		findText = settings.value("findText", "").toString();
		replaceText = settings.value("replaceText", "").toString();
		searchHistory = settings.value("searchHistory", "").toStringList();
		replaceHistory = settings.value("replaceHistory", "").toStringList();
		replace = settings.value("replace", false).toBool();
		inSelection = settings.value("inSelection", false).toBool();
		wholeWord = settings.value("wholeWord", false).toBool();
		matchCase = settings.value("matchCase", false).toBool();
		showAdvanced = settings.value("showAdvanced", false).toBool();
		mode = (SearchMode)settings.value("mode", (int)SearchMode::Normal).toInt();

		settings.endGroup();
	}

	// search mode
	enum SearchMode
	{
		Normal,
		Extended,
		RegExp
	};

	// attributes:
	static inline const QString name = "TextSearch";
	QString findText;
	QString replaceText;
	QStringList searchHistory;
	QStringList replaceHistory;
	bool replace = false;
	bool inSelection = false;
	bool wholeWord = false;
	bool matchCase = false;
	bool showAdvanced = false;
	SearchMode mode = SearchMode::Normal;
};

void unescapeString(QString &str)
{
	str.replace("\\n", "\n");
	str.replace("\\r", "\r");
	str.replace("\\t", "\t");
	str.replace("\\b", "\b");
	str.replace("\\f", "\f");
	str.replace("\\v", "\v");
}

// Insert the item to QComboBox if not yet inserted (possible in case of mouse clicks)
void insertRecord(QComboBox *cb)
{
	if (cb->lineEdit()->text() != cb->itemData(0, Qt::DisplayRole).toString())
	{
		cb->insertItem(0, cb->lineEdit()->text());
		cb->setCurrentIndex(0);
	}
}
} // namespace

class TextSearch::_TextSearch_pimpl
{
public:
	std::unique_ptr<Ui::TextSearch> ui;
	/** Editor to get information from */
	QsciScintilla *editor;
	/** Tell if the search has already been made */
	bool ongoing = false;
	/** Tell the direction of the search */
	bool totheleft = false;
};

TextSearch::TextSearch(QsciScintilla *editor, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_TextSearch_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::TextSearch>();
	_pimpl->editor = editor;

	_pimpl->ui->setupUi(this);
	_pimpl->ui->lblResults->clear();
	_pimpl->ui->searchBox->completer()->setCaseSensitivity(Qt::CaseSensitive);
	_pimpl->ui->searchBox->setInsertPolicy(QComboBox::InsertAtTop);
	_pimpl->ui->replaceBox->setInsertPolicy(QComboBox::InsertAtTop);
	_pimpl->ui->gbAdvanced->hide();
	// Set QComboBox model
	_pimpl->ui->searchBox->setModel(new QStringListModel());
	_pimpl->ui->replaceBox->setModel(new QStringListModel());
	// Set event filters
	installEventFilter(this);
	_pimpl->ui->searchBox->installEventFilter(this);
	_pimpl->ui->replaceBox->installEventFilter(this);
	_pimpl->editor->viewport()->installEventFilter(this);

	// Connections
	connect(_pimpl->ui->searchBox->lineEdit(), &QLineEdit::returnPressed, [this]() { findNext(); });
	connect(_pimpl->ui->replaceBox->lineEdit(), &QLineEdit::returnPressed, [this]() { findNext(); });
}

TextSearch::~TextSearch() = default;

bool TextSearch::eventFilter(QObject *watched, QEvent *event)
{
	if (event->type() == QEvent::Type::KeyPress)
	{
		QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
		// Backwards search
		if ((watched == _pimpl->ui->searchBox || watched == _pimpl->ui->replaceBox) && keyEvent->modifiers() & Qt::ShiftModifier
			&& (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return))
		{
			findPrevious();
			return true;
		}
		// Escape - hide
		if (keyEvent->key() == Qt::Key_Escape)
		{
			hide();
			return true;
		}
	}
	return QWidget::eventFilter(watched, event);
}

void TextSearch::findNext()
{
	find(false);
}

void TextSearch::findPrevious()
{
	find(true);
}

void TextSearch::replace()
{
	insertRecord(_pimpl->ui->replaceBox);
	if (_pimpl->ongoing)
	{
		QString replacewith = _pimpl->ui->replaceBox->currentText();
		if (!_pimpl->ui->rbtNormal->isChecked())
			unescapeString(replacewith);
		_pimpl->editor->replace(replacewith);
		_pimpl->ongoing = _pimpl->editor->findNext();
		highlightAll();
	}
}

void TextSearch::replaceAll()
{
	QString text = _pimpl->ui->chkInSelection->isChecked() && _pimpl->editor->hasSelectedText() ? _pimpl->editor->selectedText() : _pimpl->editor->text();
	if (text.isEmpty())
	{
		_pimpl->ui->lblResults->setText(tr("Nothing replaced."));
		_pimpl->ui->lblResults->setStyleSheet("color: red;");
		return;
	}

	insertRecord(_pimpl->ui->replaceBox);

	// find number of replacements
	size_t nbocc = highlightAll(true);
	if (nbocc == 0)
	{
		_pimpl->ui->lblResults->setText(tr("Nothing replaced."));
		_pimpl->ui->lblResults->setStyleSheet("color: red;");
		return;
	}
	else
	{
		_pimpl->ui->lblResults->setText(tr("Replaced %1 occurrences.").arg(nbocc));
		_pimpl->ui->lblResults->setStyleSheet("color: green;");
	}

	QString toSearch = _pimpl->ui->searchBox->currentText();
	QString replacewith = _pimpl->ui->replaceBox->currentText();
	// If whole word
	if (_pimpl->ui->chkWord->isChecked())
		toSearch = QString(R"""(\b%1\b)""").arg(toSearch);

	// Search mode
	if (!_pimpl->ui->rbtNormal->isChecked())
	{
		unescapeString(toSearch);
		unescapeString(replacewith);
	}
	if (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked())
		text.replace(QRegularExpression(toSearch,
						 _pimpl->ui->chkCase->isChecked() ? (QRegularExpression::NoPatternOption | QRegularExpression::MultilineOption)
														  : (QRegularExpression::CaseInsensitiveOption | QRegularExpression::MultilineOption)),
			replacewith);
	else
		text.replace(toSearch, replacewith, _pimpl->ui->chkCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive);

	// Replace in selection/all
	if (_pimpl->ui->chkInSelection->isChecked() && _pimpl->editor->hasSelectedText())
		_pimpl->editor->replaceSelectedText(text);
	else
	{
		const int firstLineVisible = _pimpl->editor->SendScintilla(QsciScintilla::SCI_GETFIRSTVISIBLELINE);
		_pimpl->editor->SendScintilla(QsciScintilla::SCI_SETTEXT, toByteArray(text, _pimpl->editor->isUtf8()).constData()); // we don't use `setText()` because we want to keep the history
		_pimpl->editor->SendScintilla(QsciScintilla::SCI_SETFIRSTVISIBLELINE, firstLineVisible);
	}
}

void TextSearch::markAll()
{
	const QString text = _pimpl->ui->chkInSelection->isChecked() && _pimpl->editor->hasSelectedText() ? _pimpl->editor->selectedText() : _pimpl->editor->text();

	int offset = _pimpl->ui->chkInSelection->isChecked() && _pimpl->editor->hasSelectedText() ? _pimpl->editor->SendScintilla(QsciScintilla::SCI_GETSELECTIONSTART) : 0;
	QString toSearch = _pimpl->ui->searchBox->currentText();
	// If whole word
	if (_pimpl->ui->chkWord->isChecked())
		toSearch = QString(R"""(\b%1\b)""").arg(toSearch);

	// Search mode
	if (!_pimpl->ui->rbtNormal->isChecked())
		unescapeString(toSearch);

	// Search
	int pos = -1;
	const auto regex = (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked()) ? QRegularExpression{toSearch,
						   _pimpl->ui->chkCase->isChecked() ? (QRegularExpression::NoPatternOption | QRegularExpression::MultilineOption)
															: (QRegularExpression::CaseInsensitiveOption | QRegularExpression::MultilineOption)}
																								: QRegularExpression{};
	size_t nbocc = 0;
	do
	{
		pos++;
		if (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked())
			pos = text.indexOf(regex, pos);
		else
			pos = text.indexOf(toSearch, pos, _pimpl->ui->chkCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive);
		if (pos >= 0)
		{
			addMarker(_pimpl->editor, pos + offset, TextMarker::SEARCH);
			nbocc++;
		}
	} while (pos >= 0);
	if (nbocc == 0)
	{
		_pimpl->ui->lblResults->setText(tr("Can't find \"%1\".").arg(toSearch));
		_pimpl->ui->lblResults->setStyleSheet("color: black;");
	}
	else
	{
		_pimpl->ui->lblResults->setText(tr("Marked %1 occurrences.").arg(nbocc));
		_pimpl->ui->lblResults->setStyleSheet("color: black;");
	}
}

void TextSearch::removeMarkers()
{
	_pimpl->editor->markerDeleteAll(TextMarker::SEARCH);
	_pimpl->ui->lblResults->setText(tr("Removed all search markers."));
	_pimpl->ui->lblResults->setStyleSheet("color: black;");
}

void TextSearch::setSearchMode(bool modeReplace)
{
	_pimpl->ui->chkReplace->setChecked(modeReplace);
}

void TextSearch::hideEvent(QHideEvent *event)
{
	QWidget::hideEvent(event);
	auto *conf = config();
	SettingsManager::settings().settings(TextSearchConfigObject::name, conf);
	// Return focus to TextEditor
	_pimpl->editor->setFocus();
	cancelSearch();
}

void TextSearch::showEvent(QShowEvent *event)
{
	QWidget::showEvent(event);
	updateUi(TextSearchConfigObject::name);
	// If there is some text selected - put it in the search box.
	// There is a bug related to setting a new record in the QComboBox and this is the only working solution that was found for now.
	// Unfortunate downside is that the item is inserted into the history before it is searched - that is why this functionality is limited to text without newline.
	if (_pimpl->editor->hasSelectedText() && !_pimpl->editor->selectedText().isEmpty() && !_pimpl->editor->selectedText().contains('\n')
		&& _pimpl->ui->searchBox->currentText() != _pimpl->editor->selectedText())
	{
		_pimpl->ui->searchBox->insertItem(0, _pimpl->editor->selectedText());
		_pimpl->ui->searchBox->setCurrentIndex(0);
		_pimpl->ui->searchBox->lineEdit()->setText(_pimpl->editor->selectedText());
	}
	_pimpl->ui->searchBox->lineEdit()->selectAll();
	_pimpl->ui->searchBox->lineEdit()->setFocus();
}

void TextSearch::find(bool totheleft, int line, int index)
{
	highlightAll();
	if (_pimpl->ongoing && _pimpl->totheleft == totheleft)
	{
		_pimpl->ongoing = _pimpl->editor->findNext();
		return;
	}

	insertRecord(_pimpl->ui->searchBox);
	QString toSearch = _pimpl->ui->searchBox->currentText();
	if (_pimpl->ui->rbtExtended->isChecked())
		unescapeString(toSearch);
	// If whole word
	if (_pimpl->ui->rbtRegExp->isChecked() && _pimpl->ui->chkWord->isChecked())
		toSearch = QString(R"""(\b%1\b)""").arg(toSearch);

	if (_pimpl->ui->chkInSelection->isChecked())
	{
		_pimpl->ongoing = _pimpl->editor->findFirstInSelection(toSearch,
			_pimpl->ui->rbtRegExp->isChecked(), // regexp
			_pimpl->ui->chkCase->isChecked(), // case
			_pimpl->ui->rbtRegExp->isChecked() ? false : _pimpl->ui->chkWord->isChecked(), // whole word
			!totheleft, // forward
			true, // show
			false, // POSIX regexp
			true // Cxx11 regexp
		);
	}
	else
	{
		_pimpl->ongoing = _pimpl->editor->findFirst(toSearch,
			_pimpl->ui->rbtRegExp->isChecked(), // regexp
			_pimpl->ui->chkCase->isChecked(), // case
			_pimpl->ui->rbtRegExp->isChecked() ? false : _pimpl->ui->chkWord->isChecked(), // whole word
			true, // wrap
			!totheleft, // forward
			line, // line
			index, // index
			true, // show
			false, // POSIX regexp
			true // Cxx11 regexp
		);
	}
	_pimpl->totheleft = totheleft;
	logDebug() << "TextSearch: Searching for" << toSearch;
}

SConfigObject *TextSearch::config() const
{
	auto *config = new TextSearchConfigObject();

	auto listSModel = qobject_cast<QStringListModel *>(_pimpl->ui->searchBox->model());
	if (listSModel)
		config->searchHistory = listSModel->stringList();
	auto listRModel = qobject_cast<QStringListModel *>(_pimpl->ui->replaceBox->model());
	if (listRModel)
		config->replaceHistory = listRModel->stringList();

	config->findText = _pimpl->ui->searchBox->currentText();
	config->replaceText = _pimpl->ui->replaceBox->currentText();
	config->replace = _pimpl->ui->chkReplace->isChecked();
	config->inSelection = _pimpl->ui->chkInSelection->isChecked();
	config->wholeWord = _pimpl->ui->chkWord->isChecked();
	config->matchCase = _pimpl->ui->chkCase->isChecked();
	config->showAdvanced = _pimpl->ui->btnAdvanced->isChecked();
	if (_pimpl->ui->rbtNormal->isChecked())
		config->mode = TextSearchConfigObject::SearchMode::Normal;
	else if (_pimpl->ui->rbtExtended->isChecked())
		config->mode = TextSearchConfigObject::SearchMode::Extended;
	else if (_pimpl->ui->rbtRegExp->isChecked())
		config->mode = TextSearchConfigObject::SearchMode::RegExp;

	return config;
}

void TextSearch::updateUi(const QString &config)
{
	if (config != TextSearchConfigObject::name || !SettingsManager::settings().settingsHas(config))
		return;
	auto conf = SettingsManager::settings().settings<TextSearchConfigObject>(config);

	_pimpl->ui->searchBox->clear();
	_pimpl->ui->replaceBox->clear();
	_pimpl->ui->searchBox->insertItems(0, conf.searchHistory);
	_pimpl->ui->replaceBox->insertItems(0, conf.replaceHistory);
	_pimpl->ui->searchBox->setCurrentText(conf.findText);
	_pimpl->ui->replaceBox->setCurrentText(conf.replaceText);
	_pimpl->ui->chkInSelection->setChecked(conf.inSelection);
	_pimpl->ui->chkWord->setChecked(conf.wholeWord);
	_pimpl->ui->chkCase->setChecked(conf.matchCase);
	_pimpl->ui->btnAdvanced->setChecked(conf.showAdvanced);
	conf.showAdvanced ? _pimpl->ui->gbAdvanced->show() : _pimpl->ui->gbAdvanced->hide();

	switch (conf.mode)
	{
	case TextSearchConfigObject::SearchMode::Normal:
		_pimpl->ui->rbtNormal->setChecked(true);
		break;
	case TextSearchConfigObject::SearchMode::Extended:
		_pimpl->ui->rbtExtended->setChecked(true);
		break;
	case TextSearchConfigObject::SearchMode::RegExp:
		_pimpl->ui->rbtRegExp->setChecked(true);
		break;
	}
}

size_t TextSearch::highlightAll(bool fakemode)
{
	QString text = _pimpl->ui->chkInSelection->isChecked() ? _pimpl->editor->selectedText() : _pimpl->editor->text();

	QString toSearch = _pimpl->ui->searchBox->currentText();
	int len = toSearch.length();
	int cur = -1; // Ensure starting from the beginning of a file
	int end = 0;
	QRegularExpression regexp;
	QRegularExpression::PatternOptions regexpOptions = _pimpl->ui->chkCase->isChecked() ? (QRegularExpression::NoPatternOption | QRegularExpression::MultilineOption)
																						: (QRegularExpression::CaseInsensitiveOption | QRegularExpression::MultilineOption);

	// If not normal search
	if (!_pimpl->ui->rbtNormal->isChecked())
		unescapeString(toSearch);
	// If whole word
	if (_pimpl->ui->chkWord->isChecked())
		toSearch = QString(R"""(\b%1\b)""").arg(toSearch);
	if (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked() || _pimpl->ui->rbtExtended->isChecked())
	{
		regexp.setPattern(toSearch);
		regexp.setPatternOptions(regexpOptions);
		regexp.optimize();
		len = 0;
	}
	// Find last position
	if (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked())
		end = text.lastIndexOf(QRegularExpression(toSearch, regexpOptions));
	else
		end = text.lastIndexOf(toSearch, -1, _pimpl->ui->chkCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive);

	size_t nbocc = 0;
	clearIndications(_pimpl->editor, TextIndicator::SEARCHRESULT);
	while (cur < end)
	{
		// Either regexp or whole word or extended (as we have to recalculate the length of match)
		if (_pimpl->ui->rbtRegExp->isChecked() || _pimpl->ui->chkWord->isChecked() || _pimpl->ui->rbtExtended->isChecked())
		{
			auto match = regexp.match(text, cur + std::max(1, len)); //  + len so it behaves exactly as search and replace do
			len = match.capturedLength();
			cur = match.capturedStart();
		}
		else
			cur = text.indexOf(toSearch, cur + 1, _pimpl->ui->chkCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive);
		if (!fakemode)
			addIndication(_pimpl->editor, TextIndicator::SEARCHRESULT, cur, len);
		nbocc++;
	}
	if (nbocc == 0)
	{
		_pimpl->ui->lblResults->setText(tr("Can't find \"%1\".").arg(toSearch));
		_pimpl->ui->lblResults->setStyleSheet("color: red;");
	}
	else
	{
		_pimpl->ui->lblResults->setText(tr("Found %1 occurrences.").arg(nbocc));
		_pimpl->ui->lblResults->setStyleSheet("color: green;");
	}
	return nbocc;
}

void TextSearch::cancelSearch()
{
	_pimpl->ongoing = false;
	clearIndications(_pimpl->editor, TextIndicator::SEARCHRESULT);
}
