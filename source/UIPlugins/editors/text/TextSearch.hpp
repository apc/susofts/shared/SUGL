/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTSEARCH_HPP
#define TEXTSEARCH_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"

class QsciScintilla;

struct SConfigObject;

/**
 * Search box used with a TextEditorWidget.
 *
 * This is a widget used by TextEditorWidget that places it below a TextEditor and allows to search and / or replace content.
 *
 * The widget will store in the Settings its current state, with the key `TextSearch`. Note that it will not be written
 * to the config file (or if it does, it will not be read).
 *
 * It is possible to select some text and use search without copy-pasting the searched text to the line edit. This functionality is limited to the selections without newlines.
 */
class SUGL_SHARED_EXPORT TextSearch : public QWidget
{
	Q_OBJECT

public:
	/** Constructor */
	TextSearch(QsciScintilla *editor, QWidget *parent = nullptr);
	virtual ~TextSearch() override;

	/** Used to follow events on the QsciScintilla editor. */
	virtual bool eventFilter(QObject *watched, QEvent *event) override;

public slots:
	/** find the next entered text, close to the cursor */
	void findNext();
	/** find the previous entered text, close to the cursor */
	void findPrevious();
	/** replace currently selected text (generally after a call of findNext() or findPrevious() */
	void replace();
	/** search and replace all occurrences of the entered text by the entered replacement */
	void replaceAll();
	/** Search and add a marker to all occurrences in the text */
	void markAll();
	/** Remove all search markers */
	void removeMarkers();
	/** enable or not the search mode */
	void setSearchMode(bool modeReplace);
	/**
	 * Place highlighting indicators for searched words.
	 * @param fakemode if true, only count the number of occurrences, but don't apply indicator
	 */
	size_t highlightAll(bool fakemode = false);

protected:
	void hideEvent(QHideEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	void find(bool totheleft, int line = -1, int index = -1);
	SConfigObject *config() const;

private slots:
	void cancelSearch();
	void updateUi(const QString &config);

private:
	/** pimpl */
	class _TextSearch_pimpl;
	std::unique_ptr<_TextSearch_pimpl> _pimpl;
};

#endif // TEXTSEARCH_HPP
