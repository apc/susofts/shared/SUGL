/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef ISHAREABLEPOINTSLISTIOLEXER_HPP
#define ISHAREABLEPOINTSLISTIOLEXER_HPP

#include <ShareablePoints/IShareablePointsListIO.hpp>

#include "PluginsGlobal.hpp"
#include "TextLexer.hpp"

/**
 * IO for ShareablePointsList that can be used as a Lexer for QScintilla.
 *
 * Abstract class that ease the integration of an IO class with a Lexer.
 * You **will** have to override all the pure virtual methods from IShareablePointsListIO
 * and the required virtual methods from QsciLexerCustom:
 * - IShareablePointsListIO
 * 	- IShareablePointsListIO::getMIMEType()
 * 	- all read methods
 * 	- all write methods
 * - QsciLexerCustom
 * 	- language()
 * 	- description()
 * 	- styleText()
 * 	- all defaults
 */
class SUGL_SHARED_EXPORT ShareablePointsListIOLexer : public TextLexer, public IShareablePointsListIO
{
	Q_OBJECT

public:
	ShareablePointsListIOLexer(QObject *parent = nullptr) : TextLexer(parent), IShareablePointsListIO() {}
	virtual ~ShareablePointsListIOLexer() override = default;

	// IShareablePointsListIO
	using IShareablePointsListIO::utf8;
	virtual bool utf8() const noexcept override;

protected:
	// TextLexer
	virtual bool startLexer(int start, int end) override
	{
		_lexerMode = true;
		return TextLexer::startLexer(start, end);
	}
	virtual void endLexer() override
	{
		if (!_lexerMode)
			_end = _start; // disable default styling of unstyled line
		TextLexer::endLexer();
		_lexerMode = false;
	}

protected:
	/** Tells if the lexer is activated (we are styling) or not */
	bool _lexerMode = false;
};

#endif // ISHAREABLEPOINTSLISTIOLEXER_HPP
