/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTGOTOLINE_HPP
#define TEXTGOTOLINE_HPP

#include <memory>

#include <QDialog>

#include "PluginsGlobal.hpp"

class SUGL_SHARED_EXPORT TextGoToLine : public QDialog
{
	Q_OBJECT

public:
	/** Constructor */
	TextGoToLine(int curLine, int maxLine, int curPos, int maxPos, QWidget *parent = nullptr);
	virtual ~TextGoToLine() override;

	/** @return the entered value which is either a line number or a position, depending on isLine() and isPosition() */
	int getValue();
	/** @return true if the user selected to jump to a specific line */
	bool isLine();
	/** @return true if the user selected to jump to a specific position */
	bool isPosition();

public slots:
	/** Set the current line number */
	void setCurLine(int curLine);
	/** Set the total number of lines */
	void setMaxLine(int maxLine);
	/** Set the current position */
	void setCurPos(int curPos);
	/** Set the max position (size of the document) */
	void setMaxPos(int maxPos);

protected:
	virtual void focusInEvent(QFocusEvent *event) override;

private slots:
	/** Setup the UI to have the correct line / position options */
	void changeLinePos();

private:
	/** pimpl */
	class _TextGoToLine_pimpl;
	std::unique_ptr<_TextGoToLine_pimpl> _pimpl;
};

#endif // TEXTGOTOLINE_HPP
