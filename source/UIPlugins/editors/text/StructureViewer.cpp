#include "StructureViewer.hpp"
#include "ui_StructureViewer.h"

#include <algorithm>
#include <vector>

#include <QAbstractItemModel>

#include <Qsci/qscilexer.h>
#include <Qsci/qsciscintilla.h>

std::list<FoldLine> getFolds(QsciScintilla *editor)
{
	/** Get category name from the data string */
	auto getCategory = [](const QString &data) {
		// category separating characters are: `:`, `(`
		QList<int> splits = {data.indexOf(':'), data.indexOf('(')};
		int idxSplit = -1;
		for (auto &split : splits)
		{
			// if bigger (not set) or if smaller when all set
			if ((split > idxSplit && idxSplit == -1) || (split < idxSplit && idxSplit > 0 && split > 0))
				idxSplit = split;
		}

		return data.left(idxSplit).trimmed();
	};

	const int lines = editor->lines();
	QMap<int, FoldLine> foldLines;
	std::map<int, bool> levelToIsCategory; // Mapping: is a level currently a part of category fold
	{
		bool isFolded;
		int lvl = 0, style = 0;
		QString txt;
		QString category;

		// Get all the folds
		for (int line = 0; line < lines; line++)
		{
			lvl = editor->SendScintilla(QsciScintillaBase::SCI_GETFOLDLEVEL, line);
			if (!(lvl & QsciScintilla::SC_FOLDLEVELHEADERFLAG)) // if the fold is not a header fold
				continue;
			lvl &= ~QsciScintilla::SC_FOLDLEVELHEADERFLAG & ~QsciScintilla::SC_FOLDLEVELWHITEFLAG;

			txt = editor->text(line).trimmed();
			category = getCategory(txt);
			style = editor->SendScintilla(QsciScintillaBase::SCI_GETSTYLEAT, editor->positionFromLineIndex(line, 0));
			isFolded = !editor->SendScintilla(QsciScintillaBase::SCI_GETFOLDEXPANDED, line);
			foldLines[line] = {isFolded, false, line, lvl, style, txt, category};
			levelToIsCategory[lvl] = false;
		}
	}

	// Creation of category folds
	// The principle is: sections of the folds where the same name repeats needs to be identified. These sections cannot be interrupted by
	// outer level folds, otherwise they are not continous and cannot be considered as a category fold. When such sections are identified
	// a new category fold has to be added before the first element of the section. At the same time the level of all the elements since
	// last category element need to be updated. When going out of category scope - all nested categories should be closed as well.

	// Maps to avoid linear searches for the cost of (temporary) memory usage
	std::map<int, int> levelToLine; // Mapping: level to last visible line at the same level
	std::map<int, FoldLine *> levelToLastVisibleLevelLine; // Mapping: level to last visible line at the same level
	std::list<FoldLine> allFoldLines; // Final result is a list since there may be duplicates with the same line and so that one can reference its element
	int lvlCatModifier = 0; // Modifier for the level due to category folds

	for (auto iteratorFoldLines = foldLines.begin(); iteratorFoldLines != foldLines.end();)
	{
		auto &[isFolded, isCategory, line, lvl, style, txt, category] = iteratorFoldLines.value();
		int upperLevelLine = levelToLine[lvl - 1];

		// GO IN CATEGORY
		// without outer level interruption (no more-outer level was present since last current level)
		// AND
		// the same category at the same level and no category fold yet for this lvl
		if ((upperLevelLine < line && upperLevelLine < levelToLine[lvl]) && ((category == foldLines[levelToLine[lvl]].category) && !levelToIsCategory[lvl]))
		{
			// insert new category fold before the first category fold
			auto insertionIterator = std::find(allFoldLines.begin(), allFoldLines.end(), *levelToLastVisibleLevelLine[lvl])--; // one before the last one from the same fold level
			allFoldLines.insert(insertionIterator,
				{(*insertionIterator).isFolded, true, (*insertionIterator).line, lvl + lvlCatModifier, style, (*insertionIterator).category, (*insertionIterator).category});
			lvlCatModifier++;
			// update all the folds with the proper level beginning from the first category fold until the end
			while (insertionIterator != allFoldLines.end())
			{
				(*insertionIterator).lvl += lvlCatModifier;
				insertionIterator++;
			}
			// Mark that category fold was started for this level
			levelToIsCategory[lvl] = true;
		}
		// GO OUT OF CATEGORIES (for previous)
		// if current element is outer fold (compared to the previous)
		// OR categories differ between previous element on the same level
		else if (!allFoldLines.empty() && (((iteratorFoldLines - 1).value().lvl > lvl) || (category != foldLines[levelToLine[lvl]].category)))
		{
			// Un-nest current level or the levels from last element
			std::map<int, bool>::iterator iteratorCategoryLevels = (category != foldLines[levelToLine[lvl]].category)
				? levelToIsCategory.find(lvl)
				: levelToIsCategory.find((iteratorFoldLines - 1).value().lvl);
			for (; iteratorCategoryLevels != levelToIsCategory.end(); ++iteratorCategoryLevels)
			{
				auto &foldLvl = iteratorCategoryLevels->first;
				if ((foldLvl >= lvl) && levelToIsCategory[foldLvl])
				{
					iteratorCategoryLevels->second = false;
					levelToIsCategory[foldLvl] = false;
					lvlCatModifier--;
				}
			}
		}

		// ADD FOLD
		allFoldLines.push_back({isFolded, isCategory, line, lvl + lvlCatModifier, style, txt, category});
		levelToLastVisibleLevelLine[lvl] = &allFoldLines.back();
		levelToLine[lvl] = line;
		iteratorFoldLines++;
	}

	return allFoldLines;
}

namespace
{
class StructureItem
{
public:
	struct Data
	{
		QString data;
		QFont font;
		QColor color;
		int linenbr;
	};

public:
	StructureItem() = default;
	StructureItem(Data data, StructureItem *parent = nullptr) : _parent(parent), _data(data)
	{
		if (_parent)
			_parent->appendChild(this);
	}

	void clear() noexcept
	{
		_parent = nullptr;
		_data.linenbr = -1;
		_children.clear();
	}
	// children
	void appendChild(StructureItem *child)
	{
		if (std::find_if(std::cbegin(_children), std::cend(_children), [child](const auto &v) -> bool { return v.get() == child; }) == std::cend(_children))
			_children.emplace_back(child);
		child->_parent = this;
	}
	const StructureItem *child(size_t row) const { return row < _children.size() ? _children[row].get() : nullptr; }
	StructureItem *child(size_t row) { return row < _children.size() ? _children[row].get() : nullptr; }
	const std::vector<std::unique_ptr<StructureItem>> &children() const noexcept { return _children; }
	size_t childCount() const noexcept { return _children.size(); }

	// leaves
	const Data &data() const noexcept { return _data; }
	Data &data() noexcept { return _data; }

	// parent
	size_t row() const
	{
		return _parent ? std::distance(std::cbegin(_parent->_children),
				   std::find_if(std::cbegin(_parent->_children), std::cend(_parent->_children), [this](const auto &v) -> bool { return v.get() == this; }))
					   : 0;
	}
	const StructureItem *parent() const noexcept { return _parent; }
	StructureItem *parent() noexcept { return _parent; }

private:
	StructureItem *_parent = nullptr;
	std::vector<std::unique_ptr<StructureItem>> _children;
	Data _data;
};

class StructureTreeModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	static constexpr int COLLINE = 0;
	static constexpr int COLTEXT = 1;

public:
	StructureTreeModel(QsciScintilla *editor, QWidget *parent) : QAbstractItemModel(parent), _editor(editor)
	{
		connect(_editor, &QsciScintilla::SCN_MODIFIED, this, &StructureTreeModel::editorChanged);
	}

	// QAbstracItemModel
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override
	{
		if (!hasIndex(row, column, parent) || column > StructureTreeModel::COLTEXT)
			return QModelIndex();

		auto *parentItem = parent.isValid() ? static_cast<StructureItem *>(parent.internalPointer()) : _root.get();

		return createIndex(row, column, parentItem->child((size_t)row));
	}
	QModelIndex parent(const QModelIndex &index) const override
	{
		if (!index.isValid())
			return QModelIndex();

		auto *item = static_cast<StructureItem *>(index.internalPointer());
		if (!item || !item->parent())
			return QModelIndex();
		item = item->parent();

		return createIndex((int)item->row(), 0, item);
	}
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override
	{
		if (parent.column() > 0)
			return 0;

		auto *parentItem = parent.isValid() ? static_cast<StructureItem *>(parent.internalPointer()) : _root.get();
		return (int)parentItem->childCount();
	}
	virtual int columnCount(const QModelIndex & = QModelIndex()) const override { return 2; }
	virtual QVariant data(const QModelIndex &index, int role = Qt::ItemDataRole::DisplayRole) const override
	{
		if (!index.isValid() || index.column() > StructureTreeModel::COLTEXT
			|| (index.column() == StructureTreeModel::COLLINE && role != Qt::ItemDataRole::DisplayRole && role != Qt::ItemDataRole::ToolTipRole))
			return QVariant();

		auto *item = static_cast<StructureItem *>(index.internalPointer());
		if (!item)
			return QVariant();

		if (index.column() == StructureTreeModel::COLLINE)
			return item->data().linenbr + 1;

		switch (role)
		{
		case Qt::ItemDataRole::DisplayRole:
		case Qt::ItemDataRole::ToolTipRole:
			return item->data().data;
		case Qt::ItemDataRole::FontRole:
			return item->data().font;
		case Qt::ItemDataRole::ForegroundRole:
			return QBrush(item->data().color);
		}
		return QVariant();
	}
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override
	{
		if (section > StructureTreeModel::COLTEXT || orientation != Qt::Horizontal || role != Qt::DisplayRole)
			return QVariant();
		if (section == StructureTreeModel::COLLINE)
			return tr("#");
		if (section == StructureTreeModel::COLTEXT)
			return tr("Line");
		return QVariant();
	}

	// StructureTreeModel
public slots:
	void doubleClicked(const QModelIndex &index)
	{
		if (!index.isValid() || index.column() > StructureTreeModel::COLTEXT)
			return;

		auto *item = static_cast<StructureItem *>(index.internalPointer());
		if (!item)
			return;

		const int linenbr = item->data().linenbr;
		_editor->SendScintilla(QsciScintillaBase::SCI_GOTOLINE, linenbr);
		_editor->ensureLineVisible(linenbr);
		_editor->setFocus();
	}
	void editorChanged(int, int mtype, const char *, int, int, const int, int, int, int, int)
	{
		if (!_editor->lexer())
		{
			clear();
			return;
		}

		if (!(mtype & QsciScintilla::SC_MOD_LEXERSTATE))
			return;

		clear();

		int prevlvl = 0;
		StructureItem *node = _root.get();
		QsciLexer *lexer = _editor->lexer();
		// Get the folds and categories
		const auto &foldLines = getFolds(_editor);
		// Create the nodes
		for (auto const &[isFolded, isCategory, line, lvl, style, txt, category] : foldLines)
		{
			// Nested level management
			if (lvl <= prevlvl) // go back to parent
			{
				for (int toparent = prevlvl - lvl + 1; toparent > 0 && node->parent(); toparent--) // manage folds
					node = node->parent();
			}
			prevlvl = lvl;

			// Node creation - we insert only one node at a time
			QModelIndex qmi = createIndex((int)node->row(), 0, node);
			beginInsertRows(qmi, (int)node->childCount(), (int)node->childCount());
			node = new StructureItem(StructureItem::Data{txt, lexer->defaultFont(style), lexer->defaultColor(style), line}, node);
			endInsertRows();

			if (!isFolded && !isCategory) // Expand node only if it was expanded in text and if it is not a category fold 
				emit expandNode(createIndex((int)node->row(), 0, node));
		}
		emit signalTreeCreationFinished();
	}

signals:
	void expandNode(const QModelIndex &index);
	void signalTreeCreationFinished();

private:
	void clear()
	{
		if (_root->childCount() == 0)
			return;
		beginRemoveRows(createIndex(0, 0, _root.get()), 0, (int)_root->childCount() - 1);
		_root->clear();
		endRemoveRows();
	}

private:
	QsciScintilla *_editor;
	std::unique_ptr<StructureItem> _root = std::make_unique<StructureItem>(StructureItem::Data{}, nullptr);
};
} // namespace
#include "StructureViewer.moc"

class StructureViewer::_StructureViewer_pimpl
{
public:
	std::unique_ptr<Ui::StructureViewer> ui;
	QsciScintilla *editor = nullptr;
	StructureTreeModel *model = nullptr;
	bool initialized = false;
	bool startCollapsed = false;
};

StructureViewer::StructureViewer(QsciScintilla *editor, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_StructureViewer_pimpl>())
{
	_pimpl->editor = editor;
	_pimpl->ui = std::make_unique<Ui::StructureViewer>();
	_pimpl->ui->setupUi(this);
}

StructureViewer::~StructureViewer() = default;

void StructureViewer::setStartCollapsed(bool startCollapsed)
{
	_pimpl->startCollapsed = startCollapsed;
}

void StructureViewer::showEvent(QShowEvent *event)
{
	if (!_pimpl->initialized)
	{
		initialize();
		_pimpl->initialized = true;
	}
	QWidget::showEvent(event);
}

void StructureViewer::initialize()
{
	_pimpl->model = new StructureTreeModel(_pimpl->editor, this);
	_pimpl->ui->treeView->setModel(_pimpl->model);
	connect(_pimpl->model, &StructureTreeModel::expandNode, [this](const QModelIndex &index) { _pimpl->ui->treeView->expand(index); });
	connect(_pimpl->ui->treeView, &QTreeView::doubleClicked, _pimpl->model, &StructureTreeModel::doubleClicked);
	connect(_pimpl->model, &StructureTreeModel::signalTreeCreationFinished, [this]() { if(_pimpl->startCollapsed) _pimpl->ui->treeView->collapseAll(); });
	_pimpl->model->editorChanged(0, QsciScintilla::SC_MOD_LEXERSTATE, "", 0, 0, 0, 0, 0, 0, 0); // force first tree creation
}
