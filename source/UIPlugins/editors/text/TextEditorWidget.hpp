/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTEDITORWIDGET_HPP
#define TEXTEDITORWIDGET_HPP

#include <memory>
#include <vector>

#include "PluginsGlobal.hpp"
#include "interface/SGraphicalInterface.hpp"

class QMimeData;
class IShareablePointsListIO;
class MarkerViewer;
class TextEditor;
class TextSearch;

/**
 * Widget containing a TextEditor and its corresponding TextSearch and TextGoToLine.
 *
 * This is an abstract class that holds a TextWidget. It is used to factorize code for such widgets. A TextSearch is placed below TextEditor.
 */
class SUGL_SHARED_EXPORT TextEditorWidget : public SGraphicalWidget
{
	Q_OBJECT

public:
	TextEditorWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~TextEditorWidget() override;

	// SGraphicalWidget
	virtual bool isModified() const noexcept override;
	virtual ShareablePointsList getContent() const override;

	/** @return the TextEditor */
	const TextEditor *editor() const noexcept;
	/** @return the TextEditor */
	TextEditor *editor() noexcept;
	/** Add the given actions to the contextual menu */
	void additionalContextualActions(std::vector<QAction *> actions) noexcept;
	/** @return the actions added to the contextual menu */
	const std::vector<QAction *> &additionalContextualActions() const noexcept;

public slots:
	// SGraphicalWidget
	virtual void clearContent() override;
	virtual void updateContent() override;
	virtual void paste() override;
	virtual void copy() const override;
	virtual void cut() const override;
	virtual void undo() override;
	virtual void redo() override;
	/** This method is handled by QsciScintilla (TextEditor) which can only set isModified flag to false and changing it to true is not possible. */
	virtual void isModified(bool modified) noexcept override;

	/** Jump to previous position recorded */
	void previousPosition();
	/** Jump to next position recorded */
	void nextPosition();
	/** Open a TextGoToLine window and jump to the desired line in TextEditor. */
	virtual void showGoToLine();
	/** Show a TextSearch widget. Boolean tells if we should open it in the replace mode. */
	virtual void showSearch(bool replace = false);
	/**
	 * Show out of date message.
	 * If this TextEditorWidget is used for output file, this slot should be connected to inputs modifications to show a message that tell
	 * the user that outputs are out of date.
	 */
	virtual void inputHasChanged(bool changed);
	/** Returns if search widget is visible */
	bool isSearchVisible();

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

	/**
	 * Return a string insertable in the editor from the given mime data.
	 * This function reads content from the mimedata. It starts to deserialize a ShareablePointsList from the mimedata if possible. To do so, it first tries to get
	 * content associated to the mime type of the given serializer. If it doesn't exist, it will use the function ClipboardManager::paste() to get a ShareablePointsList,
	 * and then write in the editor's language with the given serializer.
	 *
	 * @param mimedata the mime data containing the mimetypes we will look for to deserialize a ShareablePointsList
	 * @param serializer the default serializer to use
	 * @return a string representing the content of the clipboard that can be inserted in the editor.
	 */
	virtual QByteArray contentFromMime(const QMimeData *mimedata, IShareablePointsListIO &serializer);
	/**
	 * Return a mime data from the given text.
	 * This function inserts the content of the given text in the mimedata. To do so, it first tries to serialize the text with the given serializer, and then uses
	 * ClipboardManager::copy() to insert the shareablePointsList in known format. In addition, it adds the raw text associated to the mime type of the serializer
	 *
	 * @param text the text to insert in the mimedata
	 * @param mimedata the mime data containing the mimetypes we will look for to deserialize a ShareablePointsList
	 * @param serializer the default serializer to use
	 * @return the modified mimedata
	 */
	virtual QMimeData *contentToMime(const QByteArray &text, QMimeData *mimedata, IShareablePointsListIO &serializer);
	/** @return the TextSearch */
	const TextSearch *searchWidget() const noexcept;
	/** @return the TextSearch */
	TextSearch *searchWidget() noexcept;
	/** @return the MarkerViewer */
	const MarkerViewer *markerViewer() const noexcept;
	/** @return the MarkerViewer */
	MarkerViewer *markerViewer() noexcept;
	/** @return true if everything is selected, or if the not selected part is only whitespaces. Used before pasting to know if we'll replace everything */
	bool isAllSelected() const;

private:
	/** Create the status widget and actions */
	void createWidgets();
	/** Clears the position history */
	void clearPositions();

private slots:
	/** Called when the cursor position changed. The position is then recorded in the position history. */
	void positionChanged(int line, int index);

private:
	/** pimpl */
	class _TextEditorWidget_pimpl;
	std::unique_ptr<_TextEditorWidget_pimpl> _pimpl;
};

#endif // TEXTEDITORWIDGET_HPP
