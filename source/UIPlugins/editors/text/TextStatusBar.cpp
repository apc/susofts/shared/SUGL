#include "TextStatusBar.hpp"
#include "ui_TextStatusBar.h"
#include "TextUtils.hpp"

#include <Qsci/qsciscintilla.h>

class TextStatusBar::_TextStatusBar_pimpl
{
public:
	std::unique_ptr<Ui::TextStatusBar> ui;
	/** Editor to get information from */
	const QsciScintilla *editor;
};

TextStatusBar::TextStatusBar(const QsciScintilla *editor, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_TextStatusBar_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::TextStatusBar>();
	_pimpl->editor = editor;

	_pimpl->ui->setupUi(this);
	connect(_pimpl->editor, &QsciScintilla::cursorPositionChanged, this, &TextStatusBar::positionChanged);
	connect(_pimpl->editor, &QsciScintilla::selectionChanged, this, &TextStatusBar::selectionChanged);
	connect(_pimpl->editor, &QsciScintilla::textChanged, this, &TextStatusBar::textChanged);
	// update the values
	int line, index;
	_pimpl->editor->getCursorPosition(&line, &index);
	positionChanged(line, index);
	selectionChanged();
	textChanged();
}

TextStatusBar::~TextStatusBar() = default;

void TextStatusBar::positionChanged(int line, int index)
{
	_pimpl->ui->lblCurLine->setText(QString::number(line + 1));
	_pimpl->ui->lblCol->setText(QString::number(index + 1));
	_pimpl->ui->lblPos->setText(QString::number(_pimpl->editor->SendScintilla(QsciScintilla::SCI_GETCURRENTPOS)));
}

void TextStatusBar::selectionChanged()
{
	if (!_pimpl->editor->hasSelectedText())
	{
		_pimpl->ui->lblSelIndex->setText("0");
		_pimpl->ui->lblSelLines->setText("0");
		_pimpl->ui->lblSelCount->setText("0");
		return;
	}
	int lineFrom, indexFrom, lineTo, indexTo;
	_pimpl->editor->getSelection(&lineFrom, &indexFrom, &lineTo, &indexTo);
	_pimpl->ui->lblSelIndex->setText(QString::number(_pimpl->editor->positionFromLineIndex(lineTo, indexTo) - _pimpl->editor->positionFromLineIndex(lineFrom, indexFrom)));
	_pimpl->ui->lblSelLines->setText(QString::number(lineTo - lineFrom + 1));
	_pimpl->ui->lblSelCount->setText(QString::number(countIndications(_pimpl->editor, TextIndicator::SELECTEDRESULT)));
}

void TextStatusBar::textChanged()
{
	_pimpl->ui->lblLines->setText(QString::number(_pimpl->editor->lines()));
	_pimpl->ui->lblLength->setText(QString::number(_pimpl->editor->length()));
}

void TextStatusBar::mouseDoubleClickEvent(QMouseEvent *event)
{
	QWidget::mouseDoubleClickEvent(event);
	emit doubleClick();
}
