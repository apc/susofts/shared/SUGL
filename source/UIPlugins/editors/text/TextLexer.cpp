#include "TextLexer.hpp"

#include <bitset>

#include <QGuiApplication>
#include <QKeyEvent>
#include <QPixmap>

#include <Qsci/qsciscintilla.h>

#include "TextEditor.hpp"
#include "TextEditorConfig.hpp"
#include "TextUtils.hpp"

class TextLexer::_TextLexer_pimpl
{
public:
	struct foldManager
	{
		static constexpr int defaultLevel = 0x400;
		int level = defaultLevel;
		int prevLevel = defaultLevel;
		bool header = false;

		void clear() noexcept { *this = foldManager{}; }
	};

public:
	bool initDone = false;
	foldManager folder{};
};

TextLexer::TextLexer(QObject *parent) : QsciLexerCustom(parent), _styles(_curpos), _pimpl(std::make_unique<_TextLexer_pimpl>())
{
}

TextLexer::~TextLexer() = default;

void TextLexer::styleText(int start, int end)
{
	if (startLexer(start, end))
	{
		QStringRef text = &_currentText; // copy of current text
		while (!text.isEmpty())
		{
			int endline = text.indexOf('\n');
			endline = (endline >= 0) ? endline + 1 : text.size();
			_currentLine = text.left(endline);
			_errorAnnotation.clear();
			_infoAnnotation.clear();
			_styles.addLine(editor()->isUtf8() ? _currentLine.toUtf8().size() : endline);
			int startline = _curpos;

			styleText();
			if (_styles.getEndline() > 0)
				addStyle(_styles.getEndline(), 0); // 0 is default style

			// annotation
			annotate(startline, _errorAnnotation + _infoAnnotation);
			// folding
			foldLine(startline, _currentLine.trimmed().isEmpty());
			// update text
			text = text.right(text.size() - endline);
		}
		applyStyles();
	}
	endLexer();
}

bool TextLexer::caseSensitive() const
{
	TextEditorConfigObject config;
	config.read();
	return config.acCaseSensitive;
}

void TextLexer::annotate(int pos, const QString &annotation, int style)
{
	if (editor() && !annotation.isEmpty())
	{
		int line, index;
		editor()->lineIndexFromPosition(pos, &line, &index);
		editor()->annotate(line, annotation, style);
	}
}

QString TextLexer::annotation(int pos)
{
	if (!editor() || pos < 0)
		return "";

	int line, index;
	editor()->lineIndexFromPosition(pos, &line, &index);
	auto *textEditor = qobject_cast<TextEditor *>(editor());
	if (textEditor)
		return textEditor->annotation(line);
	return editor()->annotation(line);
}

void TextLexer::startFolding() noexcept
{
	_pimpl->folder.header = true;
}

void TextLexer::endFolding() noexcept
{
	if (_pimpl->folder.header)
		return;

	_pimpl->folder.level--;
	if (_pimpl->folder.level < _pimpl->folder.defaultLevel)
		_pimpl->folder.level = _pimpl->folder.defaultLevel;
}

void TextLexer::foldLine(int pos, bool isempty)
{
	if (!editor() || pos < 0)
		return;

	int level = _pimpl->folder.level;
	if (isempty)
		level |= QsciScintilla::SC_FOLDLEVELWHITEFLAG;
	if (_pimpl->folder.header)
	{
		level |= QsciScintilla::SC_FOLDLEVELHEADERFLAG;
		_pimpl->folder.header = false;
		_pimpl->folder.level++;
	}

	int line, index;
	editor()->lineIndexFromPosition(pos, &line, &index);
	editor()->SendScintilla(QsciScintilla::SCI_SETFOLDLEVEL, line, level);
}

int TextLexer::indicatorJumpTo(int indicatorType, int position, const QString &)
{
	return editor()->SendScintilla(QsciScintilla::SCI_INDICATORVALUEAT, indicatorType, position);
}

QStringRef TextLexer::readLine(int position) const noexcept
{
	if (_currentText.isEmpty())
		return QStringRef();

	int lineBegin = _currentText.lastIndexOf('\n', position);
	int lineEnd = _currentText.indexOf('\n', position);
	if (lineBegin < 0)
		lineBegin = 0;
	else
		lineBegin++;
	if (lineEnd < 0)
		lineEnd = _currentText.size() - 1;
	return _currentText.midRef(lineBegin, lineEnd - lineBegin);
}

void TextLexer::applyStyles()
{
	if (editor())
	{
		for (const auto &line : _styles)
		{
			for (const auto &[pos, style] : line)
			{
				setStyling(pos, style);
			}
		}
	}
}

bool TextLexer::startLexer(int start, int end)
{
	if (!editor())
		return false;

	_curpos = _start = start;
	_end = end;
	_currentText = editor()->text(start, end);
	if (_currentText.isEmpty())
		return false;

	// indicators
	if (!_pimpl->initDone)
	{
		// indicators
		connect(editor(), &QsciScintilla::indicatorReleased, this, &TextLexer::indicatorCtrlClicked, Qt::ConnectionType::QueuedConnection);
		// calltips
		connect(editor(), &QsciScintillaBase::SCN_DWELLSTART, this, &TextLexer::indicatorHovered);
		_pimpl->initDone = true;
	}

	// clear things
	if (start == 0 && end == editor()->length())
	{
		// annotations
		editor()->clearAnnotations();

		// markers
		editor()->markerDeleteAll(TextMarker::ERROR);
		editor()->markerDeleteAll(TextMarker::WARNING);
		editor()->markerDeleteAll(TextMarker::INFO);
		emit editor()->SCN_MODIFIED(-1, QsciScintilla::SC_MOD_CHANGEMARKER, "", -1, -1, -1, -1, -1, -1, -1);

		// indicators
		clearIndications(editor(), TextIndicator::HOVERONLY);
		clearIndications(editor(), TextIndicator::INTERNAL);
		clearIndications(editor(), TextIndicator::EXTERNAL);
	}

	initLexer();
	startStyling(_start);
	return true;
}

void TextLexer::initLexer()
{
	_pimpl->folder.clear();
	_curpos = _start;
	_currentLine.clear();
	_styles.clear();
	_errorAnnotation.clear();
	_infoAnnotation.clear();
}

void TextLexer::endLexer()
{
	if (_curpos < _end)
		setStyling(_end - _curpos, 0);
	_currentText.clear();
	_currentLine.clear();
	_styles.clear();
	_errorAnnotation.clear();
	_infoAnnotation.clear();
	if (editor())
		editor()->SendScintilla(QsciScintillaBase::SCI_CHANGELEXERSTATE, _start, _end);
	_curpos = _start = _end = -1;
}

void TextLexer::indicatorCtrlClicked(int line, int index, Qt::KeyboardModifiers state)
{
	// state has a bad value because Qt::KeyboardModifiers can't be queued (not registered meta type)
	state = QGuiApplication::keyboardModifiers();
	if (!editor() || state != Qt::KeyboardModifier::ControlModifier)
		return;

	const int position = editor()->positionFromLineIndex(line, index);
	const std::bitset<32> indicatorbit = getIndicators(editor(), position);

	if (indicatorbit[TextIndicator::INTERNAL])
	{
		const int beg = editor()->SendScintilla(QsciScintilla::SCI_INDICATORSTART, TextIndicator::EXTERNAL, position);
		const int end = editor()->SendScintilla(QsciScintilla::SCI_INDICATOREND, TextIndicator::EXTERNAL, position);
		auto indicatortxt = editor()->text(beg, end);
		const int value = indicatorJumpTo(TextIndicator::INTERNAL, position, indicatortxt);
		if (value < 0)
			return;
		auto *sped = qobject_cast<TextEditor *>(editor());
		if (sped)
			sped->jumpTo(value, false);
		else
		{
			editor()->SendScintilla(QsciScintillaBase::SCI_GOTOPOS, value);
			editor()->setFocus();
		}
	}
	else if (indicatorbit[TextIndicator::EXTERNAL])
	{
		const int beg = editor()->SendScintilla(QsciScintilla::SCI_INDICATORSTART, TextIndicator::EXTERNAL, position);
		const int end = editor()->SendScintilla(QsciScintilla::SCI_INDICATOREND, TextIndicator::EXTERNAL, position);
		auto indicatortxt = editor()->text(beg, end);
		const int value = indicatorJumpTo(TextIndicator::EXTERNAL, position, indicatortxt);
		emit globalIndicatorClicked(indicatortxt, value);
	}
}

void TextLexer::indicatorHovered(int position, int, int)
{
	if (position < 0 || !editor() || !editor()->underMouse())
		return;

	const std::bitset<32> indicatorbit = getIndicators(editor(), position);
	int indicatorType = -1;
	if (indicatorbit[TextIndicator::HOVERONLY])
		indicatorType = TextIndicator::HOVERONLY;
	else if (indicatorbit[TextIndicator::INTERNAL])
		indicatorType = TextIndicator::INTERNAL;
	else if (indicatorbit[TextIndicator::EXTERNAL])
		indicatorType = TextIndicator::EXTERNAL;
	else
		return;

	const int beg = editor()->SendScintilla(QsciScintilla::SCI_INDICATORSTART, indicatorType, position);
	const int end = editor()->SendScintilla(QsciScintilla::SCI_INDICATOREND, indicatorType, position);
	QString tip = calltip(position, indicatorType, editor()->text(beg, end));
	if (!tip.isEmpty())
		editor()->SendScintilla(QsciScintilla::SCI_CALLTIPSHOW, position, tip.toLatin1().constData());
}
