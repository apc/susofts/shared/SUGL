#include "TextGoToLine.hpp"
#include "ui_TextGoToLine.h"

class TextGoToLine::_TextGoToLine_pimpl
{
public:
	std::unique_ptr<Ui::TextGoToLine> ui;
	int curLine = -1;
	int maxLine = -1;
	int curPos = -1;
	int maxPos = -1;
};

TextGoToLine::TextGoToLine(int curLine, int maxLine, int curPos, int maxPos, QWidget *parent) : QDialog(parent), _pimpl(std::make_unique<_TextGoToLine_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::TextGoToLine>();
	_pimpl->curLine = curLine;
	_pimpl->maxLine = maxLine;
	_pimpl->curPos = curPos;
	_pimpl->maxPos = maxPos;

	_pimpl->ui->setupUi(this);
	changeLinePos();
}

TextGoToLine::~TextGoToLine() = default;

int TextGoToLine::getValue()
{
	return _pimpl->ui->spinBox->value();
}

bool TextGoToLine::isLine()
{
	return _pimpl->ui->rbtLine->isChecked();
}

bool TextGoToLine::isPosition()
{
	return _pimpl->ui->rbtPos->isChecked();
}

void TextGoToLine::setCurLine(int curLine)
{
	if (_pimpl->curLine != curLine)
	{
		_pimpl->curLine = curLine;
		changeLinePos();
	}
}

void TextGoToLine::setMaxLine(int maxLine)
{
	if (_pimpl->maxLine != maxLine)
	{
		_pimpl->maxLine = maxLine;
		changeLinePos();
	}
}

void TextGoToLine::setCurPos(int curPos)
{
	if (_pimpl->curPos != curPos)
	{
		_pimpl->curPos = curPos;
		changeLinePos();
	}
}

void TextGoToLine::setMaxPos(int maxPos)
{
	if (_pimpl->maxPos != maxPos)
	{
		_pimpl->maxPos = maxPos;
		changeLinePos();
	}
}

void TextGoToLine::focusInEvent(QFocusEvent *event)
{
	QDialog::focusInEvent(event);
	_pimpl->ui->spinBox->setFocus();
	_pimpl->ui->spinBox->selectAll();
}

void TextGoToLine::changeLinePos()
{
	if (_pimpl->ui->rbtLine->isChecked())
	{
		QString window = tr("Line number (%1 - %2),").arg(1).arg(_pimpl->maxLine);
		QString current = tr("current: %1").arg(_pimpl->curLine);
		_pimpl->ui->lblWindow->setText(window);
		_pimpl->ui->lblCurrent->setText(current);
		_pimpl->ui->spinBox->setRange(1, _pimpl->maxLine);
		_pimpl->ui->spinBox->setPrefix(tr("Line: "));
		_pimpl->ui->spinBox->setValue(_pimpl->curLine);
	}
	else
	{
		QString window = tr("Position (%1 - %2),").arg(0).arg(_pimpl->maxPos);
		QString current = tr("current: %1").arg(_pimpl->curPos);
		_pimpl->ui->lblWindow->setText(window);
		_pimpl->ui->lblCurrent->setText(current);
		_pimpl->ui->spinBox->setRange(0, _pimpl->maxPos);
		_pimpl->ui->spinBox->setPrefix(tr("Position: "));
		_pimpl->ui->spinBox->setValue(_pimpl->curPos);
	}
}
