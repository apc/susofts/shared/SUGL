#include "ShareablePointsListIOLexer.hpp"

#include <Qsci/qsciscintilla.h>

bool ShareablePointsListIOLexer::utf8() const noexcept
{
	return editor() ? editor()->isUtf8() : IShareablePointsListIO::utf8();
}
