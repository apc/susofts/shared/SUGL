/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTSTATUSBAR_HPP
#define TEXTSTATUSBAR_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"

class QsciScintilla;

/**
 * Status widget for the TextEditor.
 *
 * This widget shows the size of the current document (number of lines and number of bytes), the current position, and if necessary the current selection.
 */
class SUGL_SHARED_EXPORT TextStatusBar : public QWidget
{
	Q_OBJECT

public:
	/** Constructor */
	TextStatusBar(const QsciScintilla *editor, QWidget *parent = nullptr);
	virtual ~TextStatusBar() override;

public slots:
	/** Called when the cursor position changes. It updates the position indications (lin, index) */
	void positionChanged(int line, int index);
	/** Called when selection changes */
	void selectionChanged();
	/** Called when the document changes. It updates information about the document (number of lines...) */
	void textChanged();

signals:
	void doubleClick();

protected:
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

private:
	/** pimpl */
	class _TextStatusBar_pimpl;
	std::unique_ptr<_TextStatusBar_pimpl> _pimpl;
};

#endif // TEXTSTATUSBAR_HPP
