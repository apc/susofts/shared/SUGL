/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTEDITOR_HPP
#define TEXTEDITOR_HPP

#include <functional>
#include <memory>
#include <set>
#include <vector>

#include <Qsci/qsciscintilla.h>

#include "PluginsGlobal.hpp"

class QFocusEvent;
class QMouseEvent;

/**
 * Basic enhanced text editor.
 *
 * Text editor with the following features:
 * - show line number
 * - current line highlighted
 * - whitespace visibility and color
 * - folding
 * - brace matching
 * - annotation display
 *
 * All these features can be saved in the settings with the struct TextEditorConfig.
 *
 * This class can be inherited to add more features. Though, if you simply need to
 * add a Lexer, you can do it with this class.
 *
 * Note that the settings are automatically updated as soon as you add a TextEditorConfigObject
 * in the SettingsManager.
 */
class SUGL_SHARED_EXPORT TextEditor : public QsciScintilla
{
	Q_OBJECT

public:
	/** Constructor */
	TextEditor(QWidget *parent = nullptr);
	virtual ~TextEditor() override;

	void contextMenuEvent(QContextMenuEvent *event) override;
	/** Creates a standard context menu and populates it with TextEditor actions. */
	virtual QMenu *createContextMenu();
	/** Add the given actions to the contextual menu */
	void additionalContextualActions(std::vector<QAction *> actions) noexcept;
	/** @return the actions added to the contextual menu */
	const std::vector<QAction *> &additionalContextualActions() const noexcept;

	// clipboard management
	/**
	 * Add support for some mime types for all functions that requires it (drag & drop, copy / paste).
	 *
	 * You should provide the name of the mime types, along with a function able to generate a string from a mime data,
	 * and a function that generates a mime data from a string.
	 *
	 * @note Mime type `text/plain` is always supported and is the default.
	 *
	 * @param mimeTypes the mime types that should be supported
	 * @param fromMime a function that takes a QMimeData and returns a QString. If it can't convert any information from the QMimeData, it should return
	 *	an empty string, the `text/plain` will be taken automatically.
	 * @param toMime a function that takes a string and a QMimeData. It should return a QMimeData with all the mime types converted from the string. If you
	 *	don't return the given mime data, you should delete it and return a new one. Note that the mime type `text/plain` is already filled in the given
	 *	mime data.
	 */
	void setMimeTypesSupport(std::set<std::string> mimeTypes, std::function<QByteArray(const QMimeData *)> fromMime, std::function<QMimeData *(const QByteArray &, QMimeData *)> toMime) noexcept;

	// QScintilla
	// Shadowing some regressed methods in QScintilla. Shadowing is done as these methods are not virtual and cannot be overriden.
	// If you use this method, please make sure that you call it using TextEditor pointer, if QsciScintilla pointer is used then this
	// method implementation will not be called.
	QString annotation(int line) const;

public slots:
	// QsciScintilla
	virtual void setLexer(QsciLexer *lex = nullptr) override;

	/** Updates the widget with the given config (already connected to the SettingsManager) */
	virtual void updateUi(const QString &config);
	/** Jump to the given line / position */
	virtual void jumpTo(int position, bool isLine = true);
	/** Open a temporary PointsExportConfig settings window and copy desired attributes. */
	virtual void specialCopy();

signals:
	void globalIndicatorClicked(QString word, int value);

protected:
	// QWidget
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void focusOutEvent(QFocusEvent *event) override;
	virtual void dragEnterEvent(QDragEnterEvent *event) override;
	virtual void dropEvent(QDropEvent *event) override;

	virtual bool canInsertFromMimeData(const QMimeData *source) const override;
	virtual QByteArray fromMimeData(const QMimeData *source, bool &rectangular) const override;
	virtual QMimeData *toMimeData(const QByteArray &text, bool rectangular) const override;
	/** Highlight all instances of selected text provided that these they do not have previously set indicators. */
	virtual void highlightSelected();

private:
	/** pimpl */
	class _TextEditor_pimpl;
	std::unique_ptr<_TextEditor_pimpl> _pimpl;
};

#endif // TEXTEDITOR_HPP
