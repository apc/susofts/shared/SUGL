#include "MarkerViewer.hpp"
#include "ui_MarkerViewer.h"

#include <algorithm>
#include <bitset>
#include <map>
#include <unordered_set>
#include <vector>

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>

#include <Qsci/qsciscintilla.h>

#include <utils/StylesHelper.hpp>

namespace
{
const QString LBLTEMPLATE = QObject::tr("Total: %1 marker(s) displayed");

constexpr int COLUMN_COUNT = 3;
constexpr int COLUMN_TYPE = 0;
constexpr int COLUMN_LINENBR = 1;
constexpr int COLUMN_LINETXT = 2;

class MarkerTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	MarkerTableModel(QsciScintilla *editor, TextMarkerDefinitions &markers, QWidget *parent) : QAbstractTableModel(parent), _editor(editor), _markers(markers)
	{
		_table.reserve(100);
		connect(_editor, &QsciScintilla::SCN_MODIFIED, this, &MarkerTableModel::editorChanged);
	}

	// QAbstractTableModel
	virtual int rowCount(const QModelIndex & = QModelIndex()) const override { return (int)_table.size(); }
	virtual int columnCount(const QModelIndex & = QModelIndex()) const override { return COLUMN_COUNT; }
	virtual QVariant data(const QModelIndex &index, int role = Qt::ItemDataRole::DisplayRole) const override
	{
		if (!index.isValid() || index.column() >= COLUMN_COUNT || index.row() >= _table.size()
			|| (index.column() == COLUMN_TYPE && role != Qt::ItemDataRole::DecorationRole && role != Qt::ItemDataRole::ToolTipRole)
			|| (index.column() == COLUMN_LINENBR && role != Qt::ItemDataRole::DisplayRole && role != Qt::ItemDataRole::FontRole)
			|| (index.column() == COLUMN_LINETXT && role != Qt::ItemDataRole::DisplayRole && role != Qt::ItemDataRole::ToolTipRole && role != Qt::ItemDataRole::FontRole))
			return QVariant();

		static const QFont _font = ([]() -> QFont {
			QFont font("Arial", 9);
			font.setStyleHint(QFont::StyleHint::Monospace);
			return font;
		})();

		switch (index.column())
		{
		case COLUMN_TYPE:
			if (role == Qt::ItemDataRole::DecorationRole)
				return QPixmap(_markers.at(_table[index.row()].markerID).iconpath).scaled({16, 16});
			if (role == Qt::ItemDataRole::ToolTipRole)
				return _markers.at(_table[index.row()].markerID).name;
		case COLUMN_LINENBR:
			if (role == Qt::ItemDataRole::DisplayRole)
				return QString::number(_table[index.row()].lineNumber + 1);
			if (role == Qt::ItemDataRole::FontRole)
				return _font;
		case COLUMN_LINETXT:
			if (role == Qt::ItemDataRole::DisplayRole)
				return _table[index.row()].lineText;
			if (role == Qt::ItemDataRole::ToolTipRole)
				return _table[index.row()].lineText;
			if (role == Qt::ItemDataRole::FontRole)
				return _font;
		}
		return QVariant();
	}
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::ItemDataRole::DisplayRole) const override
	{
		if (role != Qt::ItemDataRole::DisplayRole || orientation != Qt::Orientation::Horizontal || section >= COLUMN_COUNT)
			return QVariant();

		switch (section)
		{
		case COLUMN_TYPE:
			return "";
		case COLUMN_LINENBR:
			return tr("#");
		case COLUMN_LINETXT:
			return tr("Line");
		}
		return QVariant();
	}

	// MarkerTableModel
	int getLine(int row) const { return _table[row].lineNumber; }
	int getID(int row) const { return _table[row].markerID; }
	void onDeleteAll()
	{
		std::bitset<32> markers;

		for (int i = (int)_table.size() - 1; i >= 0; i--)
		{
			const auto &item = _table[i];
			markers = _editor->markersAtLine(item.lineNumber);
			if (markers.none())
				_lines.erase(item.lineNumber);
			if (!markers[item.markerID])
			{
				beginRemoveRows({}, i, i);
				_table.erase(std::cbegin(_table) + i);
				endRemoveRows();
			}
		}
	}
	void insertRows(int markers, int line)
	{
		if (!markers)
			return;

		const std::bitset<32> markerids = markers;

		beginInsertRows({}, (int)_table.size(), (int)_table.size() + (int)markerids.count() - 1);
		for (int i = 0; i < (int)markerids.size(); i++)
		{
			if (markerids[i])
				_table.push_back(MarkerTableLine{line, _editor->text(line).simplified(), i});
		}
		_lines.insert(line);
		endInsertRows();
	}

public slots:
	void doubleClicked(const QModelIndex &index)
	{
		if (!index.isValid() || index.column() >= COLUMN_COUNT || index.row() >= _table.size())
			return;

		const int linenbr = _table[index.row()].lineNumber;
		_editor->SendScintilla(QsciScintillaBase::SCI_GOTOLINE, linenbr);
		_editor->ensureLineVisible(linenbr);
		_editor->setFocus();
	}
	void editorChanged(int pos, int mtype, const char *, int len, int, const int line, int, int, int, int)
	{
		constexpr int filter = QsciScintilla::SC_MOD_INSERTTEXT | QsciScintilla::SC_MOD_DELETETEXT | QsciScintilla::SC_MOD_CHANGEMARKER;
		const int cmtype = mtype;
		if (!(mtype & filter))
			return;

		const int cpos = pos;
		const int clen = len;
		const int cline = line;

		if (cmtype & QsciScintilla::SC_MOD_CHANGEMARKER && cline < 0) // on `QscinScintilla::markerDeleteAll()`
			onDeleteAll();
		else if (cmtype & QsciScintilla::SC_MOD_CHANGEMARKER)
		{
			const std::bitset<32> markers = _editor->markersAtLine(cline);
			if (_lines.find(cline) == std::cend(_lines)) // easy: we add new rows
				insertRows((int)markers.to_ulong(), cline);
			else // need to add / remove lines
			{
				std::bitset<32> markercopies = markers;
				for (int i = (int)_table.size() - 1; i >= 0; i--)
				{
					const auto &item = _table[i];
					if (item.lineNumber != cline)
						continue;
					if (!markers[item.markerID])
					{
						beginRemoveRows({}, i, i);
						_table.erase(std::cbegin(_table) + i);
						endRemoveRows();
					}
					else
						markercopies[item.markerID] = false;
				}
				if (markercopies.any())
					insertRows((int)markercopies.to_ulong(), cline);
				else if (markers.none())
					_lines.erase(cline);
			}
		}
		else if (cmtype & (QsciScintilla::SC_MOD_INSERTTEXT | QsciScintilla::SC_MOD_DELETETEXT)) // text updated, need to update the line text
		{
			const int finalLine = _editor->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, pos + len);
			const int rowcount = (int)_table.size();
			QString curLineText;
			int min = rowcount, max = 0;
			for (int curLine = _editor->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, pos); curLine <= finalLine; curLine++)
			{
				if (_lines.find(curLine) == std::cend(_lines))
					continue;
				curLineText = _editor->text(curLine).simplified();
				for (int row = 0; row < rowcount; row++)
				{
					if (_table[row].lineNumber == curLine)
					{
						_table[row].lineText = curLineText;
						min = std::min(min, row);
						max = std::max(max, row);
					}
				}
			}
			emit dataChanged(createIndex(min, COLUMN_LINETXT), createIndex(max, COLUMN_LINETXT), {Qt::ItemDataRole::DisplayRole, Qt::ItemDataRole::ToolTipRole});
		}
		emit markersChanged();
	}

signals:
	void markersChanged();

private:
	struct MarkerTableLine
	{
		/** line number in the text */
		int lineNumber = -1;
		/** copy of the text line */
		QString lineText;
		/** marker id / icon of the marker */
		int markerID;
	};

private:
	QsciScintilla *_editor;
	/** store the markerids for tooltips and icons */
	TextMarkerDefinitions &_markers;
	/** actual model */
	std::vector<MarkerTableLine> _table;
	/** set of lines that we have in the table */
	std::unordered_set<int> _lines;
};

class MarkerTableModelProxy : public QSortFilterProxyModel
{
	Q_OBJECT

public:
	MarkerTableModelProxy(QObject *parent) : QSortFilterProxyModel(parent)
	{
		setFilterKeyColumn(COLUMN_TYPE);
		setFilterRole(Qt::ItemDataRole::ToolTipRole);
	};

	// QSortFilterProxyModel
	virtual void setSourceModel(QAbstractItemModel *sourceModel) override
	{
		QSortFilterProxyModel::setSourceModel(sourceModel);
		_sourceModel = qobject_cast<MarkerTableModel *>(sourceModel);
	}

public slots:
	void doubleClicked(const QModelIndex &index)
	{
		if (_sourceModel)
			_sourceModel->doubleClicked(mapToSource(index));
	}

protected:
	// QSortFilterProxyModel
	virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override
	{
		if (_sourceModel)
		{
			if (source_left.column() == COLUMN_TYPE && source_right.column() == COLUMN_TYPE) // sort on marker id
			{
				if (_sourceModel->getID(source_left.row()) != _sourceModel->getID(source_right.row()))
					return _sourceModel->getID(source_left.row()) < _sourceModel->getID(source_right.row());
				return _sourceModel->getLine(source_left.row()) < _sourceModel->getLine(source_right.row());
			}
			if (source_left.column() == COLUMN_LINENBR && source_right.column() == COLUMN_LINENBR) // sort on line number
				return _sourceModel->getLine(source_left.row()) < _sourceModel->getLine(source_right.row());
		}
		return QSortFilterProxyModel::lessThan(source_left, source_right); // default sort
	}

private:
	MarkerTableModel *_sourceModel = nullptr;
};
} // namespace
#include "MarkerViewer.moc"

class MarkerViewer::_MarkerViewer_pimpl
{
public:
	std::unique_ptr<Ui::MarkerViewer> ui;
	QAbstractItemModel *model = nullptr;
	TextMarkerDefinitions markers;
};

MarkerViewer::MarkerViewer(QsciScintilla *editor, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_MarkerViewer_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::MarkerViewer>();
	_pimpl->ui->setupUi(this);

	// Table view
	auto *baseModel = new MarkerTableModel(editor, _pimpl->markers, this);
	auto *proxy = new MarkerTableModelProxy(this);
	_pimpl->model = proxy;
	proxy->setSourceModel(baseModel);
	_pimpl->ui->tableView->setModel(_pimpl->model);
	_pimpl->ui->tableView->horizontalHeader()->setSortIndicatorShown(true);
	_pimpl->ui->tableView->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
	_pimpl->ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
	_pimpl->ui->tableView->horizontalHeader()->resizeSection(0, 20);
	_pimpl->ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
	_pimpl->ui->tableView->verticalHeader()->setDefaultSectionSize(10);
	connect(_pimpl->ui->tableView, &QTableView::doubleClicked, proxy, &MarkerTableModelProxy::doubleClicked);
	// Combobox
	updateFilters(markerDefinitions);
	connect(_pimpl->ui->cbFilter, qOverload<int>(&QComboBox::currentIndexChanged), [this, proxy](int idx) -> void {
		const auto &filter = *_pimpl->ui->cbFilter;
		int filterid = filter.count() > idx ? filter.itemData(idx).toInt() : -1;

		if (filterid < 0)
			proxy->setFilterFixedString("");
		else
			proxy->setFilterFixedString(_pimpl->markers[filterid].name);
		_pimpl->ui->lblNbDisplayed->setText(LBLTEMPLATE.arg(proxy->rowCount()));
	});
	connect(baseModel, &MarkerTableModel::markersChanged, [this, proxy]() -> void { _pimpl->ui->lblNbDisplayed->setText(LBLTEMPLATE.arg(proxy->rowCount())); });
	// Theme
	setStyleSheet(QString("alternate-background-color: %1;").arg(getThemeAdaptedColor(QColor(200, 200, 200)).name(QColor::HexArgb)));
}

MarkerViewer::~MarkerViewer() = default;

void MarkerViewer::updateFilters(TextMarkerDefinitions markers)
{
	_pimpl->markers = std::move(markers);
	auto &filter = *_pimpl->ui->cbFilter;
	int curdata = filter.count() > 0 ? filter.currentData().toInt() : -1;
	filter.blockSignals(true);
	filter.clear();
	filter.addItem(QIcon(":/markers/nofiltering"), tr("Show all markers"), -1); // No filtering
	// insert items sorted on the type
	std::map<decltype(_pimpl->markers)::key_type, const decltype(_pimpl->markers)::mapped_type &> values(std::cbegin(_pimpl->markers), std::cend(_pimpl->markers));
	for (const auto &[id, def] : values)
		filter.addItem(QIcon(def.iconpath), def.name, id);
	filter.blockSignals(false);
	filter.setCurrentIndex(filter.findData(curdata));
}
