/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef MARKERVIEWER_HPP
#define MARKERVIEWER_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"
#include "TextUtils.hpp"

class QsciScintilla;

/**
 * UI for text markers. It shows all the markers of the TextEditor in a table.
 *
 * Marker list can be filtered via the combo box. The content of the combobox defaults to markerDefinitions.
 * You can add / remove markers from the filter thanks to the function updateFilters().
 */
class SUGL_SHARED_EXPORT MarkerViewer : public QWidget
{
	Q_OBJECT

public:
	MarkerViewer(QsciScintilla *editor, QWidget *parent = nullptr);
	virtual ~MarkerViewer() override;

public slots:
	/**
	 * Update the combobox to filter markers.
	 * The option to show everything is automatically built, you shouldn't add it in the given list.
	 */
	void updateFilters(TextMarkerDefinitions markers);

private:
	/** pimpl */
	class _MarkerViewer_pimpl;
	std::unique_ptr<_MarkerViewer_pimpl> _pimpl;
};

#endif // MARKERVIEWER_HPP
