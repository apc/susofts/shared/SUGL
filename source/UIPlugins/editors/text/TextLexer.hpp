/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTLEXER_HPP
#define TEXTLEXER_HPP

#include <list>
#include <memory>
#include <utility>

#include <Qsci/qscilexercustom.h>

#include "PluginsGlobal.hpp"

/**
 * Base class for custom lexers.
 *
 * This class integrates some mechanisms that ease the use of a QsciLexerCustom.
 *
 * Note that this class has an inner class `StyleManager` used to help you
 * with the styling. You shouldn't have to access it directly.
 *
 * When the Lexer is automatically called by QScintilla for update, the method
 * styleText(int start, int end) is automatically called.
 * This method will call your implementation of styleText() for each line to style.
 *
 * The line to style will be stored in _currentLine. You need to style it by calling
 * the function addStyle(). Any characters that have not been styled will get the default
 * style (`0`). You can see the characters of the current line that have not been styled
 * with the function unstyledLine().
 *
 * For each line styled, you can add an annotation by changing the value of the attributes:
 * _infoAnnotation in case of information and _errorAnnotation in case of error (these attributtes are reset for each line).
 *
 * Finally, all the styles will be applied at the end of the process, thanks to the method
 * applyStyles().
 */
class SUGL_SHARED_EXPORT TextLexer : public QsciLexerCustom
{
	Q_OBJECT

public:
	TextLexer(QObject *parent = nullptr);
	virtual ~TextLexer() override;

	// QsciLexerCustom
	/**
	 * Function automatically called by QScintilla when some styling is needed.
	 *
	 * This method will call styleText() for each line to style.
	 */
	virtual void styleText(int start, int end) override;
	// for autocompletion
	virtual bool caseSensitive() const override;

signals:
	void globalIndicatorClicked(QString word, int value);

protected:
	// styling
	/**
	 * This method should style one line.
	 * The line to style is available in _currentLine.
	 */
	virtual void styleText() = 0;
	/**
	 * Starts the lexer.
	 * Initialize all variables related to the lexer. Actually set _currentText with the content of the editor between `start` and `end`, and then call initLexer().
	 */
	virtual bool startLexer(int start, int end);
	/**
	 * Initialize the lexer.
	 * Initialize the lexer variables, assuming that _currentText is already set. This can be useful when you want to use the lexer to analyze text when the lexer is not
	 * attached to any editor. Only variables not linked to the editor should be initialized here.
	 *
	 * Note that this function is automatically called by startLexer(int, int) once it has initialized _currentText.
	 */
	virtual void initLexer();
	/** Clears the lexer */
	virtual void endLexer();
	/** Apply all the styles stored in _styles */
	virtual void applyStyles();
	/**
	 * Add a style to the _currentLine.
	 * @param offset the number of character to style
	 * @param style the style to apply
	 */
	void addStyle(int offset, int style) { _styles.addStyle(offset, style); }
	/** Removes all the styles from the _currentLine */
	void resetLine() noexcept { _styles.resetLine(); }
	/** Return the part of _currentLine that is not styled */
	QStringRef unstyledLine() const { return _currentLine.right(_currentLine.size() - _styles.getPosline()); }

	// annotations
	/**
	 * Add an annotation to the given position.
	 * @param pos a position in the line that we want to annotate
	 * @param annotation the annotation text
	 * @param style the style to give to the annotation
	 */
	virtual void annotate(int pos, const QString &annotation, int style = 0);
	/**
	 * Get the annotation at the given position.
	 * @param pos the position in the text
	 * @return the annotation at the given position if any
	 */
	virtual QString annotation(int pos);

	// folding
	/** Start a folding point for current line */
	void startFolding() noexcept;
	/** End folding for current line */
	void endFolding() noexcept;
	/** set the folding level at the line at the given position. This function should only be called in styleText(int, int) */
	void foldLine(int pos, bool isempty = false);
	/** default fold level */
	constexpr int defaultFoldLevel() const noexcept { return 0x400; }

	// indicators
	/**
	 * Returns the position where to jump in case of a CTRL+Click on an indicator.
	 *
	 * Default implementation returns the value associated with the indicator at the given position.
	 * @return the position where to jump (if -1 is returned, nothing happens)
	 * @param indicator the type of the indicator who has been CTRL+Clicked
	 * @param position the position of the click
	 * @param indicatorText the text associated with the indicator
	 */
	virtual int indicatorJumpTo(int indicatorType, int position, const QString &indicatorText);
	/**
	 * Call tip for the given position.
	 *
	 * This method returns the calltip that should be shown at the given position.
	 * @param position the position of the mouse in the text
	 * @param indicator the indicator string that the mouse is hovering
	 * @return the text for the call tip, or empty string of no call tip should be shown
	 */
	virtual QString calltip([[maybe_unused]] int position, [[maybe_unused]] int indicatorType, [[maybe_unused]] const QString &indicator) const { return QString(); }

	// others
	/**
	 * Return the line where the position is.
	 * Read the line from _currentText.
	 */
	QStringRef readLine(int position) const noexcept;

protected:
	/**
	 * Inner class used to managed styles and position pointers.
	 */
	class StyleManager
	{
	public:
		/**
		 * Constructor.
		 * @param curpos a reference on the global position cursor, so it will automatically be updated when a style is applied.
		 */
		StyleManager(int &curpos) : _curpos(curpos) {}
		virtual ~StyleManager() = default;

		/** Clear everything and reset internal cursors */
		void clear() noexcept
		{
			_currentStyles.clear();
			_posline = _endline = -1;
		}

		/**
		 * Add a line (empty) in the styles. Internal cursor _posline is reset to 0.
		 * @param size the size of the new line (for the _endline internal cursor).
		 */
		void addLine(int size)
		{
			_currentStyles.emplace_back();
			_posline = 0;
			_endline = size;
		}
		/**
		 * Remove the last line.
		 * Internal cursor is set at the end of previous line.
		 * @return the size of the removed line (addition of all offsets of the line)
		 */
		int popLine() noexcept
		{
			if (_currentStyles.empty())
				return -1;
			int offset = lineSize();
			_currentStyles.pop_back();
			_curpos -= offset;
			_posline = _endline = lineSize();
			return offset;
		}
		/**
		 * Clear the last line (removes all the styles).
		 * @return the size of the removed line (addition of all offsets of the line)
		 */
		int resetLine() noexcept
		{
			if (_currentStyles.empty())
				return -1;
			int offset = lineSize();
			_currentStyles.back().clear();
			_curpos -= offset;
			_posline = 0;
			_endline += offset;
			return offset;
		}
		/** @return the number of lines */
		size_t size() const noexcept { return _currentStyles.size(); }
		/** @return true if no lines have been added */
		bool empty() const noexcept { return _currentStyles.empty(); }
		/**
		 * Add a style to the last line.
		 * @param offset the number of character that will have the given style applied
		 * @param style the ID of the style
		 */
		void addStyle(int offset, int style)
		{
			if (_currentStyles.empty() || offset <= 0)
				return;
			if (offset > _endline)
				offset = _endline;
			_currentStyles.back().emplace_back(offset, style);
			move(offset);
		}
		/**
		 * Remove the last style in the last line.
		 * @return the size of the last style
		 */
		int popStyle() noexcept
		{
			if (_currentStyles.empty())
				return -1;
			int size = _currentStyles.back().back().first;
			_currentStyles.back().pop_back();
			move(-size);
			return size;
		}
		/** @return the size of the last line (addition of all offsets) */
		int lineSize() const noexcept
		{
			if (_currentStyles.empty())
				return -1;
			int acc = 0;
			for (const auto &s : _currentStyles.back())
				acc += s.first;
			return acc;
		}
		/** @return the number of styles in last line */
		size_t lineStylesSize() const noexcept
		{
			if (_currentStyles.empty())
				return (size_t)-1;
			return _currentStyles.back().size();
		}
		/** @return the last line (current line) */
		std::list<std::pair<int, int>> &lastLine() noexcept { return _currentStyles.back(); }
		const std::list<std::pair<int, int>> &lastLine() const noexcept { return _currentStyles.back(); }

		int getPosline() const noexcept { return _posline; }
		int getEndline() const noexcept { return _endline; }

		auto begin() noexcept { return _currentStyles.begin(); }
		auto cbegin() const noexcept { return _currentStyles.cbegin(); }
		auto end() noexcept { return _currentStyles.end(); }
		auto cend() const noexcept { return _currentStyles.cend(); }

	protected:
		/** Move the cursors to the new position (size character) */
		void move(int size) noexcept
		{
			_posline += size;
			_curpos += size;
			_endline -= size;
		}

	protected:
		/** Current position in the line */
		int _posline = -1;
		/** Number of character to the end */
		int _endline = -1;
		/** Reference to the current position pointer */
		int &_curpos;
		/**
		 * List of lines.
		 * Each line is represented by a list of (pair of) offsets and styles.
		 * the offset is the last character to have the attached style.
		 */
		std::list<std::list<std::pair<int, int>>> _currentStyles;
	};

protected:
	/** Text that is currently evaluated */
	QString _currentText;
	/** Current evaluated line in the text */
	QStringRef _currentLine;
	/** Error annotation for the current line */
	QString _errorAnnotation;
	/** Information annotation for the current line */
	QString _infoAnnotation;
	/** Starting position for the current text in the global text */
	int _start = -1;
	/** End position for the current text in the global text */
	int _end = -1;
	/** Current position in the current text during evaluation */
	int _curpos;
	/** All the styles applied to the current text */
	StyleManager _styles;

private slots:
	/**
	 * Slot connected to QsciScintilla::indicatorReleased().
	 *
	 * It will either go to the position indicated by value if the indicator type is TextIndicator::INTERNAL, or emit globalIndicatorClicked()
	 */
	void indicatorCtrlClicked(int line, int index, Qt::KeyboardModifiers state);
	/**
	 * Called when an indicator is hovered.
	 *
	 * It calls the function calltip() and show its result as a call tip.
	 */
	void indicatorHovered(int position, int x, int y);

private:
	// pimpl
	class _TextLexer_pimpl;
	std::unique_ptr<_TextLexer_pimpl> _pimpl;
};

#endif // TEXTLEXER_HPP
