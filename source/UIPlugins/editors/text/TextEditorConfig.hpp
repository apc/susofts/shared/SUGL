/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTEDITORCONFIG_HPP
#define TEXTEDITORCONFIG_HPP

#include <memory>

#include "PluginsGlobal.hpp"
#include "interface/SConfigInterface.hpp"

class QPushButton;

/**
 * Config object for TextEditor class.
 */
struct SUGL_SHARED_EXPORT TextEditorConfigObject : public SConfigObject
{
	TextEditorConfigObject();
	virtual ~TextEditorConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	// Caret Line
	/** Tells if we should highlight the current line */
	bool caretLineVisible = true;
	/** What highlight color is used for the current line */
	QColor caretLineBg = {0, 255, 255, 50};
	// Selection Result Color
	QColor SelectedResultBg = {200, 240, 200, 200};
	// White Spaces
	/** Tells if we should see invisible characters */
	int whitespaceVisible;
	/** Size of the invisible characters */
	int whitespaceSize = 2;
	/** Color for the invisible characters */
	QColor whitespaceFg = {255, 165, 0, 10};
	// autocompletion
	/** autocompletion is case sensitive */
	bool acCaseSensitive = false;
	/**
	 * Number of character to type before we show the autocompletion list.
	 * If 0 (or less), autocompletion is disabled, but we can hit CTRL+SPACE to start it manually.
	 */
	int acThreshold = 1;
	// misc
	/** How to handle code folding */
	int foldStyle;
	/** How to handle brace matching */
	int braceMatching;
	/** Display of annotations */
	int annotationDisplay;
	/** Size of a tabulation */
	int tabSize = 4;
	/** zoom (restore previous) */
	int zoom = 0;
};

/**
 * Widget to edit the configuration for TextEditor class.
 */
class SUGL_SHARED_EXPORT TextEditorConfig : public SConfigWidget
{
	Q_OBJECT

public:
	static const QString &configName() noexcept;

	/** Constructor */
	TextEditorConfig(QWidget *parent = nullptr);
	virtual ~TextEditorConfig() override;

	// SconfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private slots:
	void changeCurLineColor();
	void changeSelectionColor();
	void changeWsColor();

private:
	void changeBtnColor(QPushButton *btn, QColor color);

private:
	/** pimpl */
	class _TextEditorConfig_pimpl;
	std::unique_ptr<_TextEditorConfig_pimpl> _pimpl;
};

#endif // TEXTEDITORCONFIG_HPP
