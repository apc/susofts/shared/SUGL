/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEXTUTILS_HPP
#define TEXTUTILS_HPP

#include <bitset>
#include <unordered_map>

#include <QObject>

#include "PluginsGlobal.hpp"

class QsciScintilla;
class QString;

// margins

/**
 * Margins IDs for QScintilla.
 * By default, we can have 5 margins, from 0 to SC_MAX_MARGIN.
 */
enum TextMargin
{
	/** Margin for showing line numbers */
	LINENUMBER,
	/** Margin to show errors, warnings, info markers from the lexers */
	MARKERREPORT,
	/** Margin to mark the search results */
	MARKERSEARCH,
	/** Margin to add user markers */
	MARKERUSER,
	/** Margin for folding symbols */
	FOLDING
};

// markers

/**
 * Markers defined.
 * We can define markers from 0 to 25. The following markers are provided for conveniency.
 */
enum TextMarker
{
	// informational
	/** Error (red cross) */
	ERROR,
	/** Warning (yellow triangular sign) */
	WARNING,
	/** Information (blue circle) */
	INFO,
	// search results
	/** Search markers (magnifying glass) */
	SEARCH = 10,
	// user markers
	/** User marker (blue circle) */
	USER1 = 20,
	/** Second user marker (purple circle) */
	USER2
};

/**
 * Definition of a marker.
 * Defines its name and its icon. Used in markerDefinitions.
 */
struct TextMarkerDefinition
{
	QString name;
	QString iconpath;
};

using TextMarkerDefinitions = std::unordered_map<int, TextMarkerDefinition>;
/**
 * Map the marker ID to its definition.
 * This is used by MarkerViewer.
 *
 * Key is the marker ID (generally a TextMarker), value is its definition (name and icon).
 */
inline const TextMarkerDefinitions markerDefinitions = {
	{TextMarker::ERROR, {QObject::tr("Error"), ":/markers/error"}},
	{TextMarker::WARNING, {QObject::tr("Warning"), ":/markers/warning"}},
	{TextMarker::INFO, {QObject::tr("Info"), ":/markers/information"}},
	{TextMarker::SEARCH, {QObject::tr("Search"), ":/actions/find"}},
	{TextMarker::USER1, {QObject::tr("User1"), ":/markers/circle-blue"}},
	{TextMarker::USER2, {QObject::tr("User2"), ":/markers/circle-purple"}},
};

/**
 * Structure to facilitate advanced line editing features.
 * It stores the information for lines and position for their beginning and end.
 * It also stores the information about orientation of the selection.
 */
struct Lines
{
	bool selectionUpward = false;
	int startLine = -1;
	int startPos = -1;
	int endLine = -1;
	int endPos = -1;
};
/**
 * Initializes a structure for line editing for selected text. If no selection is provided - only currently selected line will be considered.
 * @param editor the editor
 * @return @Lines structure
 */
SUGL_SHARED_EXPORT Lines initEditingLines(QsciScintilla *editor);
/**
 * Restores the selection for the @Lines specified whose contents might have been modified.
 * @param editor the editor
 * @param lines @Lines (range) fot which selection should be restored
 */
SUGL_SHARED_EXPORT void restoreSelection(QsciScintilla *editor, const Lines &lines);

/**
 * Add a marker to the line at the given position.
 * @param editor the editor
 * @param pos the position
 * @param marker_id the ID of the marker to add in the margin, generally a TextMarker
 */
SUGL_SHARED_EXPORT void addMarker(QsciScintilla *editor, int pos, int marker_id);

// indicators

/**
 * Indicators defined.
 * We can define lexer specific indicator (0 to 7), and other indicators (8 to 31). Note that the 3 first indicators (0 to 2) already have a style assigned.
 * The following markers are provided for conveniency.
 * If some indicators have the same styiling, the indicator with higher value takes priority.
 */
enum TextIndicator
{
	// Lexer
	/** Hover only (no link, @see TextLexer::addIndication()) */
	HOVERONLY = 3,
	/** Internal link (CTRL+Click, @see TextLexer::addIndication()) */
	INTERNAL,
	/** External link (CTRL+Click, @see TextLexer::addIndication()) */
	EXTERNAL,
	// Other
	/** Result of a quick search when a word is selected */
	SELECTEDRESULT = 8,
	/** Result of a search */
	SEARCHRESULT
};

/**
 * Insert indicator at the given position.
 * @param editor the editor
 * @param indicatorType the indicator ID (generally a TextIndicator)
 * @param position the position where to put it
 * @param nbchar the number of character that should take the indicator
 * @param value the value for this indication. In case of TextIndicator::INTERNAL, the value is the position we will jump to on click
 */
SUGL_SHARED_EXPORT void addIndication(QsciScintilla *editor, int indicatorType, int position, int nbchar, int value = -1);

/**
 * Count indications of given type in the entire editor.
 * @param editor the editor
 * @param indicatorType the indicator ID (generally a TextIndicator)
 */
SUGL_SHARED_EXPORT int countIndications(const QsciScintilla *editor, int indicatorType);

/**
 * Clear provided indicator in the editor.
 * @param editor the editor
 * @param indicatorType the indicator ID (generally a TextIndicator)
 */
SUGL_SHARED_EXPORT void clearIndications(QsciScintilla *editor, int indicatorType);

/**
 * Returns the indicators at the given position as a bitset.
 * @param editor the editor
 * @param position the position where it is checked for indicators
 */
SUGL_SHARED_EXPORT std::bitset<32> getIndicators(QsciScintilla *editor, int position);

// others

/**
 * Set the background for the editor.
 *
 * Mainly used for output editors (non editable) with a QColor(230, 230, 230).
 * @param editor the editor to set the background
 * @param color the background color
 */
SUGL_SHARED_EXPORT void setBackground(QsciScintilla *editor, const QColor &color);

/**
 * Transform the given text to byte array.
 * Takes care of the encoding (UTF-8 or Latin1).
 *
 * @param text the text to transform
 * @param utf8 true if the text is in UTF-8
 * @return the raw byte array representation of text
 */
SUGL_SHARED_EXPORT QByteArray toByteArray(const QString &text, bool utf8);
/**
 * Read the byte array and create a QString from it.
 * Takes care of the encoding (UTF-8 or Latin1).
 *
 * @param text the raw byte array representation
 * @param utf8 true if the text is in UTF-8
 * @return the created QString
 */
SUGL_SHARED_EXPORT QString fromByteArray(const QByteArray &text, bool utf8);
/**
 * Read the byte array and convert it to the byte array with all white space characters removed by separator
 *
 * @param text the raw byte array representation
 * @param separator replacing all whitespaces
 * @return the raw byte array representation of text with whitespaces replaced
 */
SUGL_SHARED_EXPORT QByteArray getSeparatedText(const QByteArray &text, char separator);
/**
 * Read the byte array and convert it to the HTML structure with all white space characters treated as a single separator
 *
 * @param text the raw byte array representation
 * @param separator replacing all whitespaces
 * @return the raw byte array representation of HTML text
 */
SUGL_SHARED_EXPORT QByteArray getHTMLText(const QByteArray &text);

/**
 * Parsing the contents of QsciScintilla and aligning it in formatted columns.
 *
 * @param editor the editor
 */
SUGL_SHARED_EXPORT void alignContents(QsciScintilla *editor);

#endif // TEXTUTILS_HPP
