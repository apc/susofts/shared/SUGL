/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef STRUCTUREVIEWER_HPP
#define STRUCTUREVIEWER_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"

class QsciScintilla;

/**
 * Contains all the relevant information for a single fold line.
 **/
struct FoldLine
{
	bool operator==(const FoldLine &o) const noexcept
	{
		return isFolded == o.isFolded && isCategory == o.isCategory && o.line == line && o.lvl == lvl && o.style == style && o.txt == txt && o.category == category;
	}

	bool isFolded;
	bool isCategory;
	int line;
	int lvl;
	int style;
	QString txt;
	QString category;
};
/**
 * Creates a list with all folds for which nodes should be constructed (including category nodes)
 **/
std::list<FoldLine> getFolds(QsciScintilla *editor);

/**
 * UI for the structure of the text (tree).
 *
 * Based on the folding hints in the text, from the lexer, we create a tree representing the structure of the text.
 */
class SUGL_SHARED_EXPORT StructureViewer : public QWidget
{
	Q_OBJECT

public:
	StructureViewer(QsciScintilla *editor, QWidget *parent = nullptr);
	virtual ~StructureViewer() override;

	void setStartCollapsed(bool startCollapsed);

protected:
	void showEvent(QShowEvent *) override;

private:
	void initialize();

private:
	/** pimpl */
	class _StructureViewer_pimpl;
	std::unique_ptr<_StructureViewer_pimpl> _pimpl;
};

#endif // STRUCTUREVIEWER_HPP
