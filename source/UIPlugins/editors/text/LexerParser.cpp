#include "LexerParser.hpp"

#include <Qsci/qsciscintilla.h>

namespace
{
QString _writeErrorTXT(const QString &got, const QStringList &expected)
{
	if (expected.empty())
		return QString("Error: Unexpected '%1'.").arg(got);

	return QString("Error: got '%1', while expecting '%2'.").arg(got, expected.join("', '"));
}

QString _writeErrorHTML(const QString &got, const QStringList &expected)
{
	QString error = R"""(<h1 style="color: red;">Error</h1>)""";
	if (expected.empty())
		return error + QString(R"""(<p style="color: red;">Unexpected '%1'.</p>)""").arg(got);

	return error
		+ QString(R"""(
		<p>Got:</p>
		<ul style="color: red;"><li>%1</li></ul>
		<p>While expecting:</p>
		<ul style="color: green;"><li>%2</li></ul>
		)""")
			  .arg(got, expected.join("</li><li>"));
}
} // namespace

QString writeError(const QString &got, const QStringList &expected, bool html)
{
	return html ? _writeErrorHTML(got, expected) : _writeErrorTXT(got, expected);
}

QString _writeError(State badState, int terminal_count, int eof_symbol, const char *const spell[], int (*t_action)(int, int), bool html)
{
	// token received
	QString got;
	if (badState.token == eof_symbol)
		got = "End Of File (EOF)";
	else
		got = QString("%1:%2").arg(spell[badState.token]).arg(badState.parsed.trimmed().toString());

	// expected tokens
	QStringList expected;
	for (int tk = 0; tk < terminal_count; tk++)
	{
		if (t_action(badState.act, tk) != 0)
			expected.push_back(spell[tk]);
	}

	return writeError(got, expected, html);
}

void _deleteMarkers(QsciScintilla *editor, int marker)
{
	editor->markerDeleteAll(marker);
}

int _textLength(const QsciScintilla *editor)
{
	return editor->length();
}

bool _isEditorUtf8(const QsciScintilla *editor)
{
	return editor->isUtf8();
}
