#include "TextEditorConfig.hpp"
#include "ui_TextEditorConfig.h"

#include <QColorDialog>
#include <QPushButton>
#include <QSettings>

#include <Qsci/qsciscintilla.h>

#include "utils/StylesHelper.hpp"
namespace
{
const QString _configname = "TextEditor";
}

TextEditorConfigObject::TextEditorConfigObject() :
	// White Spaces
	whitespaceVisible((int)QsciScintilla::WhitespaceVisibility::WsInvisible),
	// misc
	foldStyle((int)QsciScintilla::FoldStyle::BoxedTreeFoldStyle),
	braceMatching((int)QsciScintilla::BraceMatch::SloppyBraceMatch),
	annotationDisplay((int)QsciScintilla::AnnotationDisplay::AnnotationHidden)
{
}

bool TextEditorConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const TextEditorConfigObject &oc = static_cast<const TextEditorConfigObject &>(o);
	return // Caret Line
		caretLineVisible == oc.caretLineVisible
		&& caretLineBg == oc.caretLineBg
		// Selection
		&& SelectedResultBg == oc.SelectedResultBg
		// White Spaces
		&& whitespaceVisible == oc.whitespaceVisible && whitespaceSize == oc.whitespaceSize
		&& whitespaceFg == oc.whitespaceFg
		// autocompletion
		&& acCaseSensitive == oc.acCaseSensitive
		&& acThreshold == oc.acThreshold
		// misc
		&& foldStyle == oc.foldStyle && braceMatching == oc.braceMatching && annotationDisplay == oc.annotationDisplay && tabSize == oc.tabSize && zoom == oc.zoom;
}

void TextEditorConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(TextEditorConfig::configName());

	// Caret Line
	settings.setValue("caretLineVisible", caretLineVisible);
	settings.setValue("caretLineBg", caretLineBg);
	// Selection Result
	settings.setValue("SelectedResultBg", SelectedResultBg);
	// White Spaces
	settings.setValue("whitespaceVisible", whitespaceVisible);
	settings.setValue("whitespaceSize", whitespaceSize);
	settings.setValue("whitespaceFg", whitespaceFg);
	// autocompletion
	settings.setValue("acCaseSensitive", acCaseSensitive);
	settings.setValue("acThreshold", acThreshold);
	// misc
	settings.setValue("foldStyle", foldStyle);
	settings.setValue("braceMatching", braceMatching);
	settings.setValue("annotationDisplay", annotationDisplay);
	settings.setValue("tabSize", tabSize);
	settings.setValue("zoom", zoom);

	settings.endGroup();
}

void TextEditorConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(TextEditorConfig::configName());

	// Caret Line
	caretLineVisible = settings.value("caretLineVisible", true).toBool();
	caretLineBg = settings.value("caretLineBg", getThemeAdaptedColor(QColor(0, 255, 255, 50))).value<QColor>();
	// Selection Result
	SelectedResultBg = settings.value("SelectedResultBg", getThemeAdaptedColor(QColor(200, 240, 200, 200))).value<QColor>();
	// White Spaces
	whitespaceVisible = settings.value("whitespaceVisible", (int)QsciScintilla::WhitespaceVisibility::WsInvisible).toInt();
	whitespaceSize = settings.value("whitespaceSize", 2).toInt();
	whitespaceFg = settings.value("whitespaceFg", getThemeAdaptedColor(QColor(255, 165, 0, 10))).value<QColor>();
	// autocompletion
	acCaseSensitive = settings.value("acCaseSensitive", false).toBool();
	acThreshold = settings.value("acThreshold", 1).toInt();
	// misc
	foldStyle = settings.value("foldStyle", (int)QsciScintilla::FoldStyle::BoxedTreeFoldStyle).toInt();
	braceMatching = settings.value("braceMatching", (int)QsciScintilla::BraceMatch::SloppyBraceMatch).toInt();
#ifdef NDEBUG
	annotationDisplay = (int)QsciScintilla::AnnotationDisplay::AnnotationHidden;
#else
	annotationDisplay = settings.value("annotationDisplay", (int)QsciScintilla::AnnotationDisplay::AnnotationHidden).toInt();
#endif
	tabSize = settings.value("tabSize", 4).toInt();
	zoom = settings.value("zoom", 0).toInt();

	settings.endGroup();
}

class TextEditorConfig::_TextEditorConfig_pimpl
{
public:
	std::unique_ptr<Ui::TextEditorConfig> ui;
	QColor curLine;
	QColor selectionResult;
	QColor whitespace;
};

const QString &TextEditorConfig::configName() noexcept
{
	return _configname;
}

TextEditorConfig::TextEditorConfig(QWidget *parent) : SConfigWidget(nullptr, parent), _pimpl(std::make_unique<_TextEditorConfig_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::TextEditorConfig>();
	_pimpl->ui->setupUi(this);
#ifdef NDEBUG // we hide the in text annotation
	_pimpl->ui->annotations->setChecked(false);
	_pimpl->ui->annotations->setVisible(false);
	_pimpl->ui->lblAnnotation->setVisible(false);
#endif
}

TextEditorConfig::~TextEditorConfig() = default;

SConfigObject *TextEditorConfig::config() const
{
	auto *config = new TextEditorConfigObject();
	config->read(); // used for zoom as it doesn't have settings for it

	// Caret Line
	config->caretLineVisible = _pimpl->ui->curLineVisible->isChecked();
	config->caretLineBg = _pimpl->curLine;
	// Selection Result
	config->SelectedResultBg = _pimpl->selectionResult;
	// White Spaces
	config->whitespaceVisible = (int)(_pimpl->ui->wsEnable->isChecked() ? QsciScintilla::WhitespaceVisibility::WsVisible : QsciScintilla::WhitespaceVisibility::WsInvisible);
	config->whitespaceSize = _pimpl->ui->wsSize->value();
	config->whitespaceFg = _pimpl->whitespace;
	// autocompletion
	config->acCaseSensitive = _pimpl->ui->chkAcCase->isChecked();
	config->acThreshold = _pimpl->ui->spinAcThreshold->value();
	// misc
	config->foldStyle = (int)(_pimpl->ui->folding->isChecked() ? QsciScintilla::FoldStyle::BoxedTreeFoldStyle : QsciScintilla::FoldStyle::NoFoldStyle);
	config->braceMatching = (int)(_pimpl->ui->braceMatching->isChecked() ? QsciScintilla::BraceMatch::SloppyBraceMatch : QsciScintilla::BraceMatch::NoBraceMatch);
	config->annotationDisplay = (int)(_pimpl->ui->annotations->isChecked() ? QsciScintilla::AnnotationDisplay::AnnotationBoxed : QsciScintilla::AnnotationDisplay::AnnotationHidden);
	config->tabSize = _pimpl->ui->tabSize->value();
	// zoom - using previously set value

	return config;
}

void TextEditorConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const TextEditorConfigObject *>(conf);
	if (!config)
		return;

	_pimpl->curLine = config->caretLineBg;
	_pimpl->selectionResult = config->SelectedResultBg;
	_pimpl->whitespace = config->whitespaceFg;

	// Caret Line
	_pimpl->ui->curLineVisible->setChecked(config->caretLineVisible);
	changeBtnColor(_pimpl->ui->curLineColorBtn, _pimpl->curLine);
	// Selection Result Color
	changeBtnColor(_pimpl->ui->selectionColorBtn, _pimpl->selectionResult);
	// White Spaces
	_pimpl->ui->wsEnable->setChecked(config->whitespaceVisible != (int)QsciScintilla::WhitespaceVisibility::WsInvisible);
	_pimpl->ui->wsSize->setValue(config->whitespaceSize);
	changeBtnColor(_pimpl->ui->wsColorBtn, _pimpl->whitespace);
	// autocompletion
	_pimpl->ui->chkAcCase->setChecked(config->acCaseSensitive);
	_pimpl->ui->spinAcThreshold->setValue(config->acThreshold);
	// misc
	_pimpl->ui->folding->setChecked(config->foldStyle == (int)QsciScintilla::FoldStyle::BoxedTreeFoldStyle);
	_pimpl->ui->braceMatching->setChecked(config->braceMatching == (int)QsciScintilla::BraceMatch::SloppyBraceMatch);
	_pimpl->ui->annotations->setChecked(config->annotationDisplay != (int)QsciScintilla::AnnotationDisplay::AnnotationHidden);
	_pimpl->ui->tabSize->setValue(config->tabSize);
}

void TextEditorConfig::reset()
{
	TextEditorConfigObject config;
	config.read();
	setConfig(&config);
}

void TextEditorConfig::restoreDefaults()
{
	TextEditorConfigObject config;
	setConfig(&config);
}

void TextEditorConfig::changeCurLineColor()
{
	QColor tmp = QColorDialog::getColor(_pimpl->curLine, this, tr("Current line highlight color"), QColorDialog::ShowAlphaChannel);
	if (tmp.isValid())
	{
		_pimpl->curLine = tmp;
		changeBtnColor(_pimpl->ui->curLineColorBtn, _pimpl->curLine);
	}
}

void TextEditorConfig::changeSelectionColor()
{
	QColor tmp = QColorDialog::getColor(_pimpl->selectionResult, this, tr("Current selection result color"), QColorDialog::ShowAlphaChannel);
	if (tmp.isValid())
	{
		_pimpl->selectionResult = tmp;
		changeBtnColor(_pimpl->ui->selectionColorBtn, _pimpl->selectionResult);
	}
}

void TextEditorConfig::changeWsColor()
{
	QColor tmp = QColorDialog::getColor(_pimpl->whitespace, this, tr("Current line highlight color"), QColorDialog::ShowAlphaChannel);
	if (tmp.isValid())
	{
		_pimpl->whitespace = tmp;
		changeBtnColor(_pimpl->ui->wsColorBtn, _pimpl->whitespace);
	}
}

void TextEditorConfig::changeBtnColor(QPushButton *btn, QColor color)
{
	// btn->setStyleSheet("background-color: " + color.name() + "; border: none;");
	color.setAlpha(255);
	QPalette pal = btn->palette();
	pal.setColor(QPalette::Button, color);
	btn->setPalette(pal);
	btn->update();
}
