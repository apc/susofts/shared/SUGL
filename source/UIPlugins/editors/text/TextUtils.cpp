#include "TextUtils.hpp"

#include <iomanip>
#include <sstream>

#include <QString>
#include <QTextStream>
#include <QXmlStreamWriter>

#include <Qsci/qscilexer.h>
#include <Qsci/qsciscintilla.h>

#include <utils/StylesHelper.hpp>

Lines initEditingLines(QsciScintilla *editor)
{
	Lines lines;
	if (editor->hasSelectedText())
	{
		int tmpl = -1, tmpp = -1;
		editor->getSelection(&lines.startLine, &lines.startPos, &lines.endLine, &lines.endPos);
		editor->getCursorPosition(&tmpl, &tmpp);
		lines.selectionUpward = lines.startLine != tmpl || lines.startPos != tmpp;
	}
	else // no selection -> we take current line
	{
		editor->getCursorPosition(&lines.startLine, &lines.startPos);
		lines.endLine = lines.startLine;
		lines.endPos = lines.startPos;
	}
	return lines;
}

void restoreSelection(QsciScintilla *editor, const Lines &lines)
{
	if (lines.startLine == lines.endLine && lines.startPos == lines.endPos) // no selection
		return;
	if (lines.selectionUpward)
		editor->setSelection(lines.startLine, lines.startPos, lines.endLine, lines.endPos);
	else
		editor->setSelection(lines.endLine, lines.endPos, lines.startLine, lines.startPos);
}

void addMarker(QsciScintilla *editor, int pos, int marker_id)
{
	if (editor)
	{
		int line, index;
		editor->lineIndexFromPosition(pos, &line, &index);
		editor->markerAdd(line, marker_id);
	}
}

void addIndication(QsciScintilla *editor, int indicatorType, int position, int nbchar, int value)
{
	if (editor)
	{
		editor->SendScintilla(QsciScintillaBase::SCI_SETINDICATORCURRENT, indicatorType); // choose indicator
		editor->SendScintilla(QsciScintillaBase::SCI_SETINDICATORVALUE, value); // associate value
		editor->SendScintilla(QsciScintillaBase::SCI_INDICATORFILLRANGE, position, nbchar); // insert it
	}
}

int countIndications(const QsciScintilla *editor, int indicatorType)
{
	int count = 0, pos = 0, endPos = editor->length();
	int indStart, indEnd;

	while (pos < endPos)
	{
		// It searches for section with or without given indicator
		indStart = editor->SendScintilla(QsciScintillaBase::SCI_INDICATORSTART, indicatorType, pos);
		indEnd = editor->SendScintilla(QsciScintillaBase::SCI_INDICATOREND, indicatorType, pos);
		if (indEnd == 0)
			return count;

		if (std::bitset<32>(editor->SendScintilla(QsciScintillaBase::SCI_INDICATORALLONFOR, indStart))[indicatorType])
			count += 1;

		pos = indEnd;
	}

	return count;
}

void clearIndications(QsciScintilla *editor, int indicatorType)
{
	if (editor)
	{
		editor->SendScintilla(QsciScintillaBase::SCI_SETINDICATORCURRENT, indicatorType); // choose indicator
		editor->SendScintilla(QsciScintillaBase::SCI_INDICATORCLEARRANGE, 0, editor->length()); // clear
	}
}

std::bitset<32> getIndicators(QsciScintilla *editor, int position)
{
	return std::bitset<32>(editor->SendScintilla(QsciScintilla::SCI_INDICATORALLONFOR, position));
}

void setBackground(QsciScintilla *editor, const QColor &color)
{
	if (!editor)
		return;
	const QColor adaptedColor = getThemeAdaptedColor(color);
	if (editor->lexer())
	{
		editor->lexer()->setDefaultPaper(adaptedColor);
		for (int i = 0; i < QsciScintilla::STYLE_MAX; i++)
		{
			if (!editor->lexer()->description(i).isEmpty())
				editor->lexer()->paperChanged(adaptedColor, i);
		}
	}
	else
	{
		editor->SendScintilla(QsciScintilla::SCI_STYLESETBACK, 0, adaptedColor);
		editor->SendScintilla(QsciScintilla::SCI_STYLESETBACK, QsciScintilla::STYLE_DEFAULT, adaptedColor);
		editor->SendScintilla(QsciScintilla::SCI_STYLECLEARALL);
	}
}

QByteArray toByteArray(const QString &text, bool utf8)
{
	return utf8 ? text.toUtf8() : text.toLatin1();
}

QString fromByteArray(const QByteArray &text, bool utf8)
{
	return utf8 ? QString::fromUtf8(text) : QString::fromLatin1(text);
}

QByteArray getSeparatedText(const QByteArray &text, char separator)
{
	QByteArray out;
	const char *data = text.constData();
	while (*data)
	{
		bool isValueSpace = QChar::isSpace(*data);
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
		if (isValueSpace && (*data) != '\n' && (out.size() && (out.back() != separator && out.back() != '\n')))
#else
		if (isValueSpace && (*data) != '\n' && (out.size() && (out[out.size() - 1] != separator && out[out.size() - 1] != '\n')))
#endif
			out += separator;
		else if ((*data) == '\n' || !isValueSpace || (!isValueSpace && out.isEmpty()))
			out += *data;
		++data;
	}
	return out;
}

QByteArray getHTMLText(const QByteArray &text)
{
	QString out_html;
	QXmlStreamWriter writer(&out_html);
	writer.setAutoFormatting(true);
	writer.writeStartDocument(); // <?xml ...>
	writer.writeStartElement("html");
	writer.writeStartElement("head");
	writer.writeStartElement("meta");
	writer.writeAttribute("content", "text/html; charset=utf-8");
	writer.writeEndElement(); // meta
	writer.writeEndElement(); // head
	writer.writeStartElement("body");
	writer.writeStartElement("table"); // these will automatically be closed with `writeEndDocument()`
	QTextStream qstream(text.constData());

	while (!qstream.atEnd())
	{
		QString line = qstream.readLine();
		QStringList list = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
		if (!list.isEmpty())
		{
			writer.writeStartElement("tr");
			for (QString &element : list)
				writer.writeTextElement("td", element);
			writer.writeEndElement(); // tr
		}
	}
	writer.writeEndDocument();

	return toByteArray(out_html, true);
}

void alignContents(QsciScintilla *editor)
{
	std::string tmp, line;
	std::istringstream sstr(toByteArray(editor->text(), editor).constData());
	std::vector<size_t> columnWidth(10);
	int overlapExtend = 4;
	// Iterate over lines and words to find the longest word in each column
	while (std::getline(sstr, line))
	{
		std::istringstream sstrLine(line);
		int colCounter = -1;
		while (sstrLine >> tmp)
		{
			colCounter++;
			// if columns vector exceeded
			if (colCounter == columnWidth.size())
				columnWidth.push_back(0);
			// if word is longer
			if ((tmp.size() + overlapExtend) > columnWidth[colCounter])
				columnWidth[colCounter] = (tmp.size() + overlapExtend);
		}
	}
	sstr.clear();
	sstr.seekg(0, sstr.beg);
	std::ostringstream osstr;
	// Iterate over lines and words and output them in proper layout
	while (std::getline(sstr, line))
	{
		int counter = 0;
		std::istringstream sstrLine(line);
		while (sstrLine >> tmp)
		{
			// anything besides the first column
			if (counter > 0)
				osstr << std::right << std::setw(columnWidth[counter]) << tmp;
			// the first column
			else
				osstr << std::left << std::setw(columnWidth[counter]) << tmp;
			counter++;
		}
		osstr << '\n';
	}
	editor->SendScintilla(QsciScintilla::SCI_SETTEXT, osstr.str().c_str());
}
