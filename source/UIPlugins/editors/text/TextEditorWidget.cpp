#include "TextEditorWidget.hpp"

#include <array>
#include <cmath>

#include <QAction>
#include <QFile>
#include <QGuiApplication>
#include <QLabel>
#include <QMimeData>
#include <QMouseEvent>
#include <QTabWidget>
#include <QTextBrowser>
#include <QVBoxLayout>

#include <Qsci/qscilexercustom.h>

#include <Logger.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/PointsExportConfig.hpp>
#include <utils/ShortcutsConfig.hpp>

#include "MarkerViewer.hpp"
#include "QtStreams.hpp"
#include "StructureViewer.hpp"
#include "TextEditor.hpp"
#include "TextGoToLine.hpp"
#include "TextSearch.hpp"
#include "TextStatusBar.hpp"
#include "TextUtils.hpp"
#include "interface/SPluginInterface.hpp"

namespace
{
/**
 * Circular buffer for position history.
 * Buffer of 16 poistions (way enough)
 */
class PositionHistory
{
public:
	constexpr void push_back(int value) noexcept
	{
		_stackHead = moveStackPosition(_stackHead);
		_stackEnd = _stackHead;
		if (_stackEnd == _stackStart)
			_stackStart = moveStackPosition(_stackStart);
		_history[_stackHead] = value;
	}

	void clear() noexcept // can only be constexpr in C++20 because of std::array::fill()
	{
		_stackStart = 0;
		_stackEnd = 0;
		_stackHead = 0;
		_history.fill(0);
	}

	constexpr int prev() noexcept
	{
		if (_stackHead != _stackStart)
			_stackHead = moveStackPosition(_stackHead, -1);
		return _history[_stackHead];
	}

	constexpr int next() noexcept
	{
		if (_stackHead != _stackEnd)
			_stackHead = moveStackPosition(_stackHead);
		return _history[_stackHead];
	}

	constexpr int back() const noexcept { return _stackEnd == _stackStart ? -1 : _history[_stackHead]; }
	constexpr bool empty() const noexcept { return _stackStart == _stackEnd; }
	constexpr bool canPrev() const noexcept { return _stackHead != _stackStart; }
	constexpr bool canNext() const noexcept { return _stackHead != _stackEnd; }

private:
	constexpr size_t moveStackPosition(size_t pos, int move = 1) const noexcept
	{
		pos += move;
		if (pos >= PositionHistory::_maxSize)
			pos %= PositionHistory::_maxSize;
		return pos;
	}

private:
	static constexpr size_t _maxSize = 16;
	size_t _stackStart = 0;
	size_t _stackEnd = 0;
	size_t _stackHead = 0;
	std::array<int, PositionHistory::_maxSize> _history = {0};
};
} // namespace

class TextEditorWidget::_TextEditorWidget_pimpl
{
public:
	// editor
	QLabel *lblOutOfDate = nullptr;
	TextEditor *editor = nullptr;
	TextSearch *search = nullptr;
	// position history
	PositionHistory positions;
	QAction *posprev = nullptr;
	QAction *posnext = nullptr;
	// markers
	MarkerViewer *markerViewer = nullptr;
	// copy/paste - if this mimetype is provided the copy was done without any serialization, and therefore, it should not be deserialized when pasted
	const QString serializationMimeType = "application/vnd.cern.susoft.surveypad.serialization";
};

TextEditorWidget::TextEditorWidget(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent), _pimpl(std::make_unique<_TextEditorWidget_pimpl>())
{
	QWidget *wgt = new QWidget(this);
	QVBoxLayout *layout = new QVBoxLayout(wgt);
	// label
	_pimpl->lblOutOfDate = new QLabel(this);
	QFont font;
	font.setBold(true);
	_pimpl->lblOutOfDate->setFont(font);
	_pimpl->lblOutOfDate->setStyleSheet("background-color: red;");
	_pimpl->lblOutOfDate->setAlignment(Qt::AlignCenter);
	_pimpl->lblOutOfDate->setTextInteractionFlags(Qt::LinksAccessibleByMouse | Qt::TextSelectableByMouse);
	_pimpl->lblOutOfDate->setText(tr("Input files have been changed, and the output file might be out of date. It might have been tampered with or modified by some other processing software."));
	_pimpl->lblOutOfDate->setFocusPolicy(Qt::NoFocus);
	_pimpl->lblOutOfDate->setMargin(0);
	_pimpl->lblOutOfDate->hide();
	layout->addWidget(_pimpl->lblOutOfDate);
	// editor
	_pimpl->editor = new TextEditor(this);
	_pimpl->editor->installEventFilter(this);
	layout->addWidget(_pimpl->editor);
	// search
	_pimpl->search = new TextSearch(editor(), this);
	_pimpl->search->hide();
	layout->addWidget(_pimpl->search);
	// ending
	setWidget(wgt);
	createWidgets();

	// copy
	connect(_pimpl->editor, &QsciScintilla::copyAvailable, this, &TextEditorWidget::copyAvailable);
	connect(_pimpl->editor, &QsciScintilla::copyAvailable, this, &TextEditorWidget::cutAvailable);
	// undo
	connect(_pimpl->editor, &QsciScintilla::textChanged, [this]() {
		emit undoAvailable(_pimpl->editor->isUndoAvailable());
		emit redoAvailable(_pimpl->editor->isRedoAvailable());
	});
	// position
	connect(_pimpl->editor, &QsciScintilla::cursorPositionChanged, this, &TextEditorWidget::positionChanged);
	connect(_pimpl->editor, &QsciScintilla::indicatorReleased, this, &TextEditorWidget::positionChanged);
	// modified
	connect(_pimpl->editor, &QsciScintilla::modificationChanged, this, &SGraphicalWidget::hasChanged);
}

TextEditorWidget::~TextEditorWidget() = default;

void TextEditorWidget::clearContent()
{
	SGraphicalWidget::clearContent();

	_pimpl->editor->clear();
	clearPositions();
}

bool TextEditorWidget::isModified() const noexcept
{
	return SGraphicalWidget::isModified() || _pimpl->editor->isModified();
}

ShareablePointsList TextEditorWidget::getContent() const
{
	return ShareablePointsList();
}

const TextEditor *TextEditorWidget::editor() const noexcept
{
	return _pimpl->editor;
}

TextEditor *TextEditorWidget::editor() noexcept
{
	return _pimpl->editor;
}

void TextEditorWidget::additionalContextualActions(std::vector<QAction *> actions) noexcept
{
	_pimpl->editor->additionalContextualActions(std::move(actions));
}

const std::vector<QAction *> &TextEditorWidget::additionalContextualActions() const noexcept
{
	return _pimpl->editor->additionalContextualActions();
}

void TextEditorWidget::updateContent()
{
	SGraphicalWidget::updateContent();
	auto lexer = qobject_cast<QsciLexerCustom *>(_pimpl->editor->lexer());
	if (lexer)
		lexer->styleText(0, _pimpl->editor->length());
}

void TextEditorWidget::paste()
{
	SGraphicalWidget::paste();
	_pimpl->editor->paste();
}

void TextEditorWidget::copy() const
{
	SGraphicalWidget::copy();
	_pimpl->editor->copy();
}

void TextEditorWidget::cut() const
{
	SGraphicalWidget::cut();
	_pimpl->editor->cut();
}

void TextEditorWidget::undo()
{
	SGraphicalWidget::undo();
	_pimpl->editor->undo();
}

void TextEditorWidget::redo()
{
	SGraphicalWidget::redo();
	_pimpl->editor->redo();
}

void TextEditorWidget::isModified(bool modified) noexcept
{
	_pimpl->editor->setModified(modified);
	SGraphicalWidget::isModified(modified);
}

void TextEditorWidget::previousPosition()
{
	_pimpl->editor->SendScintilla(QsciScintillaBase::SCI_GOTOPOS, _pimpl->positions.prev());
	_pimpl->posprev->setEnabled(_pimpl->positions.canPrev());
	_pimpl->posnext->setEnabled(_pimpl->positions.canNext());
}

void TextEditorWidget::nextPosition()
{
	_pimpl->editor->SendScintilla(QsciScintillaBase::SCI_GOTOPOS, _pimpl->positions.next());
	_pimpl->posprev->setEnabled(_pimpl->positions.canPrev());
	_pimpl->posnext->setEnabled(_pimpl->positions.canNext());
}

void TextEditorWidget::showGoToLine()
{
	int line, index;
	_pimpl->editor->getCursorPosition(&line, &index);
	auto gotoline = new TextGoToLine(line + 1, _pimpl->editor->lines(), _pimpl->editor->positionFromLineIndex(line, index), _pimpl->editor->length(), this);
	int out = gotoline->exec();
	if (out == QDialog::Accepted)
		_pimpl->editor->jumpTo(gotoline->getValue(), gotoline->isLine());
}

void TextEditorWidget::showSearch(bool replace)
{
	// Showing without firstly hiding the widget will not call showEvent (that is why hide() is called explicitly)
	_pimpl->search->hide();
	_pimpl->search->setSearchMode(replace);
	_pimpl->search->show();
}

void TextEditorWidget::inputHasChanged(bool changed)
{
	if (changed)
		_pimpl->lblOutOfDate->show();
}

bool TextEditorWidget::isSearchVisible()
{
	return _pimpl->search->isVisibleTo(this);
}

bool TextEditorWidget::_save(const QString &path)
{
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::OpenModeFlag::Truncate | QIODevice::OpenModeFlag::Text))
		return false;
	if (!editor()->write(&file))
		return false;
	return true;
}

bool TextEditorWidget::_open(const QString &path)
{
	clearPositions();
	QFile file(path);
	if (!file.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::OpenModeFlag::Text))
		return false;
	if (!editor()->read(&file))
		return false;
	_pimpl->lblOutOfDate->hide();
	_pimpl->editor->setFocus();
	updateContent();
	return true;
}

void TextEditorWidget::_newEmpty()
{
	clearPositions();
	editor()->clear();
}

QByteArray TextEditorWidget::contentFromMime(const QMimeData *mimedata, IShareablePointsListIO &serializer)
{
	if (mimedata->formats().empty())
		return "";

	// get content
	// from same plugin
	QByteArray content = mimedata->data(QString::fromStdString(serializer.getMIMEType()));
	// from another plugin
	if (content.isEmpty())
	{
		// clipboard is in UTF-8
		QMimeData tmp;
		if (!editor()->isUtf8())
		{
			for (const auto &format : mimedata->formats())
			{
				if (format == "text/html" || format.contains("json", Qt::CaseSensitivity::CaseInsensitive)) // JSON and HTML should always be UTF8
					tmp.setData(format, mimedata->data(format));
				else
					tmp.setData(format, QString::fromUtf8(mimedata->data(format)).toLatin1());
			}
			mimedata = &tmp;
		}
		// if serialized copy allowed
		if (mimedata->hasFormat(_pimpl->serializationMimeType))
		{
			try
			{
				auto spl = ClipboardManager::getClipboardManager().paste(mimedata, editor()->isUtf8());
				if (isAllSelected())
					content = serializer.write(spl).c_str();
				else
					content = serializer.write(spl.getRootFrame()).c_str();
			}
			catch (...) // nothing
			{
			}
		}
	}
	return content;
}

QMimeData *TextEditorWidget::contentToMime(const QByteArray &text, QMimeData *mimedata, IShareablePointsListIO &serializer)
{
	if (text.isEmpty() || !mimedata)
		return mimedata;
	auto &clipboard = ClipboardManager::getClipboardManager();

	// same plugin
	mimedata->setData(QString::fromStdString(serializer.getMIMEType()), text);
	// other plugins
	// put clipboard in UTF-8
	const QByteArray &bytes = editor()->isUtf8() ? text : mimedata->text().toUtf8();
	// if export
	if (PointsExportConfig::getAppPointsConfig()->exportPointsList)
	{
		try
		{
			mimedata = clipboard.copy(clipboard.serializeFromString(bytes.constData(), &serializer), mimedata, editor()->isUtf8());
			mimedata->setData(_pimpl->serializationMimeType, QByteArray()); // Mark that it was serialized copy
		}
		catch (const std::exception &e)
		{
			logInfo() << "Impossible to serialize the content for the copy: '" << e.what() << "'";
		}
	}
	// if not, just add CSV version of the copied contents to mime data
	else
	{
		mimedata->setData("text/html", getHTMLText(text));
		mimedata->setData("text/csv", getSeparatedText(text, ','));
	}
	return mimedata;
}

const TextSearch *TextEditorWidget::searchWidget() const noexcept
{
	return _pimpl->search;
}

TextSearch *TextEditorWidget::searchWidget() noexcept
{
	return _pimpl->search;
}

const MarkerViewer *TextEditorWidget::markerViewer() const noexcept
{
	return _pimpl->markerViewer;
}

MarkerViewer *TextEditorWidget::markerViewer() noexcept
{
	return _pimpl->markerViewer;
}

bool TextEditorWidget::isAllSelected() const
{
	if (!editor())
		return false;

	const QString text = editor()->text();
	QStringRef rtext = &text;
	if (rtext.trimmed().isEmpty())
		return true;
	if (!editor()->hasSelectedText())
		return false;

	int beg = editor()->SendScintilla(QsciScintilla::SCI_GETSELECTIONSTART);
	int end = editor()->SendScintilla(QsciScintilla::SCI_GETSELECTIONEND);
	if (beg == 0 && end == text.size())
		return true;
	if (rtext.left(beg).trimmed().isEmpty() && rtext.right(text.size() - end).trimmed().isEmpty())
		return true;

	return false;
}

void TextEditorWidget::createWidgets()
{
	// Help widget
	auto *helpwidget = new QTextBrowser(this);
	helpwidget->setOpenExternalLinks(true);
	connect(_pimpl->editor, &QsciScintilla::cursorPositionChanged, [this, helpwidget](int line, int) -> void { helpwidget->setHtml(editor()->annotation(line)); });
	_pimpl->markerViewer = new MarkerViewer(editor(), this);
	auto *structureViewer = new StructureViewer(editor(), this);
	auto *tab = new QTabWidget(this);
	tab->addTab(helpwidget, tr("Help"));
	tab->addTab(_pimpl->markerViewer, tr("Markers"));
	tab->addTab(structureViewer, tr("File Structure"));
	setHelpWidget(tab);
	// Status bar
	auto *status = new TextStatusBar(_pimpl->editor, this);
	connect(status, &TextStatusBar::doubleClick, this, &TextEditorWidget::showGoToLine);
	setStatusWidget(status);

	// Actions
	auto addSeparator = [this](Menus menu) -> void {
		auto *sep = new QAction(this);
		sep->setSeparator(true);
		addAction(menu, sep);
	};
	// Edit actions
	// goto
	ShortcutsConfigObject shortcutSettings = ShortcutsConfig::getShortcutConfig();
	auto *actiongoto = new QAction(QIcon(":/actions/go-to-line"), shortcutSettings.getShortcutName(ShortcutsConfigObject::GOTO), this);
	actiongoto->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::GOTO));
	actiongoto->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::GOTO));
	actiongoto->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::GOTO));
	connect(actiongoto, &QAction::triggered, this, &TextEditorWidget::showGoToLine);
	addAction(Menus::edit, actiongoto);
	// search
	auto *actionsearch = new QAction(QIcon(":/actions/find"), shortcutSettings.getShortcutName(ShortcutsConfigObject::FIND), this);
	actionsearch->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::FIND));
	actionsearch->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::FIND));
	actionsearch->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::FIND));
	connect(actionsearch, &QAction::triggered, this, [this]() -> void { showSearch(false); });
	addAction(Menus::edit, actionsearch);
	// replace
	auto *actionreplace = new QAction(shortcutSettings.getShortcutName(ShortcutsConfigObject::REPLACE), this);
	actionreplace->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::REPLACE));
	actionreplace->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::REPLACE));
	actionreplace->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::REPLACE));
	connect(actionreplace, &QAction::triggered, this, [this]() -> void { showSearch(true); });
	addAction(Menus::edit, actionreplace);
	// special copy
	addSeparator(Menus::edit);
	auto *actionspecialcpy = new QAction(QIcon(":/actions/special-copy"), tr("Special Copy..."), this);
	actionspecialcpy->setToolTip(tr("Special copy..."));
	actionspecialcpy->setStatusTip(tr("Special copy..."));
	connect(actionspecialcpy, &QAction::triggered, _pimpl->editor, &TextEditor::specialCopy);
	connect(this, &TextEditorWidget::copyAvailable, actionspecialcpy, &QAction::setEnabled);
	addAction(Menus::edit, actionspecialcpy);
	// View actions
	// position history
	_pimpl->posprev = new QAction(QIcon(":/actions/go-previous"), shortcutSettings.getShortcutName(ShortcutsConfigObject::PREVPOSITION), this);
	_pimpl->posprev->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::PREVPOSITION));
	_pimpl->posprev->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::PREVPOSITION));
	_pimpl->posprev->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::PREVPOSITION));
	_pimpl->posprev->setEnabled(false);
	connect(_pimpl->posprev, &QAction::triggered, this, &TextEditorWidget::previousPosition);
	addAction(Menus::view, _pimpl->posprev);
	_pimpl->posnext = new QAction(QIcon(":/actions/go-next"), shortcutSettings.getShortcutName(ShortcutsConfigObject::NEXTPOSITION), this);
	_pimpl->posnext->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::NEXTPOSITION));
	_pimpl->posnext->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::NEXTPOSITION));
	_pimpl->posnext->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::NEXTPOSITION));
	_pimpl->posnext->setEnabled(false);
	connect(_pimpl->posnext, &QAction::triggered, this, &TextEditorWidget::nextPosition);
	addAction(Menus::view, _pimpl->posnext);
	// zoom
	addSeparator(Menus::view);
	auto *actionZoomIn = new QAction(QIcon(":/actions/zoom-in"), shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMIN), this);
	actionZoomIn->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::ZOOMIN));
	actionZoomIn->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMIN));
	actionZoomIn->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMIN));
	connect(actionZoomIn, &QAction::triggered, _pimpl->editor, qOverload<>(&TextEditor::zoomIn));
	addAction(Menus::view, actionZoomIn);
	auto *actionZoomOut = new QAction(QIcon(":/actions/zoom-out"), shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMOUT), this);
	actionZoomOut->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::ZOOMOUT));
	actionZoomOut->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMOUT));
	actionZoomOut->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMOUT));
	connect(actionZoomOut, &QAction::triggered, _pimpl->editor, qOverload<>(&TextEditor::zoomOut));
	addAction(Menus::view, actionZoomOut);
	auto *actionZoomRestore = new QAction(QIcon(":/actions/zoom-restore"), shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMRESTORE), this);
	actionZoomRestore->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::ZOOMRESTORE));
	actionZoomRestore->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMRESTORE));
	actionZoomRestore->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::ZOOMRESTORE));
	connect(actionZoomRestore, &QAction::triggered, [this]() { _pimpl->editor->zoomTo(0); });
	addAction(Menus::view, actionZoomRestore);
	// Folding
	addSeparator(Menus::view);
	auto *actionFoldAll = new QAction(tr("Fold All"), this);
	actionFoldAll->setToolTip(tr("Fold All"));
	actionFoldAll->setStatusTip(tr("Fold All"));
	connect(actionFoldAll, &QAction::triggered, [this]() { _pimpl->editor->SendScintilla(QsciScintilla::SCI_FOLDALL, QsciScintilla::SC_FOLDACTION_CONTRACT); });
	addAction(Menus::view, actionFoldAll);
	auto *actionUnfoldAll = new QAction(tr("Unfold All"), this);
	actionUnfoldAll->setToolTip(tr("Unfold All"));
	actionUnfoldAll->setStatusTip(tr("Unfold All"));
	connect(actionUnfoldAll, &QAction::triggered, [this]() { _pimpl->editor->SendScintilla(QsciScintilla::SCI_FOLDALL, QsciScintilla::SC_FOLDACTION_EXPAND); });
	addAction(Menus::view, actionUnfoldAll);
	// markers
	addSeparator(Menus::view);
	auto *actionRmMarkers = new QAction(QIcon(":/markers/rm-markers"), tr("Remove all user markers"), this);
	actionRmMarkers->setToolTip(tr("Remove all user markers"));
	actionRmMarkers->setStatusTip(tr("Remove all user markers"));
	connect(actionRmMarkers, &QAction::triggered, [this]() {
		_pimpl->editor->markerDeleteAll(TextMarker::USER1);
		_pimpl->editor->markerDeleteAll(TextMarker::USER2);
	});
	addAction(Menus::view, actionRmMarkers);

	// Tool bar
	auto *toolbar = new QToolBar(owner()->name(), this);
	toolbar->addAction(_pimpl->posprev);
	toolbar->addAction(_pimpl->posnext);
	toolbar->addSeparator();
	toolbar->addAction(actionZoomIn);
	toolbar->addAction(actionZoomOut);
	toolbar->addSeparator();
	toolbar->addAction(actiongoto);
	toolbar->addAction(actionsearch);
	toolbar->addSeparator();
	toolbar->addAction(actionspecialcpy);
	setToolBar(toolbar);
}

void TextEditorWidget::clearPositions()
{
	_pimpl->positions.clear();
	_pimpl->posprev->setEnabled(false);
	_pimpl->posnext->setEnabled(false);
}

void TextEditorWidget::positionChanged(int line, int index)
{
	// we don't store position on selection
	if (_pimpl->editor->hasSelectedText())
		return;
	// we don't store if
	// - last position is the same as the new one
	// - we are closer than 200 characters from the last position
	// - we are not using CTRL key
	//    - this is for indicators: we want to register the click position and the arrival.
	int pos = _pimpl->editor->positionFromLineIndex(line, index);
	if (pos == _pimpl->positions.back() || (std::abs(pos - _pimpl->positions.back()) < 200 && QGuiApplication::keyboardModifiers() != Qt::KeyboardModifier::ControlModifier))
		return;

	_pimpl->positions.push_back(pos);
	_pimpl->posprev->setEnabled(_pimpl->positions.canPrev());
	_pimpl->posnext->setEnabled(_pimpl->positions.canNext());
}
