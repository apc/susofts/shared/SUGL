/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef COMMANDTABLE_HPP
#define COMMANDTABLE_HPP

#include <memory>

#include "PluginsGlobal.hpp"
#include "utils/IUndoCommand.hpp"

class TableEditor;

/**
 * CommandTable is an implementation of IUndoCommand for TableEditor, which can restore table contents to the recorded state.
 *
 * The CommandTable is used by UndoStack and restores the TableEditor contents during undo and redo action.
 *
 * @see TableEditor, UndoStack, IUndoCommand
 */
class SUGL_SHARED_EXPORT CommandTable : public IUndoCommand
{
public:
	CommandTable(TableEditor *te);
	~CommandTable() override;

	// IUndoCommand
	void restoreState() override;

private:
	/** pimpl */
	class _CommandTable_pimpl;
	std::unique_ptr<_CommandTable_pimpl> _pimpl;
};

#endif // COMMANDTABLE_HPP
