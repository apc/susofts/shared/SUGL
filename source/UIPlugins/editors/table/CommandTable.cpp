/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#include "CommandTable.hpp"

#include <string>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <io/ShareablePointsListIOJson.hpp>

#include "TableEditor.hpp"

namespace
{
std::string serialize(ShareablePointsList spl)
{
	ShareablePointsListIOJSon jsonSerializer;
	jsonSerializer.isBinary(true);
	return jsonSerializer.write(spl);
}
} // namespace

class CommandTable::_CommandTable_pimpl
{
public:
	TableEditor *tableEditor;
	std::string state;
};

ShareablePointsList deserialize(const std::string &content)
{
	ShareablePointsListIOJSon jsonSerializer;
	jsonSerializer.isBinary(true);
	return jsonSerializer.read(content);
}

CommandTable::CommandTable(TableEditor *te) : IUndoCommand(), _pimpl(std::make_unique<_CommandTable_pimpl>())
{
	_pimpl->state = serialize(te->getContent());
	_pimpl->tableEditor = te;
}

CommandTable::~CommandTable() = default;

void CommandTable::restoreState()
{
	_pimpl->tableEditor->setRowCount(0);
	_pimpl->tableEditor->setTableContent(deserialize(_pimpl->state));
}
