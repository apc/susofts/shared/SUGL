/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TABLEEDITORWIDGET_HPP
#define TABLEEDITORWIDGET_HPP

#include "PluginsGlobal.hpp"
#include "interface/SGraphicalInterface.hpp"

class TableEditor;

/**
 * Widget containing a TableEditor.
 *
 * This is an abstract class that serves as a container for a TableEditor. It connects actions done in the main application with the TableEditor widget.
 *
 * On creation of TableEditorWidget a pointer to TableEditor has to be provided.
 * Whenever there is any action done or required to be done by TableEditor, TableEditorWidget calls relevant methods of TableEditor.
 * TableEditorWidget also provides action for its TableEditor allowing to add or remove rows from the table.
 *
 * In terms of operation, TableEditorWidget is similar to TextEditorWidget, as each of them contains its respective editor and facilitates the communication with the main application.
 *
 * @see TableEditor, SGraphicalWidget, TextEditorWidget
 */
class SUGL_SHARED_EXPORT TableEditorWidget : public SGraphicalWidget
{
	Q_OBJECT

public:
	/**
	 * TableEditorWidget constructor.
	 *
	 * @param editor, TableEditor to be used by given TableEditorWidget.
	 * @see TableEditor
	 */
	TableEditorWidget(TableEditor *editor, SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~TableEditorWidget() override;

	// SGraphicalWidget
	virtual bool isModified() const noexcept override;
	virtual ShareablePointsList getContent() const override;

	/** @return the TableEditor */
	const TableEditor *editor() const noexcept;
	/** @return the TableEditor */
	TableEditor *editor() noexcept;

	/** Returns stored table precision that is used by all TableDoubleDelegate. */
	int getPrecision() const noexcept;
	/** Sets the table precision that is used by all TableDoubleDelegate, setContent method should be ovverriden to support change of precision for the TableEditorWidget. */
	virtual void setPrecision(int precision) noexcept;

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void clearContent() override;
	virtual void paste() override;
	virtual void copy() const override;
	virtual void cut() const override;
	virtual void isModified(bool modified) noexcept override;
	virtual void updateContent() override;

private:
	void createWidgets();

private:
	/** pimpl */
	class _TableEditorWidget_pimpl;
	std::unique_ptr<_TableEditorWidget_pimpl> _pimpl;
};
#endif // TABLEEDITORWIDGET_HPP
