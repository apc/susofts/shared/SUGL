/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TABLEEDITOR_HPP
#define TABLEEDITOR_HPP

#include <memory>

#include <QTableWidget>

#include "PluginsGlobal.hpp"

struct ShareablePoint;
class ShareablePointsList;
class IUndoCommand;

/**
 * Basic table editor for data visualization and modification.
 *
 * A TableEditor does not take into consideration different ShareableFrame but it flattens the data to the list of ShareablePoint.
 * Table editor has following features:
 * - copy of individual and multiple selected cells,
 * - cut/paste of entire row(s),
 * - adding/removing row(s),
 * - data can be modified with double spinbox for doubles and with combobox for booleans.
 *
 * This class should be inherited to add more features. Implementation of abstract methods are application-specific for plugins.
 *
 * @see TableEditorWidget
 */
class SUGL_SHARED_EXPORT TableEditor : public QTableWidget
{
	Q_OBJECT

public:
	TableEditor(QWidget *parent = nullptr);
	virtual ~TableEditor() override;

	// QTableWidget
	void contextMenuEvent(QContextMenuEvent *event = nullptr) override;

	/**
	 * The data from ClipboardManager should be used to insert new points into the table in the selected place.
	 *
	 * - Provided that entire row(s) is selected, it should overwrite the current data.
	 * - In case of some cells selected, it should insert rows below selection.
	 * - In case of no selection, it should append the rows at the end of the table.
	 */
	virtual void paste() = 0;
	/**
	 * The data from the table should be placed inside ClipboardManager.
	 * In every case the data should be stored as plain text.
	 * In case of succesfull serialization, selected data should also be stored as ShareablePointsList.
	 * In case of unsuccessful serialization, the data should also be stored in csv format.
	 */
	virtual void copy() = 0;
	/** @return Suitable annotation to be shown in help dock widget. */
	virtual QString getRowAnnotation(const QItemSelection &selected) const = 0;
	/** Set precision for TableDoubleDelegate. If no TableDoubleDelegate are used do not override this method. */
	virtual void precision(int){};
	/** Get precision from TableDoubleDelegate. If no TableDoubleDelegate are used do not override this method. */
	virtual int precision() { return -1; };

	/** Sets the modification status of a TableEditor. */
	void setModified(bool modified) noexcept;
	/** Returns the modification status of a TableEditor. */
	bool isModified() const noexcept;
	/** Adds empty row with new point below selection or at the end of data if no selection has been made. */
	void addRow();
	/** Remove selected rows from the table. */
	void removeRows();
	/** Sets table content based on received ShareablePointsList. Cannot emit actionDone signal. */
	virtual void setTableContent(const ShareablePointsList &spl);
	/** Creates ShareablePointsList based on the data in TableEditor and returns it. */
	virtual ShareablePointsList getContent() const;

public slots:
	/** Sets the relevant color coding for the rows in the table - needs to be overriden and implemented if color coding is desired. */
	virtual void updateRowStatus(QTableWidgetItem * = nullptr){};
	/** Open a temporary PointsExportConfig settings window and copy desired attributes. */
	virtual void specialCopy();

signals:
	/** Signal sent to notify QUndoStack about change of state of a TableEditor. */
	void actionDone(IUndoCommand *command);

protected:
	/** Should parse particular row and return a ShareablePoint. */
	virtual ShareablePoint parseRow(int row) const = 0;
	/** Based on input ShareablePoint the row in table should be constructed. */
	virtual void setPoint(int row, const ShareablePoint &point) = 0;
	/** Templated method returning pointers to QTableWidgetItem. The method allows to set QTableWidgetItems of different types inside a TableEditor. */
	template<class T>
	QTableWidgetItem *createItem(const T &t);

private:
	/** pimpl */
	class _TableEditor_pimpl;
	std::unique_ptr<_TableEditor_pimpl> _pimpl;
};

template<class T>
QTableWidgetItem *TableEditor::createItem(const T &t)
{
	auto item = new QTableWidgetItem();
	item->setData(Qt::EditRole, t);
	return item;
}

#endif // TABLEEDITOR_HPP
