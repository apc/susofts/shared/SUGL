/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TABLEDOUBLEDELEGATE_HPP
#define TABLEDOUBLEDELEGATE_HPP

#include <memory>

#include <QStyledItemDelegate>

#include "PluginsGlobal.hpp"

/**
 * Custom QStyledItemDelegate for display and modification of double values.
 *
 * TableDoubleDelegate displays the data with given precision and allows data editing. The delegate is used inside TableEditor implementations.
 * During item edition the QDoubleSpinBox is displayed and it allows to edit a value.
 * The desired precision may be changed after the delegate has been created.
 * Default precision of TableDoubleDelegate is 6 digits after decimal place.
 *
 * @see TableEditor, TableEditorWidget
 */
class SUGL_SHARED_EXPORT TableDoubleDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	TableDoubleDelegate(QObject *parent = 0);
	~TableDoubleDelegate() override;
	virtual QString displayText(const QVariant &value, const QLocale &locale) const override;
	virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	/** Set precision of the delegate. */
	void precision(int precision) const noexcept;
	/** Returns precision of a TableDoubleDelegate. */
	int precision() const noexcept;

private:
	/** pimpl */
	class _TableDoubleDelegate_pimpl;
	std::unique_ptr<_TableDoubleDelegate_pimpl> _pimpl;
};

#endif // TABLEDOUBLEDELEGATE_HPP
