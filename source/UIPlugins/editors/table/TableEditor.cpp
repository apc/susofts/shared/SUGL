#include "TableEditor.hpp"

#include <QAction>
#include <QContextMenuEvent>
#include <QHeaderView>
#include <QMenu>

#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>

#include "CommandTable.hpp"
#include "utils/PointsExportConfig.hpp"
#include "utils/SettingsManager.hpp"

class TableEditor::_TableEditor_pimpl
{
public:
	bool isModified = false;
};

TableEditor::TableEditor(QWidget *parent) : QTableWidget(parent), _pimpl(std::make_unique<_TableEditor_pimpl>())
{
	horizontalHeader()->setStretchLastSection(true);
	horizontalHeader()->setSortIndicatorShown(true);
	setSelectionMode(QAbstractItemView::ContiguousSelection);
	verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

	// Connects
	// Modification flag
	connect(model(), &QAbstractItemModel::dataChanged, [this]() -> void { setModified(true); });
	// Ability to sort by clicking headers
	connect(horizontalHeader(), &QHeaderView::sectionClicked, [this](int logicalIndex) {
		Qt::SortOrder order = horizontalHeader()->sortIndicatorOrder();
		sortByColumn(logicalIndex, order);
		horizontalHeader()->setSortIndicator(logicalIndex, order);
	});
	// Record sorting
	connect(horizontalHeader(), &QHeaderView::sectionClicked, [this](int) { emit actionDone(new CommandTable(this)); });
	// Ability to display context menu by right-clicking on vertical headers
	connect(
		verticalHeader(), &QHeaderView::customContextMenuRequested, [this](const QPoint &pos) { contextMenuEvent(new QContextMenuEvent(QContextMenuEvent::Mouse, pos)); });
	// Recording command for QUndoStack on item change
	connect(this, &TableEditor::itemChanged, [this](QTableWidgetItem *) { emit actionDone(new CommandTable(this)); });
	// if action done, update row status
	connect(this, &TableEditor::actionDone, [this]() { this->updateRowStatus(); });
}

TableEditor::~TableEditor() = default;

void TableEditor::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu menu;
	auto *actCpy = new QAction(QIcon(":/actions/special-copy"), "Special Copy", &menu);
	actCpy->setEnabled(!selectionModel()->selectedRows().isEmpty());
	connect(actCpy, &QAction::triggered, this, &TableEditor::specialCopy);
	menu.addAction(actCpy);
	menu.exec(event->globalPos());
}

void TableEditor::setModified(bool modified) noexcept
{
	_pimpl->isModified = modified;
}

bool TableEditor::isModified() const noexcept
{
	return _pimpl->isModified;
}

void TableEditor::addRow()
{
	blockSignals(true);
	static ShareablePoint point;
	point.name = "NewPoint";
	int row = selectedRanges().isEmpty() ? rowCount() : selectedRanges()[0].bottomRow() + 1; // insert in last place or at the bottom of selection
	insertRow(row);
	setPoint(row, point);
	blockSignals(false);

	emit actionDone(new CommandTable(this));
}

void TableEditor::removeRows()
{
	blockSignals(true);
	while (!selectedRanges().isEmpty())
		removeRow(selectedRanges()[0].topRow());
	blockSignals(false);

	emit actionDone(new CommandTable(this));
	emit itemSelectionChanged();
}

void TableEditor::setTableContent(const ShareablePointsList &spl)
{
	blockSignals(true);
	setRowCount(0);
	auto points = spl.getRootFrame().getAllPoints();
	for (unsigned int i = 0; i < points.size(); i++)
		setPoint(i, *points[i]);
	horizontalHeader()->setSortIndicator(-1, Qt::AscendingOrder); // reset sorting indicator
	blockSignals(false);

	updateRowStatus();
}

ShareablePointsList TableEditor::getContent() const
{
	ShareablePointsList spl;
	ShareableFrame &frame = spl.getRootFrame();
	for (int i = 0; i < rowCount(); i++)
		frame.add(new ShareablePoint(parseRow(i)));

	return spl;
}

void TableEditor::specialCopy()
{
	auto copyConfig = PointsExportConfig::showCopyDialog();
	if (!copyConfig)
		return;
	auto appConfig = PointsExportConfig::getAppPointsConfig();
	blockSignals(true);
	SettingsManager::settings().settings(PointsExportConfig::configName(), copyConfig);
	copy();
	SettingsManager::settings().settings(PointsExportConfig::configName(), appConfig);
	blockSignals(false);
}
