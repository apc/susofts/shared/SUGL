#include "TableDoubleDelegate.hpp"

#include <memory>

#include <QSpinBox>

class TableDoubleDelegate::_TableDoubleDelegate_pimpl
{
public:
	int precision = 6;
};

TableDoubleDelegate::TableDoubleDelegate(QObject *parent) : QStyledItemDelegate(parent), _pimpl(std::make_unique<_TableDoubleDelegate_pimpl>())
{
}

TableDoubleDelegate::~TableDoubleDelegate() = default;

QString TableDoubleDelegate::displayText(const QVariant &value, const QLocale &) const
{
	return QLocale(QLocale::C).toString(value.toDouble(), 'f', _pimpl->precision);
}

QWidget *TableDoubleDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
	QDoubleSpinBox *box = new QDoubleSpinBox(parent);
	box->setLocale(QLocale::C);
	box->setRange(std::numeric_limits<double>::lowest(), std::numeric_limits<double>::max());
	box->setDecimals(_pimpl->precision);
	return box;
}

void TableDoubleDelegate::precision(int precision) const noexcept
{
	_pimpl->precision = precision;
}

int TableDoubleDelegate::precision() const noexcept
{
	return _pimpl->precision;
}
