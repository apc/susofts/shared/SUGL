#include "TableEditorWidget.hpp"

#include <QItemSelectionModel>
#include <QTextBrowser>
#include <QToolBar>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <utils/ShortcutsConfig.hpp>

#include "TableEditor.hpp"
#include "interface/SPluginInterface.hpp"

class TableEditorWidget::_TableEditorWidget_pimpl
{
public:
	/** editor */
	TableEditor *editor;
	int precision;
};

TableEditorWidget::TableEditorWidget(TableEditor *editor, SPluginInterface *owner, QWidget *parent) :
	SGraphicalWidget(owner, parent), _pimpl(std::make_unique<_TableEditorWidget_pimpl>())
{
	_pimpl->editor = editor;
	setWidget(_pimpl->editor);
	createWidgets();

	// connects
	// If data changed - notify the widget
	connect(_pimpl->editor->model(), &QAbstractItemModel::dataChanged, this, &TableEditorWidget::hasChanged);
	// copy/paste
	connect(_pimpl->editor, &TableEditor::itemSelectionChanged, [this]() {
		emit copyAvailable(!_pimpl->editor->selectedItems().isEmpty()); // if anything selected
		emit cutAvailable(!_pimpl->editor->selectionModel()->selectedRows().isEmpty()); // if row selected
	});
}
TableEditorWidget::~TableEditorWidget() = default;

void TableEditorWidget::createWidgets()
{
	// help widget
	auto *helpwidget = new QTextBrowser(this);
	helpwidget->setOpenExternalLinks(true);
	connect(_pimpl->editor->selectionModel(), &QItemSelectionModel::selectionChanged,
		[this, helpwidget](const QItemSelection &selected, const QItemSelection &) -> void { helpwidget->setHtml(_pimpl->editor->getRowAnnotation(selected)); });
	helpwidget->setHtml(_pimpl->editor->getRowAnnotation(QItemSelection()));
	setHelpWidget(helpwidget);
	// Add row
	auto *actionaddrow = new QAction(QIcon(":/actions/table-add"), tr("Add row below"), this);
	actionaddrow->setToolTip(tr("Add row below"));
	actionaddrow->setStatusTip(tr("Add row below"));
	connect(actionaddrow, &QAction::triggered, _pimpl->editor, &TableEditor::addRow);
	addAction(Menus::edit, actionaddrow);
	// Remove row
	ShortcutsConfigObject shortcutSettings = ShortcutsConfig::getShortcutConfig();
	auto *actionremoverow = new QAction(QIcon(":/actions/table-delete"), shortcutSettings.getShortcutName(ShortcutsConfigObject::REMOVEROW), this);
	actionremoverow->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::REMOVEROW));
	actionremoverow->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::REMOVEROW));
	actionremoverow->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::REMOVEROW));
	actionremoverow->setEnabled(false);
	connect(actionremoverow, &QAction::triggered, _pimpl->editor, &TableEditor::removeRows);
	connect(this, &TableEditorWidget::cutAvailable, actionremoverow, &QAction::setEnabled);
	addAction(Menus::edit, actionremoverow);
	// special copy
	auto *actionspecialcpy = new QAction(QIcon(":/actions/special-copy"), tr("Special Copy..."), this);
	actionspecialcpy->setToolTip(tr("Special copy..."));
	actionspecialcpy->setStatusTip(tr("Special copy..."));
	actionspecialcpy->setEnabled(false);
	connect(actionspecialcpy, &QAction::triggered, _pimpl->editor, &TableEditor::specialCopy);
	connect(this, &TableEditorWidget::cutAvailable, actionspecialcpy, &QAction::setEnabled);
	addAction(Menus::edit, actionspecialcpy);
	// tool bar
	auto *toolbar = new QToolBar(owner()->name(), this);
	toolbar->addAction(actionspecialcpy);
	toolbar->addAction(actionaddrow);
	toolbar->addAction(actionremoverow);
	setToolBar(toolbar);
}

bool TableEditorWidget::isModified() const noexcept
{
	return _pimpl->editor->isModified();
}

ShareablePointsList TableEditorWidget::getContent() const
{
	return _pimpl->editor->getContent();
}

const TableEditor *TableEditorWidget::editor() const noexcept
{
	return _pimpl->editor;
}

TableEditor *TableEditorWidget::editor() noexcept
{
	return _pimpl->editor;
}

int TableEditorWidget::getPrecision() const noexcept
{
	return _pimpl->precision;
}

void TableEditorWidget::setPrecision(int precision) noexcept
{
	if (_pimpl->precision != precision)
	{
		_pimpl->precision = precision;
		_pimpl->editor->precision(precision);
	}
}

void TableEditorWidget::setContent(const ShareablePointsList &spl)
{
	_pimpl->editor->setTableContent(spl);
}

void TableEditorWidget::clearContent()
{
	SGraphicalWidget::clearContent();
	_pimpl->editor->clearContents();
	_pimpl->editor->setRowCount(0);
}

void TableEditorWidget::paste()
{
	SGraphicalWidget::paste();
	_pimpl->editor->paste();
}

void TableEditorWidget::copy() const
{
	SGraphicalWidget::copy();
	_pimpl->editor->copy();
}

void TableEditorWidget::cut() const
{
	SGraphicalWidget::cut();
	_pimpl->editor->copy();
	_pimpl->editor->removeRows();
}

void TableEditorWidget::isModified(bool modified) noexcept
{
	_pimpl->editor->setModified(modified);
	SGraphicalWidget::isModified(modified);
}

void TableEditorWidget::updateContent()
{
	SGraphicalWidget::updateContent();
	_pimpl->editor->updateRowStatus();
}
