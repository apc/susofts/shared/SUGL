/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TABLESTRINGDELEGATE_HPP
#define TABLESTRINGDELEGATE_HPP

#include <memory>

#include <QStyledItemDelegate>

#include "PluginsGlobal.hpp"

/**
 * Custom QStyledItemDelegate for display and validation of cells containing a string.
 *
 * TableStringDelegate allows editing of the data using QLineEdit and it validates the input using a provided regular expression.
 * By default the TableStringDelegate accepts everything in QLineEdit if no regular expression was provided.
 *
 * @see TableEditor, TableEditorWidget, TableDoubleDelegate
 */
class SUGL_SHARED_EXPORT TableStringDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	TableStringDelegate(QObject *parent = 0, QString regExp = ".*");
	~TableStringDelegate() override;

	// QStyledItemDelegate
	virtual QString displayText(const QVariant &value, const QLocale &locale) const override;
	virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	/** Set the regular expression of the delegate. */
	void pattern(QString regExp) const noexcept;
	/** Returns regular expression pattern used in the validator. */
	QString patern() const noexcept;

private:
	/** pimpl */
	class _TableStringDelegate_pimpl;
	std::unique_ptr<_TableStringDelegate_pimpl> _pimpl;
};

#endif // TABLESTRINGDELEGATE_HPP
