#include "TableStringDelegate.hpp"

#include <memory>

#include <QLineEdit>

class TableStringDelegate::_TableStringDelegate_pimpl
{
public:
	QRegularExpression regExp;
};

TableStringDelegate::TableStringDelegate(QObject *parent, QString regExp) : QStyledItemDelegate(parent), _pimpl(std::make_unique<_TableStringDelegate_pimpl>())
{
	pattern(regExp);
}

TableStringDelegate::~TableStringDelegate() = default;

QString TableStringDelegate::displayText(const QVariant &value, const QLocale &) const
{
	return value.toString();
}

QWidget *TableStringDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
	QLineEdit *le = new QLineEdit(parent);
	QRegularExpressionValidator *validator = new QRegularExpressionValidator(_pimpl->regExp, parent);
	le->setValidator(validator);
	return le;
}

void TableStringDelegate::pattern(QString regExp) const noexcept
{
	_pimpl->regExp.setPattern(regExp);
}

QString TableStringDelegate::patern() const noexcept
{
	return _pimpl->regExp.pattern();
}
