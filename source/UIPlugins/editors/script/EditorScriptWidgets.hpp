/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef EDITORSCRIPTWIDGETS_HPP
#define EDITORSCRIPTWIDGETS_HPP

#include <interface/SScriptHandler.hpp>

/**
 * SGraphicalWidget-based class providing script functionality in the form of a TextEditorWidget.
 *
 * This class provides some of the script functionality (and its actions (RELOAD, RUN)). In order to use this class
 * the virtual function setupScripts has to be called in the childs constructor. The virtual functions all come
 * with a default implementation that can be overwritten to get specialized behaviour.
 * This class should only be used as a top SGraphicalWidget of a project.
 *
 * For more information please refer to @SScriptHandler and doc/202305_ScriptHandling.
 */
class SUGL_SHARED_EXPORT TextEditorScriptWidget : public TextEditorWidget, public SScriptHandler
{
public:
	TextEditorScriptWidget(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent), SScriptHandler(owner->name(), this) {}
	virtual ~TextEditorScriptWidget() = default;

protected:
	virtual void setupScripts() override;
	virtual void scriptReload(const QString &) override;
	virtual QString getPathForExtension(const QString &extension) const override;
};

/**
 * SGraphicalWidget-based class providing script functionality in the form of a TabEditorWidget.
 *
 * This class provides full script functionality (and its actions (RELOAD, RUN, OPEN)). In order to use this class
 * the virtual function setupScripts has to be called in the childs constructor. The virtual functions all come
 * with a default implementation that can be overwritten to get specialized behaviour.
 * This class should only be used as a top SGraphicalWidget of a project.
 *
 * For more information please refer to @SScriptHandler and doc/202305_ScriptHandling.
 */
class SUGL_SHARED_EXPORT TabEditorScriptWidget : public TabEditorWidget, public SScriptHandler
{
public:
	TabEditorScriptWidget(SPluginInterface *owner, QWidget *parent) : TabEditorWidget(owner, parent), SScriptHandler(owner->name(), this) {}
	virtual ~TabEditorScriptWidget() = default;

protected:
	virtual void setupScripts() override;
	virtual bool scriptOpen(const QString &path) override;
	virtual void scriptReload(const QString &path) override;
	virtual QString getPathForExtension(const QString &extension) const override;
};

/**
 * SGraphicalWidget-based class providing script functionality without a form specification
 *
 * This class provides partial script functionality (and its actions (RELOAD, RUN, OPEN)). In order to use this class
 * the virtual function setupScripts has to be called in the childs constructor. The virtual functions all come
 * with a default implementation that can be overwritten to get specialized behaviour.
 * This class should only be used as a top SGraphicalWidget of a project.
 *
 * For more information please refer to @SScriptHandler and doc/202305_ScriptHandling.
 */
class SUGL_SHARED_EXPORT SGraphicalScriptWidget : public SGraphicalWidget, public SScriptHandler
{
public:
	SGraphicalScriptWidget(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent), SScriptHandler(owner->name(), this) {}
	virtual ~SGraphicalScriptWidget() = default;

protected:
	virtual void setupScripts() override;
	virtual void scriptReload(const QString &) override;
	virtual QString getPathForExtension(const QString &extension) const override;
};

#endif // EDITORSCRIPTWIDGETS_HPP
