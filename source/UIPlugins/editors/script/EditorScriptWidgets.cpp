#include "EditorScriptWidgets.hpp"

#include <QTabWidget>

// Helper Function
QString _getPathForExtension(const QString &extension, const QString &path)
{
	if (path.endsWith(extension))
		return path;

	return QString();
}

void TextEditorScriptWidget::setupScripts()
{
	for (auto action : getScriptActions())
	{
		addAction(Menus::script, action);
	}
}

void TextEditorScriptWidget::scriptReload(const QString &)
{
	reload();
}

QString TextEditorScriptWidget::getPathForExtension(const QString &extension) const
{
	return _getPathForExtension(extension, getPath());
}

void TabEditorScriptWidget::setupScripts()
{
	for (auto action : getScriptActions())
	{
		addAction(Menus::script, action);
	}
}

bool TabEditorScriptWidget::scriptOpen(const QString &path)
{
	if (SScriptHandler::scriptOpen(path))
		return true;
	if (!openOutputFile(getWidgetForPath(path), path, true))
		return false;
	tab()->setCurrentIndex(tab()->count() - 1);
	return true;
}

void TabEditorScriptWidget::scriptReload(const QString &path)
{
	reloadTab(path);
	tab()->setCurrentIndex(findFileIndex(path));
}

QString TabEditorScriptWidget::getPathForExtension(const QString &extension) const
{
	return getMatchingPath(extension);
}

void SGraphicalScriptWidget::setupScripts()
{
	for (auto action : getScriptActions())
	{
		addAction(Menus::script, action);
	}
}

void SGraphicalScriptWidget::scriptReload(const QString &)
{
	reload();
}

QString SGraphicalScriptWidget::getPathForExtension(const QString &extension) const
{
	return _getPathForExtension(extension, getPath());
}
