#include "WebSearch.hpp"
#include "ui_WebSearch.h"

#include <memory>

#include <QComboBox>
#include <QCompleter>
#include <QKeyEvent>
#include <QLineEdit>
#include <QPushButton>
#include <QStringListModel>
#ifdef Q_OS_WIN
#	include <QWebEngineView>
#else
class QWebEngineView : public QWidget
{
};
#endif

#include "WebSearch.hpp"

class WebSearch::_WebSearch_pimpl
{
public:
	std::unique_ptr<Ui::WebSearch> ui = nullptr;
	/** QWebEngineView that is used by Geode plugin */
	QWebEngineView *view = nullptr;
};

WebSearch::WebSearch(QWebEngineView *parent) : QWidget(parent), _pimpl(std::make_unique<_WebSearch_pimpl>())
{
	// Ui
	_pimpl->ui = std::make_unique<Ui::WebSearch>();
	_pimpl->ui->setupUi(this);
#ifdef Q_OS_WIN
	// WebView
	_pimpl->view = parent;
#endif
	// Search components
	_pimpl->ui->searchBox->completer()->setCaseSensitivity(Qt::CaseSensitive);
	_pimpl->ui->searchBox->setInsertPolicy(QComboBox::InsertAtTop);
	_pimpl->ui->searchBox->setModel(new QStringListModel());
	connect(_pimpl->ui->searchBox->lineEdit(), &QLineEdit::returnPressed, this, &WebSearch::findNext);
	// Set event filters
	installEventFilter(this);
	_pimpl->ui->searchBox->installEventFilter(this);
}

WebSearch::~WebSearch() = default;

bool WebSearch::eventFilter(QObject *watched, QEvent *event)
{
	if (event->type() == QEvent::Type::KeyPress)
	{
		QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
		// Backwards search
		if (watched == _pimpl->ui->searchBox && keyEvent->modifiers() & Qt::ShiftModifier && (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return))
		{
			findPrevious();
			return true;
		}
		// Escape - hide
		if (keyEvent->key() == Qt::Key_Escape)
		{
			hide();
			return true;
		}
	}
	return QWidget::eventFilter(watched, event);
}

void WebSearch::setVisible(bool visible)
{
#ifdef Q_OS_WIN
	if (!visible)
		_pimpl->view->findText(""); // Clear the highlight
#endif

	QWidget::setVisible(visible);
}

void WebSearch::findNext()
{
	find(false);
}

void WebSearch::findPrevious()
{
	find(true);
}

void WebSearch::hideEvent(QHideEvent *event)
{
	QWidget::hideEvent(event);
#ifdef Q_OS_WIN
	// Return focus to QWebEngineView
	_pimpl->view->setFocus();
#endif
}

void WebSearch::showEvent(QShowEvent *event)
{
	QWidget::showEvent(event);
#ifdef Q_OS_WIN
	// If there is some text selected - put it in the search box.
	// There is a bug related to setting a new record in the QComboBox and this is the only working solution that was found for now.
	// Unfortunate downside is that the item is inserted into the history before it is searched - that is why this functionality is limited to text without newline.
	if (!_pimpl->view->selectedText().isEmpty() && !_pimpl->view->selectedText().contains('\n') && _pimpl->ui->searchBox->currentText() != _pimpl->view->selectedText())
	{
		_pimpl->ui->searchBox->insertItem(0, _pimpl->view->selectedText());
		_pimpl->ui->searchBox->setCurrentIndex(0);
		_pimpl->ui->searchBox->lineEdit()->setText(_pimpl->view->selectedText());
	}
#endif
	_pimpl->ui->searchBox->lineEdit()->selectAll();
	_pimpl->ui->searchBox->lineEdit()->setFocus();
}

void WebSearch::find(bool backward)
{
#ifdef Q_OS_WIN
	auto flags = QWebEnginePage::FindFlags();
	if (backward)
		flags |= QWebEnginePage::FindBackward;

	_pimpl->view->findText(_pimpl->ui->searchBox->currentText(), flags);
#endif
}
