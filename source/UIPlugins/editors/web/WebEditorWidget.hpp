/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef WEBEDITORWIDGET_HPP
#define WEBEDITORWIDGET_HPP

#include "PluginsGlobal.hpp"
#include "interface/SGraphicalInterface.hpp"

class TableEditor;
class QWebEngineDownloadItem;

/**
 * This is an html viewing editor based on QWebEngineView. It allows displaying the html contents, reloading them and navigating back
 * and forth upon in the right-click context menu. Nonetheless, its main objective is to display a local html file that is fully offline
 * and render its contents.
 */
class SUGL_SHARED_EXPORT WebEditorWidget : public SGraphicalWidget
{
	Q_OBJECT

public:
	/**
	 * WebEditorWidget constructor.
	 */
	WebEditorWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~WebEditorWidget() override;

	// Inherited via SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

public slots:
#ifdef Q_OS_WIN
	/** Provides a dialog to select file destination and accepts the download. */
	void downloadRequested(QWebEngineDownloadItem *webItem);
#endif

	// Inherited via SGraphicalWidget
	virtual void paste() override;
	virtual void copy() const override;
	virtual void cut() const override;
	virtual void undo() override;
	virtual void redo() override;

protected:
	// Inherited via SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private slots:
	void showSearch();

private:
	/** pimpl */
	class _WebEditorWidget_pimpl;
	std::unique_ptr<_WebEditorWidget_pimpl> _pimpl;
};
#endif // WEBEDITORWIDGET_HPP
