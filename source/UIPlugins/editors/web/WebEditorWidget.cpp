#include "WebEditorWidget.hpp"

#include <memory>

#include <QDir>
#include <QShortcut>
#include <QVBoxLayout>
#ifdef Q_OS_WIN
#	include <QWebEngineProfile>
#	include <QWebEngineUrlRequestInfo>
#	include <QWebEngineUrlRequestInterceptor>
#	include <QWebEngineUrlRequestJob>
#	include <QWebEngineUrlScheme>
#	include <QWebEngineUrlSchemeHandler>
#	include <QWebEngineView>
#	include <QStandardPaths>
#	include "editors/web/WebSearch.hpp"
#endif
#include <ShareablePoints/ShareablePointsList.hpp>

#include "utils/FileDialog.hpp"

const QString customSchema = "surveypad";
const QString customSchemaCommandLink = "link";
#ifdef Q_OS_WIN
/**
 * A Custom UrlRequestInterceptor class meant to interpret the custom requests hidden in href.
 *
 * For more infor, please refer to doc/202305_ScriptHandling and see @ExampleUrlSchemeHandler
 */
class UrlRequestInterceptor : public QWebEngineUrlRequestInterceptor
{
	Q_OBJECT
public:
	UrlRequestInterceptor(QObject *parent) : QWebEngineUrlRequestInterceptor(parent) {}

	void interceptRequest(QWebEngineUrlRequestInfo &info) override
	{
		QUrl rUrl = info.requestUrl();
		if (rUrl.scheme() == customSchema)
		{
			if (rUrl.host() == customSchemaCommandLink)
			{
				QString path = rUrl.path().remove(0, 2);
				QStringList command = path.split(',');
				if (command.size() == 2)
					// For whatever reason, fromNativeSeparators sometimes returns double front slashes instead of the single one, hence the replace
					emit fileChangeRequested(QDir::fromNativeSeparators(command.first()).replace("//", "/").remove('"'), -command.last().toInt());
			}
		}
	}
signals:
	void fileChangeRequested(const QString &path, int position);
};

#	include "WebEditorWidget.moc"

// Dummy class to fulfill custom schema requirements https://doc.qt.io/qt-5/qwebengineurlschemehandler.html
// The @requestStarted will only be called if @UrlRequestInterceptor::interceptRequest does not block the request
class ExampleUrlSchemeHandler : public QWebEngineUrlSchemeHandler
{
public:
	ExampleUrlSchemeHandler::ExampleUrlSchemeHandler(QObject *parent) : QWebEngineUrlSchemeHandler(parent) {}
	void requestStarted(QWebEngineUrlRequestJob *) {}
};
#endif

class WebEditorWidget::_WebEditorWidget_pimpl
{
public:
#ifdef Q_OS_WIN
	~_WebEditorWidget_pimpl()
	{
		// For whatever reason the profile was being freed before the page which resulted in the
		// "Release of profile requested but WebEnginePage still not deleted. Expect troubles !"
		// error. Having the dedicated destructor releasing page first has solved the issue.
		delete page;
		webView->deleteLater();
		urlInterceptor->deleteLater();
		profile->deleteLater();
	}

	QWebEnginePage *page = nullptr;
	QWebEngineProfile *profile = nullptr;
	WebSearch *search = nullptr;
	UrlRequestInterceptor *urlInterceptor = nullptr;
	QWebEngineView *webView = nullptr;
#endif
};

WebEditorWidget::WebEditorWidget(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent), _pimpl(std::make_unique<_WebEditorWidget_pimpl>())
{
#ifdef Q_OS_WIN
	// Set the URL request interceptor for the page
	_pimpl->urlInterceptor = new UrlRequestInterceptor(this);
	connect(_pimpl->urlInterceptor, &UrlRequestInterceptor::fileChangeRequested, this, &SGraphicalWidget::fileChangeRequested);
	_pimpl->profile = new QWebEngineProfile("WebEditorWidgetProfile", this);
	_pimpl->profile->installUrlSchemeHandler(customSchema.toUtf8(), new ExampleUrlSchemeHandler(this));
	_pimpl->profile->setUrlRequestInterceptor(_pimpl->urlInterceptor);

	// Workaround to the issue with hitting the breakpoint in the app.exec(). Reported (not fixed) Qt issues:
	// https://bugreports.qt.io/browse/QTBUG-43264
	// https://bugreports.qt.io/browse/QTBUG-59244
	_pimpl->profile->setHttpCacheType(QWebEngineProfile::DiskHttpCache);
	QString loc = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	QString cachePath = loc + "/WebCache/Scripts";
	QString dataPath = loc + "/WebData/Scripts";
	QDir(dataPath).remove("Visited Links");
	_pimpl->profile->setPersistentStoragePath(dataPath);
	_pimpl->profile->setCachePath(cachePath);

	// Create a WebEngineView
	_pimpl->webView = new QWebEngineView(this);
	// Create a WebEnginePage
	_pimpl->page = new QWebEnginePage(_pimpl->profile, _pimpl->webView);
	_pimpl->webView->setPage(_pimpl->page);
	// Editing actions
	connect(_pimpl->webView, &QWebEngineView::loadStarted, [this]() {
		emit undoAvailable(false);
		emit redoAvailable(false);
	});
	connect(_pimpl->webView, &QWebEngineView::selectionChanged, [this]() {
		auto selectionAvailable = _pimpl->webView->hasSelection();
		emit copyAvailable(selectionAvailable);
		emit cutAvailable(selectionAvailable);
	});
	// Download handling
	connect(_pimpl->profile, &QWebEngineProfile::downloadRequested, this, &WebEditorWidget::downloadRequested);
	// Search
	_pimpl->search = new WebSearch(_pimpl->webView);
	_pimpl->search->hide();
	// Search handling
	QObject::connect(new QShortcut(QKeySequence(QObject::tr("Ctrl+F")), _pimpl->webView), &QShortcut::activated, this, &WebEditorWidget::showSearch);

	// Create a horizontal layout
	QVBoxLayout *layout = new QVBoxLayout(this);
	// Add the widgets to the layout
	layout->addWidget(_pimpl->webView);
	layout->addWidget(_pimpl->search);

	// Set and show the widgets
	setLayout(layout);
	_pimpl->webView->show();
#endif
}
WebEditorWidget::~WebEditorWidget() = default;

ShareablePointsList WebEditorWidget::getContent() const
{
	return ShareablePointsList();
}

#ifdef Q_OS_WIN
void WebEditorWidget::downloadRequested(QWebEngineDownloadItem *webItem)
{
	if (!webItem || webItem->state() != QWebEngineDownloadItem::DownloadRequested)
		return;

	// Normal download procedure - with possibly modified extension
	QString fileName = webItem->downloadFileName();
	const QString path = getSaveFileName(this, tr("Save as"), fileName);
	if (path.isEmpty())
		return;
	QFileInfo p(path);
	webItem->setDownloadDirectory(p.path());
	webItem->setDownloadFileName(p.fileName());
	webItem->accept();
}
#endif

void WebEditorWidget::paste()
{
#ifdef Q_OS_WIN

	SGraphicalWidget::paste();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Paste)->trigger();
#endif
}

void WebEditorWidget::copy() const
{
#ifdef Q_OS_WIN

	SGraphicalWidget::copy();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Copy)->trigger();
#endif
}

void WebEditorWidget::cut() const
{
#ifdef Q_OS_WIN

	SGraphicalWidget::cut();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Cut)->trigger();
#endif
}

void WebEditorWidget::undo()
{
#ifdef Q_OS_WIN

	SGraphicalWidget::undo();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Undo)->trigger();
#endif
}

void WebEditorWidget::redo()
{
#ifdef Q_OS_WIN

	SGraphicalWidget::redo();
	_pimpl->webView->pageAction(QWebEnginePage::WebAction::Redo)->trigger();
#endif
}

bool WebEditorWidget::_save(const QString &path)
{
#ifdef Q_OS_WIN

	if (_pimpl->page->contentsSize().isEmpty())
		return false;

	_pimpl->page->save(path);
#endif

	return true;
}

bool WebEditorWidget::_open(const QString &path)
{
#ifdef Q_OS_WIN
	_pimpl->webView->load(QDir::fromNativeSeparators("file:///" + path));
#endif
	return true;
}

void WebEditorWidget::_newEmpty()
{
}

void WebEditorWidget::showSearch()
{
#ifdef Q_OS_WIN
	// Showing without firstly hiding the widget will not call showEvent (that is why hide() is called explicitly)
	_pimpl->search->hide();
	_pimpl->search->show();
#endif
}
