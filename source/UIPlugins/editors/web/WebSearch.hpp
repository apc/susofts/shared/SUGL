/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef WEBSEARCH_HPP
#define WEBSEARCH_HPP

#include <QWidget>

#include "PluginsGlobal.hpp"

class QWebEngineView;

/**
 * Class handling the search on websites.
 *
 * The class provides a simple widget that allows seaerching for a next or previous occurence of the text specified in the combobox, either using
 * keyboard ENTER press or push buttons. The widget can be hidden by clicking ESC key and can be shown with CTRL+F combination.
 */
class SUGL_SHARED_EXPORT WebSearch : public QWidget
{
	Q_OBJECT

public:
	WebSearch(QWebEngineView *parent);
	~WebSearch();

	virtual bool eventFilter(QObject *watched, QEvent *event) override;

public slots:
	// QWidget
	void setVisible(bool visible) override;

	/** find the next entered text */
	void findNext();
	/** find the previous entered text*/
	void findPrevious();

protected:
	/** hide the widget and return focus */
	void hideEvent(QHideEvent *event) override;
	/** show the widget and insert selected text as the search text */
	void showEvent(QShowEvent *event) override;

private slots:
	void find(bool backward = false);

private:
	/** pimpl */
	class _WebSearch_pimpl;
	std::unique_ptr<_WebSearch_pimpl> _pimpl;
};

#endif // WEBSEARCH_HPP
