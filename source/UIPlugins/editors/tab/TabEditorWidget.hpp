/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TABEDITORWIDGET_HPP
#define TABEDITORWIDGET_HPP

#include "interface/STabInterface.hpp"

/**
 * Container that uses a tabwidget as the main widget with some specialisations for editors.
 */
class SUGL_SHARED_EXPORT TabEditorWidget : public STabInterface
{
	Q_OBJECT

public:
	TabEditorWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~TabEditorWidget() override;

public slots:
	// SGraphicalWidget
	virtual void aboutTorun() override;

protected:
	// STabInterface
	/**
	* When tab is changed (after run) preserve visibility of search widget and the first visible line.
	* Since every plugin may handle the outputs in different manner this method has to be called explicitily
	* and is not associated with any signal.
	*/
	void restoreTabsAfterRun();
	virtual SGraphicalWidget *getWidgetForPath(const QString &file) override;

protected slots:
	virtual int fileChangeRequested(const QString &path, int position = 0) override;

private:
	class _TabEditorWidget_pimpl;
	std::unique_ptr<_TabEditorWidget_pimpl> _pimpl;
};

#endif // TABEDITORWIDGET_HPP
