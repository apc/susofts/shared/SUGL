#include "TabEditorWidget.hpp"

#include <unordered_map>

#include <QHash>
#include <QMimeDatabase>
#include <QString>
#include <QTabWidget>

#include <Logger.hpp>
#include <editors/text/StructureViewer.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextEditorWidget.hpp>
#include <editors/web/WebEditorWidget.hpp>

#include "QtStreams.hpp"
#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
namespace std
{
template<>
struct hash<QString>
{
	std::size_t operator()(const QString &s) const noexcept { return (size_t)qHash(s); }
};
} // namespace std
#endif

struct Restorable
{
	bool searchVisible = false;
	int firstLineVisible = 0;
	std::list<FoldLine> foldLines;
};

class TabEditorWidget::_TabEditorWidget_pimpl
{
public:
	/** Tracks paths to opened files and if they had search visible and what their first visible line was. */
	std::unordered_map<QString, Restorable> restorablePerFile;
};

TabEditorWidget::TabEditorWidget(SPluginInterface *owner, QWidget *parent) : STabInterface(owner, parent), _pimpl(std::make_unique<_TabEditorWidget_pimpl>())
{
}

TabEditorWidget::~TabEditorWidget() = default;

void TabEditorWidget::aboutTorun()
{
	STabInterface::aboutTorun();
	_pimpl->restorablePerFile.clear();

	QTabWidget *tabWidget = tab();
	for (int i = 0; i < tabWidget->count(); i++)
	{
		// We don't yet have search for Table editors
		TextEditorWidget *tw = qobject_cast<TextEditorWidget *>(tabWidget->widget(i));
		if (tw)
		{
			// filter out non-contracted folds
			std::list<FoldLine> folds = getFolds(tw->editor());
			folds.remove_if([tw](const FoldLine &foldLine) { return !foldLine.isFolded; });

			// create restorable for a given file
			_pimpl->restorablePerFile.insert(
				{tw->getPath(), {tw->isSearchVisible(), tw->editor()->SendScintilla(QsciScintillaBase::SCI_GETFIRSTVISIBLELINE), {folds}}});
		}
	}
}

void TabEditorWidget::restoreTabsAfterRun()
{
	if (_pimpl->restorablePerFile.empty())
		return;

	for (int i = 0; i < tab()->count(); i++)
	{
		TextEditorWidget *tw = qobject_cast<TextEditorWidget *>(tab()->widget(i));
		if (!tw || (tw && tw->isSearchVisible()))
			continue;
		const auto &restorable = _pimpl->restorablePerFile.find(tw->getPath());
		if (restorable == _pimpl->restorablePerFile.end())
			continue;
		if (restorable->second.searchVisible)
			tw->showSearch();
		if (restorable->second.firstLineVisible)
			tw->editor()->setFirstVisibleLine(restorable->second.firstLineVisible);
		if (!restorable->second.foldLines.empty())
		{
			int linecount = tw->editor()->lines();
			for (const auto &foldLine : restorable->second.foldLines)
			{
				if (foldLine.line >= linecount)
					continue;
				const QString linetext = tw->editor()->text(foldLine.line).trimmed();
				// if texts matches the text at given line index
				if (foldLine.txt == linetext)
					tw->editor()->SendScintilla(QsciScintillaBase::SCI_FOLDLINE, foldLine.line, QsciScintillaBase::SC_FOLDACTION_CONTRACT);
			}
		}
	}

	_pimpl->restorablePerFile.clear();
}

SGraphicalWidget *TabEditorWidget::getWidgetForPath(const QString &file)
{
	// two types supported for now
	QMimeType mime = QMimeDatabase().mimeTypeForFile(file);
	if (mime.inherits("text/html"))
		return new WebEditorWidget(owner(), this);
	else if (mime.inherits("text/plain"))
		return new TextEditorWidget(owner(), this);

	logWarning() << "File " << file << " cannot be opened with a suitable widget in SurveyPad!";
	return nullptr;
}

int TabEditorWidget::fileChangeRequested(const QString &path, int position)
{
	int idx = STabInterface::fileChangeRequested(path);
	// returns nullptr if incorrect
	TextEditorWidget *tw = qobject_cast<TextEditorWidget *>(tab()->widget(idx));
	if (idx > -1 && tw)
	{
		if (position < 0)
			tw->editor()->jumpTo(-position, true);
		else
			tw->editor()->jumpTo(position, false);
	}

	return idx;
}
