#include "QtStreams.hpp"

#include <QColor>
#include <QDate>
#include <QDateTime>
#include <QPoint>
#include <QPointF>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QTime>
#include <QUrl>

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QColor &color)
{
	stream << color.name(QColor::HexArgb).toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QDate &date)
{
	stream << date.toString().toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QDateTime &datetime)
{
	stream << datetime.toString().toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QPoint &point)
{
	stream << "(" << point.x() << "x" << point.y() << ")";
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QPointF &point)
{
	stream << "(" << point.x() << "x" << point.y() << ")";
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QSize &size)
{
	stream << "(" << size.width() << "x" << size.height() << ")";
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QSizeF &size)
{
	stream << "(" << size.width() << "x" << size.height() << ")";
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QString &str)
{
	stream << str.toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QStringRef str)
{
	stream << str.toString().toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QTime &time)
{
	stream << time.toString().toStdString();
	return stream;
}

SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QUrl &url)
{
	stream << url.toString().toStdString();
	return stream;
}
