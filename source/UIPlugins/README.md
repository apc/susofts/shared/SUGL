UIPlugins
=========

Create a shared library that can be embedd in your graphical projects to create plugins

Usage
-----

To use this library, you need to set this compile variables:
- `UIPlugins_IMPORT`
- `QSCINTILLA_DLL`

### Warning ###

This DLL includes all classes declared in UIPlugins, but also embeds Logs from the SurveyLib. Which means that you can't link against Logs, nor against wany projects that links agains Logs.

To fix it, we will need to create a specific DLL for Logs, which is not planned yet.

Create a Plugin
---------------

### Development ###

In order to create a plugin, you need to link against this library. You'll have to implement the `SpluginInterface`.

Note that some classes are here to help you with the implementation of the interface. This is the case of `STabInterface` which creates a tab widget where you can easily add all the widgets you want.

This is also the case of `utils/TextEditor` that setups a default text editor with nice config for QScintilla.

### Test ###

You can test the plugin thanks to the `Example_Plugin_app` project. You have a full example in `Example_Plugin_plug`.
