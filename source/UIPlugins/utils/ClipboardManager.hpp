/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef CLIPBOARDMANAGER_HPP
#define CLIPBOARDMANAGER_HPP

#include <memory>
#include <utility>

#include <QObject>

#include "PluginsGlobal.hpp"

class QMimeData;

class IShareablePointsListIO;
class ShareablePointsList;

/**
 * Clipboard manager to copy / paste list of points.
 *
 * This class is meant to be used along with the module @ref shpoints.
 *
 * The clipboard manager is a singleton. Thus, you can access it from anywhere. You need to configure it
 * with one or more serializers (classes that inherits IShareablePointsListIO), so this class knows how to serialize a list of points.
 * To do so, you can use the methods add() and erase().
 *
 * If you add 3 serializers (one for CSV, one for plain text and one for JSON), the copy() function will return the same structure
 * serialized in these 3 formats. Note that it is not possible to add multiple times serializers that have the same MIME type.
 *
 * The order of the serializers matters for the paste() function: it will always try to deserialize the content of the clipboard with
 * the first serializer, if its MIME type exists. Otherwise, it will try with the second and so on.
 *
 * Moreover, if no MIME type exist, the first added serializer will be used to try to deserialize the text/plain content of the clipboard.
 *
 * @see shpoints, ShareablePointsList, IShareablePointsListIO
 */
class SUGL_SHARED_EXPORT ClipboardManager : public QObject
{
	Q_OBJECT

public:
	/** @return the instance of the translator (singleton). */
	static ClipboardManager &getClipboardManager();

	~ClipboardManager() override;

	ClipboardManager(const ClipboardManager &) = delete; // can't copy
	ClipboardManager(const ClipboardManager &&) = delete;
	ClipboardManager &operator=(const ClipboardManager &) = delete;
	ClipboardManager &operator=(const ClipboardManager &&) = delete;

	/** Helper function to insert data in the clipboard. Same as `QApplication::clipboard()->setMimeData(data);` */
	void putInClipboard(QMimeData *data);
	/**
	 * Serialize the structure with the available serializers.
	 * The structure is serialized with the different serializers that have been previously added.
	 * @param spl the structure to serialize
	 * @param mimedata the mime data where to add the mime types. If null, a new QMimeData is created and returned.
	 * @return the given mimedata or a new mimedata, filled with mime types from the known serializers IShareablePointsListIO
	 * @see add(), paste(), putInClipboard()
	 */
	QMimeData *copy(const ShareablePointsList &spl, QMimeData *mimedata = nullptr, bool utf8 = true) const;
	/**
	 * Try to deserialize the content of the clipboard into a ShareablePointsList structure.
	 * The serializer taken is the first that has a MIME type that matches the content of the clipboard.
	 * If it is not possible to deserialize the clipboard, it will throw an exception from the serializer, generally of type SPIOException.
	 * @ note If you pass a QMimeData, it will try to deserialize from it instead of the content of the clipboard.
	 * @return the deserialized structure
	 * @see add(), copy()
	 */
	ShareablePointsList paste(const QMimeData *mimedata = nullptr, bool utf8 = true) const;
	/**
	 * @return the content of the clipboard that is associated to the given mime type.
	 * If it doesn't exist, return an empty string.
	 *
	 * Same as calling : `QString s = QApplication::clipboard()->mimeData()->data(mimeType);`
	 */
	QString getFromClipboard(const QString &mimeType) const;

	/** @return true if the clipboard is empty */
	bool empty() const;
	/** Tries to create a ShareablePointsList from the given content, using the given serializer. */
	ShareablePointsList serializeFromString(const std::string &contents, IShareablePointsListIO *serializer);
	/** @see serializeFromString(const std::string&, IShareablePointsListIO *). */
	ShareablePointsList serializeFromString(const std::string &contents, const std::string &mimetype);

	/**
	 * Add a serializer in the clipboard manager.
	 * A serializer is a class that implements IShareablePointsListIO. If one or more serializers are installed in this class,
	 * a call to the copy() function will serialize the given structure in all the different formats provided by these serializers.
	 *
	 * @warning the order of the serializers matters for the paste() function: the first serializer that matches the MIME type will be taken
	 * in this function.
	 *
	 * @note it is not possible to install a serializer if there is already an existing one with the same MIME type.
	 *
	 * @note the class will take ownership over the given serializer.
	 *
	 * @param serializer a pair with
	 *    - a pointer to a newly created serializer that will be used to copy() and paste()
	 *    - a boolean that says if the serializer should be used for serializing in the clipboard
	 * @return true if the serializer has been added, false otherwise (nullptr or another serializer with the same MIME type is already installed)
	 * @see erase(), clear(), at()
	 */
	bool add(std::pair<IShareablePointsListIO *, bool> serializer);
	/**
	 * Helper method that allows you to install multiple serializers at once.
	 * @see add()
	 */
	template<class... Ts>
	void add(std::pair<IShareablePointsListIO *, bool> serializer, Ts... rest)
	{
		add(serializer);
		add(rest...);
	}
	/**
	 * Erase the serializer at the given position.
	 * If position is invalid, does nothing and return nullptr.
	 * @param position the position of the serializer to remove
	 * @return a pointer to the removed serializer. If this pointer is not kept, the serializer will automatically be deleted.
	 * @see add(), at(), get()
	 */
	std::unique_ptr<IShareablePointsListIO> erase(size_t position);
	/**
	 * Erase the given serializer.
	 * If the serializer is not installed, does nothing and return nullptr.
	 * @param serializer the serializer to remove
	 * @return a pointer to the removed serializer. If this pointer is not kept, the serializer will automatically be deleted.
	 * @see add(), at(), get()
	 */
	std::unique_ptr<IShareablePointsListIO> erase(IShareablePointsListIO *serializer);
	/**
	 * Erase the serializer with the given MIME type.
	 * If the MIME type does not match any installed serializers, does nothing and return nullptr.
	 * @param mimetype the MIME type of the serializer to remove
	 * @return a pointer to the removed serializer. If this pointer is not kept, the serializer will automatically be deleted.
	 * @see add(), at(), get()
	 */
	std::unique_ptr<IShareablePointsListIO> erase(const std::string &mimetype);
	/** Removes all the installed serializers (they are also deleted/freed). */
	void clear() noexcept;
	/** @return the number of installed serializers. */
	size_t size() const noexcept;
	/** @return the serializer at the given position. */
	const IShareablePointsListIO &at(size_t position) const;
	/** @return the serializer at the given position. */
	IShareablePointsListIO &at(size_t position);
	/** @return the installed serializer with the given MIME type (or nullptr if it doesn't exist). */
	const IShareablePointsListIO *get(const std::string &mimetype) const;
	/** @return the installed serializer with the given MIME type (or nullptr if it doesn't exist). */
	IShareablePointsListIO *get(const std::string &mimetype) { return const_cast<IShareablePointsListIO *>(const_cast<ClipboardManager const &>(*this).get(mimetype)); }

private:
	/** Private constructor: singleton. */
	ClipboardManager();

private:
	/** pimpl */
	class _ClipboardManager_pimpl;
	std::unique_ptr<_ClipboardManager_pimpl> _pimpl;
};

#endif // CLIPBOARDMANAGER_HPP
