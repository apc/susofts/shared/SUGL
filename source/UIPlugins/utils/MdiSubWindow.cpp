#include "MdiSubWindow.hpp"

#include <QCloseEvent>
#include <QFileInfo>
#include <QMessageBox>

#include <Logger.hpp>

#include "FileDialog.hpp"
#include "ProjectManager.hpp"
#include "QtStreams.hpp"
#include "interface/SGraphicalInterface.hpp"
#include "interface/SPluginInterface.hpp"

MdiSubWindow::MdiSubWindow(QWidget *parent) : QMdiSubWindow(parent)
{
}

MdiSubWindow::~MdiSubWindow() = default;

const SGraphicalWidget *MdiSubWindow::graphicalWidget() const
{
	return qobject_cast<SGraphicalWidget *>(widget());
}

SGraphicalWidget *MdiSubWindow::graphicalWidget()
{
	return qobject_cast<SGraphicalWidget *>(widget());
}

bool MdiSubWindow::askToSave(bool toBeRun)
{
	auto w = graphicalWidget();
	if (!w || !w->isModified())
		return true;

	auto message = tr("You did some changes that have not been saved to the project '%1'. You will lose everything if you don't save them. Do you want to save?").arg(w->windowTitle());
	auto buttons = QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel;
	if (toBeRun)
	{
		message = tr(
			"You did some changes that have not been saved to the project '%1'. You cannot run the project without saving the changes first. Do you want to save now?")
					  .arg(w->windowTitle());
		buttons = QMessageBox::Save | QMessageBox::Cancel;
	}

	auto answer = QMessageBox::warning(this, tr("Save project '%1'?").arg(w->windowTitle()), message, buttons, QMessageBox::Save);
	if (answer == QMessageBox::Save)
	{
		if (!w->getPath().isEmpty())
			return w->save(w->getPath());
		auto &pm = ProjectManager::getProjectManager();
		QString path = getSaveFileName(this, tr("Save file"), "Title-Example", w->owner()->getExtensions());
		if (path.isEmpty())
			return false;
		return pm.saveProject(path, w);
	}
	else if (answer == QMessageBox::Discard)
		return true;
	return false;
}

void MdiSubWindow::setGraphicalWidget(SGraphicalWidget *w)
{
	setWidget(w);
	if (!w)
		return;
	setWindowIcon(w->owner()->icon());
	connect(w, &QWidget::windowTitleChanged, this, &MdiSubWindow::updateTitle);
	connect(w, &SGraphicalWidget::hasChanged, this, &MdiSubWindow::updateTitle);
	updateTitle();
	logDebug() << "MdiSubWindow: New project:" << windowTitle();
}

void MdiSubWindow::closeEvent(QCloseEvent *e)
{
	if (askToSave())
	{
		QMdiSubWindow::closeEvent(e);
		logDebug() << "MdiSubWindow: Close project:" << windowTitle();
		emit closed(this);
	}
	else
		e->ignore();
}

void MdiSubWindow::updateTitle()
{
	auto w = graphicalWidget();
	if (!w)
		return;

	QString title = w->windowTitle();
	if (w->isModified())
		title = "* " + title;
	setWindowTitle(title);
}
