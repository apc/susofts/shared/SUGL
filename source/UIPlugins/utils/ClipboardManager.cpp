#include "ClipboardManager.hpp"

#include <vector>

#include <QApplication>
#include <QClipboard>
#include <QMimeData>

#include <Logger.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "PointsExportConfig.hpp"
#include "SettingsManager.hpp"

class ClipboardManager::_ClipboardManager_pimpl
{
public:
	/** Ordered list of (de)serializers. */
	std::vector<std::pair<std::unique_ptr<IShareablePointsListIO>, bool>> ios;
};

ClipboardManager &ClipboardManager::getClipboardManager()
{
	static ClipboardManager instance;
	return instance;
}

ClipboardManager::~ClipboardManager() = default;

void ClipboardManager::putInClipboard(QMimeData *data)
{
	QApplication::clipboard()->setMimeData(data);
}

QMimeData *ClipboardManager::copy(const ShareablePointsList &spl, QMimeData *mimedata, bool utf8) const
{
	if (!mimedata)
		mimedata = new QMimeData();
	PointsExportConfigObject sett_cpy = SettingsManager::settings().settings<PointsExportConfigObject>(PointsExportConfig::configName());
	for (auto &[io, b] : _pimpl->ios)
	{
		if (!b)
			continue;
		io->exportFieldsFrame = sett_cpy.exportFieldsFrame;
		io->exportFieldsParams = sett_cpy.exportFieldsParams;
		io->exportFieldsPoint = sett_cpy.exportFieldsPoint;
		io->exportFieldsPointsList = sett_cpy.exportFieldsPointsList;
		io->exportFieldsPosition = sett_cpy.exportFieldsPosition;
		io->utf8(utf8);
		mimedata->setData(QString::fromStdString(io->getMIMEType()), io->write(spl).c_str());
	}
	return mimedata;
}

ShareablePointsList ClipboardManager::paste(const QMimeData *mimedata, bool utf8) const
{
	if (!mimedata)
		mimedata = QApplication::clipboard()->mimeData();

	// we find the first compatible mimetype
	for (auto &[io, b] : _pimpl->ios)
	{
		if (!b)
			continue;
		if (!mimedata->hasFormat(QString::fromStdString(io->getMIMEType())))
			continue;
		io->utf8(utf8);
		try
		{
			return io->read(mimedata->data(QString::fromStdString(io->getMIMEType())).constData());
		}
		catch (...)
		{
			logDebug() << "Pasting with serializer for" << io->getMIMEType() << "failed.";
		}
	}
	// TODO: improve! for now, we try to decrypt the text content with the first serializer (which should be JSONBIN)
	// change it to use CSV (with tab as separator?)
	return _pimpl->ios.front().first->read(mimedata->text().toStdString());
}

QString ClipboardManager::getFromClipboard(const QString &mimeType) const
{
	return QApplication::clipboard()->mimeData()->data(mimeType);
}

bool ClipboardManager::empty() const
{
	return QApplication::clipboard()->mimeData()->formats().isEmpty();
}

ShareablePointsList ClipboardManager::serializeFromString(const std::string &contents, IShareablePointsListIO *serializer)
{
	if (!serializer)
		throw SPIOException("Serializer is NULL.");

	// ShareablePointsList
	try
	{
		return serializer->read(contents);
	}
	catch (...)
	{
	}
	// ShareableFrame
	try
	{
		ShareablePointsList spl("__dummy__");
		auto frame = serializer->readFrame(contents);
		spl.setRootFrame(new ShareableFrame(std::move(frame)));
		return spl;
	}
	catch (...)
	{
	}
	// ShareablePoint
	try
	{
		ShareablePointsList spl("__dummy__");
		spl.getRootFrame().setName("__dummy__");
		auto point = serializer->readPoint(contents);
		spl.getRootFrame().addPoint() = point;
		return spl;
	}
	catch (...)
	{
	}
	throw SPIOException("Impossible to serialize given contents", "", contents);
}

ShareablePointsList ClipboardManager::serializeFromString(const std::string &contents, const std::string &mimetype)
{
	return serializeFromString(contents, get(mimetype));
}

bool ClipboardManager::add(std::pair<IShareablePointsListIO *, bool> serializer)
{
	if (!serializer.first || get(serializer.first->getMIMEType())) // if serializer == nullptr or we already have a serializer with the same mimetype
		return false;
	_pimpl->ios.emplace_back(serializer);
	return true;
}

std::unique_ptr<IShareablePointsListIO> ClipboardManager::erase(size_t position)
{
	return erase(_pimpl->ios[position].first.get());
}

std::unique_ptr<IShareablePointsListIO> ClipboardManager::erase(IShareablePointsListIO *serializer)
{
	auto it = std::find_if(std::begin(_pimpl->ios), std::end(_pimpl->ios), [&serializer](const auto &io) -> bool { return io.first.get() == serializer; });
	if (it == std::end(_pimpl->ios))
		return nullptr;
	auto ptr = std::move(it->first);
	_pimpl->ios.erase(it);
	return ptr;
}

std::unique_ptr<IShareablePointsListIO> ClipboardManager::erase(const std::string &mimetype)
{
	return erase(get(mimetype));
}

void ClipboardManager::clear() noexcept
{
	_pimpl->ios.clear();
}

size_t ClipboardManager::size() const noexcept
{
	return _pimpl->ios.size();
}

const IShareablePointsListIO &ClipboardManager::at(size_t position) const
{
	return *_pimpl->ios.at(position).first;
}

IShareablePointsListIO &ClipboardManager::at(size_t position)
{
	return *_pimpl->ios.at(position).first;
}

const IShareablePointsListIO *ClipboardManager::get(const std::string &mimetype) const
{
	auto it = std::find_if(std::cbegin(_pimpl->ios), std::cend(_pimpl->ios), [&mimetype](const auto &io) -> bool { return io.first->getMIMEType() == mimetype; });
	return it == std::cend(_pimpl->ios) ? nullptr : it->first.get();
}

ClipboardManager::ClipboardManager() : _pimpl(std::make_unique<_ClipboardManager_pimpl>())
{
}
