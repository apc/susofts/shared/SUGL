/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef IUNDOCOMMAND_HPP
#define IUNDOCOMMAND_HPP

#include "PluginsGlobal.hpp"

/**
 * IUndoCommand is an interface that has to be implemented once for different widgets whose actions should be recorded in UndoStack.
 *
 * This class is meant to be used along with the UndoStack class. The class should hold all the necessary data required to restore the state of a widget.
 *
 * @see UndoStack
 */
class SUGL_SHARED_EXPORT IUndoCommand
{
public:
	virtual ~IUndoCommand() = default;

	/** Method restores recorded state (contents) for its widget. */
	virtual void restoreState() = 0;
};

#endif // IUNDOCOMMAND_HPP
