/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef NETWORKCONFIG_HPP
#define NETWORKCONFIG_HPP

#include <memory>

#include "PluginsGlobal.hpp"
#include "interface/SConfigInterface.hpp"

/**
 * Config object for NetworkManager class.
 */
struct SUGL_SHARED_EXPORT NetworkConfigObject : public SConfigObject
{
	NetworkConfigObject();
	virtual ~NetworkConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	int timeout;
};

/**
 * Widget to edit the configuration for NetworkManager class.
 */
class SUGL_SHARED_EXPORT NetworkConfig : public SConfigWidget
{
	Q_OBJECT

public:
	static const QString &configName() noexcept;

	/** Constructor */
	NetworkConfig(QWidget *parent = nullptr);
	virtual ~NetworkConfig() override;

	// SconfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;

public slots:
	virtual void restoreDefaults() override;

private:
	/** pimpl */
	class _NetworkConfig_pimpl;
	std::unique_ptr<_NetworkConfig_pimpl> _pimpl;
};

#endif // NETWORKCONFIG_HPP
