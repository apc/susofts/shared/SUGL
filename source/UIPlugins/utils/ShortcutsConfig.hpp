/*
© Copyright CERN 2000-2021. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SHORTCUTSCONFIG_HPP
#define SHORTCUTSCONFIG_HPP

#include <memory>

#include <QKeySequence>
#include <QObject>

#include <interface/SConfigInterface.hpp>
#include <utils/SettingsManager.hpp>

/**
 * Config object for SUGL shortcuts
 *
 * To get the relevant shortcut from the settings one should use @ShortcutsConfigObject::getShortcutSequence with @ShortcutsConfigObject::Shortcut enum value as input.
 *
 * The shortcuts are stored as @QKeySequence in the map as it is easier to handle them for all specific cases this way (viewing, reading, writing and outputing a sequence).
 * For now only one shortcut sequence can be set per action.
 */
struct SUGL_SHARED_EXPORT ShortcutsConfigObject : public SConfigObject
{
	virtual ~ShortcutsConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	enum Shortcut
	{
		//*** Editor ***
		COPY = 0,
		PASTE,
		CUT,
		UNDO,
		REDO,
		REMOVEROW,
		AUTOCOMPLETE,
		//*** Project ***
		NEWPROJECT,
		OPENPROJECT,
		CLOSEPROJECT,
		SAVE,
		SAVEAS,
		SAVEALL,
		RUN,
		RELOADFROMDISK,
		//*** Search ***
		GOTO,
		FIND,
		REPLACE,
		PREVPOSITION,
		NEXTPOSITION,
		//*** View ***
		ZOOMIN,
		ZOOMOUT,
		ZOOMRESTORE,
		//*** App ***
		EXIT
	};

	/* Returns the @QKeySequence for the given shortcut */
	QKeySequence getShortcutSequence(ShortcutsConfigObject::Shortcut shortcut) { return ShortcutMap[shortcut].second; }
	/* Returns the @QString describing given shortcut */
	QString getShortcutName(ShortcutsConfigObject::Shortcut shortcut) { return ShortcutMap[shortcut].first; }

	//***** SHORTCUTS *****
	/* clang-format off */
	std::map<Shortcut, QPair<QString, QKeySequence>> ShortcutMap = {
		//*** Editor ***
		{Shortcut::COPY, {QObject::tr("Copy"), QKeySequence::Copy}},
		{Shortcut::PASTE, {QObject::tr("Paste"), QKeySequence::Paste}},
		{Shortcut::CUT, {QObject::tr("Cut"), QKeySequence::Cut}},
		{Shortcut::UNDO, {QObject::tr("Undo"), QKeySequence::Undo}},
		{Shortcut::REDO, {QObject::tr("Redo"), QKeySequence::Redo}},
		{Shortcut::REMOVEROW, {QObject::tr("Remove table row"), QKeySequence::Delete}},
		{Shortcut::AUTOCOMPLETE, {QObject::tr("Auto-complete text"), QKeySequence::fromString(QObject::tr("Ctrl+Space"))}},
		//*** Project ***
		{Shortcut::NEWPROJECT, {QObject::tr("New project"), QKeySequence::New}},
		{Shortcut::OPENPROJECT, {QObject::tr("Open project"), QKeySequence::Open}},
		{Shortcut::CLOSEPROJECT, {QObject::tr("Close Project"), QObject::tr("Ctrl+W")}},
		{Shortcut::SAVE, {QObject::tr("Save"), QKeySequence::Save}},
		{Shortcut::SAVEAS, {QObject::tr("Save as"), QKeySequence::SaveAs}},
		{Shortcut::SAVEALL, {QObject::tr("Save all projects"), QObject::tr("Ctrl+Shift+S")}},
		{Shortcut::RUN, {QObject::tr("Run"), QKeySequence::fromString(QObject::tr("Ctrl+Return"))}},
		{Shortcut::RELOADFROMDISK, {QObject::tr("Reload files from disk"), QKeySequence::Refresh}},
		//*** Search ***
		{Shortcut::GOTO, {QObject::tr("Go to"), QKeySequence::fromString(QObject::tr("Ctrl+G"))}},
		{Shortcut::FIND, {QObject::tr("Search"), QKeySequence::Find}},
		{Shortcut::REPLACE, {QObject::tr("Replace"), QKeySequence::Replace}},
		{Shortcut::PREVPOSITION, {QObject::tr("Go to previous cursor position"), QKeySequence::Back}},
		{Shortcut::NEXTPOSITION, {QObject::tr("Go to next cursor position"), QKeySequence::Forward}},
		//*** View ***
		{Shortcut::ZOOMIN, {QObject::tr("Zoom in"), QKeySequence::ZoomIn}},
		{Shortcut::ZOOMOUT, {QObject::tr("Zoom out"), QKeySequence::ZoomOut}},
		{Shortcut::ZOOMRESTORE, {QObject::tr("Restore zoom"), QKeySequence::fromString(QObject::tr("Ctrl+0"))}},
		//*** App ***
		{Shortcut::EXIT, {QObject::tr("Exit application"), QKeySequence::fromString(QObject::tr("Ctrl+Q"))}}
	};
	/* clang-format on*/
};

/**
 * Widget to edit SUGL shortcuts.
 *
 * The shortcut editors (@QLabel and @QKeySequenceEdit) are created dynamically and handled based on @ShortcutsConfigObject::ShortcutMap entries.
 */
class SUGL_SHARED_EXPORT ShortcutsConfig : public SConfigWidget
{
	Q_OBJECT

public:
	static const QString &configName() noexcept;

	/** Constructor */
	ShortcutsConfig(QWidget *parent = nullptr);
	virtual ~ShortcutsConfig() override;

	// SconfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

	/* Return shortcuts config object */
	static ShortcutsConfigObject getShortcutConfig();

private:
	/** pimpl */
	class _ShortcutsConfig_pimpl;
	std::unique_ptr<_ShortcutsConfig_pimpl> _pimpl;
};

#endif // SHORTCUTSCONFIG_HPP
