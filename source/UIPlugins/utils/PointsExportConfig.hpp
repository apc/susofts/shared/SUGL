/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PONTSEXPORTCONFIG_HPP
#define PONTSEXPORTCONFIG_HPP

#include <memory>

#include <ShareablePoints/IShareablePointsListIO.hpp>

#include "PluginsGlobal.hpp"
#include "interface/SConfigInterface.hpp"

class QPushButton;

/**
 * Config object for @PointsExportConfig class.
 */
struct SUGL_SHARED_EXPORT PointsExportConfigObject : public SConfigObject
{
	virtual ~PointsExportConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Is export of Points List enabled */
	bool exportPointsList = false;
	/** List of position fields to be exported. */
	IShareablePointsListIO::Fields exportFieldsPosition{{"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez"}};
	/** List of point fields to be exported. */
	IShareablePointsListIO::Fields exportFieldsPoint{{"name", "position", "inlineComment", "headerComment", "active", "extraInfos"}};
	/** List of parameter fields to be exported. */
	IShareablePointsListIO::Fields exportFieldsParams{{"precision", "coordsys", "extraInfos"}};
	/** List of frame fields to be exported. */
	IShareablePointsListIO::Fields exportFieldsFrame{{"name", "translation", "rotation", "scale", "isfreescale", "innerFrames", "points"}};
	/** List of PointsList fields to be exported. */
	IShareablePointsListIO::Fields exportFieldsPointsList{{"title", "params", "rootFrame"}};
};

/**
 * Widget to edit the configuration for PointsExportConfig class.
 */
class SUGL_SHARED_EXPORT PointsExportConfig : public SConfigWidget
{
	Q_OBJECT

public:
	static const QString &configName() noexcept;
	/**
	 * Returns a temporary PointsExportConfig settings window that allows to pick what point fields should be copied.
	 * @return PointsExportConfigObject pointer containing selected copy configuration. If dialog will be cancelled - nullptr will be returned.
	 */
	static PointsExportConfigObject *showCopyDialog();
	/**
	 * Returns current application configuration containing the information about which point fields are set to be copied.
	 * @return PointsExportConfigObject pointer to the current application configuration.
	 */
	static PointsExportConfigObject *getAppPointsConfig();

	/** Constructor */
	PointsExportConfig(QWidget *parent = nullptr);
	virtual ~PointsExportConfig() override;

	// SconfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	/** pimpl */
	class _PointsExportConfig_pimpl;
	std::unique_ptr<_PointsExportConfig_pimpl> _pimpl;
};

#endif // PONTSEXPORTCONFIG_HPP
