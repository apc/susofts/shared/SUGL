#include "NetworkManager.hpp"

#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QTimer>
/**
 * CentOS7 does not support Qt5-qtwebengine-devel, and therefore, cannot use full capacities of NetworkManager.
 * When the infrastructure gets updated this check should be changed. Moreover, for NetworkManager to work properly
 * with TLS, OpenSSL will need to be set on Linux (and in ext_libs for SUSoftCMakeCommon).
 *
 * All guards of this type in this file should be removed after update.
 */
#ifdef Q_OS_WIN
#	include <QWebEngineCookieStore>
#	include <QWebEnginePage>
#	include <QWebEngineProfile>
#	include <QStandardPaths>
#	include <QDir>
#endif

#include <Logger.hpp>

#include "NetworkConfig.hpp"
#include "SettingsManager.hpp"

class NetworkManagerCookieJar : public QNetworkCookieJar
{
	Q_OBJECT

public:
	NetworkManagerCookieJar(QObject *parent = nullptr) : QNetworkCookieJar(parent) {}
	~NetworkManagerCookieJar() = default;

public slots:
	void deleteAllCookies() { setAllCookies({}); }
};
#include "NetworkManager.moc"

class NetworkManager::_NetworkManager_pimpl
{
public:
	/** Single instance of QNetworkAccessManager will be used through the entire application */
	QNetworkAccessManager *nam = nullptr;
	NetworkManagerCookieJar *cookieJar = nullptr;
	bool isLoggedIn = false;
	const QString geodeUrl = "https://apex-sso.cern.ch/pls/htmldb_accdb/f?p=GEODEAPEX";
	const QString geodeLoggedInUrl = "https://apex-sso.cern.ch/pls/htmldb_accdb/survey/surveypad/status/";
#ifdef Q_OS_WIN
	QWebEngineProfile *profile = nullptr;
	QWebEngineCookieStore *cookieStore = nullptr;
#endif
};

NetworkManager::NetworkManager(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_NetworkManager_pimpl>())
{
	// network manager
	_pimpl->nam = new QNetworkAccessManager();
#ifdef Q_OS_WIN
	auto conf = SettingsManager::settings().settings<NetworkConfigObject>(NetworkConfig::configName());
	_pimpl->nam->setTransferTimeout(conf.timeout * 1000);
	// cookie jar
	_pimpl->cookieJar = new NetworkManagerCookieJar(_pimpl->nam);
	_pimpl->nam->setCookieJar(_pimpl->cookieJar);
	// profile
	_pimpl->profile = new QWebEngineProfile("SurveyPad", this);
	_pimpl->profile->setPersistentCookiesPolicy(QWebEngineProfile::ForcePersistentCookies);
	_pimpl->profile->setHttpCacheType(QWebEngineProfile::DiskHttpCache);
	// Workaround to the issue with hitting the breakpoint in the app.exec(). Reported (not fixed) Qt issues:
	// https://bugreports.qt.io/browse/QTBUG-43264
	// https://bugreports.qt.io/browse/QTBUG-59244
	QString loc = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	QString cachePath = loc + "/WebCache";
	QString dataPath = loc + "/WebData";
	QDir(dataPath).remove("Visited Links");
	_pimpl->profile->setPersistentStoragePath(dataPath);
	_pimpl->profile->setCachePath(cachePath);
	// store - get and remove the cookies related to SurveyPad web profile (synchronising profile and NetworkManager)
	_pimpl->cookieStore = _pimpl->profile->cookieStore();
	connect(_pimpl->cookieStore, &QWebEngineCookieStore::cookieAdded, this, &NetworkManager::insertCookie);
	connect(_pimpl->cookieStore, &QWebEngineCookieStore::cookieRemoved, this, &NetworkManager::removeCookie);

	_pimpl->cookieStore->loadAllCookies();
	// Unfortunate cookie initialization workaround
	// For some reason just `loadAllCokies` does not load any of them
	// the only way to force it is by loading any URL
	QWebEnginePage *page = new QWebEnginePage((getProfile()), this);
	page->load(QUrl(_pimpl->geodeUrl));
	page->deleteLater();
#endif
	connect(&SettingsManager::settings(), &SettingsManager::settingsChanged, this, &NetworkManager::updateUi);
	// logged in periodical check - once after 2s, and then periodically every 60s
	QTimer *loggedInTimer = new QTimer(this);
	QTimer::singleShot(2000, this, &NetworkManager::loggedInCheck);
	loggedInTimer->setInterval(60000);
	loggedInTimer->setSingleShot(false);
	loggedInTimer->start();
	connect(loggedInTimer, &QTimer::timeout, this, &NetworkManager::loggedInCheck);
}

NetworkManager::~NetworkManager() = default;

NetworkManager &NetworkManager::getNetworkManager()
{
	static NetworkManager _instance;
	return _instance;
}

QNetworkReply *NetworkManager::post(const QNetworkRequest &request, const QByteArray &data)
{
	QNetworkReply *reply = _pimpl->nam->post(request, data);
	requestFix(reply);
	return reply;
}

QNetworkReply *NetworkManager::get(const QNetworkRequest &request)
{
	QNetworkReply *reply = _pimpl->nam->get(request);
	requestFix(reply);
	return reply;
}

void NetworkManager::requestFix(QNetworkReply *reply)
{
	// Whenever a request with QNetworkAccessManager is done CERN SSO most likely stores the last redirect (final destination) with a cookie.
	// However, this information is not stored in the cookie itself but somewhere on the server side, i.e. we cannot modify it.
	// Therefore, whenever the request is done the last link from Geode Web Browser gets replaced with the one used with QNetworkAccessManager,
	// this happens due to cookie sharing between them. In order to avoid it we make a dummy request to GEODE homepage to set it back to what it should be
	// after log in happens.
	if (!_pimpl->isLoggedIn)
	{
		connect(reply, &QNetworkReply::finished, [this]() {
			QNetworkRequest request(QUrl(_pimpl->geodeUrl));
			QNetworkReply *fakeReply = _pimpl->nam->get(request);
			connect(fakeReply, &QNetworkReply::finished, [this, fakeReply]() {
				fakeReply->deleteLater(); });
		});
	}
}

QWebEngineProfile *NetworkManager::getProfile()
{
#ifdef Q_OS_WIN
	return _pimpl->profile;
#else
	return nullptr;
#endif
}

void NetworkManager::clearCookies()
{
	_pimpl->cookieJar->deleteAllCookies();
#ifdef Q_OS_WIN
	_pimpl->profile->cookieStore()->deleteAllCookies();
#endif
}

void NetworkManager::updateUi(const QString &config)
{
	if (config != NetworkConfig::configName())
		return;
	auto conf = SettingsManager::settings().settings<NetworkConfigObject>(config);

#ifdef Q_OS_WIN
	_pimpl->nam->setTransferTimeout(conf.timeout * 1000);
#endif
}

bool NetworkManager::insertCookie(const QNetworkCookie &cookie)
{
#ifdef Q_OS_WIN
	return _pimpl->cookieJar->insertCookie(cookie);
#else
	return true;
#endif
}

bool NetworkManager::removeCookie(const QNetworkCookie &cookie)
{
#ifdef Q_OS_WIN
	return _pimpl->cookieJar->deleteCookie(cookie);
#else
	return true;
#endif
}


void NetworkManager::loggedInCheck()
{
	// REQUEST
	QNetworkRequest request(QUrl(_pimpl->geodeLoggedInUrl));
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
	// SEND
	QNetworkReply *reply = get(request);
	// GET
	connect(reply, &QNetworkReply::finished, [this, reply]() {
		auto json = reply->readAll();
		bool isLoggedIn = !json.isEmpty() && json.contains("connected");
		if (_pimpl->isLoggedIn != isLoggedIn)
			emit logInStatusChanged(isLoggedIn);
		_pimpl->isLoggedIn = isLoggedIn;

		reply->deleteLater();
	});
}
