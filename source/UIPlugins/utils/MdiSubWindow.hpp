/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef MDISUBWINDOW_HPP
#define MDISUBWINDOW_HPP

#include <QMdiSubWindow>

#include "PluginsGlobal.hpp"

class SGraphicalWidget;

/**
 * A window that uses SGraphicalWidget as its widget. This window is meant to be placed inside QMdiArea.
 *
 * A window inherits from QMdiSubWindow, additionally it allows to:
 * - explicitly use SGraphicalWidget as its widget,
 * - change the window title based on the stored SGraphicalWidget title,
 * - display the dialog to save the project if MdiSubWindow is to be closed.
 */
class SUGL_SHARED_EXPORT MdiSubWindow : public QMdiSubWindow
{
	Q_OBJECT

public:
	MdiSubWindow(QWidget *parent = nullptr);
	~MdiSubWindow() override;

	/** Returns current internal SGraphicalWidget. */
	const SGraphicalWidget *graphicalWidget() const;
	/** Returns current internal SGraphicalWidget. */
	SGraphicalWidget *graphicalWidget();
	/**
	 * If the project  is modified the user will be asked if they want to save the current progress.
	 *
	 * @param toBeRun in case the project is meant to be run.
	 * @return boolean, true in case the user wants to save (or the widget was not modified), false otherwise.
	 */
	bool askToSave(bool toBeRun = false);

public slots:
	/** Sets graphical widget and sets up the icon for the MdiSubWindow as well as relevant connections. */
	void setGraphicalWidget(SGraphicalWidget *w);

signals:
	/** Signals that the given MdiSubWindow should be closed. */
	void closed(MdiSubWindow *);

protected:
	virtual void closeEvent(QCloseEvent *e) override;

private slots:
	/** Updates the title for MdiSubWindow. */
	void updateTitle();
};

#endif // MDISUBWINDOW_HPP
