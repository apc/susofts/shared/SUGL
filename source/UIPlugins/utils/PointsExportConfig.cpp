#include "PointsExportConfig.hpp"
#include "ui_PointsExportConfig.h"

#include <QBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QSettings>

#include "SettingsManager.hpp"

namespace
{
const QString _configname = "PointsExport";
}

bool PointsExportConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const PointsExportConfigObject &oc = static_cast<const PointsExportConfigObject &>(o);
	return exportPointsList == oc.exportPointsList && exportFieldsPosition.getFields() == oc.exportFieldsPosition.getFields()
		&& exportFieldsPoint.getFields() == oc.exportFieldsPoint.getFields() && exportFieldsParams.getFields() == oc.exportFieldsParams.getFields()
		&& exportFieldsFrame.getFields() == oc.exportFieldsFrame.getFields() && exportFieldsPointsList.getFields() == oc.exportFieldsPointsList.getFields();
}

void PointsExportConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(PointsExportConfig::configName());

	// Global
	settings.setValue("exportPointsList", exportPointsList);
	// Positions
	settings.beginGroup("Position");
	settings.setValue("x", exportFieldsPosition.hasField("x"));
	settings.setValue("y", exportFieldsPosition.hasField("y"));
	settings.setValue("z", exportFieldsPosition.hasField("z"));
	settings.setValue("sigmax", exportFieldsPosition.hasField("sigmax"));
	settings.setValue("sigmay", exportFieldsPosition.hasField("sigmay"));
	settings.setValue("sigmaz", exportFieldsPosition.hasField("sigmaz"));
	settings.setValue("isfreex", exportFieldsPosition.hasField("isfreex"));
	settings.setValue("isfreey", exportFieldsPosition.hasField("isfreey"));
	settings.setValue("isfreez", exportFieldsPosition.hasField("isfreez"));
	settings.endGroup();
	// Point
	settings.beginGroup("Point");
	settings.setValue("name", exportFieldsPoint.hasField("name"));
	settings.setValue("position", exportFieldsPoint.hasField("position"));
	settings.setValue("inlineComment", exportFieldsPoint.hasField("inlineComment"));
	settings.setValue("headerComment", exportFieldsPoint.hasField("headerComment"));
	settings.setValue("active", exportFieldsPoint.hasField("active"));
	settings.setValue("extraInfos", exportFieldsPoint.hasField("extraInfos"));
	settings.endGroup();
	// Params
	settings.beginGroup("Params");
	settings.setValue("precision", exportFieldsParams.hasField("precision"));
	settings.setValue("coordsys", exportFieldsParams.hasField("coordsys"));
	settings.setValue("extraInfos", exportFieldsParams.hasField("extraInfos"));
	settings.endGroup();
	// Frame
	settings.beginGroup("Frame");
	settings.setValue("name", exportFieldsFrame.hasField("name"));
	settings.setValue("translation", exportFieldsFrame.hasField("translation"));
	settings.setValue("rotation", exportFieldsFrame.hasField("rotation"));
	settings.setValue("scale", exportFieldsFrame.hasField("scale"));
	settings.setValue("isfreescale", exportFieldsFrame.hasField("isfreescale"));
	settings.setValue("innerFrames", exportFieldsFrame.hasField("innerFrames"));
	settings.setValue("points", exportFieldsFrame.hasField("points"));
	settings.endGroup();
	// PointsList
	settings.beginGroup("PointsList");
	settings.setValue("title", exportFieldsPointsList.hasField("title"));
	settings.setValue("params", exportFieldsPointsList.hasField("params"));
	settings.setValue("rootFrame", exportFieldsPointsList.hasField("rootFrame"));
	settings.endGroup();

	settings.endGroup();
}

void PointsExportConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(PointsExportConfig::configName());

	// Global
	exportPointsList = settings.value("exportPointsList", false).toBool();
	// Positions
	settings.beginGroup("Position");
	settings.value("x", true).toBool() ? exportFieldsPosition.addField("x") : exportFieldsPosition.removeField("x");
	settings.value("y", true).toBool() ? exportFieldsPosition.addField("y") : exportFieldsPosition.removeField("y");
	settings.value("z", true).toBool() ? exportFieldsPosition.addField("z") : exportFieldsPosition.removeField("z");
	settings.value("sigmax", true).toBool() ? exportFieldsPosition.addField("sigmax") : exportFieldsPosition.removeField("sigmax");
	settings.value("sigmay", true).toBool() ? exportFieldsPosition.addField("sigmay") : exportFieldsPosition.removeField("sigmay");
	settings.value("sigmaz", true).toBool() ? exportFieldsPosition.addField("sigmaz") : exportFieldsPosition.removeField("sigmaz");
	settings.value("isfreex", true).toBool() ? exportFieldsPosition.addField("isfreex") : exportFieldsPosition.removeField("isfreex");
	settings.value("isfreey", true).toBool() ? exportFieldsPosition.addField("isfreey") : exportFieldsPosition.removeField("isfreey");
	settings.value("isfreez", true).toBool() ? exportFieldsPosition.addField("isfreez") : exportFieldsPosition.removeField("isfreez");
	settings.endGroup();
	// Point
	settings.beginGroup("Point");
	settings.value("name", true).toBool() ? exportFieldsPoint.addField("name") : exportFieldsPoint.removeField("name");
	settings.value("position", true).toBool() ? exportFieldsPoint.addField("position") : exportFieldsPoint.removeField("position");
	settings.value("inlineComment", true).toBool() ? exportFieldsPoint.addField("inlineComment") : exportFieldsPoint.removeField("inlineComment");
	settings.value("headerComment", true).toBool() ? exportFieldsPoint.addField("headerComment") : exportFieldsPoint.removeField("headerComment");
	settings.value("active", true).toBool() ? exportFieldsPoint.addField("active") : exportFieldsPoint.removeField("active");
	settings.value("extraInfos", true).toBool() ? exportFieldsPoint.addField("extraInfos") : exportFieldsPoint.removeField("extraInfos");
	settings.endGroup();
	// Params
	settings.beginGroup("Params");
	settings.value("precision", true).toBool() ? exportFieldsParams.addField("precision") : exportFieldsParams.removeField("precision");
	settings.value("coordsys", true).toBool() ? exportFieldsParams.addField("coordsys") : exportFieldsParams.removeField("coordsys");
	settings.value("extraInfos", true).toBool() ? exportFieldsParams.addField("extraInfos") : exportFieldsParams.removeField("extraInfos");
	settings.endGroup();
	// Frame
	settings.beginGroup("Frame");
	settings.value("name", true).toBool() ? exportFieldsFrame.addField("name") : exportFieldsFrame.removeField("name");
	settings.value("translation", true).toBool() ? exportFieldsFrame.addField("translation") : exportFieldsFrame.removeField("translation");
	settings.value("rotation", true).toBool() ? exportFieldsFrame.addField("rotation") : exportFieldsFrame.removeField("rotation");
	settings.value("scale", true).toBool() ? exportFieldsFrame.addField("scale") : exportFieldsFrame.removeField("scale");
	settings.value("isfreescale", true).toBool() ? exportFieldsFrame.addField("isfreescale") : exportFieldsFrame.removeField("isfreescale");
	settings.value("innerFrames", true).toBool() ? exportFieldsFrame.addField("innerFrames") : exportFieldsFrame.removeField("innerFrames");
	settings.value("points", true).toBool() ? exportFieldsFrame.addField("points") : exportFieldsFrame.removeField("points");
	settings.endGroup();
	// PointsList
	settings.beginGroup("PointsList");
	settings.value("title", true).toBool() ? exportFieldsPointsList.addField("title") : exportFieldsPointsList.removeField("title");
	settings.value("params", true).toBool() ? exportFieldsPointsList.addField("params") : exportFieldsPointsList.removeField("params");
	settings.value("rootFrame", true).toBool() ? exportFieldsPointsList.addField("rootFrame") : exportFieldsPointsList.removeField("rootFrame");
	settings.endGroup();

	settings.endGroup();
}

class PointsExportConfig::_PointsExportConfig_pimpl
{
public:
	std::unique_ptr<Ui::PointsExportConfig> ui;
};

const QString &PointsExportConfig::configName() noexcept
{
	return _configname;
}

PointsExportConfigObject *PointsExportConfig::showCopyDialog()
{
	QDialog dlg;
	QLabel *lbl = new QLabel(&dlg);
	lbl->setText("Perform a special copy - chosing export attributes in"
				 " the window will not overwrite application settings.");
	lbl->setWordWrap(true);
	QVBoxLayout *dlgLayout = new QVBoxLayout(&dlg);
	QDialogButtonBox *bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, &dlg);
	PointsExportConfig *tempExp = new PointsExportConfig(&dlg);
	PointsExportConfigObject *copyConfig = nullptr;

	connect(bb, &QDialogButtonBox::rejected, &dlg, &QDialog::reject);
	connect(bb, &QDialogButtonBox::accepted, &dlg, &QDialog::accept);
	connect(&dlg, &QDialog::accepted, [&tempExp, &copyConfig]() -> void { copyConfig = dynamic_cast<PointsExportConfigObject *>(tempExp->config()); });

	dlgLayout->addWidget(lbl);
	dlgLayout->addWidget(tempExp);
	dlgLayout->addWidget(bb);
	dlg.resize(400, 500);
	dlg.exec();

	return copyConfig;
}

PointsExportConfigObject *PointsExportConfig::getAppPointsConfig()
{
	PointsExportConfigObject *appConfig = nullptr;
	if (SettingsManager::settings().settingsHas(PointsExportConfig::configName()))
		appConfig = new PointsExportConfigObject(SettingsManager::settings().settings<PointsExportConfigObject>(PointsExportConfig::configName()));
	return appConfig;
}

PointsExportConfig::PointsExportConfig(QWidget *parent) : SConfigWidget(nullptr, parent), _pimpl(std::make_unique<_PointsExportConfig_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::PointsExportConfig>();
	_pimpl->ui->setupUi(this);
}

PointsExportConfig::~PointsExportConfig() = default;

SConfigObject *PointsExportConfig::config() const
{
	auto *config = new PointsExportConfigObject();

	// By default when PointsExportConfigObject is created it contains all the fields enabled (_allFields == _selectedFields)
	// Global
	config->exportPointsList = _pimpl->ui->exportPointsList->isChecked();
	// Positions
	_pimpl->ui->x->isChecked() ? config->exportFieldsPosition.addField("x") : config->exportFieldsPosition.removeField("x");
	_pimpl->ui->y->isChecked() ? config->exportFieldsPosition.addField("y") : config->exportFieldsPosition.removeField("y");
	_pimpl->ui->z->isChecked() ? config->exportFieldsPosition.addField("z") : config->exportFieldsPosition.removeField("z");
	_pimpl->ui->sigmax->isChecked() ? config->exportFieldsPosition.addField("sigmax") : config->exportFieldsPosition.removeField("sigmax");
	_pimpl->ui->sigmay->isChecked() ? config->exportFieldsPosition.addField("sigmay") : config->exportFieldsPosition.removeField("sigmay");
	_pimpl->ui->sigmaz->isChecked() ? config->exportFieldsPosition.addField("sigmaz") : config->exportFieldsPosition.removeField("sigmaz");
	_pimpl->ui->isfreex->isChecked() ? config->exportFieldsPosition.addField("isfreex") : config->exportFieldsPosition.removeField("isfreex");
	_pimpl->ui->isfreey->isChecked() ? config->exportFieldsPosition.addField("isfreey") : config->exportFieldsPosition.removeField("isfreey");
	_pimpl->ui->isfreez->isChecked() ? config->exportFieldsPosition.addField("isfreez") : config->exportFieldsPosition.removeField("isfreez");
	// Point
	_pimpl->ui->pointsName->isChecked() ? config->exportFieldsPoint.addField("name") : config->exportFieldsPoint.removeField("name");
	_pimpl->ui->position->isChecked() ? config->exportFieldsPoint.addField("position") : config->exportFieldsPoint.removeField("position");
	_pimpl->ui->inlineComment->isChecked() ? config->exportFieldsPoint.addField("inlineComment") : config->exportFieldsPoint.removeField("inlineComment");
	_pimpl->ui->headerComment->isChecked() ? config->exportFieldsPoint.addField("headerComment") : config->exportFieldsPoint.removeField("headerComment");
	_pimpl->ui->active->isChecked() ? config->exportFieldsPoint.addField("active") : config->exportFieldsPoint.removeField("active");
	_pimpl->ui->pointsExtraInformation->isChecked() ? config->exportFieldsPoint.addField("extraInfos") : config->exportFieldsPoint.removeField("extraInfos");
	// Params
	_pimpl->ui->coordinateSystem->isChecked() ? config->exportFieldsParams.addField("coordsys") : config->exportFieldsParams.removeField("coordsys");
	_pimpl->ui->extraInformation->isChecked() ? config->exportFieldsParams.addField("extraInfos") : config->exportFieldsParams.removeField("extraInfos");
	_pimpl->ui->precision->isChecked() ? config->exportFieldsParams.addField("precision") : config->exportFieldsParams.removeField("precision");
	// Frame
	_pimpl->ui->name->isChecked() ? config->exportFieldsFrame.addField("name") : config->exportFieldsFrame.removeField("name");
	_pimpl->ui->translation->isChecked() ? config->exportFieldsFrame.addField("translation") : config->exportFieldsFrame.removeField("translation");
	_pimpl->ui->rotation->isChecked() ? config->exportFieldsFrame.addField("rotation") : config->exportFieldsFrame.removeField("rotation");
	_pimpl->ui->scale->isChecked() ? config->exportFieldsFrame.addField("scale") : config->exportFieldsFrame.removeField("scale");
	_pimpl->ui->isfreescale->isChecked() ? config->exportFieldsFrame.addField("isfreescale") : config->exportFieldsFrame.removeField("isfreescale");
	_pimpl->ui->innerFrames->isChecked() ? config->exportFieldsFrame.addField("innerFrames") : config->exportFieldsFrame.removeField("innerFrames");
	_pimpl->ui->points->isChecked() ? config->exportFieldsFrame.addField("points") : config->exportFieldsFrame.removeField("points");
	// PointsList
	_pimpl->ui->title->isChecked() ? config->exportFieldsPointsList.addField("title") : config->exportFieldsPointsList.removeField("title");
	_pimpl->ui->params->isChecked() ? config->exportFieldsPointsList.addField("params") : config->exportFieldsPointsList.removeField("params");
	return config;
}

void PointsExportConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const PointsExportConfigObject *>(conf);
	if (!config)
		return;

	// Global
	_pimpl->ui->exportPointsList->setChecked(config->exportPointsList);
	// Positions
	_pimpl->ui->x->setChecked(config->exportFieldsPosition.hasField("x"));
	_pimpl->ui->y->setChecked(config->exportFieldsPosition.hasField("y"));
	_pimpl->ui->z->setChecked(config->exportFieldsPosition.hasField("z"));
	_pimpl->ui->sigmax->setChecked(config->exportFieldsPosition.hasField("sigmax"));
	_pimpl->ui->sigmay->setChecked(config->exportFieldsPosition.hasField("sigmay"));
	_pimpl->ui->sigmaz->setChecked(config->exportFieldsPosition.hasField("sigmaz"));
	_pimpl->ui->isfreex->setChecked(config->exportFieldsPosition.hasField("isfreex"));
	_pimpl->ui->isfreey->setChecked(config->exportFieldsPosition.hasField("isfreey"));
	_pimpl->ui->isfreez->setChecked(config->exportFieldsPosition.hasField("isfreez"));
	// Point
	_pimpl->ui->pointsName->setChecked(config->exportFieldsPoint.hasField("name"));
	_pimpl->ui->position->setChecked(config->exportFieldsPoint.hasField("position"));
	_pimpl->ui->inlineComment->setChecked(config->exportFieldsPoint.hasField("inlineComment"));
	_pimpl->ui->headerComment->setChecked(config->exportFieldsPoint.hasField("headerComment"));
	_pimpl->ui->active->setChecked(config->exportFieldsPoint.hasField("active"));
	_pimpl->ui->pointsExtraInformation->setChecked(config->exportFieldsPoint.hasField("extraInfos"));
	// Params
	_pimpl->ui->coordinateSystem->setChecked(config->exportFieldsParams.hasField("coordsys"));
	_pimpl->ui->extraInformation->setChecked(config->exportFieldsParams.hasField("extraInfos"));
	_pimpl->ui->precision->setChecked(config->exportFieldsParams.hasField("precision"));
	// Frame
	_pimpl->ui->name->setChecked(config->exportFieldsFrame.hasField("name"));
	_pimpl->ui->translation->setChecked(config->exportFieldsFrame.hasField("translation"));
	_pimpl->ui->rotation->setChecked(config->exportFieldsFrame.hasField("rotation"));
	_pimpl->ui->scale->setChecked(config->exportFieldsFrame.hasField("scale"));
	_pimpl->ui->isfreescale->setChecked(config->exportFieldsFrame.hasField("isfreescale"));
	_pimpl->ui->innerFrames->setChecked(config->exportFieldsFrame.hasField("innerFrames"));
	_pimpl->ui->points->setChecked(config->exportFieldsFrame.hasField("points"));
	// PointsList
	_pimpl->ui->title->setChecked(config->exportFieldsPointsList.hasField("title"));
	_pimpl->ui->params->setChecked(config->exportFieldsPointsList.hasField("params"));
}

void PointsExportConfig::reset()
{
	PointsExportConfigObject config;
	config.read();
	setConfig(&config);
}

void PointsExportConfig::restoreDefaults()
{
	PointsExportConfigObject config;
	setConfig(&config);
}
