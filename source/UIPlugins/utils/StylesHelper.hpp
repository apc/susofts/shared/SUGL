/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef STYLESHELPER_HPP
#define STYLESHELPER_HPP

#include "PluginsGlobal.hpp"

class QColor;

/**
* Given the input light color meant for the light theme, this method returns the same color for light theme and slightly darker version for dark theme.
* 
* This method should only be called after QApplication has been initialized. Otherwise default values (light mode) will be returned back.
**/
SUGL_SHARED_EXPORT QColor getThemeAdaptedColor(const QColor &color);

/**
 * Given the input light color meant for the light theme, this method returns the same color for light theme and inverse color for dark theme.
 *
 * This method should only be called after QApplication has been initialized. Otherwise default values (light mode) will be returned back.
 **/
SUGL_SHARED_EXPORT QColor getThemeColor(const QColor &color);

#endif // STYLESHELPER_HPP
