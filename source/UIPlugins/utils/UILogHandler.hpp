/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef UILOGHANDLER_HPP
#define UILOGHANDLER_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"

namespace Ui
{
class UILogHandler;
}

/**
 * UI Log handler that prints the logs to the QTableWidget using inner class _UILogger which automatitally registers itself when created, see registerLogger().
 */
class SUGL_SHARED_EXPORT UILogHandler : public QWidget
{
	Q_OBJECT

public:
	UILogHandler(QWidget *parent = nullptr);
	virtual ~UILogHandler() override;

public slots:
	void registerLogger();
	void unregisterLogger();
	void clearLogs();
	void updateUi(const QString &key);
	void filterLogs(int id);
	void registerQt();
	void unregisterQt();

private:
	/**
	 * _UILogger class prints the message to the QTableWidget using log() method. Moreover, when enabled in UILogHandlerConfig settings it can also handle Qt log messages.
	 */
	class _UILogger;
	_UILogger *_uiLog = nullptr;
	std::unique_ptr<Ui::UILogHandler> ui;
};

#endif // UILOGHANDLER_HPP
