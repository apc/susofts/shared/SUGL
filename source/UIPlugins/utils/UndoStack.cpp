#include "UndoStack.hpp"

#include "IUndoCommand.hpp"

class UndoStack::_UndoStack_pimpl
{
public:
	/** Stack (a vector) containing all IUndoCommand. */
	std::vector<std::unique_ptr<IUndoCommand>> stack;
	/** Current stack index, i.e. currently selected IUndoCommand. */
	size_t index = 0;
	/** Reserved space on stack for the number of initial state IUndoCommand. */
	size_t initialSize = 0;
};

UndoStack::UndoStack() : _pimpl(std::make_unique<_UndoStack_pimpl>())
{
}

UndoStack::~UndoStack() = default;

void UndoStack::undo()
{
	if (!isUndoAvailable())
		return;
	auto currType = typeid(*_pimpl->stack[_pimpl->index]).hash_code();
	_pimpl->index--;
	// If commands are of different type
	if (currType != typeid(*_pimpl->stack[_pimpl->index]).hash_code())
	{
		// Consider only earlier elements on the stack
		auto latestCommand = std::find_if(_pimpl->stack.rbegin() + (_pimpl->stack.size() - 1 - _pimpl->index), _pimpl->stack.rend(),
			[currType](const auto &command) { return typeid(*command).hash_code() == currType; });
		latestCommand->get()->restoreState();
	}
	// If commands are of the same type
	else
		_pimpl->stack[_pimpl->index]->restoreState();
	emit hasChanged(isUndoAvailable(), isRedoAvailable());
}

void UndoStack::redo()
{
	if (!isRedoAvailable())
		return;
	_pimpl->index++;
	_pimpl->stack[_pimpl->index]->restoreState();
	emit hasChanged(isUndoAvailable(), isRedoAvailable());
}

void UndoStack::push(IUndoCommand *command)
{
	if (_pimpl->index < _pimpl->stack.size() - 1)
		_pimpl->stack.resize(_pimpl->index + 1);
	_pimpl->index++;
	_pimpl->stack.emplace_back(command);
	emit hasChanged(isUndoAvailable(), isRedoAvailable());
}

void UndoStack::reset(const std::vector<IUndoCommand *> &initialCommands)
{
	_pimpl->stack.clear();
	_pimpl->stack.reserve(initialCommands.size() * 2);
	for (auto &command : initialCommands)
		_pimpl->stack.emplace_back(command);
	_pimpl->initialSize = _pimpl->stack.size();
	_pimpl->index = _pimpl->initialSize - 1;
	emit hasChanged(isUndoAvailable(), isRedoAvailable());
}

bool UndoStack::isUndoAvailable()
{
	return _pimpl->index > 0 && _pimpl->index >= _pimpl->initialSize;
}

bool UndoStack::isRedoAvailable()
{
	return (_pimpl->index >= _pimpl->initialSize - 1) && (_pimpl->index < _pimpl->stack.size() - 1);
}
