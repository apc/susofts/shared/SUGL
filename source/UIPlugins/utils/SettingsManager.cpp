#include "SettingsManager.hpp"

#include <unordered_map>

#include <QCoreApplication>
#include <QSettings>

#include <Logger.hpp>

#include "QtStreams.hpp"
#include "interface/SConfigInterface.hpp"

class SettingsManager::_SettingsManager_pimpl
{
public:
	/**
	 * Holds settings from the application.
	 *
	 * Default settings can be retrived with the "default" key.
	 */
	std::unordered_map<std::string, std::unique_ptr<SConfigObject>> settings;
	/**
	 * Holds information about the application that **are not** written by the QSettings.
	 *
	 * It starts with the following information:
	 * - `appName`: the application name (given in the init() method)
	 * - `appVersion`: the application version (given in the init() method)
	 * - `orgName`: the organization name (given in the init() method)
	 * - `orgDomain`: the organization domain (given in the init() method)
	 * - `settingsFile`: the path to the file where are written the QSettings
	 */
	std::unordered_map<std::string, std::string> innerSettings;
	/** tell if the settings are written on destroy */
	bool writeOnQuit = true;
};

SettingsManager &SettingsManager::init(const QString &appName, const QString &appVersion, const QString &orgName, const QString &orgDomain)
{
	QCoreApplication::setApplicationName(appName);
	QCoreApplication::setApplicationVersion(appVersion);
	QCoreApplication::setOrganizationName(orgName);
	QCoreApplication::setOrganizationDomain(orgDomain);

	QSettings::setDefaultFormat(QSettings::IniFormat);
	QSettings qsettings;

	auto &s = SettingsManager::settings();
	s.innerSettings("appName", appName);
	s.innerSettings("appVersion", appVersion);
	s.innerSettings("orgName", orgName);
	s.innerSettings("orgDomain", orgDomain);
	s.innerSettings("settingsFile", qsettings.fileName());
	s.innerSettings("currentDirPath", qsettings.value("currentDirPath", "").toString());

	return s;
}

SettingsManager &SettingsManager::settings()
{
	static std::unique_ptr<SettingsManager> instance;
	if (!instance)
	{
		instance.reset(new SettingsManager());

		instance->_pimpl->innerSettings["appName"] = QCoreApplication::applicationName().toStdString();
		instance->_pimpl->innerSettings["appVersion"] = QCoreApplication::applicationVersion().toStdString();
		instance->_pimpl->innerSettings["orgName"] = QCoreApplication::organizationName().toStdString();
		instance->_pimpl->innerSettings["orgDomain"] = QCoreApplication::organizationDomain().toStdString();
		instance->_pimpl->innerSettings["settingsFile"] = QSettings().fileName().toStdString();
	}
	return *instance;
}

SettingsManager::SettingsManager(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_SettingsManager_pimpl>())
{
}

SettingsManager::~SettingsManager() = default;

void SettingsManager::settings(const QString &key, SConfigObject *value)
{
	auto &setting = _pimpl->settings[key.toStdString()];
	if (setting.get() && (*setting.get() == *value)) // if setting did not change
		return;
	setting.reset(value);
	emit settingsChanged(key);
}

bool SettingsManager::settingsHas(const QString &key) const
{
	return _pimpl->settings.find(key.toStdString()) != std::end(_pimpl->settings);
}

void SettingsManager::settingsRemove(const QString &key)
{
	_pimpl->settings.erase(key.toStdString());
	emit settingsChanged(key);
}

void SettingsManager::shouldWriteSettingsOnQuit(bool write) noexcept
{
	_pimpl->writeOnQuit = write;
}

bool SettingsManager::shouldWriteSettingsOnQuit() const noexcept
{
	return _pimpl->writeOnQuit;
}

void SettingsManager::writeToConfigFile()
{
	for (const auto &[key, settings] : _pimpl->settings)
		settings->write();
	QSettings().setValue("currentDirPath", SettingsManager::settings().innerSettings("currentDirPath"));
	logDebug() << "SettingsManager: Write settings to" << QSettings().fileName();
}

void SettingsManager::innerSettings(const QString &key, const QString &value)
{
	_pimpl->innerSettings[key.toStdString()] = value.toStdString();
	emit innerSettingsChanged(key);
}

bool SettingsManager::innerSettingsHas(const QString &key) const
{
	return _pimpl->innerSettings.find(key.toStdString()) != std::end(_pimpl->innerSettings);
}

QString SettingsManager::innerSettings(const QString &key) const
{
	return QString::fromStdString(_pimpl->innerSettings.at(key.toStdString()));
}

void SettingsManager::innerSettingsRemove(const QString &key)
{
	_pimpl->innerSettings.erase(key.toStdString());
	emit innerSettingsChanged(key);
}

const SConfigObject *SettingsManager::getSettings(const QString &key) const
{
	if (!settingsHas(key))
		return nullptr;
	return _pimpl->settings.at(key.toStdString()).get();
}
