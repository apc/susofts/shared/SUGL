#include "ProjectManager.hpp"

#include <algorithm>

#include <QDir>
#include <QMdiArea>
#include <QRegularExpression>

#include <Logger.hpp>

#include "MdiSubWindow.hpp"
#include "QtStreams.hpp"
#include "TreeMdiManager.hpp"
#include "interface/SGraphicalInterface.hpp"
#include "interface/SPluginInterface.hpp"
#include "interface/SPluginLoader.hpp"

// Methods below are copies of methods from Qt version 5.12, where they were first introduced.
// They are included here as currently used CI with Linux is using Qt with version lower than 5.12 were these methods are unavailable.
// As soon as the newer version of Qt (version >= 5.12) will be provided with Linux CI, code below should be deleted and in method `openFromPath`
// `wildcardToRegularExpression` should be changed to `QRegularExpression::wildcardToRegularExpression`.
namespace
{
#if (QT_VERSION < QT_VERSION_CHECK(5, 12, 0))
/**
 * Copy of the QRegularExpression::anchoredPattern method from Qt 5.12.
 * https://code.woboq.org/qt5/qtbase/src/corelib/text/qregularexpression.h.html#142
 */
QString anchoredPattern(const QString &expression)
{
	return QLatin1String("\\A(?:") + expression + QLatin1String(")\\z");
}

/**
 * Copy of the QRegularExpression::wildcardToRegularExpression method from Qt 5.12 that returns a regular expression representation of the given glob pattern.
 * https://code.woboq.org/qt5/qtbase/src/corelib/text/qregularexpression.cpp.html#1899
 */
QString _wildcardToRegularExpression(const QString &pattern)
{
	const int wclen = pattern.length();
	QString rx;
	rx.reserve(wclen + wclen / 16);
	int i = 0;
	const QChar *wc = pattern.unicode();
#	ifdef Q_OS_WIN
	const QLatin1Char nativePathSeparator('\\');
	const QLatin1String starEscape("[^/\\\\]*");
	const QLatin1String questionMarkEscape("[^/\\\\]");
#	else
	const QLatin1Char nativePathSeparator('/');
	const QLatin1String starEscape("[^/]*");
	const QLatin1String questionMarkEscape("[^/]");
#	endif
	while (i < wclen)
	{
		const QChar c = wc[i++];
		switch (c.unicode())
		{
		case '*':
			rx += starEscape;
			break;
		case '?':
			rx += questionMarkEscape;
			break;
		case '\\':
#	ifdef Q_OS_WIN
		case '/':
			rx += QLatin1String("[/\\\\]");
			break;
#	endif
		case '$':
		case '(':
		case ')':
		case '+':
		case '.':
		case '^':
		case '{':
		case '|':
		case '}':
			rx += QLatin1Char('\\');
			rx += c;
			break;
		case '[':
			rx += c;
			// Support for the [!abc] or [!a-c] syntax
			if (i < wclen)
			{
				if (wc[i] == QLatin1Char('!'))
				{
					rx += QLatin1Char('^');
					++i;
				}
				if (i < wclen && wc[i] == QLatin1Char(']'))
					rx += wc[i++];
				while (i < wclen && wc[i] != QLatin1Char(']'))
				{
					// The '/' appearing in a character class invalidates the
					// regular expression parsing. It also concerns '\\' on
					// Windows OS types.
					if (wc[i] == QLatin1Char('/') || wc[i] == nativePathSeparator)
						return rx;
					if (wc[i] == QLatin1Char('\\'))
						rx += QLatin1Char('\\');
					rx += wc[i++];
				}
			}
			break;
		default:
			rx += c;
			break;
		}
	}
	return anchoredPattern(rx);
}
auto wildcardToRegularExpression = &_wildcardToRegularExpression;
#else
auto wildcardToRegularExpression = qOverload<const QString &>(&QRegularExpression::wildcardToRegularExpression);
#endif
} // namespace

class ProjectManager::_ProjectManager_pimpl
{
public:
	/** QMdiArea used by the Project Manager singleton displaying all the projects. */
	QMdiArea *mdiArea = nullptr;
	/** TreeMdiManager used by the Project Manager singleton showing projects in QMdiArea. */
	TreeMdiManager *projectTree = nullptr;
};

ProjectManager &ProjectManager::getProjectManager()
{
	static ProjectManager _instance;
	return _instance;
}

ProjectManager::ProjectManager(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_ProjectManager_pimpl>())
{
}

ProjectManager::~ProjectManager() = default;

QMdiArea *ProjectManager::getMdiArea()
{
	if (!_pimpl->mdiArea)
	{
		_pimpl->mdiArea = new QMdiArea();
		if (_pimpl->projectTree)
			_pimpl->projectTree->mdiArea(_pimpl->mdiArea);
	}
	return _pimpl->mdiArea;
}

TreeMdiManager *ProjectManager::getProjectTree()
{
	if (!_pimpl->projectTree)
	{
		_pimpl->projectTree = new TreeMdiManager();
		_pimpl->projectTree->mdiArea(_pimpl->mdiArea);
	}
	return _pimpl->projectTree;
}

bool ProjectManager::isModified() const noexcept
{
	if (!_pimpl->mdiArea)
		return false;
	auto windowList = _pimpl->mdiArea->subWindowList();
	return std::any_of(std::cbegin(windowList), windowList.cend(), [](const QMdiSubWindow *win) -> bool {
		auto *widget = qobject_cast<SGraphicalWidget *>(win->widget());
		if (widget)
			return widget->isModified();
		return false;
	});
}

std::vector<SGraphicalWidget *> ProjectManager::getProjects()
{
	if (!_pimpl->mdiArea)
		return std::vector<SGraphicalWidget *>();
	std::vector<SGraphicalWidget *> projects;
	projects.reserve(_pimpl->mdiArea->subWindowList().size());
	for (auto &win : _pimpl->mdiArea->subWindowList())
	{
		auto window = qobject_cast<MdiSubWindow *>(win);
		if (!window)
			continue;
		auto *widget = window->graphicalWidget();
		if (widget)
			projects.push_back(widget);
	}
	return projects;
}

std::vector<const SGraphicalWidget *> ProjectManager::getProjects() const
{
	if (!_pimpl->mdiArea)
		return std::vector<const SGraphicalWidget *>();
	std::vector<const SGraphicalWidget *> projects;
	projects.reserve(_pimpl->mdiArea->subWindowList().length());
	for (auto &win : _pimpl->mdiArea->subWindowList())
	{
		auto window = qobject_cast<const MdiSubWindow *>(win);
		if (!window)
			continue;
		auto *widget = window->graphicalWidget();
		if (widget)
			projects.push_back(widget);
	}
	return projects;
}

SGraphicalWidget *ProjectManager::findProject(const QString &path)
{
	if (!_pimpl->mdiArea)
		return nullptr;
	auto windowList = _pimpl->mdiArea->subWindowList();
	auto win = std::find_if(windowList.begin(), windowList.end(), [&path](QMdiSubWindow *window) -> bool {
		auto *widget = qobject_cast<SGraphicalWidget *>(window->widget());
		if (widget)
			return widget->getPath() == QDir::fromNativeSeparators(path);
		return false;
	});
	if (win != windowList.end())
		return qobject_cast<SGraphicalWidget *>((*win)->widget());
	return nullptr;
}

QMdiSubWindow *ProjectManager::findMdiWindow(const SGraphicalWidget *widget)
{
	if (!_pimpl->mdiArea || !widget)
		return nullptr;
	auto list = _pimpl->mdiArea->subWindowList();
	auto it = std::find_if(
		std::cbegin(list), std::cend(list), [&widget](const QMdiSubWindow *m) -> bool { return widget == qobject_cast<SGraphicalWidget *>(m->widget()); });

	return it != std::end(list) ? *it : nullptr;
}

QMdiSubWindow *ProjectManager::findMdiWindow(const SPluginInterface *plugin)
{
	if (!_pimpl->mdiArea || !plugin)
		return nullptr;
	auto list = _pimpl->mdiArea->subWindowList();
	auto it = std::find_if(std::cbegin(list), std::cend(list), [&plugin](const QMdiSubWindow *m) -> bool {
		auto *wdg = qobject_cast<SGraphicalWidget *>(m->widget());
		if (wdg)
			return plugin == wdg->owner();
		return false;
	});

	return it != std::end(list) ? *it : nullptr;
}

QString ProjectManager::getFileExtensions(SPluginInterface *plugin) const
{
	QString filter;
	const std::vector<SPluginInterface *> &plugins = plugin ? std::vector<SPluginInterface *>{plugin} : SPluginLoader::getPluginLoader().plugins();
	for (auto &plugIter : plugins) // For all the plugins
	{
		if (!plugIter->getExtensions().isEmpty()) // If saveable/openable plugin
			filter += filter.isEmpty() ? plugIter->getExtensions() : ";;" + plugIter->getExtensions();
	}

	auto filterList = filter.split(";;");
	// Remove all (*) filters
	if (filter.contains("(*)"))
	{
		auto allList = filterList.filter("(*)");
		for (const QString &l : allList)
			filterList.removeAll(l);
	}
	// Sort
	std::sort(filterList.begin(), filterList.end());
	filter = filterList.join(";;");

	// Add (*) filter at proper position
	filter = plugin ? filter + ";;Any files(*)" : "Any files(*);;" + filter;

	return filter;
}

SPluginInterface *ProjectManager::findPluginForFile(const QString &filepath, bool matchGeneral) const
{
	if (filepath.isEmpty())
		return nullptr;

	// file extension matching
	SPluginInterface *foundPlugin = nullptr;
	QString fileName = QFileInfo(filepath).fileName();
	QString usedFilter;
	// RegExp pattern used by Qt in QPlatformFileDialogHelper::filterRegExp to parse the file extension filters
	// https://code.woboq.org/qt5/qtbase/src/gui/kernel/qplatformdialoghelper.cpp.html#785
	const QString filterPattern = R"(^(.*)\(([a-zA-Z0-9_.,*? +;#\-\[\]@\{\}\/!<>\$%&=^~:\|]*)\)$)";
	QRegularExpression filterRegExp(filterPattern);
	QRegularExpression::PatternOptions options;
#ifdef Q_OS_WIN
	options = QRegularExpression::CaseInsensitiveOption;
#endif
	for (auto &plugin : SPluginLoader::getPluginLoader().plugins()) // each plugin
	{
		auto filters = plugin->getExtensions().split(";;");
		for (QString &filter : filters) // each filter category
		{
			if (!matchGeneral && filter.contains("(*)"))
				continue;

			QRegularExpressionMatch filterMatch = filterRegExp.match(filter.simplified());
			if (!filterMatch.hasMatch())
				continue;
			auto extensions = filterMatch.captured(2).split(' '); // Only the third capture is of interest (there is a full match + two capture groups)
			for (auto &extension : extensions) // each filter extension
			{
				// The method QRegularExpression::wildcardToRegularExpression is only available from Qt 5.12 and higher
				// When all CI will use Qt with version equal or higher than 5.12 this line should be changed to use QRegularExpression::wildcardToRegularExpression
				QRegularExpression rx(wildcardToRegularExpression(extension), options);
				QRegularExpressionMatch match = rx.match(fileName);
				if (match.hasMatch() && usedFilter.length() < extension.length())
				{
					foundPlugin = plugin;
					usedFilter = extension;
				}
			}
		}
	};

	return foundPlugin;
}

void ProjectManager::reset()
{
	_pimpl->projectTree = nullptr;
	_pimpl->mdiArea = nullptr;
}

bool ProjectManager::saveProject(const QString path, SGraphicalWidget *project)
{
	if (!project || path.isEmpty())
		return false;

	return project->save(path);
}

void ProjectManager::newProject(SPluginInterface *plugin)
{
	if (!_pimpl->mdiArea || !plugin)
		return;

	// Run external software
	if (!plugin->hasGraphicalInterface() && plugin->hasLaunchInterface())
		return runProject(plugin->launchInterface(), "new project");

	auto *widget = plugin->graphicalInterface();
	if (!widget)
	{
		logCritical() << tr("No graphical interface is provided by the plugin.");
		return;
	}

	widget->newEmpty();
	createSubWindow(widget);
}

void ProjectManager::openProject(const QString path, SPluginInterface *plugin)
{
	const QString standardPath = QDir::fromNativeSeparators(path);
	if (!_pimpl->mdiArea || !plugin || standardPath.isEmpty())
		return;

	// Check file size
	if (QFile(standardPath).size() >= (qint64)536870912) // if bigger than 512 MB
	{
		logCritical() << tr("The file ") << standardPath << tr(" is too big! Its size is over 512 MB.");
		return;
	}

	// Run external software
	if (!plugin->hasGraphicalInterface() && plugin->hasLaunchInterface())
		return runProject(plugin->launchInterface(), standardPath);

	// Check if project is already open
	auto project = findProject(standardPath);
	if (project)
	{
		logInfo() << tr("Project ") << project->windowTitle() << tr(" is already open. ");
		auto window = findMdiWindow(project);
		_pimpl->mdiArea->setActiveSubWindow(window);
		return;
	}

	// Create graphical widget
	auto *widget = plugin->graphicalInterface();
	if (!widget)
	{
		logCritical() << tr("No graphical interface is provided by the plugin.");
		return;
	}

	// Open project
	if (!widget->open(standardPath))
	{
		logCritical() << tr("Cannot open the project.");
		delete widget;
		return;
	}
	createSubWindow(widget);
}

void ProjectManager::openFromPath(const QString &path)
{
	if (path.isEmpty())
		return;
	auto nativePath = QDir::fromNativeSeparators(path);
	// Find fitting plugin
	auto foundPlugin = findPluginForFile(nativePath);
	if (foundPlugin)
		openProject(nativePath, foundPlugin);
	else
		logCritical() << tr("No suitable plugin to open the file has been found.");
}

bool ProjectManager::closeProject(QMdiSubWindow *sub)
{
	if (!sub && _pimpl->mdiArea)
		sub = _pimpl->mdiArea->currentSubWindow();
	if (!_pimpl->mdiArea || !sub)
		return false;

	auto *window = qobject_cast<MdiSubWindow *>(sub);
	// not managed window
	if (!window)
		return true;
	// cancel during saving
	if ((!sender() || sender() != sub) && !window->askToSave())
		return false;

	bool maximized = (_pimpl->mdiArea->viewMode() == QMdiArea::ViewMode::TabbedView || sub->isMaximized());
	_pimpl->mdiArea->removeSubWindow(sub);
	if (!sender() || sender() != sub)
		sub->deleteLater();
	_pimpl->projectTree->updateTree(_pimpl->mdiArea->currentSubWindow());

	// fix window show in small
	if (maximized && _pimpl->mdiArea->currentSubWindow())
		_pimpl->mdiArea->currentSubWindow()->showMaximized();
	else if (_pimpl->mdiArea->currentSubWindow())
		_pimpl->mdiArea->currentSubWindow()->show();

	emit hasChanged();
	return true;
}

void ProjectManager::runProject(SLaunchObject *launcher, const QString &path)
{
	if (!launcher || path.isEmpty())
		return;
	try
	{
		launcher->launch(path);
	}
	catch (const std::exception &e)
	{
		logCritical() << "Error while running:" << e.what();
	}
}

SGraphicalWidget *ProjectManager::getCurrentWidget()
{
	if (!_pimpl->mdiArea)
		return nullptr;
	auto *sub = qobject_cast<MdiSubWindow *>(_pimpl->mdiArea->currentSubWindow());
	if (sub)
		return sub->graphicalWidget();
	return nullptr;
}

QMdiSubWindow *ProjectManager::createSubWindow(SGraphicalWidget *widget)
{
	if (!_pimpl->mdiArea)
	{
		delete widget;
		return nullptr;
	}
	bool maximized = _pimpl->mdiArea->currentSubWindow() ? _pimpl->mdiArea->currentSubWindow()->isMaximized() : true;
	// create the sub window
	auto *sub = new MdiSubWindow(_pimpl->mdiArea);
	sub->setGraphicalWidget(widget); // MdiSubWindow takes TEMPORARY ownership over widget and on delete the widget is reparented to root widget
	sub->setAttribute(Qt::WA_DeleteOnClose);
	connect(widget, &SGraphicalWidget::hasChanged, this, &ProjectManager::hasChanged);
	connect(sub, &MdiSubWindow::closed, this, qOverload<QMdiSubWindow *>(&ProjectManager::closeProject));
	_pimpl->mdiArea->addSubWindow(sub); // mdiArea takes ownership over sub (MdiSubWindow)
	// show it
	if (maximized)
		sub->showMaximized();
	else
		sub->show();
	// ensure visibility of mdiArea tabs on window creation
	_pimpl->mdiArea->resize(_pimpl->mdiArea->size() - QSize(0, 1));

	emit hasChanged(); // It is required to update the status of MainWindow's windowTitle (show or hide the `content modified` asterisk)
	return sub;
}
