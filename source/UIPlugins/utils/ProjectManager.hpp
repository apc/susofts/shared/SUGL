/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROJECTMANAGER_HPP
#define PROJECTMANAGER_HPP

#include <memory>

#include <QObject>

#include "PluginsGlobal.hpp"

class QMdiSubWindow;
class QMdiArea;
class TreeMdiManager;
class SPluginInterface;
class SGraphicalWidget;
class SLaunchObject;

/**
 * Singleton responsible for managing all the projects.
 *
 * This singleton class allows controlling all the projects (create, open, save and run them) in one place.
 * This class creates and uses some widgets heavily (QMdiArea and TreeMdiManager) but it does not take ownership over them. Whenever these widgets are created when first
 * accessed by calling either getMdiArea or getProjectTree their ownership should be transferred to some other QWidget.  Morever, ownership over created MdiSubWindow is
 * assigned to QMdiArea.
 */
class SUGL_SHARED_EXPORT ProjectManager : public QObject
{
	Q_OBJECT

public:
	/** @return the instance of the ProjectManager (singleton). */
	static ProjectManager &getProjectManager();

	~ProjectManager() override;

	// A singleton class
	ProjectManager(const ProjectManager &) = delete;
	ProjectManager(ProjectManager &&) = delete;
	ProjectManager &operator=(const ProjectManager &) = delete;
	ProjectManager &operator=(ProjectManager &&) = delete;

	/** Returns QMdiArea containing all SGraphicalWidget projects. */
	QMdiArea *getMdiArea();
	/** Returns TreeMdiManager QWidget listing all SGraphicalWidget projects in a tree. */
	TreeMdiManager *getProjectTree();
	/** Returns currently displayed SGraphicalWidget in QMdiArea. */
	SGraphicalWidget *getCurrentWidget();

	/**
	 * Checks if any of the projects was modified.
	 *
	 * @return true if at least one project was modified, false otherwise.
	 */
	bool isModified() const noexcept;
	/** Returns list of projects present in QMdiArea. */
	std::vector<SGraphicalWidget *> getProjects();
	/** Returns list of projects present in QMdiArea. */
	std::vector<const SGraphicalWidget *> getProjects() const;
	/** Searches for project with the given path. Returns nullptr if no project has been found. */
	SGraphicalWidget *findProject(const QString &path);
	/** Finds a QMdiSubWindow based on a contained project. Returns nullptr if no window has been found. */
	QMdiSubWindow *findMdiWindow(const SGraphicalWidget *widget);
	/** Finds a QMdiSubWindow based on the owning plugin. Returns nullptr if no window has been found. Meant to search projects of `monoInstance` plugins. */
	QMdiSubWindow *findMdiWindow(const SPluginInterface *plugin);
	/**
	 * Returns applicable file extensions as a filter.
	 * - If plugin parameter is provided only its extension and (*) extension will be included.
	 * - If no plugin was provided, iterate through all the plugins and creates a sorted filter out of all their supported extensions.
	 *
	 * @param plugin an optional plugin if the required filter should be specialized.
	 * @return string with the appropriate filter extensions.
	 */
	QString getFileExtensions(SPluginInterface *plugin = nullptr) const;
	/**
	* @return plugin that is able to open the filepath. The priority is given to the plugin that directly matches the file extensions with its supported extensions.
	* matchGeneral option regulates if `*` all filter should be used in the match.
	*/
	SPluginInterface *findPluginForFile(const QString &filepath, bool matchGeneral = true) const;

	// Reset the ProjectManager to default state where it does not own any TreeMdiManager or QMdiArea.
	void reset();

public slots:
	// Project handling
	/** Save a project to the path. */
	bool saveProject(const QString path, SGraphicalWidget *project);
	/** Create a new project. */
	void newProject(SPluginInterface *plugin);
	/** Open a project from the path. */
	void openProject(const QString path, SPluginInterface *plugin);
	/** Open project by providing a path to it. If there is a plugin that handles given file extension - the project will be opened with this plugin. */
	void openFromPath(const QString &path);
	/** Close a project. Use current window if no window was provided. */
	bool closeProject(QMdiSubWindow *sub = nullptr);
	/**  Run a project. */
	void runProject(SLaunchObject *launcher, const QString &path);

signals:
	/** Forwards the hasChanged signal of all SGraphicalWidgets to MainWindow. */
	void hasChanged();

private:
	/** Private constructor for a singleton. */
	ProjectManager(QObject *parent = nullptr);

	/** Returns a new MdiSubWindow - which is added to QMdiArea and sets SGraphicalWidget as its internal widget. */
	QMdiSubWindow *createSubWindow(SGraphicalWidget *widget);

private:
	/** pimpl */
	class _ProjectManager_pimpl;
	std::unique_ptr<_ProjectManager_pimpl> _pimpl;
};

#endif // ProjectManager_HPP
