/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/


#ifndef SCRIPTRUNNERDIALOG_HPP
#define SCRIPTRUNNERDIALOG_HPP

#include <memory>

#include <QDialog>

namespace Ui
{
class ScriptRunnerDialog;
}

struct ExternalScript;

/**
 * A dialog controlling and showing progress of an external script.
 *
 * This is a self-contained runner that starts its process itself. The dialog allows modifying of the input file path and optional command line arguments.
 * All the statuses of the process are reflected in the dialog.
 *
 * After the run @ScriptRunnerDialog::getOutput method can be used to access the QProcess output that should be later processed by @SScriptHandler::executeActions.
 */
class ScriptRunnerDialog : public QDialog
{
	Q_OBJECT

public:
	ScriptRunnerDialog(const ExternalScript &script, const QString &inputPath, QWidget *parent = nullptr);
	~ScriptRunnerDialog() override;

	/** Get the standard output from the process to be used by @SScriptHandler::executeActions */
	QString getOutput();

public slots:
	/** Slot called when SLaunchObject is started. */
	void jobStarted();
	/** Slot called when new message from the process is received, it places the message in the 'output' section of the dialog. */
	void jobFinished(bool result);

protected:
	void closeEvent(QCloseEvent *e) override;

protected slots:
	/** Adapt the UI to changes */
	void updateUi();
	/** Select a new input file */
	void selectFile();
	/** Starts the process */
	void startJob();
	/** Stops the process */
	void cancelJob();

private:
	/** Generate a map {pluginName : exePath} */
	QMap<QString, QString> generateExePathsMap();
	/** Process GUI elements and create a @QStringList with all the arguments to be used by @QProcess */
	QStringList prepareArguments();

private:
	/** pimpl */
	class _ScriptRunnerDialog_pimpl;
	std::unique_ptr<_ScriptRunnerDialog_pimpl> _pimpl;
};

#endif // SCRIPTRUNNERDIALOG_HPP
