/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TREEMDIMANAGER_HPP
#define TREEMDIMANAGER_HPP

#include <memory>

#include <QTreeWidget>

#include "PluginsGlobal.hpp"

class QMdiArea;
class QMdiSubWindow;

class SPluginInterface;

/**
 * Customized Tree Widget.
 *
 * If you give a QMdiArea to this widget, you will be able to control it through this widget. All the projects are grouped into the respective nodes which can be expanded
 * or minimized. Main project nodes cannot be selected.
 *
 * It allows you to see the tree of all QMdiSubWindow open in the MDI area, change the current one...
 *
 * - When an item is clicked, the corresponding QMdiSubWindow will be set as active window.
 * - When an item is double clicked, the corresponding QMdiSubWindow will be either maximized or reduced, if the MDI is NOT in tab mode.
 * - When an item is clicked with mouse wheel the project will be closed.
 * - When the close icon next to the item is clicked the project will be closed.
 */
class SUGL_SHARED_EXPORT TreeMdiManager : public QTreeWidget
{
	Q_OBJECT

public:
	TreeMdiManager(QWidget *parent = nullptr);
	virtual ~TreeMdiManager() override;

	/** Sets the QMdiArea that should be used by TreeMdiManager. */
	void mdiArea(QMdiArea *mdiarea);
	/** Returns currently used QMdiArea. */
	const QMdiArea *mdiArea() const noexcept;
	/** Returns currently used QMdiArea. */
	QMdiArea *mdiArea() noexcept;
	/** Displays context menu with suitable actions depending on which part of the tree was right clicked. */
	void contextMenuEvent(QContextMenuEvent *event) override;
	/** Returns QMdiSubWindow associated with the given QTreeWidgetItem. */
	QMdiSubWindow *getMdiSubWindow(QTreeWidgetItem *item);

public slots:
	/** Updates a tree structure based on current projects.
	 *
	 * @param window is pased when the window is meant to be selected inside the TreeMdiManager.
	 */
	void updateTree(QMdiSubWindow *window = nullptr);
	/** Highlights the correspondent tree item of the selected QMdiSubWindow*/
	void highlightTreeItem(QMdiSubWindow *window);

signals:
	/** Signal requesting creation of a new project. */
	void newProject(SPluginInterface *plugin = nullptr);
	/** Signal requesting opening of a project. */
	void openProject(SPluginInterface *plugin = nullptr);
	/** Signal notifying that the given project should be closed. */
	void closeProject(QMdiSubWindow *window);
	/** Signal notifying that all plugins under given node should be runned. */
	void runAllProjects(SPluginInterface *plugin);

protected:
	void mousePressEvent(QMouseEvent *event) override;

private:
	/**
	 * Create a QTreeWidgetItem with provided project name.
	 *
	 * @param projectName name of the project.
	 * @return QTreeWidgetItem without assigned parent.
	 */
	QTreeWidgetItem *createTreeItem(const QString &projectName);
	/**
	 * Create a QTreeWidgetItem for plugin with provided plugin icon and name.
	 *
	 * @param icon corresponding to the plugin.
	 * @param pluginName plugin name.
	 * @param isSelectable if the plugin is selectable in the tree project
	 * @return QTreeWidgetItem top item (plugin node) without any children assigned.
	 */
	QTreeWidgetItem *createPluginNodeItem(const QIcon &icon, const QString &pluginName, bool isSelectable);
	/** Returns a list of all the projects in QMdiArea corresponding to the treeMdiManager sorted by project name.*/
	QList<QMdiSubWindow *> getProjects();
	/**
	 * Creates QTreeWidgetItem (plugin nodes) for each plugin sorted by:
	 * - is mono instance,
	 * - alphabetical.
	 */
	void createPluginNodes();

private slots:
	/** Change current MdiSubWindow. */
	void changeMdiWindow(QTreeWidgetItem *current, int column);
	/** On double click - minimize or maximize all MdiSubWindow. */
	void handleDoubleClick(QTreeWidgetItem *item, int column);

private:
	class _TreeMdiManager_pimpl;
	std::unique_ptr<_TreeMdiManager_pimpl> _pimpl;
};

#endif // TREEMDIMANAGER_HPP
