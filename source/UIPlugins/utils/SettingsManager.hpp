/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SETTINGSMANAGER_HPP
#define SETTINGSMANAGER_HPP

#include <memory>

#include <QObject>

#include "PluginsGlobal.hpp"

class QSettings;

struct SConfigObject;

/**
 * Manage the QSettings class.
 *
 * You should call the init() method as soon as possible in your `main()` method (right after the creation of the `QApplication`).
 * This will initilize the `QApplication` object with the name of the application and other things.
 *
 * Once done, you can have access to the QSettings by the regular way (calling the default QSettings() constructor).
 *
 * The settings() method will return an instance of SettingsManager where you can see all the settings.
 *
 * Note that the settings are written in an INI file (never in the Windows registry).
 *
 * The attribute innerSettings holds informations about the application that **are not** written in the QSettings. This can be
 * useful to share information among different plugins for instance. by default, it is filled with the following values:
 * - `appName`: the application name (given in the init() method)
 * - `appVersion`: the application version (given in the init() method)
 * - `orgName`: the organization name (given in the init() method)
 * - `orgDomain`: the organization domain (given in the init() method)
 * - `settingsFile`: the path to the file where are written the QSettings
 * - `currentDirPath`: the path where to look for open / save
 */
class SUGL_SHARED_EXPORT SettingsManager : public QObject
{
	Q_OBJECT

public:
	/**
	 * Init the `QApplication` with information about the app.
	 *
	 * This method should be called as soon as possible after the creation of the `QApplication`.
	 * @param appName the name of the application
	 * @param appVersion the version of the application
	 * @param orgName the name of the organization responsible of the application (default is `CERN_EN-SMM-APC`)
	 * @param orgDomain the domain of the organization (default is `https://home.cern`)
	 */
	static SettingsManager &init(const QString &appName, const QString &appVersion, const QString &orgName = "CERN_BE-GM-APC", const QString &orgDomain = "https://home.cern");
	/** @return an instance of SettingsManager */
	static SettingsManager &settings();

	~SettingsManager() override;

	SettingsManager(const SettingsManager &) = delete; // can't copy
	SettingsManager(SettingsManager &&) = delete;
	SettingsManager &operator=(const SettingsManager &) = delete;
	SettingsManager &operator=(SettingsManager &&) = delete;

	/** Add or change the value of the given key (ownership over the value is taken) */
	void settings(const QString &key, SConfigObject *value);
	/** @return true if the given key exists */
	bool settingsHas(const QString &key) const;
	/**
	 * Gives the config object corresponding to the given key.
	 * If the key doesn't exists, or its value can't be converted into a ConfigObject, creates a default ConfigObject and returns it so you can use the default values.
	 * @return the value of the given key
	 */
	template<class ConfigObject>
	ConfigObject settings(const QString &key) const;
	/** Remove the given key and its value */
	void settingsRemove(const QString &key);
	/**
	 * tell if the settings should be automatically written to a config file when this object is destroyed. I.E. when the program exits.
	 * @note this is the responsibility of the main program to ensure that everything will be written. This is not done automatically.
	 */
	void shouldWriteSettingsOnQuit(bool write) noexcept;
	/** @return the status of writeSettingsOnDestroy() */
	bool shouldWriteSettingsOnQuit() const noexcept;
	/** Write all the settings to the config file */
	void writeToConfigFile();

	/** Add or change the value of the given key */
	void innerSettings(const QString &key, const QString &value);
	/** @return true if the given key exists */
	bool innerSettingsHas(const QString &key) const;
	/**
	 * @return the value of the given key
	 * @throw std::out_of_range exception if key does not exist
	 */
	QString innerSettings(const QString &key) const;
	/** Remove the given key and its value */
	void innerSettingsRemove(const QString &key);

signals:
	/** Emitted when the settings changes (insertion, modification or removal) */
	void settingsChanged(const QString &key);
	/** Emitted when the inner settings changes (insertion, modification or removal) */
	void innerSettingsChanged(const QString &key);

private:
	SettingsManager(QObject *parent = nullptr);
	const SConfigObject *getSettings(const QString &key) const;

private:
	/** pimpl */
	class _SettingsManager_pimpl;
	std::unique_ptr<_SettingsManager_pimpl> _pimpl;
};

template<class ConfigObject>
inline ConfigObject SettingsManager::settings(const QString &key) const
{
	const auto *config = dynamic_cast<const ConfigObject *>(getSettings(key));
	if (config)
		return *config;
	// we don't have it in the settings, but we can still try to read it
	ConfigObject toread;
	toread.read();
	return toread;
}

#endif // SETTINGSMANAGER_HPP
