#include "StylesHelper.hpp"

#include <map>

#include <QApplication>
#include <QColor>
#include <QPalette>

template<>
struct std::hash<QColor>
{
	std::size_t operator()(const QColor &c) const noexcept { return std::hash<unsigned int>{}(c.rgba()); }
};

bool isDarkMode()
{
	static const QColor defaultColor = QApplication::palette().color(QPalette::Window);
	static const bool isDark = defaultColor.lightness() < 150;
	return isDark;
}

SUGL_SHARED_EXPORT QColor getThemeAdaptedColor(const QColor &color)
{
	static std::unordered_map<QColor, QColor> memoiser; // memoisation to make it faster
	if (memoiser.count(color))
		return memoiser[color];

	if (!isDarkMode())
	{
		memoiser[color] = color;
		return color;
	}

	// if dark mode
	double darknessFactor = 0.2;

	const QColor newColor = QColor(color.red() * darknessFactor, color.green() * darknessFactor, color.blue() * darknessFactor);
	memoiser[color] = newColor;
	return newColor;
}

SUGL_SHARED_EXPORT QColor getThemeColor(const QColor &color)
{
	static std::unordered_map<QColor, QColor> memoiser; // memoisation to make it faster
	if (memoiser.count(color))
		return memoiser[color];

	if (!isDarkMode())
	{
		memoiser[color] = color;
		return color;
	}

	int red = 255 - color.red();
	int green = 255 - color.green();
	int blue = 255 - color.blue();

	const QColor newColor(red, green, blue);
	memoiser[color] = newColor;
	return newColor;
}
