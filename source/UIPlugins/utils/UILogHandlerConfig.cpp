#include "UILogHandlerConfig.hpp"
#include "ui_UILogHandlerConfig.h"

#include <QSettings>

#include <LogMessage.hpp>

namespace
{
const QString _configname = "UILogHandler";
} // namespace

UILogHandlerConfigObject::UILogHandlerConfigObject() :
	// Log level
	logLevel{static_cast<int>(LogMessage::Type::INFO)},
	// Column visibilty
	typeVisible(true),
	timeVisible(true),
	contextVisible(true),
	// Qt log messages
	qtLogEnabled(false),
	popupEnabled(true)
{
}

bool UILogHandlerConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const UILogHandlerConfigObject &oc = static_cast<const UILogHandlerConfigObject &>(o);
	return logLevel == oc.logLevel && typeVisible == oc.typeVisible && timeVisible == oc.timeVisible
		&& contextVisible == oc.contextVisible && qtLogEnabled == oc.qtLogEnabled && popupEnabled == oc.popupEnabled;
}

void UILogHandlerConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(UILogHandlerConfig::configName());

	// Log level
	settings.setValue("logLevel", logLevel);
	// Column visibilty
	settings.setValue("typeVisible", typeVisible);
	settings.setValue("timeVisible", timeVisible);
	settings.setValue("contextVisible", contextVisible);
	// Qt log messages
	settings.setValue("qtLogEnabled", qtLogEnabled);
	// Popup messages
	settings.setValue("popupEnabled", popupEnabled);

	settings.endGroup();
}

void UILogHandlerConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(UILogHandlerConfig::configName());

	// Log level
	logLevel = settings.value("logLevel", static_cast<int>(LogMessage::Type::INFO)).toInt();
	// Column visibilty
	typeVisible = settings.value("typeVisible", true).toBool();
	timeVisible = settings.value("timeVisible", true).toBool();
	contextVisible = settings.value("contextVisible", true).toBool();
	// Qt log messages
	qtLogEnabled = settings.value("qtLogEnabled", false).toBool();
	// Popup messages
	popupEnabled = settings.value("popupEnabled", true).toBool();

	settings.endGroup();
}

class UILogHandlerConfig::_TextEditorConfig_pimpl
{
public:
	std::unique_ptr<Ui::UILogHandlerConfig> ui;
};

const QString &UILogHandlerConfig::configName() noexcept
{
	return _configname;
}

UILogHandlerConfig::UILogHandlerConfig(QWidget *parent) : SConfigWidget(nullptr, parent), _pimpl(std::make_unique<_TextEditorConfig_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::UILogHandlerConfig>();
	_pimpl->ui->setupUi(this);

#ifndef NDEBUG
	_pimpl->ui->comboBox->addItem(QIcon(":/markers/debug"), "Debug", static_cast<int>(LogMessage::Type::DEBUG));
#endif
	_pimpl->ui->comboBox->addItem(QIcon(":/markers/information"), "Information", static_cast<int>(LogMessage::Type::INFO));
	_pimpl->ui->comboBox->addItem(QIcon(":/markers/warning"), "Warning", static_cast<int>(LogMessage::Type::WARNING));
	_pimpl->ui->comboBox->addItem(QIcon(":/markers/error"), "Critical", static_cast<int>(LogMessage::Type::CRITICAL));
}

UILogHandlerConfig::~UILogHandlerConfig() = default;

SConfigObject *UILogHandlerConfig::config() const
{
	auto *config = new UILogHandlerConfigObject();

	// Log level
	config->logLevel = _pimpl->ui->comboBox->itemData(_pimpl->ui->comboBox->currentIndex()).toInt();
	// Column visibilty
	config->typeVisible = _pimpl->ui->cbType->isChecked();
	config->timeVisible = _pimpl->ui->cbTime->isChecked();
	config->contextVisible = _pimpl->ui->cbCont->isChecked();
	// Qt log messages
	config->qtLogEnabled = _pimpl->ui->cbQtLog->isChecked();
	// Popup messages
	config->popupEnabled = _pimpl->ui->cbPopup->isChecked();

	return config;
}

void UILogHandlerConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const UILogHandlerConfigObject *>(conf);
	if (!config)
		return;
	// Log level
	if (_pimpl->ui->comboBox->findData(config->logLevel) >= 0)
		_pimpl->ui->comboBox->setCurrentIndex(_pimpl->ui->comboBox->findData(config->logLevel));
	else
		_pimpl->ui->comboBox->setCurrentIndex(_pimpl->ui->comboBox->findData(static_cast<int>(LogMessage::Type::INFO)));
	// Column visibility
	_pimpl->ui->cbType->setChecked(config->typeVisible);
	_pimpl->ui->cbTime->setChecked(config->timeVisible);
	_pimpl->ui->cbCont->setChecked(config->contextVisible);
	// Qt log messages
	_pimpl->ui->cbQtLog->setChecked(config->qtLogEnabled);
	// Popup messages
	_pimpl->ui->cbPopup->setChecked(config->popupEnabled);
}

void UILogHandlerConfig::reset()
{
	UILogHandlerConfigObject config;
	config.read();
	setConfig(&config);
}

void UILogHandlerConfig::restoreDefaults()
{
	UILogHandlerConfigObject config;
	setConfig(&config);
}
