/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef UILOGHANDLERCONFIG_HPP
#define UILOGHANDLERCONFIG_HPP

#include <memory>

#include "PluginsGlobal.hpp"
#include "interface/SConfigInterface.hpp"

/**
 * Config object for UILogHandler class.
 */
struct SUGL_SHARED_EXPORT UILogHandlerConfigObject : public SConfigObject
{
	UILogHandlerConfigObject();
	virtual ~UILogHandlerConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	// Log level
	int logLevel;
	// Column visibility
	bool typeVisible;
	bool timeVisible;
	bool contextVisible;
	// Qt log messages
	bool qtLogEnabled;
	// Popup messages
	bool popupEnabled;
};

/**
 * Widget to edit the configuration for UILogHandler class.
 */
class SUGL_SHARED_EXPORT UILogHandlerConfig : public SConfigWidget
{
	Q_OBJECT

public:
	static const QString &configName() noexcept;

	/** Constructor */
	UILogHandlerConfig(QWidget *parent = nullptr);
	virtual ~UILogHandlerConfig() override;

	// SconfigWidget
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private:
	/** pimpl */
	class _TextEditorConfig_pimpl;
	std::unique_ptr<_TextEditorConfig_pimpl> _pimpl;
};

#endif // UILOGHANDLERCONFIG_HPP
