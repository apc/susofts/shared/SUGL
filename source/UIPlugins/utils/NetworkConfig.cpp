#include "NetworkConfig.hpp"
#include "ui_NetworkConfig.h"

#include <QSettings>

#include <LogMessage.hpp>
#include <utils/NetworkManager.hpp>

namespace
{
const QString _configname = "NetworkManager";
} // namespace

NetworkConfigObject::NetworkConfigObject() : timeout{10}
{
}

bool NetworkConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const NetworkConfigObject &oc = static_cast<const NetworkConfigObject &>(o);
	return timeout == oc.timeout;
}

void NetworkConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(NetworkConfig::configName());

	settings.setValue("timeout", timeout);

	settings.endGroup();
}

void NetworkConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(NetworkConfig::configName());

	timeout = settings.value("timeout", 10).toInt();

	settings.endGroup();
}

class NetworkConfig::_NetworkConfig_pimpl
{
public:
	std::unique_ptr<Ui::NetworkConfig> ui;
};

const QString &NetworkConfig::configName() noexcept
{
	return _configname;
}

NetworkConfig::NetworkConfig(QWidget *parent) : SConfigWidget(nullptr, parent), _pimpl(std::make_unique<_NetworkConfig_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::NetworkConfig>();
	_pimpl->ui->setupUi(this);
}

NetworkConfig::~NetworkConfig() = default;

SConfigObject *NetworkConfig::config() const
{
	auto *config = new NetworkConfigObject();

	config->timeout = _pimpl->ui->timeout->value();

	return config;
}

void NetworkConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const NetworkConfigObject *>(conf);
	if (!config)
		return;

	_pimpl->ui->timeout->setValue(config->timeout);
}

void NetworkConfig::reset()
{
	NetworkConfigObject config;
	config.read();
	setConfig(&config);
}

void NetworkConfig::restoreDefaults()
{
	NetworkConfigObject config;
	setConfig(&config);
	NetworkManager::getNetworkManager().clearCookies();
}
