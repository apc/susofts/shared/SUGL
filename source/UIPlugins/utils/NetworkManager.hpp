/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef NETWORKMANAGER_HPP
#define NETWORKMANAGER_HPP

#include <memory>

#include <QObject>

#include "PluginsGlobal.hpp"

class QNetworkRequest;
class QByteArray;
class QNetworkReply;
class QNetworkCookie;
class QWebEngineProfile;

/**
 * Singleton responsible for managing the network connection with Geode.
 *
 * It is managing the connection with Geode and allows to @getProfile and @post request.
 *
 * The @QWebEngineProfile is synchronised with Geode in order to share all the cookies (including the authorization cookie).
 * If a user is logged in to Geode then they are authenticated and can post to Geode.
 * Sharing cookies was the only feasible method that was found. Although cookies are meant to be persisten sometimes
 * connection still fails although the authentication cookie did not expire. Opening Geode-plugin again restores the connection.
 */
class SUGL_SHARED_EXPORT NetworkManager : public QObject
{
	Q_OBJECT

public:
	/** @return the instance of the NetworkManager (singleton). */
	static NetworkManager &getNetworkManager();

	~NetworkManager() override;

	// A singleton class
	NetworkManager(const NetworkManager &) = delete;
	NetworkManager(NetworkManager &&) = delete;
	NetworkManager &operator=(const NetworkManager &) = delete;
	NetworkManager &operator=(NetworkManager &&) = delete;

	/** Post a request using @NetworkManager's connection. */
	QNetworkReply *post(const QNetworkRequest &request, const QByteArray &data);
	/** Get a request using @NetworkManager's connection. */
	QNetworkReply *get(const QNetworkRequest &request);

	/** Get a profile that should be shared throughout the application to ensure consistent connection .*/
	QWebEngineProfile *getProfile();

public slots:
	bool insertCookie(const QNetworkCookie &cookie);
	bool removeCookie(const QNetworkCookie &cookie);
	void clearCookies();
	void updateUi(const QString &config);

private:
	/** Private constructor for a singleton. */
	NetworkManager(QObject *parent = nullptr);

	/** Sets the logged in flag directly after pinging Geode */
	void loggedInCheck();

	/** Fix related to CERN SSO cookie redirect behavior. This method should be called with every REST-like request type. */
	void requestFix(QNetworkReply *reply);

signals:
	/** By default logged in status is false, so the first time this signal is fired, it is fired with true*/
	void logInStatusChanged(bool);

private:
	/** pimpl */
	class _NetworkManager_pimpl;
	std::unique_ptr<_NetworkManager_pimpl> _pimpl;
};

#endif // NetworkManager_HPP
