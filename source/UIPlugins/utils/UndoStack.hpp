/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef IUNDOSTACK_HPP
#define IUNDOSTACK_HPP

#include <memory>

#include <QObject>

#include "PluginsGlobal.hpp"

class IUndoCommand;

/**
 * UndoStack is a container storing all relevant IUndoCommand and is indispensable if undo and redo behavior is required.
 *
 * The class contains all IUndoCommand and allows to push new commands and undo() or redo() the state of the widget based on the current stack index.
 * This way the changes can be reverted/redone on many widgets and only one UndoStack is required to manage all of them.
 * The UndoStack supports handling IUndoCommand coming from different widgets and handles what type of IUndoCommand should be reverted.
 *
 * Whenever the UndoStack is to be used for the first time or whenever it should be cleared the reset() method should be used.
 * The method clears the stack, reserves the space and places the initial commands (IUndoCommad) on the stack. These commands are protected, i.e. they cannot be reverted
 * with undo and they will always be present until the next reset() method is used (where they will be replaced by new initial state comands). They ensure that the
 * widget(s) always have some state recorded to which changes can be reverted. The ammount of IUndoCommand that should be passed to reset() method depends on how many
 * widgets should be tracked for changes.
 * Example: When UndoStack should track two widgets (e.g. table and comboBox) the reset method should be used and two pointers to class
 * implementations of IUndoCommand should be passed as arguments. As each widget is different, they probably record the state in different way, and therefore,
 * the two IUndoCommand used will be different implementations of IUndoCommand. These two commands cannot be deleted/reverted/undo and they do not count as
 * isUndoAvailable and they represent the default state of widgets before any changes.
 *
 * @see IUndoCommand
 */
class SUGL_SHARED_EXPORT UndoStack : public QObject
{
	Q_OBJECT

public:
	UndoStack();
	UndoStack(const UndoStack &) = delete;
	UndoStack &operator=(const UndoStack &) = delete;
	~UndoStack() override;

	/** Undo last IUndoCommand.
	 *
	 * Undo command is not as straightforward as redo command as it needs to ensure that all the tracked widgets have the same state as before the changes. Because of
	 * that, the undo command does not in fact revert the last IUndoCommand on the stack (based on stack index), but it reverses the last command of a given type.
	 * Example: change was done to table, after that two changes were done to comboBox, and in the end one change to the table again. Undo should not call the restoreState() for comboBox then, as in fact the
	 * last change was made on a table. That is why undo has to search for the earlier table IUndoCommand on the stack (which would be the first table change in example), restore
	 * its state and move stack index one backwards.
	 * Index: t[ ] c[ ] c[ ] t[x]    ---undo()--->    t[restoreState()] c[ ] c[x] t[ ]
	 */
	void undo();
	/** Redo last IUndoCommand. */
	void redo();
	/** Push IUndoCommand on the UndoStack. */
	void push(IUndoCommand *command);
	/** Sets initial state for the UndoStack. It clears the stack, reserves the space and sets relevant indices. */
	void reset(const std::vector<IUndoCommand *> &initialCommands);
	/** @return boolean indicating if undo action is possible. */
	bool isUndoAvailable();
	/** @return boolean indicating if redo action is possible. */
	bool isRedoAvailable();

signals:
	/** Signal emmited each time either contents of UndoStack is changed or its index is moved. Signal notifies if undo or redo are available. */
	void hasChanged(bool undoAvailable, bool redoAvailable);

private:
	/** pimpl */
	class _UndoStack_pimpl;
	std::unique_ptr<_UndoStack_pimpl> _pimpl;
};

#endif // IUNDOSTACK_HPP
