#include "UILogHandler.hpp"
#include "ui_UILogHandler.h"

#include <QColor>
#include <QComboBox>
#include <QDebug>
#include <QMessageBox>

#include <ILogHandler.hpp>
#include <LogMessage.hpp>
#include <Logger.hpp>
#include <utils/StylesHelper.hpp>

#include "SettingsManager.hpp"
#include "UILogHandlerConfig.hpp"

namespace
{
/* clang-format off */
const std::unordered_map<LogMessage::Type, QColor> _LOGCOLOR = {
	{LogMessage::Type::DEBUG, QColor(229, 253, 229)},
	{LogMessage::Type::INFO, QColor(229, 253, 252)},
	{LogMessage::Type::WARNING, QColor(252, 244, 173)},
	{LogMessage::Type::CRITICAL, QColor(255, 222, 222)},
	{LogMessage::Type::FATAL, QColor(255, 222, 222)}
};
const std::unordered_map<LogMessage::Type, const char*> _LOGICON = {
	{LogMessage::Type::DEBUG, ":/markers/debug"},
	{LogMessage::Type::INFO, ":/markers/information"},
	{LogMessage::Type::WARNING, ":/markers/warning"},
	{LogMessage::Type::CRITICAL, ":/markers/error"},
	{LogMessage::Type::FATAL, ":/markers/error"}
};
const std::unordered_map<LogMessage::Type, QString> _LOGTOOLTIP = {
	{LogMessage::Type::DEBUG, QObject::tr("Debug")},
	{LogMessage::Type::INFO, QObject::tr("Information")},
	{LogMessage::Type::WARNING, QObject::tr("Warning")},
	{LogMessage::Type::CRITICAL, QObject::tr("Error")},
	{LogMessage::Type::FATAL, QObject::tr("Fatal")}
};
const std::unordered_map<QtMsgType, LogMessage::Type> _LOGQT = {
	{QtMsgType::QtDebugMsg, LogMessage::Type::DEBUG},
	{QtMsgType::QtInfoMsg, LogMessage::Type::INFO},
	{QtMsgType::QtWarningMsg, LogMessage::Type::WARNING},
	{QtMsgType::QtCriticalMsg, LogMessage::Type::CRITICAL},
	{QtMsgType::QtFatalMsg, LogMessage::Type::FATAL}
};
/* clang-format on */

class TableWidgetItemLogType : public QTableWidgetItem
{
public:
	TableWidgetItemLogType(LogMessage::Type type) : QTableWidgetItem(QTableWidgetItem::ItemType::UserType), _type(type)
	{
		setIcon(QPixmap(_LOGICON.at(type)));
		setToolTip(_LOGTOOLTIP.at(type));
		setBackground(getThemeAdaptedColor(_LOGCOLOR.at(type)));
	}

	// sort, currently not used
	virtual bool operator<(const QTableWidgetItem &other) const override
	{
		const auto *o = dynamic_cast<const TableWidgetItemLogType *>(&other);
		if (!o)
			return QTableWidgetItem::operator<(other);
		return _type < o->_type;
	}

	// accessors
	LogMessage::Type getType() const noexcept { return _type; }

private:
	LogMessage::Type _type;
};
} // namespace

class UILogHandler::_UILogger : public ILogHandler
{
public:
	_UILogger(QTableWidget *tab) :
		_tab(tab),
		logHiddenThreshold(-1), // No filtering
		qtLogsEnabled(false),
		popupEnabled(false)
	{
	}

	virtual ~_UILogger() override = default;
	virtual void log(const LogMessage &message) override
	{
		_tab->insertRow(0);

		// Set hidden
		if (logHiddenThreshold != -1 && static_cast<int>(message.getType()) != logHiddenThreshold)
			_tab->setRowHidden(0, true);

		// Type
		_tab->setItem(0, 0, new TableWidgetItemLogType(message.getType()));

		// Time
		QTableWidgetItem *time = new QTableWidgetItem(QString::fromStdString(message.getDate("%H:%M:%S")));
		time->setToolTip(QString::fromStdString(message.getDate("%H:%M:%S")));
		time->setBackground(getThemeAdaptedColor(_LOGCOLOR.at(message.getType())));
		_tab->setItem(0, 1, time);

		// Message
		QTableWidgetItem *mess = new QTableWidgetItem(QString::fromStdString(message.getMessage()));
		mess->setToolTip(QString::fromStdString(message.getMessage()));
		mess->setBackground(getThemeAdaptedColor(_LOGCOLOR.at(message.getType())));
		_tab->setItem(0, 2, mess);

		// Context
		QTableWidgetItem *cont = new QTableWidgetItem(QString::fromStdString(message.getContext()));
		cont->setToolTip(QString::fromStdString(message.getContext()));
		cont->setBackground(getThemeAdaptedColor(_LOGCOLOR.at(message.getType())));
		_tab->setItem(0, 3, cont);

		// Create a popup
		if (popupEnabled && (message.getType() >= LogMessage::Type::CRITICAL) && !QString::fromStdString(message.getFile()).endsWith(".html"))
			QMessageBox::critical(nullptr, tr("Error"), QString::fromStdString(message.getMessage()));
	}

	void registerQt()
	{
		currentHandler = this;
		qInstallMessageHandler(handleMessages);
	}

	void unregisterQt()
	{
		qInstallMessageHandler(nullptr); // Set to default
	}

	void updateFilter(int id) { logHiddenThreshold = id; }
	void setQtEnabled(bool val) { qtLogsEnabled = val; }
	void setPopupEnabled(bool val) { popupEnabled = val; }

private:
	static void handleMessages(QtMsgType type, const QMessageLogContext &context, const QString &msg)
	{
		if (currentHandler)
			currentHandler->qtMessageHandler(type, context, msg);
	}

	void qtMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
	{
		// Check if enabled and threshold applies
		if (!qtLogsEnabled || getThreshold() > _LOGQT.at(type))
			return;
		log(LogMessage(_LOGQT.at(type), std::string(context.file ? context.file : ""), context.line, std::string(context.function ? context.function : ""), msg.toStdString()));
	}

private:
	static inline _UILogger *currentHandler = nullptr;

	QTableWidget *_tab;
	int logHiddenThreshold;
	bool qtLogsEnabled;
	bool popupEnabled;
};

UILogHandler::UILogHandler(QWidget *parent) : QWidget(parent), ui(std::make_unique<Ui::UILogHandler>())
{
	ui->setupUi(this);
	// Table widget
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
	ui->tableWidget->horizontalHeader()->resizeSection(0, 23);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
	ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Interactive);
	ui->tableWidget->horizontalHeader()->resizeSection(2, 750);
	ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	// Combo box filtering
	ui->comboBox->blockSignals(true);
	ui->comboBox->addItem(QIcon(":/markers/nofiltering"), tr("Show all logs"), -1); // No filtering
	ui->comboBox->addItem(QIcon(":/markers/information"), tr("Show only information logs"), static_cast<int>(LogMessage::Type::INFO));
	ui->comboBox->addItem(QIcon(":/markers/warning"), tr("Show only warning logs"), static_cast<int>(LogMessage::Type::WARNING));
	ui->comboBox->addItem(QIcon(":/markers/error"), tr("Show only critical logs"), static_cast<int>(LogMessage::Type::CRITICAL));
	ui->comboBox->blockSignals(false);

	// Create logger
	registerLogger();

	// Manage log settings
	connect(&SettingsManager::settings(), &SettingsManager::settingsChanged, this, &UILogHandler::updateUi);
}

UILogHandler::~UILogHandler()
{
	unregisterQt();
	unregisterLogger();
}

void UILogHandler::registerLogger()
{
	_uiLog = new _UILogger(ui->tableWidget);
	Logger::getLogger().addHandlers(_uiLog);
}

void UILogHandler::unregisterLogger()
{
	Logger::getLogger().removeHandler(_uiLog);
	_uiLog = nullptr;
}

void UILogHandler::registerQt()
{
	if (!_uiLog)
		registerLogger();
	_uiLog->registerQt();
}

void UILogHandler::unregisterQt()
{
	_uiLog->unregisterQt();
}

void UILogHandler::clearLogs()
{
	ui->tableWidget->setRowCount(0);
}

void UILogHandler::updateUi(const QString &config)
{
	if (config != UILogHandlerConfig::configName())
		return;
	auto conf = SettingsManager::settings().settings<UILogHandlerConfigObject>(config);

	ui->tableWidget->setColumnHidden(0, !conf.typeVisible);
	ui->tableWidget->setColumnHidden(1, !conf.timeVisible);
	ui->tableWidget->setColumnHidden(3, !conf.contextVisible);
	if (_uiLog)
	{
		_uiLog->setThreshold(static_cast<LogMessage::Type>(conf.logLevel));
		_uiLog->setPopupEnabled(conf.popupEnabled);
		_uiLog->setQtEnabled(conf.qtLogEnabled);
		if (conf.qtLogEnabled)
			registerQt();
		else
			unregisterQt();
	}
}

void UILogHandler::filterLogs(int id)
{
	int filter = ui->comboBox->itemData(id).toInt();
	QTableWidget &tab = *ui->tableWidget;
	for (int i = 0; i < tab.rowCount(); i++)
	{
		if (filter == -1)
			tab.setRowHidden(i, false);
		else
		{
			const auto *o = dynamic_cast<const TableWidgetItemLogType *>(tab.item(i, 0));
			if (!o)
				continue; // shouldn't happen
			if (filter == (int)LogMessage::Type::CRITICAL)
				tab.setRowHidden(i, (int)o->getType() < filter);
			else
				tab.setRowHidden(i, (int)o->getType() != filter);
		}
	}
}
