#include "TreeMdiManager.hpp"

#include <unordered_map>
#include <vector>

#include <QAction>
#include <QApplication>
#include <QContextMenuEvent>
#include <QHeaderView>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QMenu>
#include <QPen>
#include <QScrollBar>

#include <utils/StylesHelper.hpp>

#ifdef Q_OS_WIN
#	include <QDir>
#	include <QProcess>
#else
#	include <QDesktopServices>
#	include <QUrl>
#endif

#include "interface/SGraphicalInterface.hpp"
#include "interface/SPluginInterface.hpp"
#include "interface/SPluginLoader.hpp"

class TreeMdiManager::_TreeMdiManager_pimpl
{
public:
	QMdiArea *mdi = nullptr;
	std::unordered_map<QTreeWidgetItem *, QMdiSubWindow *> subs;
	std::vector<QMetaObject::Connection> connections;
	std::unordered_map<std::string, bool> nodeExpanded;
};

TreeMdiManager::TreeMdiManager(QWidget *parent) : QTreeWidget(parent), _pimpl(std::make_unique<_TreeMdiManager_pimpl>())
{
	// Appearance of the header
	setColumnCount(3);
	setHeaderHidden(true);
	setColumnWidth(0, 50); 
	header()->setStretchLastSection(false);
	header()->setSectionResizeMode(0, QHeaderView::Fixed); // project icon
	header()->setSectionResizeMode(1, QHeaderView::Stretch); // plugin/project name
	header()->setSectionResizeMode(2, QHeaderView::Fixed); // close project icon

	// Connects
	connect(this, &QTreeWidget::itemClicked, this, &TreeMdiManager::changeMdiWindow);
	connect(this, &QTreeWidget::itemDoubleClicked, this, &TreeMdiManager::handleDoubleClick);
	connect(this, &TreeMdiManager::itemPressed, [this](QTreeWidgetItem *item, int column) {
		if (item && column == columnCount() - 1 && _pimpl->subs.find(item) != _pimpl->subs.end()) // For projects, last column contains `close` icon, if pressed it should close the project
			emit closeProject(getMdiSubWindow(item));
	});
	connect(this, &QTreeWidget::itemExpanded, [this](QTreeWidgetItem *item) { _pimpl->nodeExpanded[item->data(1, Qt::DisplayRole).toString().toStdString()] = true; });
	connect(this, &QTreeWidget::itemCollapsed, [this](QTreeWidgetItem *item) { _pimpl->nodeExpanded[item->data(1, Qt::DisplayRole).toString().toStdString()] = false; });
}

TreeMdiManager::~TreeMdiManager() = default;

void TreeMdiManager::mdiArea(QMdiArea *mdiarea)
{
	if (_pimpl->mdi == mdiarea)
		return;
	if (_pimpl->mdi)
		disconnect(_pimpl->mdi, &QMdiArea::subWindowActivated, this, &TreeMdiManager::updateTree);
	_pimpl->mdi = mdiarea;
	if (_pimpl->mdi)
		connect(_pimpl->mdi, &QMdiArea::subWindowActivated, this, &TreeMdiManager::updateTree);
}

const QMdiArea *TreeMdiManager::mdiArea() const noexcept
{
	return _pimpl->mdi;
}

QMdiArea *TreeMdiManager::mdiArea() noexcept
{
	return _pimpl->mdi;
}

void TreeMdiManager::changeMdiWindow(QTreeWidgetItem *current, int)
{
	if (!_pimpl->mdi || !current || _pimpl->subs.find(current) == _pimpl->subs.end())
		return;
	_pimpl->mdi->setActiveSubWindow(getMdiSubWindow(current));
}

void TreeMdiManager::handleDoubleClick(QTreeWidgetItem *item, int)
{
	if (!item || !_pimpl->mdi)
		return;
	auto *sub = getMdiSubWindow(item);
	if (!sub) // If a project node was clicked twice - create new project
	{
		auto *plugin = SPluginLoader::getPluginLoader().getPlugin(item->data(1, Qt::DisplayRole).toString());
		if (plugin)
			emit newProject(plugin);
	}
	else
	{
		if (_pimpl->mdi->viewMode() == QMdiArea::ViewMode::TabbedView)
			return;
		else if (sub->isMaximized())
			sub->showNormal();
		else
			sub->showMaximized();
	}
}

void TreeMdiManager::updateTree(QMdiSubWindow *window)
{
	// Get projects
	auto mdiList = getProjects();

	// Preserve the scroll position if the number of projects did not increase in-between redraws
	// If it increased (new project) the scroll will go automatically to the new item
	int lastScrollPosition = verticalScrollBar()->value();
	bool shouldPreserveScroll = mdiList.size() <= _pimpl->connections.size();

	//  clear QTreeWidgetItems
	_pimpl->subs.clear();
	_pimpl->subs.reserve(mdiList.size());
	clear();
	// Delete the connections
	blockSignals(true);
	for (auto &c : _pimpl->connections)
		disconnect(c);
	_pimpl->connections.clear();
	_pimpl->connections.reserve(mdiList.size());

	// Create plugin nodes
	createPluginNodes();
	// Populate the nodes with projects - add projects to plugin node
	for (auto &mdi : mdiList)
	{
		// Get plugin name and plugin node
		auto *sgw = dynamic_cast<SGraphicalWidget *>(mdi->widget());
		if (!sgw) // If not a SGraphicalWidget
			continue;
		const QString &pluginName = sgw->owner()->name();
		auto pluginNodes = findItems(pluginName, Qt::MatchExactly, 1);
		if (pluginNodes.isEmpty()) // If plugin node is not found
			continue;
		auto *pluginNode = pluginNodes[0]; // There is always one plugin node only with the given name

		// If normal project
		if (!sgw->owner()->isMonoInstance())
		{
			auto *tItem = createTreeItem(mdi->windowTitle());
			pluginNode->addChild(tItem);
			_pimpl->subs[tItem] = mdi;
			_pimpl->connections.push_back(connect(mdi, &QWidget::windowTitleChanged, [this, tItem](const QString &title) -> void { tItem->setText(1, title); }));
			// Select
			if (mdi == window)
			{
				setCurrentItem(tItem);
				// Expand its project node
				_pimpl->nodeExpanded[pluginName.toStdString()] = true;
			}
		}
		// If project belongs to mono-instance plugin - assign project to the node
		else
		{
			pluginNode->setIcon(2, QIcon(":/actions/close"));
			_pimpl->subs[pluginNode] = mdi;
			// Select
			if (mdi == window)
				setCurrentItem(pluginNode);
		}
		// Enable the plugin node (as there was a project corresponding to it)
		pluginNode->setDisabled(false);
	}

	blockSignals(false);
	resizeColumnToContents(2);

	if (shouldPreserveScroll)
		verticalScrollBar()->setSliderPosition(lastScrollPosition);
}

void TreeMdiManager::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu menu;
	auto *item = itemAt(event->pos());
	// If there is SGraphicalWidget project at position
	if (_pimpl->subs.find(item) != _pimpl->subs.end())
	{
		// Open in file explorer
		auto *sgw = qobject_cast<SGraphicalWidget *>(getMdiSubWindow(item)->widget());
		if (!sgw)
			return;
		auto *actExplorer = new QAction(QIcon(":/actions/open"), tr("Open in file explorer"), &menu);
		connect(actExplorer, &QAction::triggered, this, [sgw]() {
#ifdef Q_OS_WIN
			QProcess::startDetached("explorer", {"/select,", QDir::toNativeSeparators(sgw->getPath())});
#else
	QDesktopServices::openUrl(QUrl::fromLocalFile(sgw->getPath()));
#endif
		});
		actExplorer->setEnabled(!sgw->getPath().isEmpty());
		menu.addAction(actExplorer);
	}
	// If no SGraphicalWidget project
	else
	{
		QString title = "%1 project";
		SPluginInterface *plugin = nullptr;
		QString pluginName;
		// If existing item but not SGW - plugin node
		if (item)
		{
			pluginName = item->data(1, Qt::DisplayRole).toString();
			title = title.arg("%1 " + pluginName);
			plugin = SPluginLoader::getPluginLoader().getPlugin(pluginName);
		}
		// New Project
		auto *actNew = new QAction(QIcon(":/actions/new"), title.arg("New"), &menu);
		connect(actNew, &QAction::triggered, [&plugin, this]() { emit newProject(plugin); });
		menu.addAction(actNew);
		// Open Project - if selected plugin that is openable or some empty space
		if (!item || (plugin && !plugin->getExtensions().isEmpty()))
		{
			auto *actOpen = new QAction(QIcon(":/actions/open"), title.arg("Open"), &menu);
			connect(actOpen, &QAction::triggered, [&plugin, this]() { emit openProject(plugin); });
			menu.addAction(actOpen);
		}
		// Run all projects - run all on empty space is not allowed
		if (item && plugin && plugin->hasLaunchInterface() && !plugin->isMonoInstance())
		{
			auto *actRunAll = new QAction(QIcon(":/actions/build"), title.arg("Run all") + 's', &menu);
			connect(actRunAll, &QAction::triggered, [&plugin, this]() { emit runAllProjects(plugin); });
			menu.addAction(actRunAll);
			actRunAll->setEnabled(item->childCount());
		}
	}

	menu.exec(event->globalPos());
}

QMdiSubWindow *TreeMdiManager::getMdiSubWindow(QTreeWidgetItem *item)
{
	auto iter = _pimpl->subs.find(item);
	if (iter == _pimpl->subs.end())
		return nullptr;
	return iter->second;
}

void TreeMdiManager::highlightTreeItem(QMdiSubWindow *window)
{
	for (const auto &sub : _pimpl->subs)
	{
		if (sub.second == window)
			return setCurrentItem(sub.first);
	}
}

void TreeMdiManager::mousePressEvent(QMouseEvent *event)
{
	// If a wheel mouse button - close the project
	if (event->button() == Qt::MiddleButton)
	{
		auto *item = itemAt(event->pos());
		if (!item || _pimpl->subs.find(item) == _pimpl->subs.end())
			return;
		setCurrentItem(item);
		emit closeProject(getMdiSubWindow(currentItem()));
	}
	// If not, default handling (what leads to emitting itemPressed signal)
	else
		QTreeWidget::mousePressEvent(event);
}

QTreeWidgetItem *TreeMdiManager::createTreeItem(const QString &projectName)
{
	auto *tItem = new QTreeWidgetItem();
	tItem->setText(1, projectName);
	tItem->setToolTip(1, projectName);
	tItem->setIcon(2, QIcon(":/actions/close"));
	tItem->setToolTip(2, tr("Close project"));
	return tItem;
}

QTreeWidgetItem *TreeMdiManager::createPluginNodeItem(const QIcon &icon, const QString &pluginName, bool isSelectable)
{
	auto *topItem = new QTreeWidgetItem(this);
	if (!isSelectable)
		topItem->setFlags(topItem->flags() & ~Qt::ItemIsSelectable);
	else
	{
		QBrush brush(getThemeAdaptedColor(QColor(200, 200, 200)));
		topItem->setBackground(0, brush);
		topItem->setBackground(1, brush);
		topItem->setBackground(2, brush);
	}
	topItem->setIcon(0, icon);
	topItem->setText(1, pluginName);
	// If the key is not in map
	if (_pimpl->nodeExpanded.find(pluginName.toStdString()) == _pimpl->nodeExpanded.end())
		_pimpl->nodeExpanded[pluginName.toStdString()] = true;
	topItem->setExpanded(_pimpl->nodeExpanded.at(pluginName.toStdString()));
	// Disable item as it is empty
	topItem->setDisabled(true);
	return topItem;
}

QList<QMdiSubWindow *> TreeMdiManager::getProjects()
{
	if (!_pimpl->mdi)
		return {};

	// Sort the project list by project name
	auto list = _pimpl->mdi->subWindowList();
	std::sort(list.begin(), list.end(), [](const QMdiSubWindow *m1, const QMdiSubWindow *m2) { return m1->windowTitle() < m2->windowTitle(); });
	return list;
}
void TreeMdiManager::createPluginNodes()
{
	// Get and sort the plugins: mono-instance > alphabetical
	auto plugins = SPluginLoader::getPluginLoader().plugins();
	std::sort(plugins.begin(), plugins.end(), [](SPluginInterface *plug1, SPluginInterface *plug2) {
		return (plug1->isMonoInstance() && !plug2->isMonoInstance())
			|| (plug1->name().toLower() < plug2->name().toLower() && (plug1->isMonoInstance() || !plug2->isMonoInstance()));
	});
	// Show plugin node (empty) for every plugin
	for (const auto &plugin : plugins)
		createPluginNodeItem(plugin->icon(), plugin->name(), plugin->isMonoInstance());
}
