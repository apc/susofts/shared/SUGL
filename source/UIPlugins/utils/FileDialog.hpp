/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef FILEDIALOG_HPP
#define FILEDIALOG_HPP

#include <QFileDialog>
#include <QFileInfo>

#include "SettingsManager.hpp"

/**
 * Open a file picker dialog to select a folder.
 *
 * This calls QFileDialog::getExistingDirectory() with the following parameters:
 * - parent as parent
 * - caption as caption
 * - appendName will be appended to current folder from the settings and will be used as dir, if not provided just the default directory will be used
 * - forwards any other arguments
 */
template<class... Args>
inline QString getExistingDirectory(QWidget *parent = nullptr, const QString &caption = QString(), const QString &appendName = QString(), Args &&... args)
{
	auto &settings = SettingsManager::settings();
	QString defaultFolder = settings.innerSettings("currentDirPath");
	if (!appendName.isEmpty())
		defaultFolder.append('/' + appendName);
	QString path = QFileDialog::getExistingDirectory(parent, caption, defaultFolder, std::forward<Args>(args)...);
	if (!path.isEmpty())
		settings.innerSettings("currentDirPath", QFileInfo(path).absolutePath());
	return path;
}

/**
 * Open a file picker dialog to select a file.
 *
 * This calls QFileDialog::getOpenFileName() with the following parameters:
 * - parent as parent
 * - caption as caption
 * - appendName will be appended to current folder from the settings and will be used as dir, if not provided just the default directory will be used
 * - forwards any other arguments
 */
template<class... Args>
inline QString getOpenFileName(QWidget *parent = nullptr, const QString &caption = QString(), const QString &appendName = QString(), Args &&... args)
{
	auto &settings = SettingsManager::settings();
	QString defaultFolder = settings.innerSettings("currentDirPath");
	if (!appendName.isEmpty())
		defaultFolder.append('/' + appendName);
	QString path = QFileDialog::getOpenFileName(parent, caption, defaultFolder, std::forward<Args>(args)...);
	if (!path.isEmpty())
		settings.innerSettings("currentDirPath", QFileInfo(path).absolutePath());
	return path;
}

/**
 * Open a file picker dialog to select files.
 *
 * This calls QFileDialog::getOpenFileNames() with the following parameters:
 * - parent as parent
 * - caption as caption
 * - appendName will be appended to current folder from the settings and will be used as dir, if not provided just the default directory will be used
 * - forwards any other arguments
 */
template<class... Args>
inline QStringList getOpenFileNames(QWidget *parent = nullptr, const QString &caption = QString(), const QString &appendName = QString(), Args &&... args)
{
	auto &settings = SettingsManager::settings();
	QString defaultFolder = settings.innerSettings("currentDirPath");
	if (!appendName.isEmpty())
		defaultFolder.append('/' + appendName);
	QStringList paths = QFileDialog::getOpenFileNames(parent, caption, defaultFolder, std::forward<Args>(args)...);
	if (!paths.isEmpty() && !paths.first().isEmpty())
		settings.innerSettings("currentDirPath", QFileInfo(paths.first()).absolutePath());
	return paths;
}

/**
 * Open a file picker dialog to select a file in order to write in it.
 *
 * This calls QFileDialog::getSaveFileName() with the following parameters:
 * - parent as parent
 * - caption as caption
 * - appendName will be appended to current folder from the settings and will be used as dir, if not provided just the default directory will be used
 * - forwards any other arguments
 */
template<class... Args>
inline QString getSaveFileName(QWidget *parent = nullptr, const QString &caption = QString(), const QString &appendName = QString(), Args &&... args)
{
	auto &settings = SettingsManager::settings();
	QString defaultFolder = settings.innerSettings("currentDirPath");
	if (!appendName.isEmpty())
		defaultFolder.append('/' + appendName);
	QString path = QFileDialog::getSaveFileName(parent, caption, defaultFolder, std::forward<Args>(args)...);
	if (!path.isEmpty())
		settings.innerSettings("currentDirPath", QFileInfo(path).absolutePath());
	return path;
}

#endif // FILEDIALOG_HPP
