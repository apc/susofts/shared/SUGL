#include "ScriptRunnerDialog.hpp"
#include "ui_ScriptRunnerDialog.h"

#include <QCloseEvent>
#include <QProcess>

#include "interface/ProcessLauncherObject.hpp"
#include "interface/SPluginInterface.hpp"
#include "interface/SPluginLoader.hpp"
#include "utils/FileDialog.hpp"

class ScriptLauncherObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	ScriptLauncherObject(SPluginInterface *owner, const QString &pathExe, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent), pathExe(pathExe)
	{
		process().setProgram(pathExe);
	}
	virtual ~ScriptLauncherObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override { return pathExe; }

	void setArguments(QStringList arguments) { process().setArguments(arguments); }
	QString getRunOutput() { return process().readAllStandardOutput(); }

private:
	const QString pathExe;
};

#include "ScriptRunnerDialog.moc"

class ScriptRunnerDialog::_ScriptRunnerDialog_pimpl
{
public:
	std::unique_ptr<Ui::ScriptRunnerDialog> ui;
	ScriptLauncherObject *launcher = nullptr;
	QString output;
	QString exePath;
};

ScriptRunnerDialog::ScriptRunnerDialog(const ExternalScript &script, const QString &inputPath, QWidget *parent) :
	QDialog(parent), _pimpl(std::make_unique<_ScriptRunnerDialog_pimpl>())
{
	// script
	_pimpl->exePath = script.path;

	// ui
	_pimpl->ui = std::make_unique<Ui::ScriptRunnerDialog>();
	_pimpl->ui->setupUi(this);
	// ui elements
	_pimpl->ui->help->setText(script.help);
	_pimpl->ui->input->setText(QDir::toNativeSeparators(inputPath));
	_pimpl->ui->arguments->setText(script.defaultArguments);
	_pimpl->ui->arguments->setFocus();
	setWindowTitle(windowTitle().arg(script.name));
	setAttribute(Qt::WA_DeleteOnClose);

	updateUi();

	// launcher
	_pimpl->launcher = new ScriptLauncherObject(nullptr, _pimpl->exePath, this);
	connect(_pimpl->launcher, &SLaunchObject::started, this, &ScriptRunnerDialog::jobStarted);
	connect(_pimpl->launcher, &SLaunchObject::finished, this, &ScriptRunnerDialog::jobFinished);
}

ScriptRunnerDialog::~ScriptRunnerDialog() = default;

void ScriptRunnerDialog::closeEvent(QCloseEvent *e)
{
	if (!_pimpl->launcher->isRunning())
		QDialog::closeEvent(e);
	else
		e->ignore();
}

void ScriptRunnerDialog::jobStarted()
{
	_pimpl->ui->progressBar->setValue(50);
	_pimpl->ui->run->setEnabled(false);
	_pimpl->ui->stop->setEnabled(true);
	_pimpl->ui->output->setText("");
	_pimpl->ui->close->setDefault(true);
}

void ScriptRunnerDialog::jobFinished(bool result)
{
	_pimpl->output = _pimpl->launcher->getRunOutput();
	_pimpl->ui->progressBar->setValue(100);
	_pimpl->ui->output->setText(_pimpl->output);
	_pimpl->ui->stop->setEnabled(false);
	_pimpl->ui->run->setEnabled(true);
	if (result)
		_pimpl->ui->progressBar->setEnabled(true);
	else
		_pimpl->ui->output->append("Error while running: " + _pimpl->launcher->error());
}

QString ScriptRunnerDialog::getOutput()
{
	return _pimpl->output;
}

void ScriptRunnerDialog::startJob()
{
	_pimpl->launcher->setArguments(prepareArguments());
	_pimpl->launcher->launch("");
	_pimpl->ui->progressBar->setEnabled(false);
}

void ScriptRunnerDialog::cancelJob()
{
	_pimpl->launcher->stop();
}

QMap<QString, QString> ScriptRunnerDialog::generateExePathsMap()
{
	auto plugins = SPluginLoader::getPluginLoader().plugins();
	QMap<QString, QString> pluginToExe;

	for (auto plugin : plugins)
	{
		if (!plugin->hasLaunchInterface())
			continue;

		// Get all the executable paths
		auto launcher = std::unique_ptr<SLaunchObject>(plugin->launchInterface());
		const QString exepath = launcher->exepath();
		if (!exepath.isEmpty())
		{
			const QString softwareName = plugin->name().left(plugin->name().length() - 6); // Remove the last 6 characters ("Plugin")
			pluginToExe[softwareName] = exepath;
		}
	}
	return pluginToExe;
}

QStringList ScriptRunnerDialog::prepareArguments()
{
	QStringList arguments;
	if (!_pimpl->ui->input->text().isEmpty())
		arguments.append({"-i", _pimpl->ui->input->text()});
	QString textArguments = _pimpl->ui->arguments->text();
	if (textArguments.isEmpty())
		return arguments;

	// In case when a script wants to use some executable we provide a ${Name} placeholder that
	// should be replaced with exepath set in the settings.
	auto plugins = SPluginLoader::getPluginLoader().plugins();
	QMap<QString, QString> pluginToExe;
	if (textArguments.contains("${"))
		pluginToExe = generateExePathsMap();

	QString currentEntry;
	bool insideQuotes = false;
	bool insidePlaceholder = false;

	// iterate over all chars
	for (const QChar &c : textArguments)
	{
		// If start or end quote
		if (c == '"')
		{
			insideQuotes = !insideQuotes;
			if (!currentEntry.isEmpty())
			{
				arguments.append(currentEntry);
				currentEntry.clear();
			}
		}
		// Placeholder cases
		else if (pluginToExe.size() && c == '$')
			insidePlaceholder = !insidePlaceholder;
		else if (pluginToExe.size() && insidePlaceholder && c == '{')
			continue;
		else if (pluginToExe.size() && insidePlaceholder && c == '}')
		{
			insidePlaceholder = !insidePlaceholder;
			if (pluginToExe.contains(currentEntry))
			{
				arguments.append(pluginToExe[currentEntry]);
				currentEntry.clear();
			}
		}
		// if space outside of quotes - append or consume space
		else if (c == ' ' && !insideQuotes)
		{
			// append
			if (!currentEntry.isEmpty())
			{
				arguments.append(currentEntry);
				currentEntry.clear();
			}
			// consume space
		}
		// just append
		else
			currentEntry += c;
	}
	if (!currentEntry.isEmpty())
		arguments.append(currentEntry);

	return arguments;
}

void ScriptRunnerDialog::updateUi()
{
	_pimpl->ui->command->setText(
		QString("%1 %2 %3 ").arg(_pimpl->exePath, _pimpl->ui->input->text().isEmpty() ? "" : "-i " + _pimpl->ui->input->text(), _pimpl->ui->arguments->text()));
}

void ScriptRunnerDialog::selectFile()
{
	const QString filename = QFileDialog::getOpenFileName(this, tr("Input file"), _pimpl->ui->input->text());
	if (!filename.isEmpty())
		_pimpl->ui->input->setText(filename);
	updateUi();
}
