#include "ShortcutsConfig.hpp"
#include "ui_ShortcutsConfig.h"

#include <QGridLayout>
#include <QKeySequenceEdit>
#include <QLabel>
#include <QSettings>

#include "SettingsManager.hpp"

namespace
{
const QString _configname = "Shortcuts";
}

bool ShortcutsConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const ShortcutsConfigObject &oc = static_cast<const ShortcutsConfigObject &>(o);
	return ShortcutMap == oc.ShortcutMap;
}

void ShortcutsConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(ShortcutsConfig::configName());

	for (auto const &[key, Shortcut] : ShortcutMap)
		settings.setValue(Shortcut.first, Shortcut.second);

	settings.endGroup();
}

void ShortcutsConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(ShortcutsConfig::configName());

	for (auto &[key, Shortcut] : ShortcutMap)
		Shortcut.second = settings.value(Shortcut.first, Shortcut.second).value<QKeySequence>();

	settings.endGroup();
}

class ShortcutsConfig::_ShortcutsConfig_pimpl
{
public:
	std::unique_ptr<Ui::ShortcutsConfig> ui;
};

const QString &ShortcutsConfig::configName() noexcept
{
	return _configname;
}

ShortcutsConfig::ShortcutsConfig(QWidget *parent) : SConfigWidget(nullptr, parent), _pimpl(std::make_unique<_ShortcutsConfig_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::ShortcutsConfig>();
	_pimpl->ui->setupUi(this);
}

ShortcutsConfig::~ShortcutsConfig() = default;

SConfigObject *ShortcutsConfig::config() const
{
	auto *config = new ShortcutsConfigObject();

	for (auto &[key, Shortcut] : config->ShortcutMap)
	{
		QKeySequenceEdit *keySeqEdit = qobject_cast<QKeySequenceEdit *>(_pimpl->ui->gridLayout->itemAtPosition(key + 1, 1)->widget());
		if (keySeqEdit)
			config->ShortcutMap[key].second = keySeqEdit->keySequence();
	}
	return config;
}

void ShortcutsConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const ShortcutsConfigObject *>(conf);
	if (!config)
		return;
	int rowCount = _pimpl->ui->gridLayout->rowCount();

	for (auto &[key, Shortcut] : config->ShortcutMap)
	{
		// If config is already populated with shortcuts
		if ((rowCount - 1) == config->ShortcutMap.size())
		{
			QKeySequenceEdit *keySeqEdit = qobject_cast<QKeySequenceEdit *>(_pimpl->ui->gridLayout->itemAtPosition(key + 1, 1)->widget());
			if (keySeqEdit)
				keySeqEdit->setKeySequence(Shortcut.second);
		}
		else
		{
			_pimpl->ui->gridLayout->addWidget(new QLabel(Shortcut.first, this), key + 1, 0);
			_pimpl->ui->gridLayout->addWidget(new QKeySequenceEdit(Shortcut.second, this), key + 1, 1);
		}
	}
}

void ShortcutsConfig::reset()
{
	ShortcutsConfigObject config;
	config.read();
	setConfig(&config);
}

void ShortcutsConfig::restoreDefaults()
{
	ShortcutsConfigObject config;
	setConfig(&config);
}

ShortcutsConfigObject ShortcutsConfig::getShortcutConfig()
{
	return SettingsManager::settings().settings<ShortcutsConfigObject>(configName());
}
