#include <ConsoleLogHandler.hpp>
#include <FileLogHandler.hpp>
#include <Logger.hpp>

/**
 * @file
 *
 * This file is necessary to call functions from Logs lib.
 * Thus, these functions will be integrated in the final DLL.
 */

volatile auto _FileLogHandler_log = &FileLogHandler::log;

volatile auto _ConsoleLogHandler_log = &ConsoleLogHandler::log;
