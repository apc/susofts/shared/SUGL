#include <QtGlobal>

#if defined(UIPlugins_EXPORT)
#	define SUGL_SHARED_EXPORT Q_DECL_EXPORT
#elif defined(UIPlugins_IMPORT)
#	define SUGL_SHARED_EXPORT Q_DECL_IMPORT
#endif

#ifndef SUGL_SHARED_EXPORT
#	define SUGL_SHARED_EXPORT
#endif
