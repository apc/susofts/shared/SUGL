/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

/**
 * @file.
 *
 * Stream operators for Qt basic types.
 *
 * This allows to use the standard `operator<<` function with standard C++ streams, with Qt types.
 * This may particularly be useful for logging Qt objects with the Logger class, for instance.
 *
 * The following types are supported:
 *	- QColor
 *	- QDate
 *	- QDateTime
 *	- QPoint
 *	- QPointF
 *	- QSize
 *	- QSizeF
 *	- QString
 *	- QTime
 *	- QUrl
 */

#ifndef QTSTREAMS_HPP
#define QTSTREAMS_HPP

#include <ostream>

#include "PluginsGlobal.hpp"

class QColor;
class QDate;
class QDateTime;
class QPoint;
class QPointF;
class QSize;
class QSizeF;
class QString;
class QStringRef;
class QTime;
class QUrl;

/** Stream a QColor */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QColor &color);

/** Stream a QDate */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QDate &date);

/** Stream a QDateTime */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QDateTime &datetime);

/** Stream a QPoint */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QPoint &point);

/** Stream a QPointF */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QPointF &point);

/** Stream a QSize */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QSize &size);

/** Stream a QSizeF */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QSizeF &size);

/** Stream a QString */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QString &str);

/** Stream a QStringRef */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QStringRef str);

/** Stream a QTime */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QTime &time);

/** Stream a QUrl */
SUGL_SHARED_EXPORT std::ostream &operator<<(std::ostream &stream, const QUrl &url);

#endif // QTSTREAMS_HPP
