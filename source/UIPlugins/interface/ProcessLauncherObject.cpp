#include "ProcessLauncherObject.hpp"

#include <QDir>
#include <QProcess>

#include <Logger.hpp>

#include "QtStreams.hpp"

class ProcessLauncherObject::_ProcessLauncherObject_pimpl
{
public:
	QProcess process;
	QString errorStr;
};

ProcessLauncherObject::ProcessLauncherObject(SPluginInterface *owner, QObject *parent) :
	SLaunchObject(owner, parent), _pimpl(std::make_unique<_ProcessLauncherObject_pimpl>())
{
	connect(&_pimpl->process, &QProcess::started, this, &SLaunchObject::started);
	connect(&_pimpl->process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, &ProcessLauncherObject::processFinished);
	connect(&_pimpl->process, &QProcess::errorOccurred, this, &ProcessLauncherObject::errorOccurred);
}

ProcessLauncherObject::~ProcessLauncherObject() = default;

bool ProcessLauncherObject::isRunning() const noexcept
{
	return _pimpl->process.state() == QProcess::ProcessState::Running;
}

QString ProcessLauncherObject::error() const
{
	return _pimpl->process.readAllStandardError() + _pimpl->errorStr;
}

void ProcessLauncherObject::setWorkingDirectory(const QString &dir)
{
	_pimpl->process.setWorkingDirectory(dir);
}

QString ProcessLauncherObject::workingDirectory() const
{
	return _pimpl->process.workingDirectory();
}

void ProcessLauncherObject::launch(const QString &)
{
	logInfo() << "Running:" << QDir::toNativeSeparators(_pimpl->process.program()) + ' ' + QDir::toNativeSeparators(_pimpl->process.arguments().join(' '));
	emit loading(-1, tr("Running: ") + _pimpl->process.program() + ' ' + _pimpl->process.arguments().join(' '));
	_pimpl->process.start(QIODevice::OpenModeFlag::ReadOnly);
}

void ProcessLauncherObject::stop()
{
	_pimpl->process.kill();
}

QProcess &ProcessLauncherObject::process()
{
	return _pimpl->process;
}

void ProcessLauncherObject::errorOccurred(int error)
{
	_pimpl->errorStr = tr("Error");
	switch (error)
	{
	case QProcess::ProcessError::FailedToStart:
		_pimpl->errorStr = tr("The process failed to start. The path to the executable may be wrong: ") + exepath();
		break;
	case QProcess::ProcessError::Crashed:
		_pimpl->errorStr = tr("The process crashed.");
		break;
	case QProcess::ProcessError::Timedout:
		_pimpl->errorStr = tr("The process is lost");
		break;
	case QProcess::ProcessError::WriteError:
	case QProcess::ProcessError::ReadError:
		_pimpl->errorStr = tr("Error of I/O with the process");
		break;
	case QProcess::ProcessError::UnknownError:
		_pimpl->errorStr = tr("Unknown error");
		break;
	}
	logWarning() << _pimpl->errorStr;
	emit loading(-1, _pimpl->errorStr);
}

void ProcessLauncherObject::processFinished(int exitCode, int exitStatus)
{
	logDebug() << "ProcessLauncherObject: Process" << exepath() << "finished (exitCode:" << exitCode << "; existStatus:" << exitStatus << ")";
	emit finished(exitCode == 0 && exitStatus == QProcess::ExitStatus::NormalExit);
}
