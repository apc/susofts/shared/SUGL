/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SPLUGININTERFACE_HPP
#define SPLUGININTERFACE_HPP

#include <QIcon>
#include <QtPlugin>

#include "SConfigInterface.hpp"
#include "SGraphicalInterface.hpp"
#include "SLaunchInterface.hpp"

/** String ID of the plugin */
#define SPluginInterfaceIID "cern.susoft.sugl.uiplugins.SPluginInterface/v1.0"

/**
 * Plugin interface;
 *
 * All plugins should implement this interface.
 *
 * This will be laoded by the plugin launcher. The role of the class is to provide access to the other interfaces (config, graphical, launch)
 *
 * For your plugin to be loaded, you need to use the following ID string:
 * `SPluginInterfaceIID`.
 *
 * @warning As we may have several occurences of the same plugin, all the functions here should always return a newly created object
 * on each call. The deletion of the objects shouldn't be managed by the plugin.
 */
class SPluginInterface
{
public:
	virtual ~SPluginInterface() = default;

	/** @return the name of the plugin (will be used for display in the interface) */
	virtual const QString &name() const noexcept = 0;
	/** @return the icon of the plugin (will be used for display in the interface) */
	virtual QIcon icon() const = 0;

	/** Initialize the plugin, called only once when the plugin is loaded. */
	virtual void init() = 0;
	/** Called only once when the plugin is unloaded. This method should release all objects it owns. */
	virtual void terminate() = 0;

	/** @return true if the following interface is implemented */
	virtual bool hasConfigInterface() const noexcept = 0;
	/** @return the config inerface. If there is none, it should return a null pointer */
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) = 0;
	/** @return true if the following interface is implemented */
	virtual bool hasGraphicalInterface() const noexcept = 0;
	/** @return the graphical interface */
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) = 0;
	/** @return true if the following interface is implemented */
	virtual bool hasLaunchInterface() const noexcept = 0;
	/** @return the launcher inerface. If there is none, it should return a null pointer */
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) = 0;

	/**
	 * @return file extension corresponding to the plugin.
	 * Return string should comply with format used by QFileDialog (e.g. "Text file (*.txt);;All (*)").
	 */
	virtual QString getExtensions() const noexcept = 0;
	/** @return the boolean describing if there can be only one project instance per plugin (true) or the plugin can have multiple projects (false). */
	virtual bool isMonoInstance() const noexcept = 0;
};

Q_DECLARE_INTERFACE(SPluginInterface, SPluginInterfaceIID)

#endif // SPLUGININTERFACE_HPP
