/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef PROCESSLAUNCHEROBJECT_HPP
#define PROCESSLAUNCHEROBJECT_HPP

#include <memory>

#include "SLaunchInterface.hpp"

class QProcess;

/**
 * Abstract class for managing process.
 *
 * If you inherit this class, you'll need to override the missing SLaunchObject::exepath() function.
 */
class SUGL_SHARED_EXPORT ProcessLauncherObject : public SLaunchObject
{
	Q_OBJECT

public:
	ProcessLauncherObject(SPluginInterface *owner, QObject *parent = nullptr);
	virtual ~ProcessLauncherObject() override;

	// SLaunchObject
	virtual bool isRunning() const noexcept override;
	/** This will return all the errors written by the process */
	virtual QString error() const override;

	// ProcessLauncherObject
	void setWorkingDirectory(const QString &dir);
	QString workingDirectory() const;

public slots:
	// SLaunchObject
	/** Override SLaunchObject. You should set the executable and the parameters in your implementation before calling this. */
	virtual void launch(const QString &) override;
	virtual void stop() override;

protected:
	/** @return the process that will be run (exe and args) */
	QProcess &process();

protected slots:
	virtual void processFinished(int exitCode, int exitStatus);
	virtual void errorOccurred(int error);

private:
	class _ProcessLauncherObject_pimpl;
	std::unique_ptr<_ProcessLauncherObject_pimpl> _pimpl;
};

#endif // PROCESSLAUNCHEROBJECT_HPP
