#include "SPluginLoader.hpp"

#include <algorithm>
#include <unordered_set>

#include <QCoreApplication>
#include <QDir>
#include <QPluginLoader>
#include <QSettings>

#include "SPluginInterface.hpp"

class SPluginLoader::_SPluginLoader_pimpl
{
public:
	/** the plugin directory from where to load plugins */
	QString plugindir;
	/** the Qt loaders */
	std::vector<std::unique_ptr<QPluginLoader>> plugins;
	/** A set with all plugins names */
	std::unordered_set<std::string> pluginsNames;
	/** Multimap storing all the entry points (entry point name as a key, and then classes associated with this name). */
	std::unordered_multimap<std::string, EntryPoint> entrypoints;
	/** Map storing all the scripts (plugin name as a key, and then scripts available for a plugin). */
	std::unordered_map<std::string, std::vector<ExternalScript>> scripts;

public:
	/** @return an iterator on the given plugin in _pluginsNames */
	decltype(std::begin(_SPluginLoader_pimpl::plugins)) find(const QString &pluginName)
	{
		return std::find_if(std::begin(plugins), std::end(plugins), [&pluginName](auto &p) -> bool {
			const auto *plugin = qobject_cast<SPluginInterface *>(p->instance());
			return (plugin && plugin->name() == pluginName);
		});
	}
	/** @return true if the given file is a valid plugin that has been loaded in the program */
	QString loadFile(const QString &fileName)
	{
		plugins.emplace_back(std::make_unique<QPluginLoader>(fileName));
		auto *plugin = qobject_cast<SPluginInterface *>(plugins.back()->instance());
		if (plugin && pluginsNames.insert(plugin->name().toStdString()).second)
			plugin->init();
		else
		{
			plugins.pop_back();
			return "";
		}
		return plugin->name();
	}
};

SPluginLoader &SPluginLoader::getPluginLoader()
{
	static SPluginLoader instance;
	return instance;
}

SPluginLoader::SPluginLoader(QObject *parent) : QObject(parent), _pimpl(std::make_unique<_SPluginLoader_pimpl>())
{
	_pimpl->plugindir = QCoreApplication::instance()->applicationDirPath() + "/plugins";
}

SPluginLoader::~SPluginLoader() = default;

const QString &SPluginLoader::pluginDir() const noexcept
{
	return _pimpl->plugindir;
}

void SPluginLoader::setPluginDir(QString plugindir) noexcept
{
	_pimpl->plugindir = std::move(plugindir);
}

size_t SPluginLoader::size() const noexcept
{
	return _pimpl->plugins.size();
}

bool SPluginLoader::has(const QString &pluginName)
{
	return _pimpl->pluginsNames.find(pluginName.toStdString()) != std::end(_pimpl->pluginsNames);
}

void SPluginLoader::load()
{
	unload();
	QDir pluginsDir(_pimpl->plugindir);
	const auto entryList = pluginsDir.entryList(QDir::Files);

	for (const auto &fileName : entryList)
	{
		QString name = _pimpl->loadFile(pluginsDir.absoluteFilePath(fileName));
		if (!name.isEmpty())
			emit pluginLoaded(name);
	}
}

bool SPluginLoader::load(const QString &pluginName)
{
	unload(pluginName);
	QDir pluginsDir(_pimpl->plugindir);
	const auto entryList = pluginsDir.entryList({'*' + pluginName + '*'}, QDir::Files);

	for (const auto &fileName : entryList)
	{
		QString name = _pimpl->loadFile(pluginsDir.absoluteFilePath(fileName));
		if (!name.isEmpty())
		{
			emit pluginLoaded(name);
			return true;
		}
	}
	return false;
}

void SPluginLoader::unload()
{
	for (auto &p : _pimpl->plugins)
	{
		auto *plugin = qobject_cast<SPluginInterface *>(p->instance());
		if (plugin)
			plugin->terminate();
		p->unload();
	}
	_pimpl->plugins.clear();
	const auto copy = _pimpl->pluginsNames;
	_pimpl->pluginsNames.clear();
	for (const auto &n : copy)
		emit pluginUnloaded(QString::fromStdString(n));
}

bool SPluginLoader::unload(const QString &pluginName)
{
	auto it = _pimpl->find(pluginName);

	if (it == std::end(_pimpl->plugins))
		return false;

	auto *plugin = qobject_cast<SPluginInterface *>((*it)->instance());
	if (plugin)
		plugin->terminate();
	bool success = (*it)->unload();
	_pimpl->plugins.erase(it);
	_pimpl->pluginsNames.erase(pluginName.toStdString());
	removePluginEntryPoints(pluginName);
	emit pluginUnloaded(pluginName);
	return success;
}

SPluginInterface *SPluginLoader::getPlugin(const QString &pluginName)
{
	auto it = _pimpl->find(pluginName);
	if (it == std::end(_pimpl->plugins))
		return nullptr;
	return qobject_cast<SPluginInterface *>(it->get()->instance());
}

std::vector<SPluginInterface *> SPluginLoader::plugins() const
{
	std::vector<SPluginInterface *> vect;
	SPluginInterface *plugin = nullptr;

	for (auto &p : _pimpl->plugins)
	{
		plugin = qobject_cast<SPluginInterface *>(p->instance());
		if (plugin)
			vect.push_back(plugin);
	}
	return vect;
}

std::unordered_map<std::string, SPluginInterface *> SPluginLoader::pluginsMap()
{
	std::unordered_map<std::string, SPluginInterface *> map;
	SPluginInterface *plugin = nullptr;

	for (auto &p : _pimpl->plugins)
	{
		plugin = qobject_cast<SPluginInterface *>(p->instance());
		if (plugin)
			map.emplace(plugin->name().toStdString(), plugin);
	}
	return map;
}

const std::vector<std::unique_ptr<QPluginLoader>> &SPluginLoader::loaders() const
{
	return _pimpl->plugins;
}

void SPluginLoader::registerEntryPoint(QString entryname, EntryPoint entrypoint)
{
	if (entryname.isEmpty() || !entrypoint.meta)
		return;

	auto it = std::find_if(_pimpl->entrypoints.begin(), _pimpl->entrypoints.end(), [entrypoint](const auto &pair) -> bool { return pair.second.meta == entrypoint.meta; });
	if (it != _pimpl->entrypoints.end())
		return;

	_pimpl->entrypoints.emplace(entryname.toStdString(), std::move(entrypoint));
}

std::vector<EntryPoint> SPluginLoader::getEntryPoints(const QString &entryname) const
{
	std::vector<EntryPoint> entrypoints;
	auto [itbegin, itend] = _pimpl->entrypoints.equal_range(entryname.toStdString());
	for (; itbegin != itend; itbegin++)
		entrypoints.push_back(itbegin->second);
	return entrypoints;
}

const std::vector<ExternalScript> &SPluginLoader::getScripts(const QString &pluginName) const
{
	return _pimpl->scripts[pluginName.toStdString()];
}

void SPluginLoader::removePluginEntryPoints(const QString &pluginName)
{
	auto iter = _pimpl->entrypoints.begin();
	while (iter != _pimpl->entrypoints.end())
	{
		if (!iter->second.plugin)
			continue;
		if (iter->second.plugin->name() == pluginName)
			iter = _pimpl->entrypoints.erase(iter);
		else
			iter++;
	}
}

void SPluginLoader::loadScripts()
{
	const QString &dirScript = QCoreApplication::instance()->applicationDirPath() + "/scripts/";
	QSettings scriptsIni(dirScript + "scripts.ini", QSettings::IniFormat);
	QStringList scripts = scriptsIni.childGroups();

	for (const auto &scriptName : scripts)
	{
		scriptsIni.beginGroup(scriptName);
		// Create the ExternalScript entry and populate its data
		ExternalScript scriptEntry;
		scriptEntry.name = scriptName;
		QStringList childKeys = scriptsIni.childKeys();
		for (const auto &childKey : childKeys)
		{
			auto childValue = scriptsIni.value(childKey);
			if (childKey == "path")
				scriptEntry.path = QDir::toNativeSeparators(dirScript + childValue.toString());
			else if (childKey == "help")
				scriptEntry.help = QString(childValue.toByteArray()).remove(0, 1); // dangling b removal
			else if (childKey == "alias")
				scriptEntry.alias = childValue.toStringList().join(','); // join in case there are commas
			else if (childKey == "defaultarguments")
				scriptEntry.defaultArguments = childValue.toString();
			else if (childKey == "description")
				scriptEntry.description = childValue.toStringList().join(' '); // join in case there are commas
			else if (childKey == "documentation")
				scriptEntry.documentation = childValue.toString();
			else if (childKey == "supportedplugins")
				scriptEntry.supportedPlugins = childValue.toStringList();
			else if (childKey == "expectedinputextensions")
				scriptEntry.expectedInputExtensions = childValue.toStringList();
		}

		// Assign the ExternalScript to plugins
		for (const auto &suppPlug : scriptEntry.supportedPlugins)
			_pimpl->scripts[suppPlug.toStdString()].push_back(scriptEntry);

		scriptsIni.endGroup();
	}
}
