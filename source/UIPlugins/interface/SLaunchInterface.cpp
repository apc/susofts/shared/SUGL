#include "SLaunchInterface.hpp"

class SLaunchObject::_SLaunchObject_pimpl
{
public:
	/** Plugins that owns this interface */
	SPluginInterface *owner;
};

SLaunchObject::SLaunchObject(SPluginInterface *owner, QObject *parent) : QObject(parent), _pimpl(std::make_unique<_SLaunchObject_pimpl>())
{
	_pimpl->owner = owner;
}

SLaunchObject::~SLaunchObject() = default;

const SPluginInterface *SLaunchObject::owner() const noexcept
{
	return _pimpl->owner;
}
