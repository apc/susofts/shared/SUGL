/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SLAUNCHINTERFACE_HPP
#define SLAUNCHINTERFACE_HPP

#include <memory>

#include <QObject>

#include "PluginsGlobal.hpp"

class SPluginInterface;

/**
 * Stoppable class to manage threads.
 *
 * This class can be inheritted to manage a thread and use it in a SLaunchObject.
 * It has been created from the example here: https://thispointer.com/c11-how-to-stop-or-terminate-a-thread/
 *
 * You just have to implement the run() function. This function should look like this:
 *
 * @code{.cpp}
 * class MyThread : public SLaunchThread
 * {
 *	void run() override
 *	{
 *		while (!stopRequested())
 *			; //do sth
 *	}
 * };
 * @endcode
 */
class SUGL_SHARED_EXPORT SLaunchThread
{
public:
	virtual ~SLaunchThread() = default;

	/**
	 * This is where the logic of your method should go.
	 * This is the method that should be launched from a thread:
	 *
	 * @code{.cpp}
	 *	MyThread t;
	 *	std::thread th([&t]() -> void { t.run(); });
	 * @endcode
	 */
	virtual void run() = 0;

	/** Launch the run() method. */
	void operator()() { run(); }
	/**
	 * Asks the run method to stop, which will stop the thread if run from a thread.
	 *
	 * @code{.cpp}
	 *	t.requestStop();
	 * @endcode
	 */
	void requestStop() { _shouldStop = true; }
	/**
	 * Tells if a stop has been requested via requestStop().
	 * @note You don't have to use this method internally, you can use directly _shouldStop if needed.
	 */
	bool stopRequested() const { return _shouldStop; }

protected:
	/** If true,  the thread should stop. */
	bool _shouldStop = false;
};

/**
 * The object manages a plugin executable.
 *
 * The objects contains a path to the executable and is able to launch and stop its process.
 *
 * On start, finish, and during launch, it emits relevant status signals.
 */
class SUGL_SHARED_EXPORT SLaunchObject : public QObject
{
	Q_OBJECT

public:
	SLaunchObject(SPluginInterface *owner, QObject *parent = nullptr);
	virtual ~SLaunchObject() override;

	/** @return the path to the executable to run */
	virtual QString exepath() const = 0;

	/** @return true if the task is currently running, false if it is stopped, or not launch yet */
	virtual bool isRunning() const noexcept = 0;
	/** @return an error message in case the finished() signal emitted false */
	virtual QString error() const = 0;

	/** @return the name of the plugin (will be used for diplay in the interface) */
	virtual const SPluginInterface *owner() const noexcept;
	virtual SPluginInterface *owner() noexcept { return const_cast<SPluginInterface *>(static_cast<const SLaunchObject *>(this)->owner()); }

public slots:
	/**
	 * Launch the executable. This should be done in another thread.
	 * @param file the file to give as a paramter to the executable.
	 *
	 * Generally, the file comes from the interface, or the command line argument.
	 */
	virtual void launch(const QString &file) = 0;
	/** stop the executable */
	virtual void stop() = 0;

signals:
	/** emitted on launch */
	void started();
	/** emitted during launch. -1 is emitted if we don't know the advancement. Otherwise, it should send a percentage. The QString emitted is an eventual message to show. */
	void loading(int, QString = "");
	/** emitted when the run is finished. Emit true if it correctly finished, false otherwise */
	void finished(bool);

private:
	class _SLaunchObject_pimpl;
	std::unique_ptr<_SLaunchObject_pimpl> _pimpl;
};

#endif // SLAUNCHINTERFACE_HPP
