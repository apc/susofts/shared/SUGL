#include "SConfigInterface.hpp"

#include <QSettings>

bool SApplicationConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const SApplicationConfigObject &oc = static_cast<const SApplicationConfigObject &>(o);
	return autoUpdate == oc.autoUpdate;
}

void SApplicationConfigObject::write() const
{
	QSettings settings;
	settings.setValue("autoUpdate", (int)autoUpdate);
}

void SApplicationConfigObject::read()
{
	QSettings settings;
	autoUpdate = (FileDetectionType)settings.value("autoUpdate", (int)FileDetectionType::ON_FOCUS).toInt();
}

class SConfigWidget::_SConfigWidget_pimpl
{
public:
	/** Plugins that owns this interface */
	SPluginInterface *owner;
};

SConfigWidget::SConfigWidget(SPluginInterface *owner, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_SConfigWidget_pimpl>())
{
	_pimpl->owner = owner;
}

SConfigWidget::~SConfigWidget() = default;

const SPluginInterface *SConfigWidget::owner() const noexcept
{
	return _pimpl->owner;
}
