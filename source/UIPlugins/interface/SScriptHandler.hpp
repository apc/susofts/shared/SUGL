/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SSCRIPTHANDLER_HPP
#define SSCRIPTHANDLER_HPP

#include <memory>

#include <QAction>
#include <QList>
#include <QWidget>

#include "editors/tab/TabEditorWidget.hpp"
#include "editors/text/TextEditorWidget.hpp"
#include "interface/SPluginInterface.hpp"

struct ExternalScript;
class SGraphicalWidget;
class SPluginInterface;

/**
 * This class handles the external scripts related to a given SGraphicalWidget (and more precisely to its plugin). After running a script it allows
 * executing some specific actions, each taking one argument:
 *     - RUN - runs some software/external process,
 *     - RELOAD - reloads a file opened in a project, if not provided it will open it,
 *     - OPEN - opens a file as part of a project, if a file is already opened it should be reloaded.
 * The actions available for a given project can be obtained with @getScriptActions() and should be set inside @setupScripts() method.
 *
 * This class should be used by inheriting both from it and from some SGraphicalWidget that should be also passed to this class as a parent
 * argument in a constructor. Since no class can inherit from two Qt classes, this allows to use all Qt functionalities and keep
 * SGraphicalWidget and Script domains separate. Moreover, this class makes sense only for top-SGraphicalWidget of each project since
 * scripts can manipulate all the files of a project.
 *
 * When inherited from this class getPathForExtension(), scriptReload(), and setupScripts() methods need to be implemented. The reason for
 * not implementing it directly here is that the methods to get the access to paths should be protected/private. Moreover, the handling of these methods
 * will be different based on the SGraphicalWidget(or STabInterface) provided. Lastly, this class should complement other SGraphicalWidgets anyway and should
 * not function alone.
 */
class SUGL_SHARED_EXPORT SScriptHandler
{
public:
	SScriptHandler(QString pluginName, SGraphicalWidget *parent);
	virtual ~SScriptHandler();

	QList<QAction *> getScriptActions();

protected:
	/** @return was succesful. To be overriden to support part-of-project open. If not succesful children classes may try to open it in any desired way.  */
	virtual bool scriptOpen(const QString &path);
	/** Make sure that all processes finish before the scriptRun method exits. */
	virtual void scriptRun(const QString &path);
	virtual void scriptReload(const QString &path) = 0;

	virtual QString getPathForExtension(const QString &) const = 0;
	virtual void setupScripts() = 0;

private:
	void startRunDialog(const ExternalScript &script);
	void executeActions(const QString &scriptOutput);
	void createScriptActions();

private:
	/** pimpl */
	class _SScriptHandler_pimpl;
	std::unique_ptr<_SScriptHandler_pimpl> _pimpl;
};

#endif // SSCRIPTHANDLER_HPP
