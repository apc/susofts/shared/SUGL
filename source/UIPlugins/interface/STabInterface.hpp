/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef STABINTERFACE_HPP
#define STABINTERFACE_HPP

#include <functional>
#include <memory>

#include "PluginsGlobal.hpp"
#include "SGraphicalInterface.hpp"

class QTabWidget;

/**
 * Abstract class for a graphical plugin that uses a tabwidget as the main widget.
 *
 * To use this class, you should override all the methods from SGraphicalWidget that are not overriden here.
 *
 * All the tab handling logic of this class is implemented in the tabChanged() and reloadTab() methods (see its documentation).
 *
 * In order to support opening some external files (non-project files) getWidgetForPath() should be implemented and openOutputFile() should be used
 * along with this functionality.
 */
class SUGL_SHARED_EXPORT STabInterface : public SGraphicalWidget
{
	Q_OBJECT

public:
	STabInterface(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~STabInterface() override;

	// SGraphicalWidget
	virtual QString getPath() const override;
	virtual bool isModified() const noexcept override;
	virtual ShareablePointsList getContent() const override;
	virtual QWidget *statusWidget() override;
	virtual QWidget *helpWidget() override;
	virtual const std::unordered_multimap<Menus, QAction *> &actions() override;
	virtual QToolBar *toolbar() override;

public slots:
	// SGraphicalWidget
	virtual void clearContent() override;
	virtual void updateContent() override;
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void paste() override;
	virtual void copy() const override;
	virtual void cut() const override;
	virtual void undo() override;
	virtual void redo() override;
	virtual void isModified(bool modified) noexcept override;
	virtual void retranslate() override;

protected:
	// SGraphicalWidget
	virtual bool _save(const QString &path) override;
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

	// STabInterface
	/** @return the tab widget */
	QTabWidget *tab() noexcept;
	/** @return the tab widget */
	const QTabWidget *tab() const noexcept;
	/** Change the description mode. */
	void sameDescriptionMode(bool mode) noexcept;
	/**
	 * Tell if we are in the same description mode.
	 *
	 * In the same description mode, all the tabs describe the same object. Thus, when we change tab, we update the new tab with the content of the previous one.
	 * When this mode is disabled, it means that different tabs describe different objects, thus no connections are made between tabs.
	 */
	bool sameDescriptionMode() const noexcept;
	/** Should return SGraphicalWidget suitable for opening a given absolute path */
	virtual SGraphicalWidget *getWidgetForPath(const QString &) { return nullptr; }
	// Provided a widget and a file path, opens an output file in a separate tab
	virtual SGraphicalWidget *openOutputFile(SGraphicalWidget *widget, const QString &file, bool checkForOpened = false);
	/** Reloads a tab at a given index */
	virtual void reloadTab(const QString &path);
	/** Returns the index at which the opened file is located if it matches either a path or an extension */
	virtual int findFileIndex(const QString &pathORextention) const;
	/** For the given extension returns a file matching it, it if exists */
	virtual const QString getMatchingPath(const QString &extension) const;
	/**
	 * Run the given function in a safely signal disabled environment.
	 *
	 * If the function throws, the signals are correctly put back and the exception re-thrown.
	 */
	template<class T>
	T runWithNoSignals(std::function<T()> func);

protected slots:
	/**
	 * Called when the current tab changes.
	 *
	 * if the new tab is a SGraphicalWidget, it will automatically receive the current ShareablePointsList.
	 * All its signals will aslo be connected to the signals of this class, so the plugin manager will get the right signals.
	 */
	virtual void tabChanged(int pos);
	// returns tab index to which the change happened
	virtual int fileChangeRequested(const QString &path, int position = 0);

private:
	class _STabInterface_pimpl;
	std::unique_ptr<_STabInterface_pimpl> _pimpl;
};

template<class T>
inline T STabInterface::runWithNoSignals(std::function<T()> func)
{
	blockSignals(true);
	try
	{
		T tmp = func();
		blockSignals(false);
		return tmp;
	}
	catch (...)
	{
		blockSignals(false);
		throw;
	}
}

/** Specialization for void functions. */
template<>
inline void STabInterface::runWithNoSignals<void>(std::function<void()> func)
{
	blockSignals(true);
	try
	{
		func();
		blockSignals(false);
	}
	catch (...)
	{
		blockSignals(false);
		throw;
	}
}

#endif // STABINTERFACE_HPP
