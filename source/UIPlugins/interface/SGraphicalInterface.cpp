#include "SGraphicalInterface.hpp"

#include <algorithm>

#include <QAction>
#include <QDateTime>
#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QFocusEvent>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QWidget>

#include <Logger.hpp>

#include "QtStreams.hpp"
#include "SConfigInterface.hpp"
#include "utils/SettingsManager.hpp"
#include "utils/ShortcutsConfig.hpp"

namespace
{
SApplicationConfigObject::FileDetectionType getFileUpdateConfig()
{
	const auto config = SettingsManager::settings().settings<SApplicationConfigObject>(SettingsManager::settings().innerSettings("appName"));
	return config.autoUpdate;
}

void clearFileSystemWatcher(QFileSystemWatcher &watcher)
{
	const auto &files = watcher.files();
	if (!files.isEmpty())
		watcher.removePaths(files);
}
} // namespace

class SGraphicalWidget::_SGraphicalWidget_pimpl
{
public:
	/** modification state */
	bool modified = false;
	/** Plugins that owns this interface */
	SPluginInterface *owner = nullptr;
	// widgets
	QWidget *statusWidget = nullptr;
	QWidget *helpWidget = nullptr;
	std::unordered_multimap<Menus, QAction *> actions;
	QToolBar *toolBar = nullptr;
	// reload file
	QAction *actionReload = nullptr;
	// file watcher
	bool watcherEnabled = false;
	QFileSystemWatcher watcher;
	bool reloading = false;
	qint64 lastSaved = 0;
	QString path;
};

SGraphicalWidget::SGraphicalWidget(SPluginInterface *owner, QWidget *parent) : QWidget(parent), _pimpl(std::make_unique<_SGraphicalWidget_pimpl>())
{
	_pimpl->owner = owner;

	// reload
	ShortcutsConfigObject shortcutSettings = ShortcutsConfig::getShortcutConfig();
	_pimpl->actionReload = new QAction(QIcon(":/actions/refresh"), shortcutSettings.getShortcutName(ShortcutsConfigObject::RELOADFROMDISK), this);
	_pimpl->actionReload->setShortcut(shortcutSettings.getShortcutSequence(ShortcutsConfigObject::RELOADFROMDISK));
	_pimpl->actionReload->setToolTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::RELOADFROMDISK));
	_pimpl->actionReload->setStatusTip(shortcutSettings.getShortcutName(ShortcutsConfigObject::RELOADFROMDISK));
	connect(_pimpl->actionReload, &QAction::triggered, this, &SGraphicalWidget::reload);

	// settings
	connect(&SettingsManager::settings(), &SettingsManager::settingsChanged, this, &SGraphicalWidget::updateUi);
	// external file updates
	updateUi(SettingsManager::settings().innerSettings("appName"));
	connect(&_pimpl->watcher, &QFileSystemWatcher::fileChanged, this, &SGraphicalWidget::fileExternallyModified);
}

SGraphicalWidget::~SGraphicalWidget() = default;

bool SGraphicalWidget::eventFilter(QObject *watched, QEvent *event)
{
	if (event->type() == QEvent::Type::FocusIn)
		focusInEvent(static_cast<QFocusEvent *>(event));
	else if (event->type() == QEvent::Type::FocusOut)
		focusOutEvent(static_cast<QFocusEvent *>(event));

	return QWidget::eventFilter(watched, event);
}

bool SGraphicalWidget::save(const QString &path)
{
	clearFileSystemWatcher(_pimpl->watcher);

	if (!_save(path))
		return false;

	logDebug() << "SGraphicalWidget: Save to file" << path;
	_pimpl->path = path;
	isModified(false);
	_pimpl->lastSaved = QFileInfo(getPath()).lastModified().toSecsSinceEpoch();
	if (_pimpl->watcherEnabled && !getPath().isEmpty())
		_pimpl->watcher.addPath(getPath());
	_pimpl->actionReload->setEnabled(true);
	return true;
}

bool SGraphicalWidget::open(const QString &path)
{
	clearFileSystemWatcher(_pimpl->watcher);

	if (!_open(path))
		return false;

	logDebug() << "SGraphicalWidget: Open file" << path;
	_pimpl->path = path;
	isModified(false);
	_pimpl->lastSaved = QFileInfo(getPath()).lastModified().toSecsSinceEpoch();
	if (_pimpl->watcherEnabled && !getPath().isEmpty())
		_pimpl->watcher.addPath(getPath());
	_pimpl->actionReload->setEnabled(true);
	return true;
}

bool SGraphicalWidget::reload()
{
	return open(getPath());
}

void SGraphicalWidget::newEmpty()
{
	clearFileSystemWatcher(_pimpl->watcher);
	_pimpl->path.clear();
	_pimpl->lastSaved = 0;

	_newEmpty();

	isModified(true);
	_pimpl->actionReload->setEnabled(false);
}

QString SGraphicalWidget::getPath() const
{
	return _pimpl->path;
}

bool SGraphicalWidget::isModified() const noexcept
{
	return _pimpl->modified;
}

const SPluginInterface *SGraphicalWidget::owner() const noexcept
{
	return _pimpl->owner;
}

QWidget *SGraphicalWidget::statusWidget()
{
	return _pimpl->statusWidget;
}

QWidget *SGraphicalWidget::helpWidget()
{
	return _pimpl->helpWidget;
}

const std::unordered_multimap<SGraphicalWidget::Menus, QAction *> &SGraphicalWidget::actions()
{
	return _pimpl->actions;
}

QToolBar *SGraphicalWidget::toolbar()
{
	return _pimpl->toolBar;
}

void SGraphicalWidget::isModified(bool modified) noexcept
{
	_pimpl->modified = modified;
	emit hasChanged();
}

void SGraphicalWidget::updateUi(const QString &configname)
{
	if (configname != SettingsManager::settings().innerSettings("appName"))
		return;
	SApplicationConfigObject::FileDetectionType autoUpdate = getFileUpdateConfig();

	_pimpl->watcher.blockSignals(
		autoUpdate == SApplicationConfigObject::FileDetectionType::DISABLED || (autoUpdate == SApplicationConfigObject::FileDetectionType::ON_FOCUS && !isVisible()));
}

void SGraphicalWidget::focusInEvent(QFocusEvent *event)
{
	QWidget::focusInEvent(event);

	if (_pimpl->reloading || getFileUpdateConfig() != SApplicationConfigObject::FileDetectionType::ON_FOCUS)
		return;

	fileExternallyModified(getPath());
	_pimpl->watcher.blockSignals(!_pimpl->watcherEnabled);
}

void SGraphicalWidget::focusOutEvent(QFocusEvent *event)
{
	QWidget::focusOutEvent(event);

	if (getFileUpdateConfig() == SApplicationConfigObject::FileDetectionType::ON_FOCUS)
		_pimpl->watcher.blockSignals(true);
}

void SGraphicalWidget::setWidget(QWidget *w)
{
	if (!layout())
	{
		auto *layout = new QVBoxLayout(this);
		layout->setSpacing(0);
		layout->setContentsMargins(0, 0, 0, 0);
		setLayout(layout);
	}
	delete layout()->takeAt(0);
	layout()->addWidget(w);
	setFocusProxy(w);
}

void SGraphicalWidget::setStatusWidget(QWidget *w)
{
	delete _pimpl->statusWidget;
	_pimpl->statusWidget = w;
	emit statusWidgetChanged(_pimpl->statusWidget);
}

void SGraphicalWidget::setHelpWidget(QWidget *w)
{
	delete _pimpl->helpWidget;
	_pimpl->helpWidget = w;
	emit helpWidgetChanged(_pimpl->helpWidget);
}

void SGraphicalWidget::addAction(Menus m, QAction *a)
{
	_pimpl->actions.emplace(m, a);
	emit actionsChanged();
}

void SGraphicalWidget::clearActions()
{
	for (auto &[menu, action] : _pimpl->actions)
		delete action;
	_pimpl->actions.clear();
	emit actionsChanged();
}

void SGraphicalWidget::setToolBar(QToolBar *t)
{
	delete _pimpl->toolBar;
	_pimpl->toolBar = t;
	emit toolbarChanged(_pimpl->toolBar);
}

void SGraphicalWidget::enablefileWatcher(bool enabled)
{
	_pimpl->watcherEnabled = enabled;
	if (_pimpl->watcherEnabled)
	{
		if (!getPath().isEmpty())
			_pimpl->watcher.addPath(getPath());
		addAction(Menus::file, _pimpl->actionReload);
	}
	else
	{
		clearFileSystemWatcher(_pimpl->watcher);
		// we need to remove the reload action
		const auto [itfirst, itlast] = _pimpl->actions.equal_range(Menus::file);
		const auto it = std::find_if(itfirst, itlast, [this](const auto &p) -> bool { return p.second == _pimpl->actionReload; });
		if (it != itlast)
			_pimpl->actions.erase(it);
	}
}

bool SGraphicalWidget::enablefileWatcher() const noexcept
{
	return _pimpl->watcherEnabled;
}

void SGraphicalWidget::installEventFilterAll(QObject *eventFilter, QWidget *child)
{
	if (!child || !eventFilter)
		return;

	child->installEventFilter(eventFilter);
	const auto &children = child->children();
	QWidget *w = nullptr;
	for (auto &c : children)
	{
		if (w = qobject_cast<QWidget *>(c))
			installEventFilterAll(eventFilter, w);
	}
}

void SGraphicalWidget::fileExternallyModified(const QString &path)
{
	if (_pimpl->reloading || getFileUpdateConfig() == SApplicationConfigObject::FileDetectionType::DISABLED || path != getPath()
		|| QFileInfo(getPath()).lastModified().toSecsSinceEpoch() <= _pimpl->lastSaved)
		return;
	if (!_pimpl->watcherEnabled && getFileUpdateConfig() != SApplicationConfigObject::FileDetectionType::ON_FOCUS)
		return;

	if (!_pimpl->watcherEnabled) // we are on focus but we don't watch, we call parents until root or another watch the file
	{
		QWidget *prt = this;
		SGraphicalWidget *sgw = nullptr;
		while (prt->parentWidget())
		{
			prt = prt->parentWidget();
			sgw = qobject_cast<SGraphicalWidget *>(prt);
			if (sgw)
			{
				sgw->fileExternallyModified(path);
				return;
			}
		}
		return;
	}

	_pimpl->reloading = true;
	// file has been modified from outside of the app
	if (_pimpl->watcher.files().contains(getPath()))
	{
		QMessageBox::StandardButton answer = QMessageBox::StandardButton::Yes;
		if (getFileUpdateConfig() != SApplicationConfigObject::FileDetectionType::SILENT)
			answer = QMessageBox::question(this, tr("Reload"), QString("\"%1\"\n\n%2").arg(path, tr("This file has been modified by another program.\nDo you want to reload it?")));
		if (answer == QMessageBox::StandardButton::Yes)
		{
			open(path);
			_pimpl->reloading = false;
			return;
		}
	}

	// we now are in a state where file is not synced with disk (didn't reload or file has been moved or deleted)
	clearFileSystemWatcher(_pimpl->watcher);
	isModified(true);
	_pimpl->reloading = false;
	return;
}
