#include "SScriptHandler.hpp"

#include <QAction>
#include <QEventLoop>
#include <QFileInfo>
#include <QFileSystemModel>
#include <QProcess>

#include <Logger.hpp>

#include "QtStreams.hpp"
#include "interface/SGraphicalInterface.hpp"
#include "interface/SPluginInterface.hpp"
#include "interface/SPluginLoader.hpp"
#include "utils/ProjectManager.hpp"
#include "utils/ScriptRunnerDialog.hpp"

class SScriptHandler::_SScriptHandler_pimpl
{
public:
	_SScriptHandler_pimpl(const QString pluginName, SGraphicalWidget *parent) : pluginName(pluginName), parent(parent) {}

public:
	QList<QAction *> externalScriptActions; /// nonowning, SGraphicalWidget parent is owning
	const QString pluginName;
	SGraphicalWidget *parent = nullptr; // nonowning
};

SScriptHandler::SScriptHandler(QString pluginName, SGraphicalWidget *parent) : _pimpl(std::make_unique<_SScriptHandler_pimpl>(pluginName, parent))
{
	createScriptActions();
}
SScriptHandler::~SScriptHandler() = default;

QList<QAction *> SScriptHandler::getScriptActions()
{
	return _pimpl->externalScriptActions;
}

void SScriptHandler::startRunDialog(const ExternalScript &script)
{
	// Based on extension and type of parent widget find suitable files for argument
	const QStringList &extensions = script.expectedInputExtensions;
	QString selectedInputFile;
	QStringList arguments;
	if (!extensions.isEmpty())
	{
		for (const auto &extension : extensions)
		{
			selectedInputFile = getPathForExtension(extension);
			if (!selectedInputFile.isEmpty())
				break;
		}
	}
	// If nothing perfectly matching was found
	if (selectedInputFile.isEmpty())
		selectedInputFile = _pimpl->parent->getPath();

	ScriptRunnerDialog *runs = new ScriptRunnerDialog(script, selectedInputFile, _pimpl->parent);
	runs->connect(runs, &ScriptRunnerDialog::finished, runs, [runs, this](int) {
		executeActions(runs->getOutput());
		runs->deleteLater();
	});
	runs->show();
}

void SScriptHandler::createScriptActions()
{
	const std::vector<ExternalScript> scripts = SPluginLoader::getPluginLoader().getScripts(_pimpl->pluginName);
	for (const auto &script : scripts)
	{
		QFileInfo fileInfo(script.path);
		std::unique_ptr<QAction> externalAction = std::make_unique<QAction>(script.alias, _pimpl->parent);
		QFileSystemModel model;
		// icon resize to optimization QMenu Script speed
		QIcon icon = model.fileIcon(model.index(fileInfo.absoluteFilePath()));
		QPixmap pixmap = icon.pixmap(icon.availableSizes().first());
		QPixmap resizedPixmap = pixmap.scaled(16, 16, Qt::KeepAspectRatio, Qt::SmoothTransformation);
		externalAction->setIcon(resizedPixmap);
		externalAction->setToolTip(script.description);
		QObject::connect(externalAction.get(), &QAction::triggered, [this, script]() -> void { startRunDialog(script); });

		_pimpl->externalScriptActions.append(externalAction.release());
	}
}

void SScriptHandler::executeActions(const QString &scriptOutput)
{
	QStringList lines = scriptOutput.split(QRegExp("[\r\n]"), QString::SkipEmptyParts);
	int index = lines.lastIndexOf("ACTIONS:");
	if (index < 0)
	{
		logInfo() << "No actions were listed to be executed!";
		return;
	}

	QStringList::const_iterator linesIter = lines.begin() + index + 1;
	while (linesIter != lines.constEnd())
	{
		const QString &line = QDir::fromNativeSeparators((*linesIter));
		if (line.startsWith("OPEN"))
			scriptOpen(line.right(line.size() - 5));
		else if (line.startsWith("RUN"))
			scriptRun(line.right(line.size() - 4));
		else if (line.startsWith("RELOAD"))
			scriptReload(line.right(line.size() - 7));
		linesIter++;
	}
}

bool SScriptHandler::scriptOpen(const QString &path)
{
	auto &pm = ProjectManager::getProjectManager();
	// this approach is not ideal and there is some redundancy
	// i.e. x2 calls to findPLuginForFile for a single open
	if (!pm.findPluginForFile(path, false)) // if not a plugin, go back to TabEditorScriptWidget::scriptOpen and open a new tab as part of the project
		return false;
	pm.openFromPath(path); // otherwise open an entirely new project
	return true;
}

void SScriptHandler::scriptRun(const QString &path)
{
	// internal run
	auto &pm = ProjectManager::getProjectManager();
	auto *plugin = pm.findPluginForFile(path, false);
	if (plugin) // open/switch and run the project
	{
		pm.openProject(path, plugin); // open or switch
		auto *project = pm.getCurrentWidget();
		auto *launcher = plugin->launchInterface(project);
		QEventLoop loop;
		// Precaution for blocking event loop when there is no need
		bool earlyFired = false;
		bool status = true;
		loop.connect(launcher, &SLaunchObject::finished, [&earlyFired, &status](bool s) {
			status = s;
			earlyFired = true;
		});
		loop.connect(launcher, &SLaunchObject::finished, &loop, &QEventLoop::quit);
		loop.connect(launcher, &SLaunchObject::finished, project, &SGraphicalWidget::runFinished);
		pm.runProject(launcher, path);
		if (!earlyFired)
			loop.exec();
		else
			project->runFinished(status);
	}
	// external run
	else
	{
		QProcess externalProcess;
		externalProcess.setProgram(path);
		externalProcess.start();
		externalProcess.waitForFinished(10000); // sets current thread to sleep and waits for externalProcess finish
	}
}
