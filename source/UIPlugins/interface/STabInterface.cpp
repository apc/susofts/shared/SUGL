#include "STabInterface.hpp"

#include <list>

#include <QDateTime>
#include <QFileInfo>
#include <QTabBar>
#include <QTabWidget>

#include <Logger.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>

#include "QtStreams.hpp"

namespace
{
/**
 * Call the given function on the tab widget.
 * Function is only called if the tab is a SGraphicalWidget. The tab selected is the current tab, or the tab at the given position.
 */
void callOnTab(const std::function<void(SGraphicalWidget *)> &f, QTabWidget *tab, int position = -1)
{
	auto *graph = position < 0 ? qobject_cast<SGraphicalWidget *>(tab->currentWidget()) : qobject_cast<SGraphicalWidget *>(tab->widget(position));
	if (graph)
		f(graph);
}

/** Same as callOnTab() but with a return value. */
template<class Ret>
Ret callOnTab(const std::function<Ret(SGraphicalWidget *)> &f, QTabWidget *tab, Ret defaultReturn, int position = -1)
{
	auto *graph = position < 0 ? qobject_cast<SGraphicalWidget *>(tab->currentWidget()) : qobject_cast<SGraphicalWidget *>(tab->widget(position));
	if (graph)
		return f(graph);
	return defaultReturn;
}

class _StabInterface_QTabWidget : public QTabWidget
{
	Q_OBJECT
public:
	_StabInterface_QTabWidget(QWidget *parent = nullptr) : QTabWidget(parent) {}

protected:
	virtual void tabInserted(int index) override
	{
		QTabWidget::tabInserted(index);
		auto *tab = qobject_cast<SGraphicalWidget *>(widget(index));
		if (tab)
			connect(tab, &SGraphicalWidget::hasChanged, this, &_StabInterface_QTabWidget::changeTitle);
	}

private slots:
	void changeTitle()
	{
		if (auto *tab = qobject_cast<SGraphicalWidget *>(sender()))
			setTabText(indexOf(tab), tab->isModified() ? tab->windowTitle() + " *" : tab->windowTitle());
	}
};
} // namespace

class STabInterface::_STabInterface_pimpl
{
public:
	/** the tab widget */
	_StabInterface_QTabWidget *tabWidget = nullptr;
	/** Current tab index */
	int currentTabIndex = -1;
	/** Connections for the current tab */
	std::list<QMetaObject::Connection> tabconnections;
	/** Tell if the tabs describes the same object or not */
	bool sameDescriptionMode = true;
	/** Internally stored actions for the STabInterface */
	std::unordered_multimap<STabInterface::Menus, QAction *> tabActions;
};

#include "STabInterface.moc"

STabInterface::STabInterface(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent), _pimpl(std::make_unique<_STabInterface_pimpl>())
{
	_pimpl->tabWidget = new _StabInterface_QTabWidget(this);
	_pimpl->tabWidget->setDocumentMode(true);
	sameDescriptionMode(true);

	setWidget(_pimpl->tabWidget);

	connect(_pimpl->tabWidget, &QTabWidget::currentChanged, this, &STabInterface::tabChanged);
}

STabInterface::~STabInterface() = default;

QWidget *STabInterface::statusWidget()
{
	return callOnTab<QWidget *>([](SGraphicalWidget *tab) -> QWidget * { return tab->statusWidget(); }, _pimpl->tabWidget, nullptr);
}

QWidget *STabInterface::helpWidget()
{
	return callOnTab<QWidget *>([](SGraphicalWidget *tab) -> QWidget * { return tab->helpWidget(); }, _pimpl->tabWidget, nullptr);
}

const std::unordered_multimap<STabInterface::Menus, QAction *> &STabInterface::actions()
{
	_pimpl->tabActions = SGraphicalWidget::actions();
	auto tabWidget = dynamic_cast<SGraphicalWidget *>(tab()->currentWidget());
	if (tabWidget)
		_pimpl->tabActions.insert(std::cbegin(tabWidget->actions()), std::cend(tabWidget->actions()));
	return _pimpl->tabActions;
}

QToolBar *STabInterface::toolbar()
{
	return callOnTab<QToolBar *>([](SGraphicalWidget *tab) -> QToolBar * { return tab->toolbar(); }, _pimpl->tabWidget, nullptr);
}

SGraphicalWidget *STabInterface::openOutputFile(SGraphicalWidget *widget, const QString &path, bool checkForOpened)
{
	if (path.isEmpty())
	{
		logWarning() << "Given path was empty";
		return nullptr;
	}

	// If a file was already opened, go to it
	if (checkForOpened)
	{
		int idx = findFileIndex(path);
		if (idx > -1)
		{
			logInfo() << "File " << path << " was already opened";
			return nullptr;
		}
	}

	QFileInfo fiProject(getPath());
	QFileInfo fiOutput(path);
	// Check file size
	if (fiOutput.size() >= (qint64)536870912) // if bigger than 512 MB
	{
		logWarning() << tr("The file ") << path << tr(" is too big! Its size is over 512 MB.");
		return nullptr;
	}
	// If it is older than input
	if (!fiOutput.exists())
	{
		logWarning() << "Cannot open the output path " << path << " since the file does not exist. ";
		return nullptr;
	}
	if (fiProject.lastModified() >= fiOutput.lastModified())
	{
		logWarning() << "The output path " << path << " is older than the project input path. It might have been tampered with or modified by some other procession software.";
	}
	// Correct open case
	if (widget && widget->open(path))
	{
		const QString &winTitle = widget->windowTitle().isEmpty() ? fiOutput.baseName() : widget->windowTitle();
		widget->setWindowTitle(winTitle);
		tab()->addTab(widget, winTitle);
		tab()->setTabToolTip(tab()->indexOf(widget), path);
		return widget;
	}

	logWarning() << "Can't open file " << path;
	return nullptr;
}

void STabInterface::reloadTab(const QString &path)
{
	int idx = findFileIndex(path);
	// if it does not exist - open new file
	if (idx < 0)
	{
		SGraphicalWidget *wdg = getWidgetForPath(path);
		if (wdg)
			openOutputFile(wdg, path);
	}
	// if it does - reload
	else
	{
		SGraphicalWidget *wdg = qobject_cast<SGraphicalWidget *>(tab()->widget(idx));
		if (wdg)
			wdg->reload();
	}
}

const QString STabInterface::getMatchingPath(const QString &extension) const
{
	int idx = findFileIndex(extension);
	if (idx < 0)
		return QString();
	// call getPath() for a tab at an index found with findFileIndex
	return callOnTab<QString>([](SGraphicalWidget *tab) -> QString { return tab->getPath(); }, _pimpl->tabWidget, QString(), idx);
}

QString STabInterface::getPath() const
{
	if (_pimpl->sameDescriptionMode)
		return SGraphicalWidget::getPath();
	return callOnTab<QString>([](SGraphicalWidget *tab) -> QString { return tab->getPath(); }, _pimpl->tabWidget, QString());
}

bool STabInterface::isModified() const noexcept
{
	if (_pimpl->sameDescriptionMode)
		return callOnTab<bool>([](SGraphicalWidget *tab) -> bool { return tab->isModified(); }, _pimpl->tabWidget, false);

	bool modified = false;
	for (int i = 0; i < _pimpl->tabWidget->count() && !modified; i++)
		modified |= callOnTab<bool>([](SGraphicalWidget *tab) -> bool { return tab->isModified(); }, _pimpl->tabWidget, false, i);
	return modified;
}

ShareablePointsList STabInterface::getContent() const
{
	return callOnTab<ShareablePointsList>([](SGraphicalWidget *tab) -> ShareablePointsList { return tab->getContent(); }, _pimpl->tabWidget, ShareablePointsList());
}

void STabInterface::updateContent()
{
	SGraphicalWidget::updateContent();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->updateContent(); }, _pimpl->tabWidget);
}

void STabInterface::clearContent()
{
	SGraphicalWidget::clearContent();
	runWithNoSignals<void>([this]() -> void {
		for (int i = 0; i < _pimpl->tabWidget->count(); i++)
			callOnTab([](SGraphicalWidget *tab) -> void { tab->clearContent(); }, _pimpl->tabWidget, i);
	});
	emit hasChanged();
}

void STabInterface::setContent(const ShareablePointsList &spl)
{
	SGraphicalWidget::setContent(spl);
	callOnTab([&spl](SGraphicalWidget *tab) -> void { tab->setContent(spl); }, _pimpl->tabWidget);
}

void STabInterface::paste()
{
	SGraphicalWidget::paste();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->paste(); }, _pimpl->tabWidget);
}

void STabInterface::copy() const
{
	SGraphicalWidget::copy();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->copy(); }, _pimpl->tabWidget);
}

void STabInterface::cut() const
{
	SGraphicalWidget::cut();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->cut(); }, _pimpl->tabWidget);
}

void STabInterface::undo()
{
	SGraphicalWidget::undo();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->undo(); }, _pimpl->tabWidget);
}

void STabInterface::redo()
{
	SGraphicalWidget::redo();
	callOnTab([](SGraphicalWidget *tab) -> void { tab->redo(); }, _pimpl->tabWidget);
}

void STabInterface::isModified(bool modified) noexcept
{
	SGraphicalWidget::isModified(modified);
	runWithNoSignals<void>([this, modified]() -> void {
		if (_pimpl->sameDescriptionMode)
		{
			for (int i = 0; i < _pimpl->tabWidget->count(); i++)
				callOnTab([modified](SGraphicalWidget *tab) -> void { tab->isModified(modified); }, _pimpl->tabWidget, i);
		}
		else
			callOnTab([modified](SGraphicalWidget *tab) -> void { tab->isModified(modified); }, _pimpl->tabWidget);
	});
	emit hasChanged();
}

void STabInterface::retranslate()
{
	SGraphicalWidget::retranslate();
	for (int i = 0; i < _pimpl->tabWidget->count(); i++)
		callOnTab([](SGraphicalWidget *tab) -> void { tab->retranslate(); }, _pimpl->tabWidget, i);
}

bool STabInterface::_save(const QString &path)
{
	return callOnTab<bool>([path](SGraphicalWidget *tab) -> bool { return tab->save(path); }, _pimpl->tabWidget, false);
}

bool STabInterface::_open(const QString &path)
{
	return callOnTab<bool>([path](SGraphicalWidget *tab) -> bool { return tab->open(path); }, _pimpl->tabWidget, false);
}

void STabInterface::_newEmpty()
{
	callOnTab([](SGraphicalWidget *tab) -> void { tab->newEmpty(); }, _pimpl->tabWidget);
}

QTabWidget *STabInterface::tab() noexcept
{
	return _pimpl->tabWidget;
}

const QTabWidget *STabInterface::tab() const noexcept
{
	return _pimpl->tabWidget;
}

void STabInterface::sameDescriptionMode(bool mode) noexcept
{
	_pimpl->sameDescriptionMode = mode;
	_pimpl->tabWidget->setTabPosition(mode ? QTabWidget::TabPosition::South : QTabWidget::TabPosition::North);
	_pimpl->tabWidget->tabBar()->setExpanding(mode);
	_pimpl->tabWidget->setTabBarAutoHide(mode);
}

bool STabInterface::sameDescriptionMode() const noexcept
{
	return _pimpl->sameDescriptionMode;
}

int STabInterface::findFileIndex(const QString &pathORextention) const
{
	if (pathORextention.isEmpty())
		return -1;

	// Select condition to use
	QString toCompare = pathORextention;
	std::function<QString(SGraphicalWidget *)> conditionPath = [&toCompare](
																   SGraphicalWidget *tab) -> QString { return tab->getPath() == toCompare ? tab->getPath() : QString(); };
	std::function<QString(SGraphicalWidget *)> conditionExtension = [&toCompare](SGraphicalWidget *tab) -> QString {
		return QFileInfo(tab->getPath()).suffix() == toCompare ? tab->getPath() : QString();
	};

	// Specify conditions
	std::function<QString(SGraphicalWidget *)> condition;
	if (pathORextention[0] == '.')
	{
		toCompare = pathORextention.mid(1);
		condition = conditionExtension;
	}
	else
		condition = conditionPath;

	for (int i = 0; i < _pimpl->tabWidget->count(); i++)
	{
		// If a file with a given extension is found - return it
		const auto path = callOnTab<QString>(condition, _pimpl->tabWidget, QString(), i);
		if (!path.isEmpty())
			return i;
	}

	return -1;
}

void STabInterface::tabChanged(int pos)
{
	if (pos == _pimpl->currentTabIndex)
		return;
	auto *newtab = qobject_cast<SGraphicalWidget *>(_pimpl->tabWidget->widget(pos));
	auto *currentab = qobject_cast<SGraphicalWidget *>(_pimpl->tabWidget->widget(_pimpl->currentTabIndex));

	// if newtab is a SGraphicalWidget, we first try to get the content from last SGraphicalWidget tab
	// if this is not possible, we cancel the change
	ShareablePointsList spl;
	if (_pimpl->sameDescriptionMode && newtab && currentab)
	{
		try
		{
			spl = currentab->getContent();
		}
		catch (const SPIOException &e)
		{
			_pimpl->tabWidget->setCurrentIndex(_pimpl->currentTabIndex);
			logCritical() << tr("Can't read contents") << '\n'
						  << tr("Your current file is not valid as it is not possible to read it correctly. If you change the tab now, you may lose everything. Please "
								"fix this first.");
			logDebug() << e.what() << e.contents();
			return;
		}
	}

	_pimpl->currentTabIndex = pos;
	// first we diconnect everything from previous tab
	for (const auto &c : _pimpl->tabconnections)
		disconnect(c);
	_pimpl->tabconnections.clear();
	if (!newtab)
	{
		emit statusWidgetChanged(nullptr);
		emit helpWidgetChanged(nullptr);
		emit actionsChanged();
		emit toolbarChanged(nullptr);
		return;
	}
	if (!_pimpl->sameDescriptionMode) // content cleared to be filled with previous tab
		logDebug() << "STabInterface: tab changed to" << newtab->windowTitle();
	// then we connect the new tab
	std::swap(currentab, newtab);
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::hasChanged, this, &STabInterface::hasChanged));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::fileChangeRequested, this, &STabInterface::fileChangeRequested));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::copyAvailable, this, &STabInterface::copyAvailable));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::cutAvailable, this, &STabInterface::cutAvailable));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::undoAvailable, this, &STabInterface::undoAvailable));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::redoAvailable, this, &STabInterface::redoAvailable));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::statusWidgetChanged, this, &STabInterface::statusWidgetChanged));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::helpWidgetChanged, this, &STabInterface::helpWidgetChanged));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::actionsChanged, this, &STabInterface::actionsChanged));
	_pimpl->tabconnections.push_back(connect(currentab, &SGraphicalWidget::toolbarChanged, this, &STabInterface::toolbarChanged));
	// finally we update the new tab
	currentab->retranslate();
	if (newtab && _pimpl->sameDescriptionMode)
	{
		currentab->setContent(spl);
		currentab->isModified(newtab->isModified());
		newtab->clearContent();
		newtab->isModified(currentab->isModified());
	}
	emit statusWidgetChanged(currentab->statusWidget());
	emit helpWidgetChanged(currentab->helpWidget());
	emit actionsChanged();
	emit toolbarChanged(currentab->toolbar());
}

int STabInterface::fileChangeRequested(const QString &path, int)
{
	QString filePath = path;
	// if no path provided or if it does not exist
	int idx = findFileIndex(path);
	if (idx > -1)
		tab()->setCurrentIndex(idx);

	return idx;
}
