/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SPLUGINLOADER_HPP
#define SPLUGINLOADER_HPP

#include <memory>
#include <unordered_map>
#include <vector>

#include <QObject>

#include "PluginsGlobal.hpp"

class QPluginLoader;

class SPluginInterface;

/** Structure representing an entry point.
 *
 * An entry point is an object that is registered with a specific key (@see SPluginLoader::registerEntryPoint()) and implements some specific functions that can be used by other plugins.
 * The entry points are meant to allow plugin-plugin communication and, should the need arise, they provide some normally inaccessible plugin-specific funtionality to other plugins.
 */
struct EntryPoint
{
	/** The meta object that is linked to the class that can be used by other plugins. */
	const QMetaObject *meta;
	/** Plugin owner of the entry point. */
	SPluginInterface *plugin;
};

struct ExternalScript
{
	QString name;
	QString alias;
	QString path;
	QString help;
	QString defaultArguments;
	QString description;
	QString documentation;
	QStringList supportedPlugins;
	QStringList expectedInputExtensions;
};
/**
 * Singleton responsible of loading plugins and handling entry points.
 *
 * This class loads all plugins that are in the pluginDir(). By default, this directory is a "plugins" subfolder
 * in the app directory.
 *
 * You can load one specific plugin with the method load(const std::string& pluginName). Note that if a plugin with the
 * same name already exists, it will be unloaded.
 *
 * This class also handles the entry points and provides the methods to registerEntryPoints() and to getEntryPoints().
 * A class to be registered as an entry point can be registered once only (for specific QMetaObject), preferably when its plugin owner is loaded (although there are no restrictions to that).
 *
 * Lastly, this class handles the external scripts to be used by all the plugins. It keeps all the relevant data of scripts and provides a method to access script for a given plugin.
 */
class SUGL_SHARED_EXPORT SPluginLoader : public QObject
{
	Q_OBJECT

public:
	/** @return the instance of the translator (singleton). */
	static SPluginLoader &getPluginLoader();

	~SPluginLoader() override;

	SPluginLoader(const SPluginLoader &) = delete; // can't copy
	SPluginLoader(SPluginLoader &&) = delete;
	SPluginLoader &operator=(const SPluginLoader &) = delete;
	SPluginLoader &operator=(SPluginLoader &&) = delete;

	/** @return the folder from where the plugins are loaded (default is a subfolder in the app directory) */
	const QString &pluginDir() const noexcept;
	/** Change the directory where to find plugins */
	void setPluginDir(QString plugindir) noexcept;

	/** @return the number of loaded plugins */
	size_t size() const noexcept;
	/** @return true if the given plugin has been loaded */
	bool has(const QString &pluginName);
	/** load all the plugins from the pluginDir(). All previously loaded plugins are unloaded before. */
	void load();
	/** load the given module from the pluginDir(). If a plugin with the same name already exists, it will be unloaded. */
	bool load(const QString &pluginName);
	/** unload all plugins */
	void unload();
	/** @return true if the given plugin has been unloaded (false if it wasn't loaded, or if Qt hasn't been able to unload it) */
	bool unload(const QString &pluginName);
	/** Loads all the scripts and categorizes them. */
	void loadScripts();

	/** @return the plugin with the given name */
	SPluginInterface *getPlugin(const QString &pluginName);
	/** @return a list of all plugins */
	std::vector<SPluginInterface *> plugins() const;
	/** @return a map of all plugins, the keys are the names of the plugins */
	std::unordered_map<std::string, SPluginInterface *> pluginsMap();
	/** @return a list of Qt loaders */
	const std::vector<std::unique_ptr<QPluginLoader>> &loaders() const;

	/**
	 * Register a new entry point.
	 *
	 * Different plugins may register different classes with the same entry point name. Although, each QMetaObject class can only be registered once.
	 * Every entry point should have valid pointer to QMetaObject and SPluginInterface.
	 * @param entryname the entry point name to which class is to be registered.
	 * @param entrypoint the EntryPoint class associated with the entryname that will automatically unregister on its plugin unload.
	 */
	void registerEntryPoint(QString entryname, EntryPoint entrypoint);
	/** @return all the registered classes associated with given entry point name. */
	std::vector<EntryPoint> getEntryPoints(const QString &entryname) const;
	/** @return all the registered scripts provided for a given plugin. */
	const std::vector<ExternalScript>& getScripts(const QString &pluginName) const;

signals:
	/** Emitted when a plugin has been loaded */
	void pluginLoaded(const QString &pluginName);
	/** Emitted when a plugin has been unloaded */
	void pluginUnloaded(const QString &pluginName);

private:
	/** Private constructor: singleton. */
	SPluginLoader(QObject *parent = nullptr);

	/**
	 * Remove all the entry points for given plugin.
	 *
	 * This method is used when plugin is to be unloaded and dangling entrypoints to it are undesired.
	 * @param pluginName name of the plugin whose entrypoints should be removed.
	 */
	void removePluginEntryPoints(const QString &pluginName);

private:
	/** pimpl */
	class _SPluginLoader_pimpl;
	std::unique_ptr<_SPluginLoader_pimpl> _pimpl;
};

#endif // SPLUGINLOADER_HPP
