/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SGRAPHICALINTERFACE_HPP
#define SGRAPHICALINTERFACE_HPP

#include <memory>
#include <unordered_map>

#include <QToolBar>
#include <QWidget>

#include "PluginsGlobal.hpp"

class QAction;

class ShareablePointsList;

class SPluginInterface;

/**
 * Main widget for graphical plugins.
 *
 * This interface lists all the functions that need to be implemented by a graphical plugin.
 *
 * This widget should be able to load and show a ShareablePointsList. It will be inserted in the main window.
 *
 * If you don't need status or help widget, you can return a null pointer in these functions.
 *
 * Note that this is just a container for your content. This widget can hold only one widget, and has no margin
 * (so your inner widget won't look strange).
 */
class SUGL_SHARED_EXPORT SGraphicalWidget : public QWidget
{
	Q_OBJECT

public:
	/** Gives the main menu entry where to add actions */
	enum class Menus
	{
		/** File menu */
		file,
		/** Edit menu */
		edit,
		/** View menu */
		view,
		/** Plugin menu, actions will be added in a subgroup that has the name of the plugin */
		plugin,
		/** Script menu */
		script
	};

	SGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~SGraphicalWidget() override;

	// QWidget
	virtual bool eventFilter(QObject *watched, QEvent *event) override;

	/** Saves all the files open in the plugin. Should return false if the action has been canceled. Passed path should never be empty. */
	virtual bool save(const QString &path);
	/** Open a new project in the plugin. Should return false if the action has been canceled. */
	virtual bool open(const QString &path);
	/** Reload the currently opened file. */
	virtual bool reload();
	/** Creates an empty project in the plugin (it should create all needed files with some templates to help the user. */
	virtual void newEmpty();
	/**
	 * @return a path to the file where plugin was saved.
	 * Ideally, the file returned is the (first) parameter to give to the executable when we run the associated program.
	 */
	virtual QString getPath() const;

	/** @return true if the content has been modified and not saved */
	virtual bool isModified() const noexcept;
	/** @return the content of the widget as a ShareablePointsList */
	virtual ShareablePointsList getContent() const = 0;

	/** @return the name of the plugin (will be used for siplay in the interface) */
	virtual const SPluginInterface *owner() const noexcept;
	virtual SPluginInterface *owner() noexcept { return const_cast<SPluginInterface *>(static_cast<const SGraphicalWidget *>(this)->owner()); }

	/** @return a widget that will be inserted in the status bar (if not needed, you can return `nullptr`) */
	virtual QWidget *statusWidget();
	/** @return a widget that will be inserted in the help dock (if not needed, you can return `nullptr`) */
	virtual QWidget *helpWidget();
	/** @return a list of actions to be inserted in the menus */
	virtual const std::unordered_multimap<Menus, QAction *> &actions();
	/** @return a tool bar to be integrated in the main window */
	virtual QToolBar *toolbar();

	/**
	 * Tells if the file watcher should be enabled (by default false).
	 *
	 * If the file watcher is enabled, all the external changes made to the currently opened file will be tracked. Thus, depending on the application settings, we can ask
	 * the user if he wants to reload the file from disk if changes have been made outside of the application.
	 *
	 * To get the current file, we use the function getPath() that can be overridden.
	 *
	 * If disabled, we totally disable the file watcher.
	 */
	void enablefileWatcher(bool enabled);
	/** Tells if the file watcher is enabled (by default false). */
	bool enablefileWatcher() const noexcept;

public slots:
	/** Clears the content of the widget */
	virtual void clearContent() {}
	/** Update the widget. If the widget interacts with others, it should check the status of the others. */
	virtual void updateContent() {}

	/** Set the content to display in the widget */
	virtual void setContent(const ShareablePointsList &) {}
	/** Paste content from clipboad. @see ClipboardManager */
	virtual void paste() {}
	/** Copy the current selection in the clipboard. @see ClipboardManager */
	virtual void copy() const {}
	/** Cut the current selection in the clipboard. @see ClipboardManager */
	virtual void cut() const {}
	/** Undo previous change in the contents. */
	virtual void undo() {}
	/** Redo the change in the contents. */
	virtual void redo() {}

	/** Change the modification flag */
	virtual void isModified(bool modified) noexcept;

	/** Automatically connected to SettingsManager::settingsChanged() signal */
	virtual void updateUi(const QString &configname);
	/**
	 * Ask the widget to reload translations.
	 *
	 * By default this method does nothing. If you built your widget thanks to the Qt Designer,
	 * you can call the method `ui->retranslateUi(this);`.
	 */
	virtual void retranslate() {}

	/** This method is called just before the SLaunchInterface associated is ran. */
	virtual void aboutTorun() {}
	/**
	 * This method is called once the SLaunchInterface associated has finished its run.
	 * It is ensured that a call to aboutToRun() will be made before any call to this function.
	 * @param status true if the SLaunchInterface finished correctly, false otherwise.
	 */
	virtual void runFinished(bool) {}

signals:
	/** Sent when the modification flag is changed */
	void hasChanged();
	/** Sent when the copy is available or not available anymore */
	void copyAvailable(bool);
	/** Sent when the modification flag is changed, position is negative for line, positive for position in text */
	void fileChangeRequested(const QString &path, int position);
	/** Sent when the copy is available or not available anymore */
	void cutAvailable(bool);
	/** Sent when the undo is available or not available anymore */
	void undoAvailable(bool);
	/** Sent when the redo is available or not available anymore */
	void redoAvailable(bool);
	/** Sent when the status widget is changed. If `nullptr` is sent, it will remove the status widget. */
	void statusWidgetChanged(QWidget *);
	/** Sent when the help widget is changed. If `nullptr` is sent, it will remove the help widget. */
	void helpWidgetChanged(QWidget *);
	/** Sent when the actions changed. */
	void actionsChanged();
	/** Sent when the toolbar is changed. If `nullptr` is sent, it will remove the toolbar. */
	void toolbarChanged(QToolBar *);

protected:
	// QWidget
	virtual void focusInEvent(QFocusEvent *event) override;
	virtual void focusOutEvent(QFocusEvent *event) override;

	/** Child implementation of save(). */
	virtual bool _save(const QString &path) = 0;
	/** Child implementation of open(). */
	virtual bool _open(const QString &path) = 0;
	/** Child implementation of _newEmpty(). */
	virtual void _newEmpty() = 0;

	/**
	 * Add the given widget inside.
	 * If any widgets were added before, they will be removed.
	 * @param w the new widget
	 */
	virtual void setWidget(QWidget *w);
	/** Set the status widget */
	virtual void setStatusWidget(QWidget *w);
	/** Set the help widget */
	virtual void setHelpWidget(QWidget *w);
	/** Add an action */
	virtual void addAction(Menus m, QAction *a);
	/** Clear the actions */
	virtual void clearActions();
	/** Set the toolbar */
	virtual void setToolBar(QToolBar *t);

	/**
	 * Install the given eventFilter as evenFilter on child and all its widget children.
	 *
	 * @param eventFilter the event filter to install
	 * @param child the event filter will be installed on child on all his widget children
	 */
	void installEventFilterAll(QObject *eventFilter, QWidget *child);

private slots:
	void fileExternallyModified(const QString &path);

private:
	class _SGraphicalWidget_pimpl;
	std::unique_ptr<_SGraphicalWidget_pimpl> _pimpl;
};

#endif // SGRAPHICALINTERFACE_HPP
