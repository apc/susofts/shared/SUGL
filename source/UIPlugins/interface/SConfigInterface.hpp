/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef SCONFIGINTERFACE_HPP
#define SCONFIGINTERFACE_HPP

#include <memory>

#include <QWidget>

#include "PluginsGlobal.hpp"

class SPluginInterface;

/**
 * Oject that holds the config of the widget.
 *
 * This object can write himself in QSettings and read from it.
 *
 * All subclasses should override comparison operators. The objects should be considered equal only if they are of the same derived class.
 */
struct SConfigObject
{
	virtual ~SConfigObject() = default;

	virtual bool operator==(const SConfigObject &o) const noexcept { return typeid(*this) == typeid(o); }
	virtual bool operator!=(const SConfigObject &o) const noexcept { return !(*this == o); }

	/** Write the config to QSettings */
	virtual void write() const = 0;
	/** Read the config from QSettings */
	virtual void read() = 0;
};

/**
 * Configuration object for applications.
 *
 * This class should serve as a base for application config objects. It should also be stored in the SettingsManager
 * so it can be retreived like this:
 * @code{cpp}
 * // name under which the application settings are stored
 * QString name = SettingsManager::settings().innerSettings("appName");
 * // get the settings
 * auto config = SettingsManager::settings().settings<SApplicationConfigObject>(name);
 * @endcode
 */
struct SUGL_SHARED_EXPORT SApplicationConfigObject : public SConfigObject
{
	virtual ~SApplicationConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/**
	 * Enumeration used to defined the detection of external changes of a file.
	 *
	 * This should be used to reload a file in the application, when it has been modified outside.
	 */
	enum class FileDetectionType
	{
		/** The detection should only triggers when the editor has focus */
		ON_FOCUS,
		/** The detection is always on */
		ALL_FILES,
		/** The detection is always on, and we don't ask the user to reload files */
		SILENT,
		/** We don't detect any external file changes */
		DISABLED
	};
	/** How to handle external file changes. */
	FileDetectionType autoUpdate = FileDetectionType::ON_FOCUS;
};

/**
 * Plugin config interface.
 *
 * The widget used to change settings of the plugin.
 *
 * The widget should be able to return a SConfigObject that will then be loaded in the graphical part.
 */
class SUGL_SHARED_EXPORT SConfigWidget : public QWidget
{
	Q_OBJECT

public:
	SConfigWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~SConfigWidget() override;

	/** @return the config according to the fields in the widget (new object on the heap) */
	virtual SConfigObject *config() const = 0;
	/** sets the config in the widget (prefilled fields) */
	virtual void setConfig(const SConfigObject *conf) = 0;
	/**
	 * Reset the widget with the parameters saved in QSettings.
	 * Reads the last saved settings and fill the form with it. If config() is called right after, its returned value should
	 * match exactly the last saved settings.
	 */
	virtual void reset() = 0;
	/**
	 * Reset the widget with the default values (hard coded).
	 * Restore the default settings and fill the form with it. If config() is called right after, its returned value should
	 * match exactly the default settings.
	 */
	virtual void restoreDefaults() = 0;

	/** @return the name of the plugin (will be used for siplay in the interface) */
	virtual const SPluginInterface *owner() const noexcept;
	virtual SPluginInterface *owner() noexcept { return const_cast<SPluginInterface *>(static_cast<const SConfigWidget *>(this)->owner()); }

private:
	class _SConfigWidget_pimpl;
	std::unique_ptr<_SConfigWidget_pimpl> _pimpl;
};

#endif // SCONFIGINTERFACE_HPP
