# Including the following directories allows to include ui_... which are post-compilation files
INCLUDE_DIRECTORIES(
	${CMAKE_CURRENT_BINARY_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}
	${TUT_INCLUDE_PATH}
)

#include different modules
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/QScintilla")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/UIPlugins/")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Tests/")
