[Resources for SUSofts](#resources-for-susofts)
[Set up Translations](#set-up-translations)
[Full example](#full-example)
[License](#license)

Resources for SUSofts
=====================

Resources (mainly images) for survey graphical software. This setup creates an RCC file (Qt binary resource file) that can be imported in any Qt software.

Files
-----

Resources are organized in folders per goal: in [actions](./actions), you'll find all icons relative to actions (new, copy, past...). the file [resources.qrc](./resources.qrc) lists all the icon. It is the file that needs to be imported in QtDesigner to be able to use the icons.

### Actions ###
All images related to actions in Qt, like scissors for the *cut* action...

### Icons ###
Icons of software (Qt icon, SUSofts icon...)

### Markers ###
Markers for text editors.

How to use
----------

You need to include the file [resources.cmake](./resources.cmake) in your `CMakeLists.txt`. Then, you need to call the function `create_resources` with all the QRC files you want to include. Note that the SUGL QRC file is stored in the variable `${SUGL_RESOURCES}`.

Finally, you have to copy the generated `.rcc` file next to your executable.

Set up Translations
===================

Translations for survey graphical software. This setup creates Qt translation files (TS and QM).

Files
-----

TS files are translation sources, automatically created by Qt by scanning all the sources. Then, it can be translated in any language.

QM files are the binary version of TS files. They are compiled with the application, and embeds one language.

How to use
----------

You need to include the file [resources.cmake](./resources.cmake) in your `CMakeLists.txt`. Then, you need to call the function `create_translations` with all the TS files you want to manage. The TS files will be automatically created / updated.

This function will also create the QM files corresponding to the TS files. It will compile them in the binary dir, and create a QRC file that includes QM files, and also the Qt translation files, needed to translate buttons or dialogs. The path to this QRC file is returned by the function, you just need to add it at you call to the function `create_resources`.

**You should never clean / rebuild** the `translations` target. This will recreate the TS files, and you would lose all the translations done. Build the target is enough to update everything.

Finally, there is [translations](../UIPlugins/translations/) package that help you to manage the translations in the C++ code.

Full example
------------

First, you need to include the project in your `CMakeLists.txt`:
```cmake
set(PROJECT_NAME myproject)

# We use the standard CMake protocol for SUSofts
# see https://gitlab.cern.ch/DataProcessingAndAnalysis/SUSoftCMakeCommon
set(USE_QT TRUE)
set(QT_WANTED_MODULES "Qt5Core" "Qt5Gui" "Qt5Widgets")
include("../../SUSoftCMakeCommon/CMakeLists.txt")
set(CMAKE_AUTOUIC ON)

# We include the project resources
include("../lib/SUGL/resources/resources.cmake")

# We create the executable

add_executable(${PROJECT_NAME} WIN32
	main.cpp

	# myproject headers
	myproject_mainwindow.hpp
	# myproject sources
	myproject_mainwindow.cpp
	# myproject uis
	myproject_mainwindow.ui
)
target_include_directories(${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(${PROJECT_NAME}
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
)

# Now, we create the translations:
# wanted languages are English, French and German
set(TRANSLATIONS "de" "en" "fr")
create_translations(RSRC_TR_FILE
	"${CMAKE_CURRENT_SOURCE_DIR}/resources/translations/"
	"surveypad_"
	"${TRANSLATIONS}"
)
# Finally, the resources
set(RESOURCES
	"${CMAKE_CURRENT_SOURCE_DIR}/resources/resources.qrc"
	${RSRC_TR_FILE}
	${SUGL_RESOURCES}
)
create_resources(resources
	"${RESOURCES}"
)
# and we add dependency so the resources will always be up to date
add_dependencies(${PROJECT_NAME} resources)

# To finish, we copy the RCC file next to the executable
if(WIN32)
	set(RSRCES_PATH "${CMAKE_CURRENT_BINARY_DIR}/resources/resources.rcc")
	post_build_copy_dlls(${PROJECT_NAME} "${QT_COPY_DLLS_DEBUG}" "${QT_COPY_DLLS_RELEASE}" "")
	post_build_copy_dlls(${PROJECT_NAME} "${RSRCES_PATH}" "${RSRCES_PATH}" "resources")
endif()
```

Then, in your `main.cpp`:
```cpp
#include <QApplication>
#include <QResource>

#include "myproject_mainwindow.hpp"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	
	// We load the resources
	QResource::registerResource("resources/resources.rcc");
	// We load French language
	// Note that in SUGL/UIPlugins, there is a Translator class that ease the way you manage translations
	QTranslator tr, trqt, trqtbase;
	tr.load(":/translations/surveypad_fr");
	trqt.load(":/translations/qt_fr");
	trqtbase.load(":/translations/qtbase_fr");
	app.installTranslator(tr);
	app.installTranslator(trqt);
	app.installTranslator(trqtbase);

	MainWindow mainWin;
	mainWin.show();
	return app.exec();
}
```

License
-------

Some images used in the resources are under the LGPL license. They all come from <https://github.com/KDE/breeze-icons>. Check the file [LGPL.LIB](./LGPL.LIB) for more information about he LGPL license.
