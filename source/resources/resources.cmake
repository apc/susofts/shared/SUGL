# Resources functions to build RCC binary resource file and manage translations.
#
# The QRC resource file for the SUGL (that contains action images...) is stored in ${SUGL_RESOURCES}.

############################
# Translations:

# Create a target "translations" that adds translation for your project.
# The target "translations" should never be cleaned as it removes all translated strings.
# Build it should be enough to update the TS files. Then, you can build it again to update the QM files.
# param RSRC_TR_FILE OUT parameter: the path to the QRC resource file listing all QM files, you can use it in the function `create_resources` below
# param path the path where are stored the TS files
# param prefix the prefix name of the TS files, like "myapp_"
# param languages the list of languages codes (like en, fr, de...)
# param buildsubdir the path to the subdir in the cmake build directory
#
# The filenames should correspond to "${path}/${prefix}${languages}.ts"
# for instance: source/translations/myapp_fr.ts
function(create_translations RSRC_TR_FILE path prefix languages buildsubdir)
	find_package("Qt5LinguistTools" REQUIRED)
	set(ts_files "")
	set(_translated_files "")
	foreach (_language ${languages})
		# add translation files to resources
		set(_tr_file "${prefix}${_language}")
		list(APPEND ts_files "${path}/${_tr_file}.ts")
		set(_translated_files "${_translated_files}        <file alias=\"${_tr_file}\">${buildsubdir}/${_tr_file}.qm</file>\n")
	endforeach()
	configure_file("${SUGL_ROOT}/source/resources/translations.qrc.in" "${PROJECT_BINARY_DIR}/translations.qrc")
	qt5_create_translation(QM_FILES
		# sources
		${PROJECT_SOURCE_DIR}
		${PROJECT_BINARY_DIR}
		# TS files
		${ts_files}
		# options
		OPTIONS -no-obsolete -extensions 'ui,c,c++,cc,cpp,cxx,h,h++,hh,hpp,hxx,qml,qrc'
	)
	add_custom_target(translations
		DEPENDS ${QM_FILES}
	)
	set(_translated_files "")
	foreach (_language ${languages})
		# make build target work properly, see https://bugreports.qt.io/browse/QTBUG-41736
		set(_ts_lst_file "${buildsubdir}${CMAKE_FILES_DIRECTORY}/${prefix}${_language}_lst_file")
		add_custom_command(TARGET translations
			PRE_BUILD
			COMMAND Qt5::lupdate
			ARGS -no-obsolete -extensions 'ui,c,c++,cc,cpp,cxx,h,h++,hh,hpp,hxx,qml,qrc' "@${_ts_lst_file}" -ts "${path}/${prefix}${_language}.ts"
			VERBATIM
		)
	endforeach()
	set_target_properties(translations PROPERTIES FOLDER "resources")
	set(RSRC_TR_FILE "${PROJECT_BINARY_DIR}/translations.qrc" PARENT_SCOPE)
endfunction()

############################
# Resources:

# Location of the SUGL resources
set(SUGL_RESOURCES "${SUGL_ROOT}/source/resources/resources.qrc")

# Create a RCC binary resource file.
# The created resource file will be named ${target_name}.rcc and it is put in a "resources" subdir in the binary path.
# param target_name the name of the created target in the IDE.
# param qrc_files the list of QRC files that should be included in this RCC
function(create_resources target_name qrc_files)
	find_package(Qt5 COMPONENTS Core REQUIRED)
	qt5_add_binary_resources(${target_name}
		${qrc_files}
		DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/resources/${target_name}.rcc"
	)

	# We create the destination folder
	add_custom_target(${target_name}_folder
		COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_CURRENT_BINARY_DIR}/resources/"
		COMMENT "Creating resources folder"
	)
	add_dependencies(${target_name} ${target_name}_folder)
	set_target_properties(${target_name} ${target_name}_folder PROPERTIES FOLDER "resources")
endfunction()
