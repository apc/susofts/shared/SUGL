<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MarkerTableModel</name>
    <message>
        <location filename="../../UIPlugins/editors/text/MarkerViewer.cpp" line="84"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/MarkerViewer.cpp" line="86"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarkerViewer</name>
    <message>
        <location filename="../../UIPlugins/editors/text/MarkerViewer.cpp" line="330"/>
        <source>Show all markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_MarkerViewer.h" line="73"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_MarkerViewer.h" line="73"/>
        <source>Marker Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_MarkerViewer.h" line="74"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_MarkerViewer.h" line="74"/>
        <source>Total: %1 marker(s) displayed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MdiSubWindow</name>
    <message>
        <location filename="../../UIPlugins/utils/MdiSubWindow.cpp" line="37"/>
        <source>You did some changes that have not been saved to the project &apos;%1&apos;. You will lose everything if you don&apos;t save them. Do you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/MdiSubWindow.cpp" line="41"/>
        <source>You did some changes that have not been saved to the project &apos;%1&apos;. You cannot run the project without saving the changes first. Do you want to save now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/MdiSubWindow.cpp" line="47"/>
        <source>Save project &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/MdiSubWindow.cpp" line="53"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetworkConfig</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_NetworkConfig.h" line="70"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_NetworkConfig.h" line="70"/>
        <source>Request timeout (0 to disable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_NetworkConfig.h" line="71"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_NetworkConfig.h" line="71"/>
        <source> s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_NetworkConfig.h" line="72"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_NetworkConfig.h" line="72"/>
        <source>Clear Cookies</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PointsExportConfig</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="344"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="344"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="346"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="346"/>
        <source>When enabled all copy operations done in software will be done according to the configuration below (with some additional headers and quirks when selected text is not Points List)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="348"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="348"/>
        <source>Enable Points List on every copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="350"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="350"/>
        <source>Title of point list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="352"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="352"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="354"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="354"/>
        <source>Parameters of points list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="356"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="356"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="358"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="358"/>
        <source>Coordinate system of measured points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="360"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="360"/>
        <source>Coordinate system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="362"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="362"/>
        <source>Extra information about measured points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="364"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="364"/>
        <source>Extra Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="366"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="366"/>
        <source>Precision of the measured points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="368"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="368"/>
        <source>Precision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="369"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="369"/>
        <source>Depending on the operation,
this may or may not appear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="372"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="372"/>
        <source>Information about frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="374"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="374"/>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="376"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="376"/>
        <source>Name of the frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="378"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="406"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="378"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="406"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="380"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="380"/>
        <source>Translation from the parent frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="382"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="382"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="384"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="384"/>
        <source>Rotation from the parent frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="386"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="386"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="388"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="388"/>
        <source>Scale from the parent frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="390"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="390"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="392"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="392"/>
        <source>Is frame scalable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="394"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="394"/>
        <source>Is free scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="396"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="396"/>
        <source>Inner frames of a given frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="398"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="398"/>
        <source>Inner frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="400"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="400"/>
        <source>Points in the points list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="402"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="402"/>
        <source>Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="404"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="404"/>
        <source>Point&apos;s name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="408"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="408"/>
        <source>Point&apos;s inline comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="410"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="410"/>
        <source>Inline comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="412"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="412"/>
        <source>Comment above the point, if it applies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="414"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="414"/>
        <source>Header comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="416"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="416"/>
        <source>If a point should be a part of computations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="418"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="418"/>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="420"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="420"/>
        <source>Extra information about a point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="422"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="422"/>
        <source>Extra infos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="424"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="424"/>
        <source>Point&apos;s position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="426"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="426"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="428"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="428"/>
        <source>Position on x axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="430"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="430"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="432"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="432"/>
        <source>Position on y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="434"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="434"/>
        <source>y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="436"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="436"/>
        <source>Z or H depending on the context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="438"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="438"/>
        <source>z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="440"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="440"/>
        <source>Precision on the x axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="442"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="442"/>
        <source>sigma x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="444"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="444"/>
        <source>Precision on the y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="446"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="446"/>
        <source>sigma y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="448"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="448"/>
        <source>Precision on the z axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="450"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="450"/>
        <source>sigma z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="452"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="452"/>
        <source>Is the point movable on x axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="454"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="454"/>
        <source>Is free x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="456"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="456"/>
        <source>Is the point movable on y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="458"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="458"/>
        <source>Is free y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="460"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="460"/>
        <source>Is the point movable on z axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_PointsExportConfig.h" line="462"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_PointsExportConfig.h" line="462"/>
        <source>Is free z</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessLauncherObject</name>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="50"/>
        <source>Running: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="66"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="70"/>
        <source>The process failed to start. The path to the executable may be wrong: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="73"/>
        <source>The process crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="76"/>
        <source>The process is lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="80"/>
        <source>Error of I/O with the process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/ProcessLauncherObject.cpp" line="83"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectManager</name>
    <message>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="373"/>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="397"/>
        <source>Project </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="373"/>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="397"/>
        <source> is already open. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="382"/>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="424"/>
        <source>Cannot open the project.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="407"/>
        <source>No suitable plugin to open the file has been found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ProjectManager.cpp" line="418"/>
        <source>No graphical interface is provided by the plugin.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../source/test_ShortcutsConfig.cpp" line="16"/>
        <source>Ctrl + X,  Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/test_ShortcutsConfig.cpp" line="18"/>
        <location filename="../source/test_ShortcutsConfig.cpp" line="111"/>
        <source>Shift + Ctrl + ALT + G, X, Y, Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../source/test_ShortcutsConfig.cpp" line="53"/>
        <location filename="../source/test_ShortcutsConfig.cpp" line="67"/>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="86"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/MarkerViewer.cpp" line="17"/>
        <source>Total: %1 marker(s) displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="82"/>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="37"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="83"/>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="36"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="84"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="85"/>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="94"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="86"/>
        <source>User1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextUtils.hpp" line="87"/>
        <source>User2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="76"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="77"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="78"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="79"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="80"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="81"/>
        <source>Remove table row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="82"/>
        <source>Auto-complete text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="82"/>
        <source>Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="84"/>
        <source>New project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="85"/>
        <source>Open project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="86"/>
        <source>Close Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="87"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="88"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="89"/>
        <source>Save all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="89"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="90"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="90"/>
        <source>Ctrl+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="91"/>
        <source>Reload files from disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="93"/>
        <source>Go to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="93"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="95"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="96"/>
        <source>Go to previous cursor position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="97"/>
        <source>Go to next cursor position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="99"/>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="100"/>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="101"/>
        <source>Restore zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="101"/>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="103"/>
        <source>Exit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/ShortcutsConfig.hpp" line="103"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="34"/>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="35"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="38"/>
        <source>Fatal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SGraphicalWidget</name>
    <message>
        <location filename="../../UIPlugins/interface/SGraphicalInterface.cpp" line="326"/>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/SGraphicalInterface.cpp" line="326"/>
        <source>This file has been modified by another program.
Do you want to reload it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>STabInterface</name>
    <message>
        <location filename="../../UIPlugins/interface/STabInterface.cpp" line="271"/>
        <source>Can&apos;t read contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/interface/STabInterface.cpp" line="272"/>
        <source>Your current file is not valid as it is not possible to read it correctly. If you change the tab now, you may lose everything. Please fix this first.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutsConfig</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_ShortcutsConfig.h" line="98"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_ShortcutsConfig.h" line="98"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_ShortcutsConfig.h" line="99"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_ShortcutsConfig.h" line="99"/>
        <source>Each shortcut has one definition that will call the actions and the definitions should not overlap.
A shortcut can consist of up to four different sequences that have to be performed one after the other to call
the action. Some shortcuts, even though editable, cannot be entirely replaced, these include text editor actions
such as copy, cut, paste, undo, redo. By providing a nonstandard key shortcut here we can add one more way 
to invoke actions, but the default shortcuts will still work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_ShortcutsConfig.h" line="104"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_ShortcutsConfig.h" line="104"/>
        <source>SurveyPad has to be restarted after modifying the shortcuts for them to be applied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_ShortcutsConfig.h" line="105"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_ShortcutsConfig.h" line="105"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_ShortcutsConfig.h" line="106"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_ShortcutsConfig.h" line="106"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StructureTreeModel</name>
    <message>
        <location filename="../../UIPlugins/editors/text/StructureViewer.cpp" line="254"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/StructureViewer.cpp" line="256"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StructureViewer</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_StructureViewer.h" line="50"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_StructureViewer.h" line="50"/>
        <source>Structure Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableEditorWidget</name>
    <message>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="49"/>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="50"/>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="51"/>
        <source>Add row below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="65"/>
        <source>Special Copy...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="66"/>
        <location filename="../../UIPlugins/editors/table/TableEditorWidget.cpp" line="67"/>
        <source>Special copy...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorConfig</name>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorConfig.cpp" line="201"/>
        <location filename="../../UIPlugins/editors/text/TextEditorConfig.cpp" line="221"/>
        <source>Current line highlight color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorConfig.cpp" line="211"/>
        <source>Current selection result color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="282"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="282"/>
        <source>Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="283"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="283"/>
        <source>Current line:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="284"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="284"/>
        <source>Highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="285"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="285"/>
        <source>Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="289"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="289"/>
        <source>Highlights the repetitions of the selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="291"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="291"/>
        <source>Smart Highlighting Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="292"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="292"/>
        <source>Show white spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="293"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="303"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="313"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="293"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="303"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="313"/>
        <source> characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="294"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="294"/>
        <source>White space color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="297"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="297"/>
        <source>Number of character to type before the auto-completion list is displayed. If set to &quot;0&quot;, the auto-completion list is disabled during typing. Note that it can always been displayed thanks the the &quot;CTRL+SPACE&quot; shortcut.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="299"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="299"/>
        <source>Auto-completion threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="301"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="301"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Number of character to type before the auto-completion list is displayed. If set to &amp;quot;0&amp;quot;, the auto-completion list is disabled during typing. Note that it can always been displayed thanks the shortcut.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="304"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="304"/>
        <source>Respect case:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="305"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="305"/>
        <source>Case sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="306"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="306"/>
        <source>Code folding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="307"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="307"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="308"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="308"/>
        <source>Braces:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="309"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="309"/>
        <source>Brace matching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="310"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="310"/>
        <source>This setting is disabled in Release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="311"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="311"/>
        <source>Show annotations inside the text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="312"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="312"/>
        <source>Size of a Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="314"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="314"/>
        <source>White space size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextEditorConfig.h" line="315"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextEditorConfig.h" line="315"/>
        <source>Whitespaces:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorWidget</name>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="526"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="527"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="528"/>
        <source>Fold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="531"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="532"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="533"/>
        <source>Unfold All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="482"/>
        <source>Special Copy...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="127"/>
        <source>Input files have been changed, output files are out of date. Please run again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="442"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="443"/>
        <source>Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="444"/>
        <source>File Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="483"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="484"/>
        <source>Special copy...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="538"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="539"/>
        <location filename="../../UIPlugins/editors/text/TextEditorWidget.cpp" line="540"/>
        <source>Remove all user markers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextGoToLine</name>
    <message>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="90"/>
        <source>Line number (%1 - %2),</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="91"/>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="101"/>
        <source>current: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="95"/>
        <source>Line: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="100"/>
        <source>Position (%1 - %2),</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextGoToLine.cpp" line="105"/>
        <source>Position: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextGoToLine.h" line="94"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextGoToLine.h" line="94"/>
        <source>Go To...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextGoToLine.h" line="95"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextGoToLine.h" line="95"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextGoToLine.h" line="96"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextGoToLine.h" line="96"/>
        <source>current: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextGoToLine.h" line="97"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextGoToLine.h" line="97"/>
        <source>Offset (position)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextGoToLine.h" line="98"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextGoToLine.h" line="98"/>
        <source>Line number (0 - 0),</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextSearch</name>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="212"/>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="223"/>
        <source>Nothing replaced.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="229"/>
        <source>Replaced %1 occurrences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="300"/>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="492"/>
        <source>Can&apos;t find &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="305"/>
        <source>Marked %1 occurrences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="313"/>
        <source>Removed all search markers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/editors/text/TextSearch.cpp" line="497"/>
        <source>Found %1 occurrences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="268"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="268"/>
        <source>Search and Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="269"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="269"/>
        <source>In Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="271"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="274"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="271"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="274"/>
        <source>Close search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="278"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="281"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="278"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="281"/>
        <source>Jump to previous result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="285"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="285"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="288"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="288"/>
        <source>Match case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="289"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="289"/>
        <source>Match whole word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="290"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="290"/>
        <source>Extended (\n, \r,\ t, \b, \f, \v)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="292"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="295"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="292"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="295"/>
        <source>Add a bookmark on all lines matching the search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="297"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="297"/>
        <source>Mark All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="298"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="298"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="299"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="299"/>
        <source>RegExp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="301"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="304"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="301"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="304"/>
        <source>Remove all Search bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="306"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="306"/>
        <source>Remove Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="307"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="307"/>
        <source>Find:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="309"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="312"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="309"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="312"/>
        <source>Jump to next result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="315"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="315"/>
        <source>Replace with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="317"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="320"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="317"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="320"/>
        <source>Replace all found occurrences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="322"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="322"/>
        <source>Replace All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="324"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="327"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="324"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="327"/>
        <source>Replace current occurrence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="329"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="329"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="331"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextSearch.h" line="334"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="331"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextSearch.h" line="334"/>
        <source>Expand search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextStatusBar</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="188"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="188"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="189"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="189"/>
        <source>Length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="190"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="192"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="194"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="196"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="198"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="200"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="202"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="204"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="190"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="192"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="194"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="196"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="198"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="200"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="202"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="204"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="191"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="191"/>
        <source>Lines:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="193"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="193"/>
        <source>Ln:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="195"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="195"/>
        <source>Col:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="197"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="197"/>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="199"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="199"/>
        <source>Sel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="201"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="201"/>
        <source>|</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_TextStatusBar.h" line="203"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_TextStatusBar.h" line="203"/>
        <source>Count:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TreeMdiManager</name>
    <message>
        <location filename="../../UIPlugins/utils/TreeMdiManager.cpp" line="191"/>
        <source>Open in file explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/TreeMdiManager.cpp" line="278"/>
        <source>Close project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UILogHandler</name>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="119"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="174"/>
        <source>Show all logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="175"/>
        <source>Show only information logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="176"/>
        <source>Show only warning logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../UIPlugins/utils/UILogHandler.cpp" line="177"/>
        <source>Show only critical logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandler.h" line="86"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandler.h" line="86"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandler.h" line="88"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandler.h" line="88"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clears log messages from table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandler.h" line="92"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandler.h" line="92"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandler.h" line="94"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandler.h" line="94"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandler.h" line="96"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandler.h" line="96"/>
        <source>Context</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UILogHandlerConfig</name>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="166"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="166"/>
        <source>Set the desired log level and visibility of log columns. When choosing a particular log level visibility, logs at this level and above are enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="167"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="167"/>
        <source>Show critical log messages in pop-up window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="168"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="168"/>
        <source>Popup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="169"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="169"/>
        <source>Log level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="170"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="170"/>
        <source>Enable Qt log messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="171"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="171"/>
        <source>Qt logs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="172"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="172"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="173"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="173"/>
        <source>Time column:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="174"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="174"/>
        <source>Show time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="175"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="175"/>
        <source>Context column:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="176"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="176"/>
        <source>Show context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Debug/ui_UILogHandlerConfig.h" line="177"/>
        <location filename="../../../../../build/sugl/source/UIPlugins/UIPlugins_autogen/include_Release/ui_UILogHandlerConfig.h" line="177"/>
        <source>Type column:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>test</name>
    <message>
        <location filename="../source/test_Translator.cpp" line="93"/>
        <location filename="../source/test_Translator.cpp" line="98"/>
        <location filename="../source/test_Translator.cpp" line="103"/>
        <source>language: English</source>
        <translation>langue : francais</translation>
    </message>
</context>
</TS>
