#include <QApplication>
#include <QClipboard>
#include <QMimeData>

#include <tut/tut.hpp>

#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <utils/ClipboardManager.hpp>

namespace
{
class DumbIO : public IShareablePointsListIO
{
public:
	DumbIO(const std::string &mimetype) : _mimetype(mimetype) {}
	const std::string &getContent() const noexcept { return _content; }
	void setContent(const std::string &str = "") { _content = str; }

	virtual const std::string &getMIMEType() const override { return _mimetype; }

	virtual ShareablePointsList read(const std::string &str) override
	{
		_content = str;
		return ShareablePointsList();
	}
	virtual ShareableExtraInfos readExtraInfos(const std::string &) override { return ShareableExtraInfos(); }
	virtual ShareableFrame readFrame(const std::string &) override { return ShareableFrame(std::make_shared<ShareableParams>()); }
	virtual ShareableParams readParams(const std::string &) override { return ShareableParams(); }
	virtual ShareablePoint readPoint(const std::string &) override { return ShareablePoint(); }
	virtual ShareablePosition readPosition(const std::string &) override { return ShareablePosition(); }

	virtual std::string write(const ShareablePointsList &) override { return _mimetype + ' ' + _content; }
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &) override { return std::string(); }
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &) override { return std::string(); }
	virtual std::string write(const ShareablePosition &) override { return std::string(); }

private:
	std::string _mimetype;
	std::string _content;
};
} // namespace

namespace tut
{
struct clipboardmanagerdata
{
	~clipboardmanagerdata() { ClipboardManager::getClipboardManager().clear(); }
};

typedef test_group<clipboardmanagerdata> tg;
tg uiplugins_clipboardmanager_test_group("Test ClipboardManager class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ClipboardManager: Test of singleton");

	ensure_equals(&ClipboardManager::getClipboardManager(), &ClipboardManager::getClipboardManager());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ClipboardManager: Test add, at, clear, size");

	auto &clipboard = ClipboardManager::getClipboardManager();
	ensure_equals(clipboard.size(), 0UL);

	auto *io1 = new DumbIO("io1");
	bool pos = clipboard.add({io1, true});
	ensure(pos);
	ensure_equals(clipboard.size(), 1UL);
	ensure_equals(&clipboard.at(0), io1);

	pos = clipboard.add({io1, true});
	ensure_not(pos);
	ensure_equals(clipboard.size(), 1UL);
	ensure_equals(&clipboard.at(0), io1);

	auto *io2 = new DumbIO("io2");
	pos = clipboard.add({io2, true});
	ensure(pos);
	ensure_equals(clipboard.size(), 2UL);
	ensure_equals(&clipboard.at(0), io1);
	ensure_equals(&clipboard.at(1), io2);

	auto *io10 = new DumbIO("io10");
	auto *io20 = new DumbIO("io20");
	auto *io30 = new DumbIO("io30");
	clipboard.add(std::pair{io10, true}, std::pair{io20, true}, std::pair{io30, true});
	ensure_equals(clipboard.size(), 5UL);
	ensure_equals(&clipboard.at(2), io10);
	ensure_equals(&clipboard.at(3), io20);
	ensure_equals(&clipboard.at(4), io30);

	clipboard.clear();
	ensure_equals(clipboard.size(), 0UL);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ClipboardManager: Test erase, size");

	auto &clipboard = ClipboardManager::getClipboardManager();
	ensure_equals(clipboard.size(), 0UL);

	auto *io1 = new DumbIO("io1");
	auto *io2 = new DumbIO("io2");
	auto *io3 = new DumbIO("io3");
	clipboard.add(std::pair{io1, true}, std::pair{io2, true}, std::pair{io3, true});
	ensure_equals(clipboard.size(), 3UL);

	clipboard.erase(io2);
	ensure_equals(clipboard.size(), 2UL);
	ensure_equals(&clipboard.at(0), io1);
	ensure_equals(&clipboard.at(1), io3);

	clipboard.erase(io1);
	ensure_equals(clipboard.size(), 1UL);
	ensure_equals(&clipboard.at(0), io3);

	clipboard.erase("io3");
	ensure_equals(clipboard.size(), 0UL);
}

template<>
template<>
void testobject::test<4>()
{
	set_test_name("ClipboardManager: Test get");

	auto &clipboard = ClipboardManager::getClipboardManager();
	ensure_equals(clipboard.size(), 0UL);
	ensure(clipboard.get("io1") == nullptr);

	auto *io1 = new DumbIO("io1");
	auto *io2 = new DumbIO("io2");
	auto *io3 = new DumbIO("io3");
	clipboard.add(std::pair{io1, true}, std::pair{io2, true}, std::pair{io3, true});
	ensure_equals(clipboard.size(), 3UL);

	ensure_equals(clipboard.get("io1"), io1);
	ensure_equals(clipboard.get("io2"), io2);
	ensure_equals(clipboard.get("io3"), io3);

	const auto &clip2 = ClipboardManager::getClipboardManager();
	ensure_equals(clip2.get("io1"), io1);
	ensure_equals(clip2.get("io2"), io2);
	ensure_equals(clip2.get("io3"), io3);

	clipboard.clear();
	ensure(clipboard.get("io1") == nullptr);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ClipboardManager: Test copy, paste");

	auto &clipboard = ClipboardManager::getClipboardManager();
	auto *io1 = new DumbIO("io1");
	auto *io2 = new DumbIO("io2");
	auto *io3 = new DumbIO("io3");
	clipboard.add(std::pair{io1, true}, std::pair{io2, true}, std::pair{io3, true});
	ensure_equals(clipboard.size(), 3UL);
	io1->setContent("lol1");
	io2->setContent("lol2");
	io3->setContent("lol3");

	clipboard.putInClipboard(clipboard.copy(ShareablePointsList()));
	const auto &mimedata = QApplication::clipboard()->mimeData();
	ensure_equals(mimedata->formats().size(), 3);
	ensure(mimedata->hasFormat("io1"));
	ensure(mimedata->hasFormat("io2"));
	ensure(mimedata->hasFormat("io3"));
	ensure_equals(mimedata->data("io1").toStdString(), "io1 lol1");
	ensure_equals(mimedata->data("io2").toStdString(), "io2 lol2");
	ensure_equals(mimedata->data("io3").toStdString(), "io3 lol3");

	io1->setContent();
	io2->setContent();
	io3->setContent();
	clipboard.paste();
	ensure_equals(io1->getContent(), "io1 lol1");
	ensure_equals(io2->getContent(), "");
	ensure_equals(io3->getContent(), "");
}
} // namespace tut
