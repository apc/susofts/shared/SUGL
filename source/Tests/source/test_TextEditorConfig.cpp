#include <Qsci/qsciscintilla.h>

#include <tut/tut.hpp>

#include <QtStreams.hpp>
#include <editors/text/TextEditorConfig.hpp>
#include <utils/SettingsManager.hpp>

namespace
{
TextEditorConfigObject getConfig()
{
	TextEditorConfigObject t;
	// Caret Line
	t.caretLineVisible = false;
	t.caretLineBg = QColor(0, 0, 0, 0);
	// White Spaces
	t.whitespaceVisible = (int)QsciScintilla::WhitespaceVisibility::WsVisible;
	t.whitespaceSize = 15;
	t.whitespaceFg = QColor(255, 255, 255, 255);
	// misc
	t.foldStyle = (int)QsciScintilla::FoldStyle::NoFoldStyle;
	t.braceMatching = (int)QsciScintilla::BraceMatch::NoBraceMatch;
	return t;
}
} // namespace

namespace tut
{
struct texteditorconfigdata
{
	texteditorconfigdata() { SettingsManager::init("textapp", "version"); }
	~texteditorconfigdata() { std::remove(SettingsManager::settings().innerSettings("settingsFile").toUtf8().constData()); }
};

typedef test_group<texteditorconfigdata> tg;
tg uiplugins_texteditorconfig_test_group("Test TextEditorConfig class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
/* ********************** */
/* TextEditorConfigObject */
/* ********************** */

template<>
template<>
void testobject::test<1>()
{
	set_test_name("TextEditorConfigObject: Test of default constructor");

	TextEditorConfigObject t;

	// Caret Line
	ensure(t.caretLineVisible);
	ensure_equals(t.caretLineBg, QColor(0, 255, 255, 50));
	// White Spaces
	ensure_equals(t.whitespaceVisible, (int)QsciScintilla::WhitespaceVisibility::WsInvisible);
	ensure_equals(t.whitespaceSize, 2);
	ensure_equals(t.whitespaceFg, QColor(255, 165, 0, 10));
	// misc
	ensure_equals(t.foldStyle, (int)QsciScintilla::FoldStyle::BoxedTreeFoldStyle);
	ensure_equals(t.braceMatching, (int)QsciScintilla::BraceMatch::SloppyBraceMatch);
	ensure_equals(t.annotationDisplay, (int)QsciScintilla::AnnotationDisplay::AnnotationHidden);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("TextEditorConfigObject: Test of empty read()");

	TextEditorConfigObject t;
	t.read();

	// Caret Line
	ensure(t.caretLineVisible);
	ensure_equals(t.caretLineBg, QColor(0, 255, 255, 50));
	// White Spaces
	ensure_equals(t.whitespaceVisible, (int)QsciScintilla::WhitespaceVisibility::WsInvisible);
	ensure_equals(t.whitespaceSize, 2);
	ensure_equals(t.whitespaceFg, QColor(255, 165, 0, 10));
	// misc
	ensure_equals(t.foldStyle, (int)QsciScintilla::FoldStyle::BoxedTreeFoldStyle);
	ensure_equals(t.braceMatching, (int)QsciScintilla::BraceMatch::SloppyBraceMatch);
	ensure_equals(t.annotationDisplay, (int)QsciScintilla::AnnotationDisplay::AnnotationHidden);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("TextEditorConfigObject: Test of read() and write()");

	{
		TextEditorConfigObject t = getConfig();
		t.write();
	}

	// test!!!
	TextEditorConfigObject t;
	t.read();
	// Caret Line
	ensure(t == getConfig());
}

/* ********************** */
/*    TextEditorConfig    */
/* ********************** */

template<>
template<>
void testobject::test<10>()
{
	set_test_name("TextEditorConfig: Test of setConfig(), reset() and restoreDefaults()");

	TextEditorConfig widget;
	// initialization
	{
		auto t = getConfig();
		t.write();
		// we change `t` so it is different from what is written (for `reset()`)
		t.caretLineVisible = true;
		widget.setConfig(&t);
	}
	// test of getConfig()
	{
		std::unique_ptr<TextEditorConfigObject> t(dynamic_cast<TextEditorConfigObject *>(widget.config()));
		ensure(t != nullptr);
		auto test = getConfig();
		test.caretLineVisible = true;
		ensure(*t == test);
	}
	// test of reset()
	{
		widget.reset();
		std::unique_ptr<TextEditorConfigObject> t(dynamic_cast<TextEditorConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == getConfig());
	}
	// test of restoreDefaults()
	{
		widget.restoreDefaults();
		std::unique_ptr<TextEditorConfigObject> t(dynamic_cast<TextEditorConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == TextEditorConfigObject());
	}
}
} // namespace tut
