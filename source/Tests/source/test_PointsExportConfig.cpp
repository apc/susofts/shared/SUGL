#include <Qsci/qsciscintilla.h>

#include <tut/tut.hpp>

#include <QtStreams.hpp>
#include <utils/PointsExportConfig.hpp>
#include <utils/SettingsManager.hpp>

namespace
{
PointsExportConfigObject getConfig()
{
	PointsExportConfigObject t;
	// Global
	t.exportPointsList = true;
	// Position
	t.exportFieldsPosition.removeField("x", "y", "z");
	// Point
	t.exportFieldsPoint.removeField("headerComment", "active", "extraInfos");
	// Params - .selectedFields are empty
	t.exportFieldsParams.removeField("precision", "coordsys", "extraInfos");
	// Frame
	t.exportFieldsFrame.removeField("name", "rotation", "isfreescale", "points");
	return t;
}
} // namespace

namespace tut
{
struct pointsexportconfigdata
{
	pointsexportconfigdata() { SettingsManager::init("pointsexportapp", "version"); }
	~pointsexportconfigdata() { std::remove(SettingsManager::settings().innerSettings("settingsFile").toUtf8().constData()); }
};

typedef test_group<pointsexportconfigdata> tg;
tg uiplugins_pointsexportconfig_test_group("Test PointsExportConfig class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
/* ************************ */
/* PointsExportConfigObject */
/* ************************ */

template<>
template<>
void testobject::test<1>()
{
	set_test_name("PointsExportConfigObject: Test of default constructor");

	PointsExportConfigObject t;

	// Position
	ensure(t.exportFieldsPosition.getFields() == std::unordered_set<std::string>({"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez"}));
	ensure(t.exportFieldsPosition.getAllFields() == std::unordered_set<std::string>({"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez"}));
	// Point
	ensure(t.exportFieldsPoint.getFields() == std::unordered_set<std::string>({"name", "position", "inlineComment", "headerComment", "active", "extraInfos"}));
	ensure(t.exportFieldsPoint.getAllFields() == std::unordered_set<std::string>({"name", "position", "inlineComment", "headerComment", "active", "extraInfos"}));
	// Params
	ensure(t.exportFieldsParams.getFields() == std::unordered_set<std::string>({"precision", "coordsys", "extraInfos"}));
	ensure(t.exportFieldsParams.getAllFields() == std::unordered_set<std::string>({"precision", "coordsys", "extraInfos"}));
	// Frame
	ensure(t.exportFieldsFrame.getFields() == std::unordered_set<std::string>({"name", "translation", "rotation", "scale", "isfreescale", "innerFrames", "points"}));
	ensure(t.exportFieldsFrame.getAllFields() == std::unordered_set<std::string>({"name", "translation", "rotation", "scale", "isfreescale", "innerFrames", "points"}));
	// PointsList
	ensure(t.exportFieldsPointsList.getFields() == std::unordered_set<std::string>({"title", "params", "rootFrame"}));
	ensure(t.exportFieldsPointsList.getAllFields() == std::unordered_set<std::string>({"title", "params", "rootFrame"}));
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("PointsExportConfigObject: Test of empty read()");

	PointsExportConfigObject t;
	t.read();

	// Position
	ensure(t.exportFieldsPosition.getFields() == std::unordered_set<std::string>({"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez"}));
	ensure(t.exportFieldsPosition.getAllFields() == std::unordered_set<std::string>({"x", "y", "z", "sigmax", "sigmay", "sigmaz", "isfreex", "isfreey", "isfreez"}));
	// Point
	ensure(t.exportFieldsPoint.getFields() == std::unordered_set<std::string>({"name", "position", "inlineComment", "headerComment", "active", "extraInfos"}));
	ensure(t.exportFieldsPoint.getAllFields() == std::unordered_set<std::string>({"name", "position", "inlineComment", "headerComment", "active", "extraInfos"}));
	// Params
	ensure(t.exportFieldsParams.getFields() == std::unordered_set<std::string>({"precision", "coordsys", "extraInfos"}));
	ensure(t.exportFieldsParams.getAllFields() == std::unordered_set<std::string>({"precision", "coordsys", "extraInfos"}));
	// Frame
	ensure(t.exportFieldsFrame.getFields() == std::unordered_set<std::string>({"name", "translation", "rotation", "scale", "isfreescale", "innerFrames", "points"}));
	ensure(t.exportFieldsFrame.getAllFields() == std::unordered_set<std::string>({"name", "translation", "rotation", "scale", "isfreescale", "innerFrames", "points"}));
	// PointsList
	ensure(t.exportFieldsPointsList.getFields() == std::unordered_set<std::string>({"title", "params", "rootFrame"}));
	ensure(t.exportFieldsPointsList.getAllFields() == std::unordered_set<std::string>({"title", "params", "rootFrame"}));
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("PointsExportConfigObject: Test of read() and write()");

	{
		PointsExportConfigObject t = getConfig();
		t.write();
	}

	PointsExportConfigObject t;
	t.read();
	ensure(t == getConfig());
}

/* ************************ */
/*    PointsExportConfig    */
/* ************************ */

template<>
template<>
void testobject::test<10>()
{
	set_test_name("PointsExportConfig: Test of setConfig(), reset() and restoreDefaults()");

	PointsExportConfig widget;
	// initialization
	{
		auto t = getConfig();
		t.write();
		// we change `t` so it is different from what is written (for `reset()`)
		t.exportFieldsPosition.addField("x", "y");
		widget.setConfig(&t);
	}
	// test of getConfig()
	{
		std::unique_ptr<PointsExportConfigObject> t(dynamic_cast<PointsExportConfigObject *>(widget.config()));
		ensure(t != nullptr);
		auto test = getConfig();
		test.exportFieldsPosition.addField("x", "y");
		ensure(*t == test);
	}
	// test of reset()
	{
		widget.reset();
		std::unique_ptr<PointsExportConfigObject> t(dynamic_cast<PointsExportConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == getConfig());
	}
	// test of restoreDefaults()
	{
		widget.restoreDefaults();
		std::unique_ptr<PointsExportConfigObject> t(dynamic_cast<PointsExportConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == PointsExportConfigObject());
	}
}
} // namespace tut
