#include <ostream>
#include <list>

// Section below only applies to GCC version >= 12.
// The way the dependent names are resolved has changed. As of now, non-dependent names (e.g. <class T>) are looked up and bound at the point of the template definition.
// This binding persists even if there is a better match defined later. 
// Since TUT framework uses templates the `operator<<` needs to be available before the template definition happens, that is why it is done before tut is included.
// The other solution would be to place the operator inside the `std` namespace and benefit from Argument-dependent lookup (ADL). However, in that case the function
// would be available everywhere. With the anonymous namespace, we still keep it available only in the scope of this source file.
// For more infor please refer to: https://en.cppreference.com/w/cpp/language/dependent_name and https://en.cppreference.com/w/cpp/language/adl
namespace {
std::ostream &operator<<(std::ostream &out, const std::list<std::pair<int, int>> &styles);
}

#include <tut/tut.hpp>

#include <QtStreams.hpp>
#include <ShareablePoints/IShareablePointsListIO.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <editors/text/LexerParser.hpp>

namespace
{
class DumbIOTable
{
public:
	enum VariousConstants // random values
	{
		EOF_SYMBOL = 0,
		EOL_SYMBOL = 0,
		ACCEPT_STATE = 150,
		TERMINAL_COUNT = 30
	};

	static inline const char *const spell[] = {""};
	static inline const short lhs[] = {0};
	static inline const short rhs[] = {0};
	static inline const short goto_default[] = {0};
	static inline const short action_default[] = {0};
	static inline const short action_index[] = {0};
	static inline const short action_info[] = {0};
	static inline const short action_check[] = {0};

	static inline int nt_action(int, int) { return 0; }

	static inline int t_action(int, int) { return 0; }
};

class DumbIO : public LexerParser<DumbIOTable>
{
	Q_OBJECT

public:
	DumbIO(QObject *parent = nullptr) : LexerParser(parent)
	{
		_lexerMode = true;
		commentChars("%");
	}

	using LexerParser::addStyleSym;
	using LexerParser::resetLine;

	void setText(QString text) noexcept
	{
		_currentText = std::move(text);
		initLexer();
		_currentLine = &_currentText;
		_styles.addLine(_currentLine.size());
	}
	const QString &getText() const noexcept { return _currentText; }
	const std::list<std::pair<int, int>> &getStyles() const noexcept { return _styles.lastLine(); }
	void popStyle() noexcept { _styles.popStyle(); }
	State nextToken() { return LexerParser::nextToken(); }
	const char *wordCharacters() const { return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789."; }

	virtual const std::string &getMIMEType() const override
	{
		static const std::string mimetype = "dumb";
		return mimetype;
	}
	virtual const char *language() const override { return ""; }
	virtual QString description(int) const override { return ""; }

	virtual ShareablePointsList read(const std::string &) override { return ShareablePointsList(); }
	virtual ShareableExtraInfos readExtraInfos(const std::string &) override { return ShareableExtraInfos(); }
	virtual ShareableFrame readFrame(const std::string &) override { return ShareableFrame(std::make_shared<ShareableParams>()); }
	virtual ShareableParams readParams(const std::string &) override { return ShareableParams(); }
	virtual ShareablePoint readPoint(const std::string &) override { return ShareablePoint(); }
	virtual ShareablePosition readPosition(const std::string &) override { return ShareablePosition(); }

	virtual std::string write(const ShareablePointsList &) override { return std::string(); }
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &) override { return std::string(); }
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &) override { return std::string(); }
	virtual std::string write(const ShareablePosition &) override { return std::string(); }

private:
	virtual int nextToken(QStringRef) override { return 0; }
	virtual void consumeRule(int) override {}
};

std::ostream &operator<<(std::ostream &out, const std::list<std::pair<int, int>> &styles)
{
	out << "{ ";
	for (const auto &s : styles)
		out << "{" << s.first << ',' << s.second << "} ";
	out << '}';
	return out;
}
} // namespace

#include "test_LexerParser.moc"

namespace tut
{
struct lexerparserdata
{
};

typedef test_group<lexerparserdata> tg;
tg uiplugins_lexerparser_test_group("Test LexerParser class.");
typedef tg::object testobject;


template<>
template<>
void testobject::test<1>()
{
	set_test_name("LexerParser: Test of nextToken()");

	DumbIO dumb;

	dumb.setText("This is the test n1 1111.\n");
	ensure_equals(dumb.nextToken().parsed, "This ");
	ensure_equals(dumb.nextToken().parsed, "is ");
	ensure_equals(dumb.nextToken().parsed, "the ");
	ensure_equals(dumb.nextToken().parsed, "test ");
	ensure_equals(dumb.nextToken().parsed, "n1 ");
	ensure_equals(dumb.nextToken().parsed, "1111.");
	ensure_equals(dumb.nextToken().parsed, "\n");

	dumb.setText(" ( the best one!)%comment lol\n");
	ensure_equals(dumb.nextToken().parsed, " ( ");
	ensure_equals(dumb.nextToken().parsed, "the ");
	ensure_equals(dumb.nextToken().parsed, "best ");
	ensure_equals(dumb.nextToken().parsed, "one");
	ensure_equals(dumb.nextToken().parsed, "!");
	ensure_equals(dumb.nextToken().parsed, ")");
	ensure_equals(dumb.nextToken().parsed, "%comment lol");
	ensure_equals(dumb.nextToken().parsed, "\n");

	dumb.setText("\n%comment2 lol2 %\n");
	ensure_equals(dumb.nextToken().parsed, "\n");
	ensure_equals(dumb.nextToken().parsed, "%comment2 lol2 %");
	ensure_equals(dumb.nextToken().parsed, "\n");

	dumb.setText("\t%comment3 lol3");
	ensure_equals(dumb.nextToken().parsed, "\t%comment3 lol3");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("LexerParser: Test of addStyleSym() straight mode");

	DumbIO dumb;
	dumb.setText("This is a test.");
	QStringRef text = &dumb.getText();
	QStringRef token;
	const auto &styles = dumb.getStyles();
	ensure(styles.empty());

	// This
	token = dumb.nextToken().parsed;
	ensure_equals(token, "This ");
	dumb.addStyleSym(1, token);
	ensure_equals(styles, decltype(styles){{5, 1}});

	// is
	token = dumb.nextToken().parsed;
	ensure_equals(token, "is ");
	dumb.addStyleSym(2, token);
	ensure_equals(styles, decltype(styles){{5, 1}, {3, 2}});

	// a
	token = dumb.nextToken().parsed;
	ensure_equals(token, "a ");
	dumb.addStyleSym(3, token);
	ensure_equals(styles, decltype(styles){{5, 1}, {3, 2}, {2, 3}});

	// test.
	token = dumb.nextToken().parsed;
	ensure_equals(token, "test.");
	dumb.addStyleSym(4, token);
	ensure_equals(styles, decltype(styles){{5, 1}, {3, 2}, {2, 3}, {5, 4}});
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("LexerParser: Test of addStyleSym() shuffle mode");

	DumbIO dumb;
	dumb.setText("This is a test.");
	QStringRef text = &dumb.getText();
	const auto &styles = dumb.getStyles();
	ensure(styles.empty());

	QStringRef This = dumb.nextToken().parsed;
	ensure_equals(This, "This ");
	QStringRef is = dumb.nextToken().parsed;
	ensure_equals(is, "is ");
	QStringRef a = dumb.nextToken().parsed;
	ensure_equals(a, "a ");
	QStringRef test = dumb.nextToken().parsed;
	ensure_equals(test, "test.");

	// is
	dumb.addStyleSym(2, is);
	ensure_equals(styles, decltype(styles){{5, 0}, {3, 2}});

	// test.
	dumb.addStyleSym(4, test);
	ensure_equals(styles, decltype(styles){{5, 0}, {3, 2}, {2, 0}, {5, 4}});

	// This
	dumb.addStyleSym(1, This);
	ensure_equals(styles, decltype(styles){{5, 1}, {3, 2}, {2, 0}, {5, 4}});

	// a
	dumb.addStyleSym(3, a);
	ensure_equals(styles, decltype(styles){{5, 1}, {3, 2}, {2, 3}, {5, 4}});
}

template<>
template<>
void testobject::test<4>()
{
	set_test_name("LexerParser: Test of addStyleSym() medley mode");

	DumbIO dumb;
	dumb.setText("aa bb cc dd ee ");
	QStringRef text = &dumb.getText();
	const auto &styles = dumb.getStyles();
	ensure(styles.empty());

	QStringRef a = dumb.nextToken().parsed;
	ensure_equals(a, "aa ");
	QStringRef b = dumb.nextToken().parsed;
	ensure_equals(b, "bb ");
	QStringRef c = dumb.nextToken().parsed;
	ensure_equals(c, "cc ");
	QStringRef d = dumb.nextToken().parsed;
	ensure_equals(d, "dd ");
	QStringRef e = dumb.nextToken().parsed;
	ensure_equals(e, "ee ");
	QStringRef token;

	auto reinit = [a, b, c, d, e, &dumb, &styles]() -> void {
		dumb.resetLine();
		dumb.addStyleSym(1, a);
		dumb.addStyleSym(2, b);
		dumb.addStyleSym(3, c);
		dumb.addStyleSym(4, d);
		dumb.addStyleSym(5, e);
		ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}, {3, 5}});
	};

	// total recovery
	reinit();
	token = text.mid(3, 3); // 1 token
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 10}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(3, 6); // 2 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {6, 10}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(3, 9); // 3 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {9, 10}, {3, 5}});
	reinit();
	token = text.mid(0, 15); // all tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{15, 10}});

	// partial covering left
	reinit();
	token = text.mid(0, 4); // 1.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{4, 10}, {2, 2}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(3, 5); // 1.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {5, 10}, {1, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(6, 8); // 2.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {8, 10}, {1, 5}});
	reinit();
	token = text.mid(0, 13); // 4.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{13, 10}, {2, 5}});

	// partial covering right
	reinit();
	token = text.mid(11, 4); // 1.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {2, 4}, {4, 10}});
	reinit();
	token = text.mid(7, 5); // 1.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {1, 3}, {5, 10}, {3, 5}});
	reinit();
	token = text.mid(1, 8); // 2.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{1, 1}, {8, 10}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(2, 13); // 4.5 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{2, 1}, {13, 10}});

	// middle insertion
	reinit();
	token = text.mid(2, 2); // 2 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{2, 1}, {2, 10}, {2, 2}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(4, 4); // 2 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {1, 2}, {4, 10}, {1, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(4, 10); // 4 tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {1, 2}, {10, 10}, {1, 5}});
	reinit();
	token = text.mid(1, 13); // all tokens
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{1, 1}, {13, 10}, {1, 5}});

	// inner insertion
	reinit();
	token = text.mid(3, 1); // 1 char left
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {1, 10}, {2, 2}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(3, 2); // 2 chars left
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {2, 10}, {1, 2}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(4, 1); // 1 char middle
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {1, 2}, {1, 10}, {1, 2}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(5, 1); // 1 char right
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {2, 2}, {1, 10}, {3, 3}, {3, 4}, {3, 5}});
	reinit();
	token = text.mid(4, 2); // 2 chars left
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {1, 2}, {2, 10}, {3, 3}, {3, 4}, {3, 5}});

	// overflow
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(12, 2); // to the right
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}, {2, 10}});
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(14, 1); // jump to the right
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}, {2, 0}, {1, 10}});
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(11, 3); // 1 token middle
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {2, 4}, {3, 10}});
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(7, 7); // 2 tokens middle
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {1, 3}, {7, 10}});
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(6, 7); // 2 tokens right align
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {7, 10}});
	reinit();
	dumb.popStyle();
	ensure_equals(styles, decltype(styles){{3, 1}, {3, 2}, {3, 3}, {3, 4}});
	token = text.mid(0, 12); // all tokens right align
	dumb.addStyleSym(10, token);
	ensure_equals(styles, decltype(styles){{12, 10}});
}
} // namespace tut
