#include <tut/tut.hpp>

#include <editors/text/TextEditor.hpp>
#include <editors/text/TextEditorConfig.hpp>
#include <utils/SettingsManager.hpp>

namespace tut
{
struct texteditordata
{
	~texteditordata() { SettingsManager::settings().settingsRemove(TextEditorConfig::configName()); }
};

typedef test_group<texteditordata> tg;
tg uiplugins_texteditor_test_group("Test TextEditor class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("TextEditor: Test of empty updateUi()");

	SettingsManager::settings().settings(TextEditorConfig::configName(), new TextEditorConfigObject());
	TextEditor t;

	ensure((bool)t.SendScintilla(QsciScintilla::SCI_GETCARETLINEVISIBLE));
	ensure_equals(t.whitespaceVisibility(), QsciScintilla::WhitespaceVisibility::WsInvisible);
	ensure_equals(t.whitespaceSize(), 2);
	ensure_equals(t.braceMatching(), QsciScintilla::BraceMatch::SloppyBraceMatch);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("TextEditor: Test of empty updateUi()");

	TextEditor t;
	{
		TextEditorConfigObject conf;
		// Caret Line
		conf.caretLineVisible = false;
		conf.caretLineBg = QColor(0, 0, 0, 0);
		// White Spaces
		conf.whitespaceVisible = (int)QsciScintilla::WhitespaceVisibility::WsVisible;
		conf.whitespaceSize = 15;
		conf.whitespaceFg = QColor(255, 255, 255, 255);
		// misc
		conf.foldStyle = (int)QsciScintilla::FoldStyle::NoFoldStyle;
		conf.braceMatching = (int)QsciScintilla::BraceMatch::NoBraceMatch;
		conf.annotationDisplay = (int)QsciScintilla::AnnotationDisplay::AnnotationBoxed;
		SettingsManager::settings().settings(TextEditorConfig::configName(), new TextEditorConfigObject(conf));
	}

	ensure_not((bool)t.SendScintilla(QsciScintilla::SCI_GETCARETLINEVISIBLE));
	ensure_equals(t.whitespaceVisibility(), QsciScintilla::WhitespaceVisibility::WsVisible);
	ensure_equals(t.whitespaceSize(), 15);
	ensure_equals(t.braceMatching(), QsciScintilla::BraceMatch::NoBraceMatch);
}
} // namespace tut
