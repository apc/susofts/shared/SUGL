#include <QEventLoop>
#include <QMdiArea>
#include <QTimer>
#include <QWidget>

#include <tut/tut.hpp>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <interface/SGraphicalInterface.hpp>
#include <interface/SLaunchInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <utils/ProjectManager.hpp>
#include <utils/TreeMdiManager.hpp>

#include "utils.hpp"

namespace
{
class pmSGW : public SGraphicalWidget
{
public:
	pmSGW(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent) {}
	virtual ~pmSGW() override = default;

	virtual ShareablePointsList getContent() const override { return ShareablePointsList(); }

public slots:
	// On successful run _modified will be set to false
	virtual void runFinished(bool state) override { isModified(state); }

protected:
	virtual bool _save(const QString &) override { return true; }
	virtual bool _open(const QString &) override { return true; }
	virtual void _newEmpty() override {}
};

class pmLaunch : public SLaunchObject
{
public:
	pmLaunch(SPluginInterface *owner, QObject *parent = nullptr) : SLaunchObject(owner, parent) {}
	virtual ~pmLaunch() override = default;

	// SGraphicalInterface
	virtual QString exepath() const override { return "DummyTest"; }
	virtual bool isRunning() const noexcept override { return true; }
	virtual QString error() const override { return "Oh darn!"; }

public slots:
	// SGraphicalInterface
	virtual void launch(const QString &) override
	{
		QTimer::singleShot(100, [this]() { emit finished(true); });
	}
	virtual void stop() override {}
};

class pmPlugin : public QObject, public SPluginInterface
{
	Q_INTERFACES(SPluginInterface)

public:
	pmPlugin(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~pmPlugin() override = default;

	virtual const QString &name() const noexcept override { return _name; }
	virtual QIcon icon() const override { return QIcon(); }

	virtual void init() override {}
	virtual void terminate() override {}

	virtual bool hasConfigInterface() const noexcept override { return false; }
	virtual SConfigWidget *configInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget * = nullptr) override { return new pmSGW(this, nullptr); }
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return new pmLaunch(this, nullptr); }
	virtual QString getExtensions() const noexcept override { return ""; }
	virtual bool isMonoInstance() const noexcept override { return false; };

private:
	const QString _name = "IAmDummy!";
};
} // namespace

namespace tut
{
struct projectmanagerdata
{
	// Variables used globally for the test
	ProjectManager &pm = ProjectManager::getProjectManager();
	pmPlugin dumbPlug;
	QWidget tempParent;

	// Initializes mdiArea and projectTree
	projectmanagerdata()
	{
		pm.getMdiArea()->setParent(&tempParent);
		pm.getProjectTree()->setParent(&tempParent);
	}
	~projectmanagerdata()
	{
		// Cleaning the projects
		for (auto &project : pm.getProjects())
		{
			project->isModified(false);
			pm.closeProject(pm.findMdiWindow(project));
		}
		// Setting mdiArea and projectTree to nullptr
		pm.reset();
	}
};

typedef test_group<projectmanagerdata> tg;
tg uiplugins_projectmanager_test_group("Test ProjectManager class.");
typedef tg::object testobject;

template<>
template<>
void testobject::test<1>()
{
	set_test_name("ProjectManager: Test of singleton");

	ensure_equals(&ProjectManager::getProjectManager(), &ProjectManager::getProjectManager());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ProjectManager: Test getMdiArea");

	auto *mdiArea = pm.getMdiArea();
	ensure(mdiArea);
	ensure(mdiArea == pm.getMdiArea());
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ProjectManager: Test getProjectTree");

	pm.reset();
	auto *projectTree = pm.getProjectTree();
	projectTree->setParent(&tempParent);
	ensure(projectTree);
	ensure_not(projectTree->mdiArea());
	ensure(projectTree == pm.getProjectTree());
	// Checking if proper MdiArea was set
	pm.getMdiArea()->setParent(&tempParent);
	ensure(pm.getMdiArea() == projectTree->mdiArea());
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ProjectManager: Test getCurrentWidget");

	ensure_not(pm.getCurrentWidget());
	pm.newProject(&dumbPlug);
	auto *wdg = pm.getCurrentWidget();
	ensure(wdg);
	// Project changed
	pm.newProject(&dumbPlug);
	ensure_not(wdg == pm.getCurrentWidget());
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ProjectManager: Test newProject");

	pm.newProject(&dumbPlug);
	ensure(pm.getCurrentWidget());
	ensure_equals(pm.getProjects().size(), 1);
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ProjectManager: Test saveProject");

	// Succesfull save
	ensure(pm.saveProject("notEmptyPath", new pmSGW(&dumbPlug, &tempParent)));
	// Failed save
	ensure_not(pm.saveProject("", new pmSGW(&dumbPlug, &tempParent)));
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ProjectManager: Test openProject");

	// Failed open
	pm.openProject("", &dumbPlug);
	ensure_not(pm.getCurrentWidget());
	// Successful open
	pm.openProject("notEmptyPath", &dumbPlug);
	ensure(pm.getCurrentWidget());
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ProjectManager: Test closeProject");

	pm.newProject(&dumbPlug);
	pm.getCurrentWidget()->isModified(false);
	pm.closeProject(pm.findMdiWindow(pm.getCurrentWidget()));
	ensure_not(pm.getCurrentWidget());
	ensure_equals(pm.getProjects().size(), 0);
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ProjectManager: Test runProject");

	pmSGW *project = new pmSGW(&dumbPlug, &tempParent);
	auto *launcher = dumbPlug.launchInterface();
	QObject::connect(launcher, &SLaunchObject::finished, project, &SGraphicalWidget::runFinished);
	QEventLoop finishedLoop;
	QObject::connect(launcher, &SLaunchObject::finished, &finishedLoop, &QEventLoop::quit);
	// Unsuccessful run - no path or launcher
	pm.runProject(nullptr, "");
	ensure_not(project->isModified());
	// Successful run (should set _modified to false)
	pm.runProject(launcher, "path");
	finishedLoop.exec(); // Wait until launcher's finished signal fires
	ensure(project->isModified());
}

template<>
template<>
void testobject::test<20>()
{
	set_test_name("ProjectManager: Test getProjects");

	pm.newProject(&dumbPlug);
	pm.newProject(&dumbPlug);
	pm.newProject(&dumbPlug);
	ensure_equals(pm.getProjects().size(), 3);
}

template<>
template<>
void testobject::test<21>()
{
	set_test_name("ProjectManager: Test isModified");

	pm.newProject(&dumbPlug);
	pm.newProject(&dumbPlug);
	// Both modified
	ensure(pm.isModified());
	for (auto &project : pm.getProjects())
		project->save("");
	// Both not modified
	ensure_not(pm.isModified());
	// One modified
	pm.getCurrentWidget()->isModified(true);
	ensure(pm.isModified());
}

template<>
template<>
void testobject::test<22>()
{
	set_test_name("ProjectManager: Test findProject");

	pm.newProject(&dumbPlug);
	auto *projectOne = pm.getCurrentWidget();
	pm.saveProject("projectOne", projectOne);

	pm.newProject(&dumbPlug);
	auto *projectTwo = pm.getCurrentWidget();
	pm.saveProject("projectTwo", projectTwo);

	pm.newProject(&dumbPlug);
	auto *projectThree = pm.getCurrentWidget();

	ensure_equals(pm.findProject("projectOne"), projectOne);
	ensure_equals(pm.findProject(""), projectThree); // default path is empty
	ensure_equals(pm.findProject("projectTwo"), projectTwo);
        ensure(pm.findProject("IDoNotExist") == nullptr);
}

template<>
template<>
void testobject::test<23>()
{
	set_test_name("ProjectManager: Test findMdiWindow");

	pm.newProject(&dumbPlug);
	auto *window = pm.getMdiArea()->currentSubWindow();
	ensure_equals(pm.findMdiWindow(pm.getCurrentWidget()), window);

	pm.newProject(&dumbPlug);
	auto *windowOne = pm.getMdiArea()->currentSubWindow();
	auto *projectOne = pm.getCurrentWidget();

	pm.newProject(&dumbPlug);
	auto *windowTwo = pm.getMdiArea()->currentSubWindow();
	auto *projectTwo = pm.getCurrentWidget();

	pm.newProject(&dumbPlug);
	auto *windowThree = pm.getMdiArea()->currentSubWindow();
	auto *projectThree = pm.getCurrentWidget();

	ensure_equals(pm.findMdiWindow(projectOne), windowOne);
	ensure_equals(pm.findMdiWindow(projectThree), windowThree);
	ensure_equals(pm.findMdiWindow(projectTwo), windowTwo);
        ensure(pm.findMdiWindow(static_cast<SGraphicalWidget *>(nullptr)) == nullptr);
}
} // namespace tut
