#pragma warning(push)
#pragma warning(disable : 4512)
#include <tut/tut.hpp>
#include <Behavior.h>
#include <StringManager.h>
#pragma warning(pop)



namespace tut
{
	struct test_Behavior{};
	typedef test_group<test_Behavior> factory;
	typedef factory::object object;
}

namespace
{
	tut::factory tf("Test Behavior");
}

namespace tut
{
	// Test default constructor
	template<>
	template<>
	void object::test<1>()
	{
		Behavior def;
		ensure_equals("behavior::ctor_default::size ", def.size(), 0);
	}


	// Test copy constructor
	template<>
	template<>
	void object::test<2>()
	{
		Behavior bug(Behavior::BehaviorCode::ERR_projectFileMissing, L"MyProject.xxx");
		bug += Behavior(Behavior::BehaviorCode::ERR_inputData, L"Sigma error");

		Behavior copy(bug);
		ensure_equals("behavior::ctor_copy::size ", copy.size(), 2);
		std::vector<Behavior> bugSplit = bug.split();
		std::vector<Behavior> copySplit = copy.split();

		for(size_t i = 0; i < bugSplit.size(); i++)
		{
			bool match = false;
			for(size_t j = 0; j < copySplit.size();j++)
				if(bug.code() == copy.code() && bug.additionalInfo(bug.code()) == copy.additionalInfo(copy.code()))
					match = true;

			// Only ensure that we match pairs for every objects
			ensure(match);
		}
	}

	// Test param constructor
	template<>
	template<>
	void object::test<3>()
	{
		Behavior param(Behavior::BehaviorCode::ERR_LSCalculation, L"LS problem");
		std::vector<Behavior> vec = param.split();
		ensure_equals("behavior::ctor_param::size ", vec.size(), 1);
		ensure_equals("behavior::ctor_param::code ", vec[0].code(), Behavior::BehaviorCode::ERR_LSCalculation);
		ensure_equals("behavior::ctor_param::info ", toStr(vec[0].additionalInfo(vec[0].code())), "LS problem");
	}

	// Test : Operator=
	template<>
	template<>
	void object::test<4>()
	{
		Behavior def;
		Behavior errs(Behavior::BehaviorCode::ERR_LSCalculation, L"LS problem");
		errs += Behavior(Behavior::BehaviorCode::ERR_inputData, L"Sigma error");

		def = errs;
		std::vector<Behavior> behaviors = def.split();
		ensure_equals("behavior::op=::size", def.size(), 2);
		for (int i = 0; i < def.size(); i++)
		{
			if (behaviors[i].code() == Behavior::BehaviorCode::ERR_LSCalculation)
				ensure_equals("behavior::op=::info1", toStr(behaviors[i].additionalInfo(behaviors[i].code())), "LS problem");
			else if (behaviors[i].code() == Behavior::BehaviorCode::ERR_inputData)
				ensure_equals("behavior::op=::info2", toStr(behaviors[i].additionalInfo(behaviors[i].code())), "Sigma error");
			else
				ensure("we shouldn't arrive here", false);
		}
	}

	// Test bool()
	template<>
	template<>
	void object::test<5>()
	{
		Behavior noError;
		ensure_equals("behavior::bool()", noError.size(), 0);
		ensure_equals("behavior::bool()", noError == true, true);
	
		noError += Behavior(Behavior::BehaviorCode::ERR_noError);
		ensure_equals("behavior::bool()", noError.size(), 1);
		ensure_equals("behavior::bool()", noError == true, true);

		noError += Behavior(Behavior::BehaviorCode::ERR_fileUnReadable);
		ensure_equals("behavior::bool()", noError.size(), 2);
		ensure_equals("behavior::bool()", noError == true, false);

		Behavior error(Behavior::BehaviorCode::ERR_fileUnReadable);
		ensure_equals("behavior::bool()", error.size(), 1);
		ensure_equals("behavior::bool()", error == true, false);
	}

	// Test extract()
	template<>
	template<>
	void object::test<6>()
	{
		Behavior errs(Behavior::BehaviorCode::ERR_LSCalculation, L"LS problem");
		errs += Behavior(Behavior::BehaviorCode::ERR_inputData, L"Sigma error");

		Behavior extracted = errs.extract(Behavior::BehaviorCode::ERR_inputData);

		ensure_equals("behavior::extract()", errs.size(), 1);
		ensure_equals("behavior::extract()", extracted.size(), 1);
		ensure_equals("behavior::extract()", errs.code(), Behavior::BehaviorCode::ERR_LSCalculation);
		ensure_equals("behavior::extract()", extracted.code(), Behavior::BehaviorCode::ERR_inputData);
		ensure_equals("behavior::extract()", toStr(errs.additionalInfo(errs.code())), "LS problem");
		ensure_equals("behavior::extract()", toStr(extracted.additionalInfo(extracted.code())), "Sigma error");
	}


	// Test operator== && operator[]
	template<>
	template<>
	void object::test<7>()
	{
		Behavior errs(Behavior::BehaviorCode::ERR_LSCalculation, L"LS problem");
		errs += Behavior(Behavior::BehaviorCode::ERR_inputData, L"Sigma error");

		ensure("behavior::op==",(errs == Behavior::BehaviorCode::ERR_inputData) == true);
		ensure("behavior::op==", (errs == Behavior::BehaviorCode::ERR_LSCalculation) == true);
		ensure("behavior::op==", (errs == Behavior::BehaviorCode::ERR_ignoreChanges) == false);
		ensure("behavior::op[]", errs[Behavior::BehaviorCode::ERR_inputData] == true);
		ensure("behavior::op[]", errs[Behavior::BehaviorCode::ERR_LSCalculation] == true);
		ensure("behavior::op[]", errs[Behavior::BehaviorCode::ERR_ignoreChanges] == false);

		Behavior noError;
		ensure("behavior::op==", (noError == Behavior::BehaviorCode::ERR_noError) == true);
		ensure("behavior::op[]", noError[Behavior::BehaviorCode::ERR_noError] == true);
	}

	// Test operator+= & split()
	template<>
	template<>
	void object::test<8>()
	{
		Behavior err1(Behavior::BehaviorCode::ERR_LSCalculation, L"1");
		err1 += Behavior(Behavior::BehaviorCode::ERR_inputData, L"2");
		
		Behavior err2(Behavior::BehaviorCode::ERR_noOutputFile, L"3");
		err2 += Behavior(Behavior::BehaviorCode::ERR_outputFileMissing, L"4");

		err1 += Behavior();
		err1 += err2;
		
		std::vector<Behavior> split = err1.split();
		for (size_t i = 0; i < split.size(); i++)
		{
			if (split[i].code() == Behavior::BehaviorCode::ERR_LSCalculation)
				ensure_equals("behavior::op+=::info1", toStr(split[i].additionalInfo(split[i].code())), "1");
			else if (split[i].code() == Behavior::BehaviorCode::ERR_inputData)
				ensure_equals("behavior::op+=::info1", toStr(split[i].additionalInfo(split[i].code())), "2");
			else if (split[i].code() == Behavior::BehaviorCode::ERR_noOutputFile)
				ensure_equals("behavior::op+=::info1", toStr(split[i].additionalInfo(split[i].code())), "3");
			else if (split[i].code() == Behavior::BehaviorCode::ERR_outputFileMissing)
				ensure_equals("behavior::op+=::info1", toStr(split[i].additionalInfo(split[i].code())), "4");
			else
				ensure("we shouldn't arrive here", false);
		}
	}

	// Test code() & additionalInfo() & size()
	template<>
	template<>
	void object::test<9>()
	{
		Behavior behavior(Behavior::BehaviorCode::ERR_LSCalculation, L"1");
		ensure_equals("behavior::code()", behavior.code(), Behavior::BehaviorCode::ERR_LSCalculation);
		ensure_equals("behavior::info()", toStr(behavior.additionalInfo(behavior.code())), "1");
		ensure_equals("behavior::size()", behavior.size(), 1);

		Behavior noError;
		ensure_equals("behavior::code()", noError.code(), Behavior::BehaviorCode::ERR_noError);
		ensure_equals("behavior::info()", toStr(noError.additionalInfo(noError.code())), "");
		ensure_equals("behavior::size()", noError.size(), 0);
	}

	// Test getType()
	template<>
	template<>
	void object::test<10>()
	{
		auto check = [](Behavior const& behavior)
		{
			switch(behavior.code())
			{
				// INFO
			case Behavior::BehaviorCode::ERR_noError:
			case Behavior::BehaviorCode::ERR_savingOk:
			case Behavior::BehaviorCode::ERR_endOfTheWorld:
			case Behavior::BehaviorCode::ERR_actionCanceled:
				ensure_equals("Behavior::getType()", behavior.getType(), Behavior::Type::Info); break;


				// ERROR
			case Behavior::ERR_projectFileMissing:
			case Behavior::ERR_inputFileMissing:
			case Behavior::ERR_savingProjectFile:
			case Behavior::ERR_savingFile:
			case Behavior::ERR_transformation:
			case Behavior::ERR_unknownExcptInTransformation:
			case Behavior::ERR_inputData:
			case Behavior::ERR_LSCalculation:
			case Behavior::ERR_results:
				ensure_equals("Behavior::getType()", behavior.getType(), Behavior::Type::Error); break;

				// WARNING
			case Behavior::ERR_fileUnReadable:
			case Behavior::ERR_outputFileMissing:
			case Behavior::ERR_noOutputFile:
			case Behavior::ERR_ignoreChanges:
			case Behavior::ERR_readingContent:
			case Behavior::ERR_virtualProjectRemoved:
				ensure_equals("Behavior::getType()", behavior.getType(), Behavior::Type::Warning); break;
			}
		};

		Behavior behavior1(Behavior::BehaviorCode::ERR_noError);
		Behavior behavior2(Behavior::BehaviorCode::ERR_savingOk);
		Behavior behavior3(Behavior::BehaviorCode::ERR_endOfTheWorld);
		Behavior behavior4(Behavior::BehaviorCode::ERR_actionCanceled);
		Behavior behavior5(Behavior::BehaviorCode::ERR_projectFileMissing);
		Behavior behavior6(Behavior::BehaviorCode::ERR_inputFileMissing);
		Behavior behavior7(Behavior::BehaviorCode::ERR_savingProjectFile);
		Behavior behavior8(Behavior::BehaviorCode::ERR_savingFile);
		Behavior behavior9(Behavior::BehaviorCode::ERR_transformation);
		Behavior behavior10(Behavior::BehaviorCode::ERR_unknownExcptInTransformation);
		Behavior behavior11(Behavior::BehaviorCode::ERR_inputData);
		Behavior behavior12(Behavior::BehaviorCode::ERR_LSCalculation);
		Behavior behavior13(Behavior::BehaviorCode::ERR_results);
		Behavior behavior14(Behavior::BehaviorCode::ERR_fileUnReadable);
		Behavior behavior15(Behavior::BehaviorCode::ERR_outputFileMissing);
		Behavior behavior16(Behavior::BehaviorCode::ERR_noOutputFile);
		Behavior behavior17(Behavior::BehaviorCode::ERR_ignoreChanges);
		Behavior behavior18(Behavior::BehaviorCode::ERR_readingContent);
		Behavior behavior19(Behavior::BehaviorCode::ERR_virtualProjectRemoved);

		check(behavior1);
		check(behavior2);
		check(behavior3);
		check(behavior4);
		check(behavior5);
		check(behavior6);
		check(behavior7);
		check(behavior8);
		check(behavior9);
		check(behavior10);
		check(behavior11);
		check(behavior12);
		check(behavior13);
		check(behavior14);
		check(behavior15);
		check(behavior16);
		check(behavior17);
		check(behavior18);
		check(behavior19);
	}
}
