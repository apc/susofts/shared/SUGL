#include <tut/tut.hpp>

#include <ConsoleLogHandler.hpp>
#include <FileLogHandler.hpp>
#include <ILogHandler.hpp>
#include <Logger.hpp>

namespace tut
{
struct uipluginsdlldata
{
	~uipluginsdlldata()
	{
		auto &logger = Logger::getLogger();
		logger.clearCounters();
		logger.clearHandlers();
	}
};

typedef test_group<uipluginsdlldata> tg;
tg uiplugins_dll_test_group("Test of the UIPlugins shared library.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("UIPlugins shared library: Test of Logs");

	class DumbHandler : public ILogHandler
	{
	public:
		void log(const LogMessage &m) override
		{
			msg.setMessage(m.getMessage());
			msg.setFile(m.getFile());
			msg.setLine(m.getLine());
			msg.setFunction(m.getFunction());
			msg.setType(m.getType());
		};
		LogMessage msg{LogMessage::Type::WARNING};
	};

	auto &logger = Logger::getLogger();
	auto *h = new DumbHandler();
	h->setThreshold(LogMessage::Type::DEBUG);
	logger.addHandlers(h);
	int line = -1;

	line = __LINE__ + 1;
	logDebug() << "this is"
			   << "an error message:" << 45 << ' ' << 12.3;
	ensure_equals(h->msg.getType(), LogMessage::Type::DEBUG);
	ensure_equals(h->msg.getMessage(), "this is an error message: 45   12.3");
	ensure_equals(h->msg.getLine(), line);
	ensure(h->msg.getFunction().find("test") != std::string::npos);
	ensure_equals(logger.errorNumber(), 0UL);
	ensure_equals(logger.warningNumber(), 0UL);
	ensure_not(logger.hasErrors());
	ensure_not(logger.hasWarnings());
}
} // namespace tut
