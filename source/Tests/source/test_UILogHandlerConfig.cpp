#include <Qsci/qsciscintilla.h>

#include <tut/tut.hpp>

#include <LogMessage.hpp>
#include <QtStreams.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/UILogHandlerConfig.hpp>

namespace
{
UILogHandlerConfigObject getConfig()
{
	UILogHandlerConfigObject t;
	// Log level
	t.logLevel = static_cast<int>(LogMessage::Type::CRITICAL);
	// Column visibilty
	t.typeVisible = false;
	t.timeVisible = false;
	t.contextVisible = true;
	t.qtLogEnabled = false;
	t.popupEnabled = false;

	return t;
}
} // namespace

namespace tut
{
struct uiloghandlerconfigdata
{
	uiloghandlerconfigdata() { SettingsManager::init("uiloghandlerapp", "version"); }
	~uiloghandlerconfigdata() { std::remove(SettingsManager::settings().innerSettings("settingsFile").toUtf8().constData()); }
};

typedef test_group<uiloghandlerconfigdata> tg;
tg uiplugins_uiloghandlerconfig_test_group("Test UILogHandlerConfig class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
/* ************************ */
/* UILogHandlerConfigObject */
/* ************************ */

template<>
template<>
void testobject::test<1>()
{
	set_test_name("UILogHandlerConfigObject: Test of default constructor");

	UILogHandlerConfigObject t;

	// Log level
	ensure(t.logLevel == static_cast<int>(LogMessage::Type::INFO));
	// Column visibilty
	ensure(t.typeVisible == true);
	ensure(t.timeVisible == true);
	ensure(t.contextVisible == true);
	ensure(t.qtLogEnabled == false);
	ensure(t.popupEnabled == true);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("UILogHandlerConfigObject: Test of empty read()");

	UILogHandlerConfigObject t;
	t.read();

	// Log level
	ensure(t.logLevel == static_cast<int>(LogMessage::Type::INFO));
	// Column visibilty
	ensure(t.typeVisible == true);
	ensure(t.timeVisible == true);
	ensure(t.contextVisible == true);
	ensure(t.qtLogEnabled == false);
	ensure(t.popupEnabled == true);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("UILogHandlerConfigObject: Test of read() and write()");

	{
		UILogHandlerConfigObject t = getConfig();
		t.write();
	}

	UILogHandlerConfigObject t;
	t.read();
	ensure(t == getConfig());
}

/* ************************ */
/*    UILogHandlerConfig    */
/* ************************ */

template<>
template<>
void testobject::test<10>()
{
	set_test_name("UILogHandlerConfig: Test of setConfig(), reset() and restoreDefaults()");

	UILogHandlerConfig widget;
	// initialization
	{
		auto t = getConfig();
		t.write();
		// we change `t` so it is different from what is written (for `reset()`)
		t.contextVisible = false;
		widget.setConfig(&t);
	}
	// test of getConfig()
	{
		std::unique_ptr<UILogHandlerConfigObject> t(dynamic_cast<UILogHandlerConfigObject *>(widget.config()));
		ensure(t != nullptr);
		auto test = getConfig();
		test.contextVisible = false;
		ensure(*t == test);
	}
	// test of reset()
	{
		widget.reset();
		std::unique_ptr<UILogHandlerConfigObject> t(dynamic_cast<UILogHandlerConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == getConfig());
	}
	// test of restoreDefaults()
	{
		widget.restoreDefaults();
		std::unique_ptr<UILogHandlerConfigObject> t(dynamic_cast<UILogHandlerConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == UILogHandlerConfigObject());
	}
}
} // namespace tut
