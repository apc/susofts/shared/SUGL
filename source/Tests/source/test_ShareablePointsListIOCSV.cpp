#include <algorithm>

#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <io/ShareablePointsListIOCSV.hpp>

namespace tut
{
struct shareablepointslistiocsvdata
{
};

typedef test_group<shareablepointslistiocsvdata> tg;
tg uiplugins_spliocsv_test_group("Test ShareablePointsListIOCSV class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ShareablePointsListIOCSV: Test of base class");

	ShareablePointsListIOCSV csv;
	ensure_equals(csv.writeHeaders(), true);
	ensure_equals(csv.getSeparator(), ',');
	ensure_equals(csv.getTextDelimiter(), '"');
	ensure_equals(csv.getMIMEType(), "text/csv");

	csv.writeHeaders(false);
	csv.setSeparator(';');
	csv.setTextDelimiter('@');
	ensure_equals(csv.writeHeaders(), false);
	ensure_equals(csv.getSeparator(), ';');
	ensure_equals(csv.getTextDelimiter(), '@');
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ShareablePointsListIOCSV: Test of reading bad CSV");

	// emtpy
	ShareablePointsListIOCSV csv;
	ensure_THROW(csv.read(""), SPIOException);

	// garbage
	ensure_THROW(csv.read("lol"), SPIOException);
	ensure_THROW(csv.read("lol\nlol"), SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ShareablePointsListIOCSV: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false};

	ShareablePointsListIOCSV csv;
	ShareablePosition loaded = csv.readPosition(csv.write(position));
	ensure("CSV test", position == loaded);

	// not complete
	csv.exportFieldsPosition.removeField("sigmax", "sigmay", "isfreey");
	loaded = csv.readPosition(csv.write(position));
	ensure_equals(loaded.x, position.x);
	ensure_equals(loaded.y, position.y);
	ensure_equals(loaded.z, position.z);
	ensure_equals(loaded.sigmax, 0);
	ensure_equals(loaded.sigmay, 0);
	ensure_equals(loaded.sigmaz, position.sigmaz);
	ensure_equals(loaded.isfreex, position.isfreex);
	ensure_not(loaded.isfreey);
	ensure_equals(loaded.isfreez, position.isfreez);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ShareablePointsListIOCSV: Test of extraInfos IO");

	const ShareableExtraInfos infos{{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}, {"params4", "value4"}};

	ShareablePointsListIOCSV csv;
	ShareableExtraInfos loaded = csv.readExtraInfos(csv.write(infos));
	ensure(loaded == ShareableExtraInfos{});
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ShareablePointsListIOCSV: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false}, "inline comment", "header comment", false};

	ShareablePointsListIOCSV csv;
	ShareablePoint loaded = csv.readPoint(csv.write(point));
	ensure("CSV test", point == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ShareablePointsListIOCSV: Test of params IO");

	const ShareableParams params{4, ShareableParams::ECoordSys::kGeodeticSphere, {{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}}};

	ShareablePointsListIOCSV csv;
	const std::string &written = csv.write(params);
	ShareableParams loaded = csv.readParams(written);
	ensure(written.empty());
	ensure(loaded == ShareableParams{});
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ShareablePointsListIOCSV: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	frame.addPoint().name = "p0";
	frame.addPoint().name = "p1";
	frame.addFrame().addPoint().name = "p2";
	frame.addFrame().addPoint().name = "p3";
	auto &child3 = frame.addFrame();
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	child3.addPoint().name = "p5";
	child3.addFrame().addPoint().name = "p6";

	ShareablePointsListIOCSV csv;
	ShareableFrame loaded = csv.readFrame(csv.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure("CSV test", std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ShareablePointsListIOCSV: Test of points list IO");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	ShareablePointsListIOCSV csv;
	ShareablePointsList loaded = csv.read(csv.write(spl));
	ensure("CSV test", spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("ShareablePointsListIOCSV: Test read and write files");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	ShareablePointsListIOCSV csv;
	IShareablePointsListIO::writeFile("splistcsvTest.csv", csv.write(spl));
	auto loaded = csv.read(IShareablePointsListIO::readFile("splistcsvTest.csv"));
	ensure("CSV test", spl == loaded);
	std::remove("splistcsvTest.csv");
}

template<>
template<>
void testobject::test<20>()
{
	set_test_name("ShareablePointsListIOCSV: Header tests - standard header");
	ShareablePointsListIOCSV csv;
	ShareablePointsList spl = csv.read("name,x,y,h,sigmax,sigmay,sigmaz,isfreex,isfreey,isfreez\n\
		point,1,2,3,0.1,0.2,0.3,true,false,true");
	ShareablePoint point = spl.getRootFrame().getPoint(0);

	ensure_equals(point.name, "point");
	ensure_equals(point.position.x, 1);
	ensure_equals(point.position.y, 2);
	ensure_equals(point.position.z, 3); // h changed to z after parsing
	ensure_equals(point.position.sigmax, 0.1);
	ensure_equals(point.position.sigmay, 0.2);
	ensure_equals(point.position.sigmaz, 0.3);
	ensure_equals(point.position.isfreex, true);
	ensure_not(point.position.isfreey);
	ensure_equals(point.position.isfreez, true);
}

template<>
template<>
void testobject::test<21>()
{
	set_test_name("ShareablePointsListIOCSV: Header tests - unit header");
	ShareablePointsListIOCSV csv;
	ShareablePointsList spl = csv.read("name,X [m],Y [m],H [m],sigmax,sigmay,sigmaz,isfreex,isfreey,isfreez\n\
		point,1,2,3,0.1,0.2,0.3,true,false,true");
	ShareablePoint point = spl.getRootFrame().getPoint(0);

	ensure_equals(point.name, "point");
	ensure_equals(point.position.x, 1);
	ensure_equals(point.position.y, 2);
	ensure_equals(point.position.z, 3);// H [m] changed to z after parsing
	ensure_equals(point.position.sigmax, 0.1);
	ensure_equals(point.position.sigmay, 0.2);
	ensure_equals(point.position.sigmaz, 0.3);
	ensure_equals(point.position.isfreex, true);
	ensure_not(point.position.isfreey);
	ensure_equals(point.position.isfreez, true);
}
} // namespace tut
