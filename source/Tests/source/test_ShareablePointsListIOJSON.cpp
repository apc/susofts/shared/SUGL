#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <io/ShareablePointsListIOJson.hpp>

namespace tut
{
struct shareablepointslistiojsondata
{
};

typedef test_group<shareablepointslistiojsondata> tg;
tg uiplugins_spliojson_test_group("Test ShareablePointsListIOJson class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ShareablePointsListIOJson: Test of base class");

	ShareablePointsListIOJSon json;
	ensure_equals(json.isBinary(), false);
	ensure_equals(json.isIndented(), true);
	ensure_equals(json.getMIMEType(), "application/json");

	json.isBinary(true);
	json.isIndented(false);
	ensure_equals(json.isBinary(), true);
	ensure_equals(json.isIndented(), false);
	ensure_equals(json.getMIMEType(), "application/vnd.cern.susoft.binary.json");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ShareablePointsListIOJson: Test of reading bad JSON");

	// emtpy
	ShareablePointsListIOJSon json;
	ensure_THROW(json.read(""), SPIOException);
	json.isBinary(true);
	ensure_THROW(json.read(""), SPIOException);

	// garbage
	json.isBinary(false);
	ensure_THROW(json.read("lol"), SPIOException);
	json.isBinary(true);
	ensure_THROW(json.read("lol"), SPIOException);

	// array
	json.isBinary(false);
	ensure_THROW(json.read("[]"), SPIOException);
	json.isBinary(true);
	ensure_THROW(json.read("[]"), SPIOException);

	// empty object
	json.isBinary(false);
	ensure_THROW(json.read("{}"), SPIOException);
	json.isBinary(true);
	ensure_THROW(json.read("{}"), SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ShareablePointsListIOJson: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false};

	// JSON
	ShareablePointsListIOJSon json;
	ShareablePosition loaded = json.readPosition(json.write(position));
	ensure("JSON test", position == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.readPosition(json.write(position));
	ensure("Binary test", position == loaded);

	// not complete
	json.isBinary(false);
	json.exportFieldsPosition.removeField("sigmax", "sigmay", "isfreey");
	loaded = json.readPosition(json.write(position));
	ensure_equals(loaded.x, position.x);
	ensure_equals(loaded.y, position.y);
	ensure_equals(loaded.z, position.z);
	ensure_equals(loaded.sigmax, 0);
	ensure_equals(loaded.sigmay, 0);
	ensure_equals(loaded.sigmaz, position.sigmaz);
	ensure_equals(loaded.isfreex, position.isfreex);
	ensure_not(loaded.isfreey);
	ensure_equals(loaded.isfreez, position.isfreez);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ShareablePointsListIOJson: Test of extraInfos IO");

	const ShareableExtraInfos infos{{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}, {"params4", "value4"}};

	// JSON
	ShareablePointsListIOJSon json;
	ShareableExtraInfos loaded = json.readExtraInfos(json.write(infos));
	ensure("JSON test", infos == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.readExtraInfos(json.write(infos));
	ensure("Binary test", infos == loaded);
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ShareablePointsListIOJson: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false}, "inline comment", "header comment", false,
		{{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}}};

	// JSON
	ShareablePointsListIOJSon json;
	ShareablePoint loaded = json.readPoint(json.write(point));
	ensure("JSON test", point == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.readPoint(json.write(point));
	ensure("Binary test", point == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ShareablePointsListIOJson: Test of params IO");

	const ShareableParams params{4, ShareableParams::ECoordSys::kGeodeticSphere, {{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}}};

	// JSON
	ShareablePointsListIOJSon json;
	ShareableParams loaded = json.readParams(json.write(params));
	ensure("JSON test", params == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.readParams(json.write(params));
	ensure("Binary test", params == loaded);
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ShareablePointsListIOJson: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params, "root");
	frame.setTranslation({12, 13, 14, 0.00001, 0.00001, 0.00001, false, true, false});
	frame.setRotation({0.1, 0.2, 0.3, 0.00001, 0.00001, 0.00001, true, true, false});
	frame.setScale(12.4569);
	frame.isFreeScale(true);

	frame.addPoint().name = "p0";
	frame.addPoint().name = "p1";
	frame.addFrame().addPoint().name = "p2";
	frame.addFrame().addPoint().name = "p3";
	auto &child3 = frame.addFrame();
	child3.setName("child3");
	child3.setTranslation({12, 13, 14, 12, 12, 12, false, true, false});
	child3.setRotation({0.1, 0.2, 0.3, 12, 12, 12, true, true, false});
	child3.setScale(4569);
	child3.isFreeScale(false);
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false, {{"params", "lol"}}});
	child3.addPoint().name = "p5";
	child3.addFrame().addPoint().name = "p6";

	// JSON
	ShareablePointsListIOJSon json;
	ShareableFrame loaded = json.readFrame(json.write(frame));
	ensure("JSON test", frame == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.readFrame(json.write(frame));
	ensure("Binary test", frame == loaded);
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ShareablePointsListIOJson: Test of points list IO");

	ShareablePointsList spl("list");
	spl.getParams().precision = 12;
	spl.getParams().coordsys = ShareableParams::ECoordSys::k2DPlusH;
	spl.getParams().extraInfos = {{"lol", "ketru"}, {"lil", "truc"}};
	auto &frame = spl.getRootFrame().addFrame();
	frame.setName("child3");
	frame.setTranslation({12, 13, 14, 12, 12, 12, false, true, false});
	frame.setRotation({0.1, 0.2, 0.3, 12, 12, 12, true, true, false});
	frame.setScale(4569);
	frame.isFreeScale(false);
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false, {{"params", "lol"}}});
	frame.addPoint().name = "p5";
	frame.addFrame().addPoint().name = "p6";

	// JSON
	ShareablePointsListIOJSon json;
	ShareablePointsList loaded = json.read(json.write(spl));
	ensure("JSON test", spl == loaded);
	// DAT
	json.isBinary(true);
	loaded = json.read(json.write(spl));
	ensure("Binary test", spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("ShareablePointsListIOJson: Test read and write files");

	ShareablePointsList spl("list");
	spl.getParams().precision = 12;
	spl.getParams().coordsys = ShareableParams::ECoordSys::k2DPlusH;
	spl.getParams().extraInfos = {{"lol", "ketru"}, {"lil", "truc"}};
	auto &frame = spl.getRootFrame().addFrame();
	frame.setName("child3");
	frame.setTranslation({12, 13, 14, 12, 12, 12, false, true, false});
	frame.setRotation({0.1, 0.2, 0.3, 12, 12, 12, true, true, false});
	frame.setScale(4569);
	frame.isFreeScale(false);
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false, {{"params", "lol"}}});
	frame.addPoint().name = "p5";
	frame.addFrame().addPoint().name = "p6";

	// JSON
	ShareablePointsListIOJSon json;
	IShareablePointsListIO::writeFile("splistjsonTest.json", json.write(spl));
	auto loaded = json.read(IShareablePointsListIO::readFile("splistjsonTest.json"));
	ensure("JSON test", spl == loaded);
	std::remove("splistjsonTest.json");
	// DAT
	json.isBinary(true);
	IShareablePointsListIO::writeFile("splistjsonTest.dat", json.write(spl), true);
	loaded = json.read(IShareablePointsListIO::readFile("splistjsonTest.dat", true));
	ensure("Binary test", spl == loaded);
	std::remove("splistjsonTest.dat");
}
} // namespace tut
