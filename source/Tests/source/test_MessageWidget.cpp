#pragma warning(push)
#pragma warning(disable : 4512)
#include <tut/tut.hpp>
#include <MessageWidget.h>
#pragma warning(pop)



namespace tut
{
	struct test_MessageWidget{};
	typedef test_group<test_MessageWidget> factory;
	typedef factory::object object;
}

namespace
{
	tut::factory tf("Test MessageWidget");
}

namespace tut
{
	
	template<>
	template<>
	void object::test<1>()
	{
		auto check = [](MessageWidget const& mw, Behavior::Type const& type, QString const& mainLabel, QString const& secondLabel) 
		{
			
			ensure_equals("MessageWidget::type : ", mw.getType(), type);
			ensure_equals("MessageWidget::mainLabel : ", mw.getMainLabel().toStdString(), mainLabel.toStdString().c_str());
			ensure_equals("MessageWidget::secondLabel : ", mw.getSecondLabel().toStdString(), secondLabel.toStdString().c_str());

		};

		for(int i = int(Behavior::BehaviorCode::ERR_noError);
				i < int(Behavior::BehaviorCode::ERR_endOfTheWorld);
				i++)
		{
			MessageWidget mw(Behavior(Behavior::BehaviorCode(i), L"abc"));

			ensure_equals("MessageWidget::type", mw.objectName().toStdString(), "MessageWidget");
			ensure_equals("MessageWidget::type", mw.minimumHeight(), 0);

			switch(Behavior::BehaviorCode(i))
			{
			case(Behavior::BehaviorCode::ERR_noError) : 
				check(mw, Behavior::Type::Info, "", "abc"); break;
			case(Behavior::BehaviorCode::ERR_savingOk) : 
				check(mw, Behavior::Type::Info, "Files saved", "abc"); break;
			case(Behavior::BehaviorCode::ERR_actionCanceled) : 
				check(mw, Behavior::Type::Info, "Action canceled", "abc"); break;
			case(Behavior::BehaviorCode::ERR_projectFileMissing) : 
				check(mw, Behavior::Type::Error, "Project file is missing", "abc"); break;
			case(Behavior::BehaviorCode::ERR_inputFileMissing) : 
				check(mw, Behavior::Type::Error, "Input file is missing or corrupted", "abc"); break;
			case(Behavior::BehaviorCode::ERR_fileUnReadable) : 
				check(mw, Behavior::Type::Warning, "Input file exists but can't be read", "abc"); break;
			case(Behavior::BehaviorCode::ERR_outputFileMissing) : 
				check(mw, Behavior::Type::Warning, "An output file is missing", "abc"); break;
			case(Behavior::BehaviorCode::ERR_noOutputFile) : 
				check(mw, Behavior::Type::Warning, "No output file has been loaded", "abc"); break;
			case(Behavior::BehaviorCode::ERR_savingProjectFile) : 
				check(mw, Behavior::Type::Error, "Unable to save the project file", "abc"); break;
			case(Behavior::BehaviorCode::ERR_savingFile) : 
				check(mw, Behavior::Type::Error, "Unable to save the file", "abc"); break;
			case(Behavior::BehaviorCode::ERR_ignoreChanges) : 
				check(mw, Behavior::Type::Warning, "Changes ignored", "abc"); break;
			case(Behavior::BehaviorCode::ERR_readingContent) : 
				check(mw, Behavior::Type::Warning, "Error when reading file content", "abc"); break;
			case(Behavior::BehaviorCode::ERR_transformation) : 
				check(mw, Behavior::Type::Error, "Transformation exception", "abc"); break;
			case(Behavior::BehaviorCode::ERR_unknownExcptInTransformation) : 
				check(mw, Behavior::Type::Error, "Unknown Exception when transforming", "abc"); break;
			case(Behavior::BehaviorCode::ERR_virtualProjectRemoved) : 
				check(mw, Behavior::Type::Warning, "Virtual project removed because empty", "abc"); break;
			case(Behavior::BehaviorCode::ERR_inputData) : 
				check(mw, Behavior::Type::Error, "Error in the Input Data (LS process)", "abc"); break;
			case(Behavior::BehaviorCode::ERR_LSCalculation) : 
				check(mw, Behavior::Type::Error, "Error in the Least Square Process", "abc"); break;
			case(Behavior::BehaviorCode::ERR_results) : 
				check(mw, Behavior::Type::Error, "Problem with results", "abc"); break;
			case(Behavior::BehaviorCode::ERR_endOfTheWorld) : 
				check(mw, Behavior::Type::Info, "The end of the world will come soon.", "abc"); break;
			}
		}
	}
}
