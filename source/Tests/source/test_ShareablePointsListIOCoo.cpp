#include <algorithm>

#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <QtStreams.hpp>
#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <io/CooLexerIO.hpp>

namespace tut
{
struct coolexeriodata
{
};

typedef test_group<coolexeriodata> tg;
tg uiplugins_coolexerio_test_group("Test CooLexerIO class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("CooLexerIO: Test of base class");

	CooLexerIO coo;
	ensure(coo.columnsHeader().isEmpty());
	ensure_equals(coo.getMIMEType(), "application/vnd.cern.susoft.surveypad.cooPoint");

	coo.columnsHeader("lol");
	ensure_equals(coo.columnsHeader(), "lol");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("CooLexerIO: Test of reading bad coo");

	// emtpy
	CooLexerIO coo;
	ensure_THROW(coo.read(""), SPIOException);

	// garbage
	ensure_THROW(coo.read("lol"), SPIOException);

	// good start
	ensure_THROW(coo.read("#\n#\n\n %NOM"), SPIOException);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("CooLexerIO: Test of calling virtual functions");

	class dummyLexer : public CooLexerIO
	{
	public:
		dummyLexer(QObject *parent = nullptr) : CooLexerIO(parent) {}
		~dummyLexer() override = default;

	public:
		int pointCount = 0;
		int coordinateCount = 0;
		int sigmaCount = 0;
		int displacementCount = 0;
		int infoCount = 0;
		int typeCount = 0;

	protected:
		virtual void gotPointName([[maybe_unused]] QStringRef sym) override { pointCount++; }
		virtual void gotPointCoordinate([[maybe_unused]] QStringRef sym) override { coordinateCount++; }
		virtual void gotPointSigma([[maybe_unused]] QStringRef sym) override { sigmaCount++; }
		virtual void gotPointDisplacement([[maybe_unused]] QStringRef sym) override { displacementCount++; }
		virtual void gotPointInfo([[maybe_unused]] QStringRef sym) override { infoCount++; }
		virtual void gotPointType([[maybe_unused]] QStringRef sym) override { typeCount++; }
	};

	std::string text = R"""(
	# comment
	#
	%
	%NOM X Y Z ID DX DY DZ DCUM OPTION
	% (M) (M) (M) (MM) (MM) (MM)
	%
	a      1   1   1   11   22   22   22   3333    CALA
	)""";

	// emtpy
	dummyLexer coo;
	coo.read(text);

	ensure_equals(coo.columnsHeader(), "%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	ensure_equals(coo.pointCount, 1);
	ensure_equals(coo.coordinateCount, 3);
	ensure_equals(coo.sigmaCount, 0);
	ensure_equals(coo.displacementCount, 3);
	ensure_equals(coo.infoCount, 2);
	ensure_equals(coo.typeCount, 1);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("CooLexerIO: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12};

	// coo
	CooLexerIO coo;
	ShareablePosition loaded = coo.readPosition(coo.write(position));
	ensure(position == loaded);

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.readPosition(coo.write(position));
	ensure(position == loaded);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("CooLexerIO: Test of extraInfos IO");

	const ShareableExtraInfos infos{{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}, {"params4", "value4"}};

	// coo
	CooLexerIO coo;
	ShareableExtraInfos loaded = coo.readExtraInfos(coo.write(infos));
	ensure(loaded == ShareableExtraInfos());

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.readExtraInfos(coo.write(infos));
	ensure(loaded == ShareableExtraInfos());
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("CooLexerIO: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0, 0, 0, true, true, false}, "", "", true, {{"id", "12"}, {"DCUM", "48973"}}};

	// coo
	CooLexerIO coo;
	ShareablePoint loaded = coo.readPoint(coo.write(point));
	ensure(point == loaded);

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.readPoint(coo.write(point));
	ensure(point == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("CooLexerIO: Test of params IO");

	const ShareableParams params{4, ShareableParams::ECoordSys::kGeodeticSphere, {{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}}};

	// coo
	CooLexerIO coo;
	ShareableParams loaded = coo.readParams(coo.write(params));
	ensure(loaded == ShareableParams());

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.readParams(coo.write(params));
	ensure(loaded == ShareableParams());
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("CooLexerIO: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params, "root");
	frame.setTranslation({12, 13, 14, 0.00001, 0.00001, 0.00001, false, true, true});
	frame.setRotation({0.1, 0.2, 0.3, 0.00001, 0.00001, 0.00001, true, true, false});
	frame.setScale(12.4569);
	frame.isFreeScale(true);

	frame.add(new ShareablePoint{"p0", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	frame.add(new ShareablePoint{"p1", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	frame.addFrame().add(new ShareablePoint{"p2", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	frame.addFrame().add(new ShareablePoint{"p3", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	auto &child3 = frame.addFrame();
	child3.setName("child3");
	child3.setTranslation({12, 13, 14, 12, 12, 12, true, true, true});
	child3.setRotation({0.1, 0.2, 0.3, 12, 12, 12, true, true, false});
	child3.setScale(4569);
	child3.isFreeScale(false);
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "", "", true, {{"id", "12"}, {"DCUM", "48973"}}});
	child3.add(new ShareablePoint{"p5", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	child3.addFrame().add(new ShareablePoint{"p6", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});

	// coo
	CooLexerIO coo;
	ShareableFrame loaded = coo.readFrame(coo.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.readFrame(coo.write(frame));
	const auto &fpointsHeader = frame.getAllPoints(), lpointsHeader = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpointsHeader), std::cend(fpointsHeader), std::cbegin(lpointsHeader), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("CooLexerIO: Test of points list IO");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"name", {12.5, 0.769, 9.12, 0, 0, 0, false, false, false}, "", "", true, {{"id", "12"}, {"DCUM", "48973"}}});
	frame.add(new ShareablePoint{"p5", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	frame.add(new ShareablePoint{"p6", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});

	// coo
	CooLexerIO coo;
	ShareablePointsList loaded = coo.read(coo.write(spl));
	ensure(spl == loaded);

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.read(coo.write(spl));
	ensure(spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("CooLexerIO: Test read and write files");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {12.5, 0.769, 9.12, 0, 0, 0, true, true, false}, "", "", true, {{"id", "12"}, {"DCUM", "48973"}}});
	frame.add(new ShareablePoint{"p5", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});
	frame.add(new ShareablePoint{"p6", {}, "", "", true, {{"id", "-1"}, {"DCUM", "-1"}}});

	// coo
	CooLexerIO coo;
	IShareablePointsListIO::writeFile("splistcooTest.coo", coo.write(spl));
	auto loaded = coo.read(IShareablePointsListIO::readFile("splistcooTest.coo"));
	ensure(spl == loaded);
	std::remove("splistjsonTest.coo");

	// header available
	coo.columnsHeader("%NOM X Y Z ID DX DY DZ DCUM OPTION\n% (M) (M) (M) (MM) (MM) (MM)\n%");
	loaded = coo.read(coo.write(spl));
	ensure(spl == loaded);
}

template<>
template<>
void testobject::test<20>()
{
	set_test_name("CooLexerIO: Test read for files with FRAMEs defined");

	const std::string contents = R"(#LGC2 v2.4.0-dev, compiled on Sep 27 2022
#*RS2K
%
%
%LES SIGMAS ET COVARIANCES SONT CALCULEES PAR RAPPORT AU SIGMA ZERO A POSTERIORI
%
%NOM                           X           Y           Z      SX     SY     SZ        COVXY      COVXZ      COVYZ          DX          DY          DZ 
%                             (M)         (M)         (M)    (MM)   (MM)   (MM)       (MM2)      (MM2)      (MM2)         (MM)        (MM)        (MM)
%
 LHC.TRIPOD.4L514.    -1126.57733 10433.09079  2415.39846                                                                                             
 LHC.GGPSO.4L519.     -1135.82458 10428.69070  2414.20588    0.27   0.12   0.06     0.01352    0.00241    0.00033        -3.09       -1.74        0.01
 LHC.STL.180118_1L5.  -1121.58715 10433.73980  2415.56245    0.21   0.06   0.03     0.00988    0.00234    0.00055        -3.52       -1.30       -0.09                                                                                           
%
%
%FRAME TRANSFORMATION PARAMETERS
%
%FRAME_NAME          FRAME_ID                             TX                 TY                 TZ                 RX                 RY                 RZ                SCL  (TX)  (TY)  (TZ)  (RX)  (RY)  (RZ) (SCL)
%                                                        (M)                (M)                (M)              (GON)              (GON)              (GON)
%
 RSTB_LHC.MBXW.F4L5  1_1                    -1125.4952500000   10433.9162100000    2414.9716200000     399.9223627051     399.1095764176      74.6710349380       1.0000000000     1     1     1     1     1     1     1
 RSTRI_LHC.MBXW.F4L5 1_1_1                      0.0003534180      -0.0045768543       0.0091983655       0.0054681371       0.0000000000     399.9950980452       1.0000000000     0     0     0     0     1     0     1
 RSTRV_LHC.MBXW.F4L5 1_1_1_1                    0.0000000000       0.0000000000       0.0000000000       0.0000000000       0.5048218611       0.0000000000       1.0000000000     1     1     1     1     1     1     1
 RSTB_LHC.MBXW.E4L5  1_2                    -1121.5628200000   10435.5690700000    2414.9186400000     399.9223373595     399.1095816591      74.6830025296       1.0000000000     1     1     1     1     1     1     1
 RSTRI_LHC.MBXW.E4L5 1_2_1                     -0.0000895201      -0.0048078424       0.0089942670       0.0016060086       0.0000000000       0.0008660468       1.0000000000     0     0     0     0     1     0     1
									)";

	// coo
	CooLexerIO coo;
	coo.read(contents);
}

} // namespace tut
