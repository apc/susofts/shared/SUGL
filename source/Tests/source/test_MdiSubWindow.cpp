#include <tut/tut.hpp>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <interface/SGraphicalInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <utils/MdiSubWindow.hpp>

#include "utils.hpp"

namespace
{
class mdiSGW : public SGraphicalWidget
{
public:
	mdiSGW(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent) {}
	virtual ~mdiSGW() override = default;

	virtual ShareablePointsList getContent() const override { return ShareablePointsList(); }

protected:
	virtual bool _save(const QString &path) override
	{
		setWindowTitle(path);
		return false;
	}
	virtual bool _open(const QString &) override { return false; }
	virtual void _newEmpty() override {}
};

class mdiPlugin : public QObject, public SPluginInterface
{
	Q_INTERFACES(SPluginInterface)

public:
	mdiPlugin(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~mdiPlugin() override = default;

	virtual const QString &name() const noexcept override { return _name; }
	virtual QIcon icon() const override { return QIcon(); }

	virtual void init() override {}
	virtual void terminate() override {}

	virtual bool hasConfigInterface() const noexcept override { return false; }
	virtual SConfigWidget *configInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget * = nullptr) override { return new mdiSGW(this, nullptr); }
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }
	virtual QString getExtensions() const noexcept override { return ""; }
	virtual bool isMonoInstance() const noexcept override { return false; };

private:
	const QString _name = "IAmDummy!";
};
} // namespace

namespace tut
{
struct MdiSubWindowdata
{
	mdiPlugin mdiPlug;
};

typedef test_group<MdiSubWindowdata> tg;
tg uiplugins_MdiSubWindow_test_group("Test MdiSubWindow class.");
typedef tg::object testobject;

template<>
template<>
void testobject::test<1>()
{
	set_test_name("MdiSubWindow: Test of graphicalWidget and setGraphicalWidget");

	MdiSubWindow mdiWin;
	// Default nullptr
        ensure(mdiWin.graphicalWidget() == nullptr);
	// Setting SGW
	mdiSGW widget(&mdiPlug, nullptr);
	mdiWin.setGraphicalWidget(&widget);
	ensure_equals(mdiWin.graphicalWidget(), &widget);
	// Setting to nullptr (allowed)
	mdiWin.setGraphicalWidget(nullptr);
        ensure(mdiWin.graphicalWidget() == nullptr);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("MdiSubWindow: implicit test of updateTitle");

	MdiSubWindow mdiWin;
	const QString title1 = "I changed my title!";
	const QString title2 = "Oops, I did it again!";

	ensure(mdiWin.windowTitle().isEmpty());
	mdiSGW widget(&mdiPlug, nullptr);
	mdiWin.setGraphicalWidget(&widget);
	widget.save(title1);
	ensure(mdiWin.windowTitle() == title1);
	widget.save(title2);
	ensure(mdiWin.windowTitle() == title2);
}
} // namespace tut
