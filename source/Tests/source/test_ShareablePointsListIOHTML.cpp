#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <io/ShareablePointsListIOHTML.hpp>

namespace tut
{
struct shareablepointslistiohtmldata
{
};

typedef test_group<shareablepointslistiohtmldata> tg;
tg uiplugins_spliohtml_test_group("Test ShareablePointsListIOHTML class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("ShareablePointsListIOHTML: Test of base class");

	ShareablePointsListIOHTML html;
	ensure(html.writeHeaders());
	ensure(html.isIndented());
	ensure_equals(html.getMIMEType(), "text/html");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ShareablePointsListIOHTML: Test of reading bad HTML");

	// emtpy
	ShareablePointsListIOHTML html;
	ensure_THROW(html.read(""), SPIOException);

	// garbage
	ensure_THROW(html.read("lol"), SPIOException);

	// array
	ensure_THROW(html.read("[]"), SPIOException);

	// empty object
	ensure_THROW(html.read("<html />"), SPIOException);
	ensure_THROW(html.read(R"""(<?xml version="1.0" encoding="UTF - 8"?><html />)"""), SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ShareablePointsListIOHTML: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false};

	ShareablePointsListIOHTML html;
	ShareablePosition loaded = html.readPosition(html.write(position));
	ensure(position == loaded);

	// not complete
	html.exportFieldsPosition.removeField("sigmax", "sigmay", "isfreey");
	loaded = html.readPosition(html.write(position));
	ensure_equals(loaded.x, position.x);
	ensure_equals(loaded.y, position.y);
	ensure_equals(loaded.z, position.z);
	ensure_equals(loaded.sigmax, 0);
	ensure_equals(loaded.sigmay, 0);
	ensure_equals(loaded.sigmaz, position.sigmaz);
	ensure_equals(loaded.isfreex, position.isfreex);
	ensure_not(loaded.isfreey);
	ensure_equals(loaded.isfreez, position.isfreez);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("ShareablePointsListIOHTML: Test of extraInfos IO");

	const ShareableExtraInfos infos{{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}, {"params4", "value4"}};

	ShareablePointsListIOHTML html;
	ShareableExtraInfos loaded = html.readExtraInfos(html.write(infos));
	ensure(loaded == ShareableExtraInfos{});
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("ShareablePointsListIOHTML: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0.001, 0.00001, 0.0000005, false, true, false}, "inline comment", "header comment", false};

	ShareablePointsListIOHTML html;
	ShareablePoint loaded = html.readPoint(html.write(point));
	ensure(point == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("ShareablePointsListIOHTML: Test of params IO");

	const ShareableParams params{4, ShareableParams::ECoordSys::kGeodeticSphere, {{"params1", "value1"}, {"params2", "value2"}, {"params3", "value3"}}};

	ShareablePointsListIOHTML html;
	ShareableParams loaded = html.readParams(html.write(params));
	ensure(loaded == ShareableParams{});
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("ShareablePointsListIOHTML: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params, "root");
	frame.setTranslation({12, 13, 14, 0.00001, 0.00001, 0.00001, false, true, false});
	frame.setRotation({0.1, 0.2, 0.3, 0.00001, 0.00001, 0.00001, true, true, false});
	frame.setScale(12.4569);
	frame.isFreeScale(true);

	frame.addPoint().name = "p0";
	frame.addPoint().name = "p1";
	frame.addFrame().addPoint().name = "p2";
	frame.addFrame().addPoint().name = "p3";
	auto &child3 = frame.addFrame();
	child3.setName("child3");
	child3.setTranslation({12, 13, 14, 12, 12, 12, false, true, false});
	child3.setRotation({0.1, 0.2, 0.3, 12, 12, 12, true, true, false});
	child3.setScale(4569);
	child3.isFreeScale(false);
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	child3.addPoint().name = "p5";
	child3.addFrame().addPoint().name = "p6";

	ShareablePointsListIOHTML html;
	ShareableFrame loaded = html.readFrame(html.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("ShareablePointsListIOHTML: Test of points list IO");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	// full
	ShareablePointsListIOHTML html;
	ShareablePointsList loaded = html.read(html.write(spl));
	ensure(spl == loaded);
	// no header
	html.writeHeaders(false);
	loaded = html.read(html.write(spl));
	ensure(spl == loaded);
	// partial, no headers
	html.exportFieldsPoint.removeField("inlineComment");
	loaded = html.read(html.write(spl));
	for (size_t i = 0; i < 3; i++)
		frame.getPoint(i).inlineComment = "";
	ensure(spl == loaded);
	// partial, with headers, no <xml>
	html.writeHeaders(true);
	html.exportFieldsPosition.addField("inlineComment");
	std::string output = html.write(spl);
	output.erase(0, output.find("\n") + 1);
	loaded = html.read(output);
	ensure(spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("ShareablePointsListIOHTML: Test read and write files");

	ShareablePointsList spl;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "header", false});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	ShareablePointsListIOHTML html;
	html.isIndented(false);
	IShareablePointsListIO::writeFile("splisthtmlTest.html", html.write(spl));
	auto loaded = html.read(IShareablePointsListIO::readFile("splisthtmlTest.html"));
	ensure(spl == loaded);
	std::remove("splisthtmlTest.html");
}

template<>
template<>
void testobject::test<20>()
{
	set_test_name("ShareablePointsListIOHTML: Header tests - standard header");
	ShareablePointsListIOHTML html;
	ShareablePointsList spl = html.read(R"(<?xml version="1.0"?>
<html>
    <head>
        <meta content="text/html; charset=utf-8"/>
    </head>
    <body>
        <table>
            <tr>
                <th>name</th>
                <th>x</th>
                <th>y</th>
                <th>z</th>
                <th>sigmax</th>
                <th>sigmay</th>
                <th>sigmaz</th>
                <th>isfreex</th>
                <th>isfreey</th>
                <th>isfreez</th>
                <th>inlineComment</th>
                <th>headerComment</th>
                <th>active</th>
                <th>extraInfos</th>
            </tr>
            <tr>
                <!--header-->
                <td>point</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>0.1</td>
                <td>0.2</td>
                <td>0.3</td>
                <td>true</td>
                <td>false</td>
                <td>true</td>
                <td>inline</td>
                <td>header</td>
                <td>false</td>
                <td></td>
            </tr>
        </table>
    </body>
</html>)");
	ShareablePoint point = spl.getRootFrame().getPoint(0);

	ensure_equals(point.name, "point");
	ensure_equals(point.position.x, 1);
	ensure_equals(point.position.y, 2);
	ensure_equals(point.position.z, 3); // h changed to z after parsing
	ensure_equals(point.position.sigmax, 0.1);
	ensure_equals(point.position.sigmay, 0.2);
	ensure_equals(point.position.sigmaz, 0.3);
	ensure_equals(point.position.isfreex, true);
	ensure_not(point.position.isfreey);
	ensure_equals(point.position.isfreez, true);
}

template<>
template<>
void testobject::test<21>()
{
	set_test_name("ShareablePointsListIOHTML: Header tests - unit header");
	ShareablePointsListIOHTML html;
	ShareablePointsList spl = html.read(R"(<?xml version="1.0"?>
<html>
    <head>
        <meta content="text/html; charset=utf-8"/>
    </head>
    <body>
        <table>
            <tr>
                <th>name</th>
                <th>X [m]</th>
                <th>Y [m]</th>
                <th>Z [m]</th>
                <th>sigmax</th>
                <th>sigmay</th>
                <th>sigmaz</th>
                <th>isfreex</th>
                <th>isfreey</th>
                <th>isfreez</th>
                <th>inlineComment</th>
                <th>headerComment</th>
                <th>active</th>
                <th>extraInfos</th>
            </tr>
            <tr>
                <!--header-->
                <td>point</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>0.1</td>
                <td>0.2</td>
                <td>0.3</td>
                <td>true</td>
                <td>false</td>
                <td>true</td>
                <td>inline</td>
                <td>header</td>
                <td>false</td>
                <td></td>
            </tr>
        </table>
    </body>
</html>)");
	ShareablePoint point = spl.getRootFrame().getPoint(0);

	ensure_equals(point.name, "point");
	ensure_equals(point.position.x, 1);
	ensure_equals(point.position.y, 2);
	ensure_equals(point.position.z, 3); // H [m] changed to z after parsing
	ensure_equals(point.position.sigmax, 0.1);
	ensure_equals(point.position.sigmay, 0.2);
	ensure_equals(point.position.sigmaz, 0.3);
	ensure_equals(point.position.isfreex, true);
	ensure_not(point.position.isfreey);
	ensure_equals(point.position.isfreez, true);
}
} // namespace tut
