#include <QMdiArea>
#include <QMdiSubWindow>
#include <QTreeWidgetItem>

#include <tut/tut.hpp>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <interface/SGraphicalInterface.hpp>
#include <interface/SPluginInterface.hpp>
#include <utils/MdiSubWindow.hpp>
#include <utils/TreeMdiManager.hpp>

#include "utils.hpp"

namespace
{
class treeSGW : public SGraphicalWidget
{
public:
	treeSGW(SPluginInterface *owner, QWidget *parent) : SGraphicalWidget(owner, parent) {}
	virtual ~treeSGW() override = default;

	virtual ShareablePointsList getContent() const override { return ShareablePointsList(); }

protected:
	virtual bool _save(const QString &) override { return false; }
	virtual bool _open(const QString &) override { return false; }
	virtual void _newEmpty() override {}
};

class tree1stPlugin : public QObject, public SPluginInterface
{
	Q_INTERFACES(SPluginInterface)

public:
	tree1stPlugin(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~tree1stPlugin() override = default;

	virtual const QString &name() const noexcept override { return _name; }
	virtual QIcon icon() const override { return QIcon(); }

	virtual void init() override {}
	virtual void terminate() override {}

	virtual bool hasConfigInterface() const noexcept override { return false; }
	virtual SConfigWidget *configInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget * = nullptr) override { return new treeSGW(this, nullptr); }
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }
	virtual QString getExtensions() const noexcept override { return ""; }
	virtual bool isMonoInstance() const noexcept override { return false; };

private:
	const QString _name = "I am the 1st plugin!";
};

class tree2ndPlugin : public QObject, public SPluginInterface
{
	Q_INTERFACES(SPluginInterface)

public:
	tree2ndPlugin(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~tree2ndPlugin() override = default;

	virtual const QString &name() const noexcept override { return _name; }
	virtual QIcon icon() const override { return QIcon(); }

	virtual void init() override {}
	virtual void terminate() override {}

	virtual bool hasConfigInterface() const noexcept override { return false; }
	virtual SConfigWidget *configInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget * = nullptr) override { return new treeSGW(this, nullptr); }
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }
	virtual QString getExtensions() const noexcept override { return ""; }
	virtual bool isMonoInstance() const noexcept override { return false; };

private:
	const QString _name = "I am the 2nd plugin!";
};
} // namespace

namespace tut
{
struct TreeMdiManagerdata
{
	tree1stPlugin firstPlug;
	tree2ndPlugin secondPlug;
};

typedef test_group<TreeMdiManagerdata> tg;
tg uiplugins_TreeMdiManager_test_group("Test TreeMdiManager class.");
typedef tg::object testobject;

template<>
template<>
void testobject::test<1>()
{
	set_test_name("TreeMdiManager: Test of mdiArea - setter and getter. ");

	TreeMdiManager treeManager;
	// Default - no mdiArea
        ensure(treeManager.mdiArea() == nullptr);
	// Setting the area
	QMdiArea tempArea;
	treeManager.mdiArea(&tempArea);
	ensure_equals(treeManager.mdiArea(), &tempArea);
	// Setting the area to nullptr (allowed)
	treeManager.mdiArea(nullptr);
        ensure(treeManager.mdiArea() == nullptr);
}
} // namespace tut
