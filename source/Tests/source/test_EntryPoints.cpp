#include <vector>

#include <QObject>

#include <tut/tut.hpp>

#include <interface/SPluginInterface.hpp>
#include <interface/SPluginLoader.hpp>

namespace
{
class EPDummyPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	EPDummyPlugin(QObject *parent = nullptr) : QObject(parent) {}
	virtual ~EPDummyPlugin() override = default;

	virtual const QString &name() const noexcept override { return _name; }
	virtual QIcon icon() const override { return QIcon(); }

	virtual void init() override {}
	virtual void terminate() override {}

	virtual bool hasConfigInterface() const noexcept override { return false; }
	virtual SConfigWidget *configInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasGraphicalInterface() const noexcept override { return false; }
	virtual SGraphicalWidget *graphicalInterface(QWidget * = nullptr) override { return nullptr; }
	virtual bool hasLaunchInterface() const noexcept override { return false; }
	virtual SLaunchObject *launchInterface(QObject * = nullptr) override { return nullptr; }
	virtual QString getExtensions() const noexcept override { return ""; }
	virtual bool isMonoInstance() const noexcept override { return false; };

private:
	const QString _name = "IAmDummy!";
};

class EPDummyEntryPoint : public QObject
{
	Q_OBJECT

public:
	Q_INVOKABLE EPDummyEntryPoint(QObject *parent = nullptr) : QObject(parent) {}

	Q_INVOKABLE std::vector<int> getDummyInts() { return _myDummies; }
	Q_INVOKABLE void setDummyInts(std::vector<int> dummies) { _myDummies = dummies; }
	Q_INVOKABLE bool addThreeDummyInts(int dummy1, int dummy2, int dummy3)
	{
		_myDummies.clear();
		_myDummies.push_back(dummy1);
		_myDummies.push_back(dummy2);
		_myDummies.push_back(dummy3);
		return false;
	}

private:
	std::vector<int> _myDummies;
};
} // namespace

#include "test_EntryPoints.moc"

namespace tut
{
struct entrypointdata
{
	// Variables used globally for the test
	EPDummyPlugin dumbPlug;
	SPluginLoader &pluginLoader = SPluginLoader::getPluginLoader();
};

typedef test_group<entrypointdata> tg;
tg uiplugins_entrypoint_test_group("Test entry point system.");
typedef tg::object testobject;

template<>
template<>
void testobject::test<1>()
{
	set_test_name("EntryPoints: entry point registration");

	// Nothing registered
	ensure_equals(pluginLoader.getEntryPoints("dummy_test").size(), 0);

	// One registered
	pluginLoader.registerEntryPoint("dummy_test", {&EPDummyEntryPoint::staticMetaObject, &dumbPlug});
	ensure_equals(pluginLoader.getEntryPoints("dummy_test").size(), 1);

	// Register the same QMetaObject again - is not allowed so it should not add it to entry point list
	pluginLoader.registerEntryPoint("dummy_test", {&EPDummyEntryPoint::staticMetaObject, &dumbPlug});
	ensure_equals(pluginLoader.getEntryPoints("dummy_test").size(), 1);
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("EntryPoints: entry point - calling a method giving output");

	// Class was previously registered in SPluginLoader singleton
	auto testEP = pluginLoader.getEntryPoints("dummy_test");
	std::unique_ptr<QObject> qobj(testEP[0].meta->newInstance());
	ensure(qobj.get());

	std::vector<int> dummies;
	bool call = QMetaObject::invokeMethod(qobj.get(), "getDummyInts", Q_RETURN_ARG(std::vector<int>, dummies));
	ensure(call);
	ensure_equals(dummies.size(), 0);
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("EntryPoints: entry point - calling a method requiring arguments");

	// Class was previously registered in SPluginLoader singleton
	auto testEP = pluginLoader.getEntryPoints("dummy_test");
	std::unique_ptr<QObject> qobj(testEP[0].meta->newInstance());
	ensure(qobj.get());

	std::vector<int> newDummies{1, 2, 3};
	bool callSet = QMetaObject::invokeMethod(qobj.get(), "setDummyInts", Q_ARG(std::vector<int>, newDummies));
	ensure(callSet);

	std::vector<int> dummies;
	bool callGet = QMetaObject::invokeMethod(qobj.get(), "getDummyInts", Q_RETURN_ARG(std::vector<int>, dummies));
	ensure(callGet);
	ensure_equals(dummies.size(), 3);
	ensure(dummies == newDummies);
}

template<>
template<>
void testobject::test<4>()
{
	set_test_name("EntryPoints: entry point - calling a method requiring multiple arguments and returning a result");

	// Class was previously registered in SPluginLoader singleton
	auto testEP = pluginLoader.getEntryPoints("dummy_test");
	std::unique_ptr<QObject> qobj(testEP[0].meta->newInstance());
	ensure(qobj.get());

	bool ok = true;
	bool callSet = QMetaObject::invokeMethod(qobj.get(), "addThreeDummyInts", Q_RETURN_ARG(bool, ok), Q_ARG(int, 3), Q_ARG(int, 2), Q_ARG(int, 1));
	ensure(callSet);
	ensure_not(ok);

	std::vector<int> dummies;
	bool callGet = QMetaObject::invokeMethod(qobj.get(), "getDummyInts", Q_RETURN_ARG(std::vector<int>, dummies));
	ensure(callGet);
	ensure_equals(dummies.size(), 3);
	ensure(std::vector<int>{3, 2, 1} == dummies);
}
} // namespace tut
