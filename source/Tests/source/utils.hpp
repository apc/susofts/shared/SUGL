/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

#include <iostream>

/**
Preventing the error: ambiguous overload for ‘operator<<’ (operand types are ‘std::basic_ostream<char>’ and ‘std::nullptr_t’)
This method should be deleted as soon as gcc will be able to handle this overload (msvc already handles it)
*/
inline std::ostream &operator<<(std::ostream &s, std::nullptr_t)
{
	return s << "nullptr";
}

#endif // TEST_UTILS_HPP
