#include <cstdio>
#include <exception>

#include <QCoreApplication>
#include <QSettings>

#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <QtStreams.hpp>
#include <interface/SConfigInterface.hpp>
#include <utils/SettingsManager.hpp>

namespace tut
{
struct settingsmanagerdata
{
	~settingsmanagerdata()
	{
		auto &settings = SettingsManager::settings();
		settings.disconnect();
		settings.settingsRemove("lol");
		settings.innerSettingsRemove("lol");
		std::remove(settings.innerSettings("settingsFile").toUtf8().constData());
	}
};

typedef test_group<settingsmanagerdata> tg;
tg uiplugins_settingsmanagerdata_test_group("Test SettingsManager class.");
typedef tg::object testobject;
} // namespace tut

namespace
{
class DummyConfig : public SConfigObject
{
public:
	DummyConfig(const std::string &v = "") : SConfigObject(), value(v) {}
	bool operator==(const SConfigObject &o) const noexcept override
	{
		if (!SConfigObject::operator==(o))
			return false;
		const DummyConfig& oc = static_cast<const DummyConfig &>(o);
		return value == oc.value;
	}

	virtual void write() const override {}
	virtual void read() override {}

	friend std::ostream &operator<<(std::ostream &stream, const DummyConfig &conf);

	std::string value;
};

std::ostream &operator<<(std::ostream &stream, const DummyConfig &conf)
{
	stream << "DummyConfig: '" << conf.value << "'";
	return stream;
}
} // namespace

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("SettingsManager: Test of singleton");

	ensure_equals(&SettingsManager::settings(), &SettingsManager::settings());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("SettingsManager: Test of init()");

	SettingsManager::init("app", "version");

	ensure_equals(QCoreApplication::applicationName().toStdString(), "app");
	ensure_equals(QCoreApplication::applicationVersion().toStdString(), "version");
	ensure_equals(QCoreApplication::organizationName().toStdString(), "CERN_BE-GM-APC");
	ensure_equals(QCoreApplication::organizationDomain().toStdString(), "https://home.cern");

	auto &s = SettingsManager::settings();
	ensure_equals(s.innerSettings("appName"), "app");
	ensure_equals(s.innerSettings("appVersion"), "version");
	ensure_equals(s.innerSettings("orgName"), "CERN_BE-GM-APC");
	ensure_equals(s.innerSettings("orgDomain"), "https://home.cern");
	ensure(s.innerSettingsHas("settingsFile"));
	ensure_not(s.innerSettingsHas("lol"));
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("SettingsManager: Test of QSettings");

	SettingsManager::init("app", "version");

	// save
	QSettings settings;
	settings.beginGroup(""); // just to test
	settings.setValue("key", "Examplevalue"); // /key
	settings.endGroup();
	QSettings handmade;
	handmade.beginGroup("handmade");
	handmade.setValue("key", "another example"); // /handmade/key
	handmade.endGroup();

	// load
	QSettings load(SettingsManager::settings().innerSettings("settingsFile"), QSettings::IniFormat);
	ensure_equals(load.value("key").toString().toStdString(), "Examplevalue");
	ensure_equals(load.value("handmade/key").toString().toStdString(), "another example");
}

template<>
template<>
void testobject::test<4>()
{
	set_test_name("SettingsManager: Test of innerSettings");

	SettingsManager::init("app", "version");

	QString test;
	auto &settings = SettingsManager::settings();
	QObject::connect(&settings, &SettingsManager::innerSettingsChanged, [&test](const QString &key) -> void { test = key; });

	test = "";
	settings.innerSettings("lol", "woot");
	ensure_equals(test, "lol");
	ensure_equals(settings.innerSettings("lol"), "woot");
	ensure(settings.innerSettingsHas("lol"));

	test = "";
	settings.innerSettings("lol", "truc");
	ensure_equals(test, "lol");
	ensure_equals(settings.innerSettings("lol"), "truc");
	ensure(settings.innerSettingsHas("lol"));

	test = "";
	settings.innerSettingsRemove("lol");
	ensure_equals(test, "lol");
	ensure_THROW(settings.innerSettings("lol"), std::out_of_range);
	ensure_not(settings.innerSettingsHas("lol"));
}

template<>
template<>
void testobject::test<5>()
{
	set_test_name("SettingsManager: Test of settings");

	SettingsManager::init("app", "version");

	QString test;
	DummyConfig *conf;
	auto &settings = SettingsManager::settings();
	QObject::connect(&settings, &SettingsManager::settingsChanged, [&test](const QString &key) -> void { test = key; });

	test = "";
	conf = new DummyConfig("woot");
	settings.settings("lol", conf);
	ensure_equals(test, "lol");
	ensure_equals(settings.settings<DummyConfig>("lol"), *conf);
	ensure(settings.settingsHas("lol"));

	test = "";
	conf = new DummyConfig("truc");
	settings.settings("lol", conf);
	ensure_equals(test, "lol");
	ensure_equals(settings.settings<DummyConfig>("lol"), *conf);
	ensure(settings.settingsHas("lol"));

	test = "";
	conf = nullptr;
	settings.settingsRemove("lol");
	ensure_equals(test, "lol");
	ensure_equals(settings.settings<DummyConfig>("lol"), DummyConfig());
	ensure_not(settings.settingsHas("lol"));
}
} // namespace tut
