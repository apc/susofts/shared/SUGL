#include <Qsci/qsciscintilla.h>

#include <tut/tut.hpp>

#include <QKeySequence>

#include <utils/SettingsManager.hpp>
#include <utils/ShortcutsConfig.hpp>

namespace
{
ShortcutsConfigObject getConfig()
{
	ShortcutsConfigObject t;

	t.ShortcutMap[ShortcutsConfigObject::COPY].second = QKeySequence::fromString(QObject::tr("Ctrl + X,  Y"));
	t.ShortcutMap[ShortcutsConfigObject::PASTE].second = QKeySequence::Copy;
	t.ShortcutMap[ShortcutsConfigObject::CLOSEPROJECT].second = QKeySequence::fromString(QObject::tr("Shift + Ctrl + ALT + G, X, Y, Z"));

	return t;
}
} // namespace

namespace tut
{
struct ShortcutsConfigdata
{
	ShortcutsConfigdata() { SettingsManager::init("shortcutsapp", "version"); }
	~ShortcutsConfigdata() { std::remove(SettingsManager::settings().innerSettings("settingsFile").toUtf8().constData()); }
};

typedef test_group<ShortcutsConfigdata> tg;
tg uiplugins_ShortcutsConfig_test_group("Test ShortcutsConfig class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
/* ************************ */
/* ShortcutsConfigObject */
/* ************************ */

template<>
template<>
void testobject::test<1>()
{
	set_test_name("ShortcutsConfigObject: Test of default constructor");

	ShortcutsConfigObject t;

	ensure(t.ShortcutMap[ShortcutsConfigObject::COPY].second == QKeySequence::Copy);
	ensure(t.ShortcutMap[ShortcutsConfigObject::PASTE].second == QKeySequence::Paste);
	ensure(t.ShortcutMap[ShortcutsConfigObject::CLOSEPROJECT].second == QKeySequence::fromString(QObject::tr("Ctrl+W")));
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("ShortcutsConfigObject: Test of empty read()");

	ShortcutsConfigObject t;
	t.read();

	ensure(t.ShortcutMap[ShortcutsConfigObject::COPY].second == QKeySequence::Copy);
	ensure(t.ShortcutMap[ShortcutsConfigObject::PASTE].second == QKeySequence::Paste);
	ensure(t.ShortcutMap[ShortcutsConfigObject::CLOSEPROJECT].second == QKeySequence::fromString(QObject::tr("Ctrl+W")));
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("ShortcutsConfigObject: Test of read() and write()");

	{
		ShortcutsConfigObject t = getConfig();
		t.write();
	}

	ShortcutsConfigObject t;
	t.read();
	ensure(t == getConfig());
}

/* ************************ */
/*    ShortcutsConfig    */
/* ************************ */

template<>
template<>
void testobject::test<10>()
{
	set_test_name("ShortcutsConfig: Test of setConfig(), reset() and restoreDefaults()");

	ShortcutsConfig widget;
	// initialization
	{
		auto t = getConfig();
		t.write();
		// we change `t` so it is different from what is written (for `reset()`)
		t.ShortcutMap[ShortcutsConfigObject::COPY].second = QKeySequence::Cut;
		widget.setConfig(&t);
	}
	// test of getConfig()
	{
		std::unique_ptr<ShortcutsConfigObject> t(dynamic_cast<ShortcutsConfigObject *>(widget.config()));
		ensure(t != nullptr);
		auto test = getConfig();
		test.ShortcutMap[ShortcutsConfigObject::COPY].second = QKeySequence::Cut;
		ensure(test.ShortcutMap[ShortcutsConfigObject::CLOSEPROJECT].second == QKeySequence::fromString(QObject::tr("Shift + Ctrl + ALT + G, X, Y, Z")));
		ensure(*t == test);
	}
	// test of reset()
	{
		widget.reset();
		std::unique_ptr<ShortcutsConfigObject> t(dynamic_cast<ShortcutsConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == getConfig());
	}
	// test of restoreDefaults()
	{
		widget.restoreDefaults();
		std::unique_ptr<ShortcutsConfigObject> t(dynamic_cast<ShortcutsConfigObject *>(widget.config()));
		ensure(t != nullptr);
		ensure(*t == ShortcutsConfigObject());
		ensure(t->ShortcutMap[ShortcutsConfigObject::COPY].second == QKeySequence::Copy);
	}
}
} // namespace tut
