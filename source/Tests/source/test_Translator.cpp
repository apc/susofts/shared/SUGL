#include <QActionGroup>
#include <QCoreApplication>
#include <QResource>

#include <tut/tut.hpp>

#include <translations/Translator.hpp>

namespace tut
{
struct translatordata
{
	translatordata() { QResource::registerResource("resources/resources.rcc"); }
	~translatordata()
	{
		QResource::unregisterResource("resources/resources.rcc");
		Translator::getTranslator().disconnect();
		Translator::getTranslator().clear();
	}
};

typedef test_group<translatordata> tg;
tg uiplugins_translator_test_group("Test Translator class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("Translator: Test of singleton");

	ensure_equals(&Translator::getTranslator(), &Translator::getTranslator());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("Translator: Test getters & setters");

	auto &t = Translator::getTranslator();
	ensure_equals(t.getPrefix(), "");
	ensure_equals(t.getcurrentLanguage(), "");

	t.setPrefix("lol");
	ensure_equals(t.getPrefix(), "lol");
	ensure_equals(t.size(), 0UL);
	ensure(t.empty());
	ensure_not(t.has("lol_"));
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("Translator: Test loadLanguages()");

	auto &t = Translator::getTranslator();
	t.setPrefix("test_");
	t.loadLanguages();

	ensure_equals(t.size(), 2UL);
	ensure_not(t.empty());
	ensure(t.getLanguages() == std::unordered_set<std::string>{"en", "fr"});
	ensure(t.has("en"));
	ensure(t.has("fr"));

	t.clear();
	ensure_equals(t.getPrefix(), "test_");
	ensure_equals(t.size(), 0UL);
	ensure(t.empty());
	ensure_not(t.has("en"));
	ensure_not(t.has("fr"));
}

template<>
template<>
void testobject::test<4>()
{
	set_test_name("Translator: Test changeLanguage()");

	auto &t = Translator::getTranslator();
	std::string lg;
	QObject::connect(&t, &Translator::languageChanged, [&lg](const std::string &s) -> void { lg = s; });
	t.setPrefix("test_");
	t.loadLanguages();

	ensure_equals(lg, "");
	ensure_equals(t.getcurrentLanguage(), "");
	ensure_equals(QCoreApplication::translate("test", "language: English").toStdString(), "language: English");

	t.changeLanguage("fr");
	ensure_equals(lg, "fr");
	ensure_equals(t.getcurrentLanguage(), "fr");
	ensure_equals(QCoreApplication::translate("test", "language: English").toStdString(), "langue : francais");

	t.changeLanguage("en");
	ensure_equals(lg, "en");
	ensure_equals(t.getcurrentLanguage(), "en");
	ensure_equals(QCoreApplication::translate("test", "language: English").toStdString(), "language: English");

	t.clear();
	ensure_equals(t.getcurrentLanguage(), "");
}

template<>
template<>
void testobject::test<5>()
{
	set_test_name("Translator: Test buildActions()");

	auto &t = Translator::getTranslator();
	std::string lg;
	t.setPrefix("test_");
	t.loadLanguages();

	const auto *actions = t.buildActions();
	const auto &actionlist = actions->actions();

	ensure_equals(actionlist.size(), 2);
	ensure_equals(actionlist[0]->text().toStdString(), "en");
	ensure_equals(actionlist[1]->text().toStdString(), "fr");

	delete actions;
}
} // namespace tut
