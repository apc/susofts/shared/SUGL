Integration for QScintilla
==========================

QScintilla is an external Qt library that is built on top of Scintilla, to provide a powerful text editor widget.

Unfortunately, there is no public Git repository for QScintilla. thus, we have to compile it ourselves. This is automatically done thanks to our [QScintilla repository](https://gitlab.cern.ch/apc/susofts/shared/qscintilla).

This directory downloads binaries from the aforementioned repository. Then, it creates a `qscintilla2_qt5` library for CMake. Thus, the only thing you need to do is to link against this library.

How to use
----------

You just need to add this directory in you main CMake. This will create a new target `QSscintilla` that downloads the binaries. It will also create a library that you can link against in CMake.

The library is already configured so you don't need to add include directories or whatsoever.

**Note** that you'll have to add the compile definition `QSCINTILLA_DLL` in order to correctly compile against the library.

If you want to get the path to the library binary (`.so` file for Linux, `.dll` file for Windows), you can get it from the following properties of the target `qscintilla2_qt5`:
- `IMPORTED_LOCATION_DEBUG` for debug file
- `IMPORTED_LOCATION_RELEASE` for release file

A full example on how to integrate this library can be seen in the example app. Below a short way to do it.

### Steps ###

1. Include this directory
```cmake
add_directory("${SUGL_ROOT}/QScintilla")
```
2. Add the compile variable `QSCINTILLA_DLL` to your target
```cmake
target_compile_definitions(${PROJECT_NAME} PRIVATE QSCINTILLA_DLL)
```
3. Link against `qscintilla2_qt5`
```cmake
target_link_libraries(${PROJECT_NAME} qscintilla2_qt5)
```
4. Copy the DLL after the compilation and run `windeployqt`
```cmake
get_target_property(QSCINTILLA_DLL_DEBUG qscintilla2_qt5 IMPORTED_LOCATION_DEBUG)
get_target_property(QSCINTILLA_DLL qscintilla2_qt5 IMPORTED_LOCATION_RELEASE)
if(WIN32)
	deployqt(${PROJECT_NAME})
	deployqt(${PROJECT_NAME} "$<TARGET_FILE_DIR:${PROJECT_NAME}>" "$<$<CONFIG:debug>:${QSCINTILLA_DLL_DEBUG}>$<$<CONFIG:release>:${QSCINTILLA_DLL}>")
endif()
post_build_copy_dlls(${PROJECT_NAME} "${QSCINTILLA_DLL_DEBUG}" "${QSCINTILLA_DLL}" "")
```

### Example ###

Here is an example that shows how to use these variables in a `CMakeLists.txt`:

```cmake
set(PROJECT_NAME myproject)

# We use the standard CMake protocol for SUSofts
# see https://gitlab.cern.ch/DataProcessingAndAnalysis/SUSoftCMakeCommon
# Note that all the following Qt modules are needed for QScintilla
set(USE_QT TRUE)
set(QT_WANTED_MODULES "Qt5Core" "Qt5Gui" "Qt5PrintSupport" "Qt5Widgets")
include("../../SUSoftCMakeCommon/CMakeLists.txt")
set(CMAKE_AUTOUIC ON)

# We include QScintilla
include("../lib/SUGL/QScintilla/qscintilla.cmake")

# We create the executable
add_executable(${PROJECT_NAME} WIN32
	main.cpp
	# myproject headers
	myproject_mainwindow.hpp
	# myproject sources
	myproject_mainwindow.cpp
	# myproject uis
	myproject_mainwindow.ui
	# myproject resources
	myproject.qrc
)
target_compile_definitions(${PROJECT_NAME} PRIVATE QSCINTILLA_DLL)
target_include_directories(${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}
)
target_link_libraries(${PROJECT_NAME}
	Qt5::Core
	Qt5::Widgets
	# We add QScintilla
	qscintilla2_qt5
)

# Now, we have to copy the libraries after the compilation so we can run the produced executable
# Please note that to run your executable on Linux, you have to prepend the command with "LD_LIBRARY_PATH=."
get_target_property(QSCINTILLA_DLL_DEBUG qscintilla2_qt5 IMPORTED_LOCATION_DEBUG)
get_target_property(QSCINTILLA_DLL qscintilla2_qt5 IMPORTED_LOCATION_RELEASE)
if(WIN32)
	deployqt(${PROJECT_NAME})
	deployqt(${PROJECT_NAME} "$<TARGET_FILE_DIR:${PROJECT_NAME}>" "$<$<CONFIG:debug>:${QSCINTILLA_DLL_DEBUG}>$<$<CONFIG:release>:${QSCINTILLA_DLL}>")
endif()
post_build_copy_dlls(${PROJECT_NAME} "${QSCINTILLA_DLL_DEBUG}" "${QSCINTILLA_DLL}" "")
```

Update QScintilla
-----------------

If you want to update the version of QScintilla, you need first to update our [QScintilla repository](https://gitlab.cern.ch/apc/susofts/shared/qscintilla). See information there.

Then, you need to update the download link and the hashes in the `CMakeLists.txt` (one for [Linux](./CMakeLists.txt#L9-10) and one for [Windows](./CMakeLists.txt#L12-13)). And voilà!
