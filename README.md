[![pipeline status](https://gitlab.cern.ch/apc/susofts/shared/SUGL/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/shared/SUGL/commits/master)

SUGL (SUrvey Graphical Library)
===============================

This project builds the SUGL, used by graphical Survey applications.

Documentation
-------------

### Doxygen ###

The Doxygen documentation is meant for developers only. Follow the [Build instructions](#build-instructions) to set up your projects. Then you can build the `doc` target to create the Doxygen documentation. You will need [Doxygen](https://www.stack.nl/~dimitri/doxygen/download.html#srcbin) and [GraphViz](http://www.graphviz.org/download/#executable-packages) installed and configured.

Once built, you can open the file `build/html/index.html` as an entry point to the documentation.

### Other ###

You can find further documentation in the folder [doc](./doc). In this folder, you'll find:
- the UML diagram of the project, and some detailed for different parts. Note that you can open the SVG file in a web browser (avoid Edge as it totally messes up the image), and the `.uxf` files with [Umlet](http://www.umlet.com/).

How to build
------------

First of all, ensure that you have all the submodules initialized with:

```bash 
$ git submodule update --init
```

Then create a `build` folder and run `CMake` from there:

```bash
$ mkdir build && cd build/
$ cmake -G "Visual Studio 16 2019" -a x64 ../source # Use another generator here if you wish
```

Content
-------

The SUGL provides different modules. Among other, here are the most interesting ones:
- `QScintilla`: downloads the binaries and create a CMake library to easily use them (see the [QScintilla repository](https://gitlab.cern.ch/apc/susofts/shared/qscintilla))
- `resources`: builds Qt resources files
- `UIPlugins`: create a shared library that can be embedd in your graphical projects to create plugins
